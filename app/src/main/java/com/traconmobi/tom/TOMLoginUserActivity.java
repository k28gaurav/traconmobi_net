package com.traconmobi.tom;

//import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.traconmobi.tom.app.App;

import com.traconmobi.tom.http.ParseAuthenticate;
import com.traconmobi.tom.model.Row;
import com.traconmobi.tom.model.Row1;
import com.traconmobi.tom.model.Row2;
import com.traconmobi.tom.model.Value;
import com.traconmobi.tom.model.Value1;
import com.traconmobi.tom.model.Value2;
import com.traconmobi.tom.rest.model.ApiResponse;
import com.traconmobi.tom.rest.model.ApiResponse1;
import com.traconmobi.tom.rest.model.ECOMParser;
import com.traconmobi.tom.rest.model.Escan;
import com.traconmobi.tom.rest.model.TokenParser;
import com.traconmobi.tom.service.Autosync;
import com.traconmobi.tom.service.InitializeCouchDb;
import com.traconmobi.tom.singeleton.UserParser;


//import net.sqlcipher.Cursor;
//import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteException;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.database.sqlite.SQLiteException;

public class TOMLoginUserActivity extends Activity implements TaskDelegate {

   /* static {
        System.loadLibrary("ndkfoo");
    }

    private native String CreateNativeCall();*/

    SampleAlarmReceiver alarm;
    TokenParser tokenParser;
    UserParser userParser;

    // The minimum distance to change Updates in meters
    private static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = (float) 11.1; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute


    public static String baseUrl = "";


    private String getToken = "";
    private String getTimezone = "";

    public String getpickup_reason_response, getpickup_response, session_cust_locate_resp, session_trnsfr_usr_resp, session_udreason_resp, getpickupscan_response, gettomuser_response, session_user_resp, getoutscan_response, session_outscan_resp, usr_btry_name, usr_imei, usr_batry_usage, getemail_response;
    public static String device_id, deviceid, u_name, usr_id, cust_acc_no, u_IMEI, u_loc, u_loc_id, u_last_login_dt, u_login_dt, serverdate, compny_nme, new_dt, currentdate, CiDateTime, uname, username, password, IMEI;

    TextView tv, version_name, progresstextview;
    public EditText txtUserName, txtPassword;
    Button btnstepin;

    public String status, tme_stmp, token = "a2lfrq40kdh0nrut1vl0ani4a2", SHA1, Error_msg;

    protected Cursor c_TOM_handover, c_TOM_cust_loc_mstr, c_TOM_trnsfr_usr_mstr, c_TOM_paymnt_rsn_mstr, c_TOM_ud_rsn_mstr, c_TOM_pay_failed, c_TOM_pay_coltd, c_tom_del_p, c_TOM_email, cr_chk_email, c_tom_pickup_scan, cr_chkuser, cr_chkusr, cr_chkdbusr, cr_chkdel, cr_chkdb, c_TOM_Users, c_unattempt_tom_del, c_tom_del, c_TOM_NOTIFICATION_TRN, c_TOM_UD, cr_chkdbdel, crs_batry_usage;

    //******DB PATH******/
    static File DB_FULL_PATH = new File(Environment.getExternalStorageDirectory()
            + "/Android/data/com.traconmobi.net/app_database/");
    public static String passphrasekey = "x'2DD29CA851E7B56E4697B0E1F08507293D761A05CE4D1B628663F411A8086D99'";

    boolean SESSION_FLAG_START;
    SQLiteDatabase db, db_opn, db_btry_usage, db_del, db_outscan, db_email;

    static final String EXTRA_MESSAGE = null;
    private static final String TAG = "TOMLoginUserActivity";
    List<String> savedList = new ArrayList<String>();
    /**
     * The dialog.
     */
    public static File file;

    double latitude, longitude;
    static Context context;

    // Session Manager Class
    SessionManager session;

    AlertDialog.Builder alertDialog;
    DialogInterface dialog;

    int cnt;

    private LongOperation loginTask;
    static TinyDB tiny;
    private boolean strt_cliked;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            Fabric.with(this, new Crashlytics());
            setContentView(R.layout.activity_tomlogin_user);

            //**********************GETTING THE VERSION NAME************//
            PackageInfo pinfo = null;
            try {
                pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            } catch (NameNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

           /* String targetApi = CreateNativeCall();
            String[] parse = targetApi.split("&&&");
            baseUrl = parse[0];*/

            alarm = new SampleAlarmReceiver();
            userParser = UserParser.getInstance();

            int versionNumber = pinfo.versionCode;
            String versionName = pinfo.versionName;

            version_name = (TextView) findViewById(R.id.version);
            version_name.setText(versionName);

            progresstextview = (TextView) findViewById(R.id.progresstextView);
            btnstepin = (Button) findViewById(R.id.btnsignin);

            //***********************USING LocationManager FOR SETTING DEVICE GPS ON/OFF ***********************/
          /*  LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            LocationListener mlocListener = new MyLocationListener();
            mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, mlocListener);*/
            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
//   		Boolean isInternetPresent = false;
            Boolean isInternetPresent = cd.isConnectingToInternet(); // true or false
            // get Internet status
            isInternetPresent = cd.isConnectingToInternet();

            // check for Internet status
            if (isInternetPresent) {
            } else {
                // Internet connection is not present
                // Ask user to connect to Internet
                showDataDisabledAlertToUser();
            }

            //***********************USING TELEPHONYMANAGER TO GET THE (DEVICE ID/IMEI NUMBER) OF THE DEVICE***********************/
            TelephonyManager telemngr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            device_id = telemngr.getDeviceId();
            deviceid = device_id;

            //***********************USING CALENDAR CLASS TO GET THE CURRENT DATETIME***********************/
            Calendar ci = Calendar.getInstance();
            String month = ci.get(Calendar.MONTH) + 1 + "";
            if (month.length() < 2) {
                month = "0" + month;
            }
            String date = ci.get(Calendar.DAY_OF_MONTH) + "";
            if (date.length() < 2) {
                date = "0" + date;
            }

            CiDateTime = "" + ci.get(Calendar.YEAR) + "-" + month + "-" +
                    date + " " +
                    ci.get(Calendar.HOUR) + ":" +
                    ci.get(Calendar.MINUTE) + ":" +
                    ci.get(Calendar.SECOND);
            currentdate = CiDateTime.substring(0, 10);

            try {
                Date tdate = new Date();
                tme_stmp = new Timestamp(tdate.getTime()).toString();
                Timestamp ts = new Timestamp(System.currentTimeMillis());
                Crashlytics.log(Log.ERROR, TAG, "ts" + ts);

                new_dt = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                String new_dt1 = new SimpleDateFormat("yyyyMMddHHMMSS").format(new Date());
                Crashlytics.log(Log.ERROR, TAG, new_dt1);

                String str_date = "2015-11-16";
                DateFormat formatter_date = new SimpleDateFormat("yyyy-MM-dd");
                formatter_date.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                Date date1 = (Date) formatter_date.parse(str_date);
                Crashlytics.log(Log.ERROR, TAG, "Today is " + date1.getTime() / 1000);

                String str_tme = "15:53:48";
                DateFormat formatter_tme = new SimpleDateFormat("HH:mm:ss");
                formatter_tme.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                Date tme = (Date) formatter_tme.parse(str_tme);
                Crashlytics.log(Log.ERROR, TAG, "Today time is " + tme.getTime());

                final SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                Date dateObj;
                String newDateStr = null;
                dateObj = df.parse(str_tme);
                SimpleDateFormat fd = new SimpleDateFormat("HH:mm:ss");
                newDateStr = fd.format(dateObj);
                Crashlytics.log(Log.ERROR, TAG, "Today timestamp is " + newDateStr);

                usingDateFormatterWithTimeZone(date1.getTime());
//            usingDateFormatterWithTimeZone(1446661800);
            } catch (Exception e) {
                Crashlytics.log(Log.ERROR, TAG, "new_dt Exception" + e.getMessage());
            }


            //***********************CODE TO CREATE DATABASE TABLES**********************/

            if (!DB_FULL_PATH.exists())
                DB_FULL_PATH.mkdirs();

            file = new File(DB_FULL_PATH, "TOM.sqlite");
            try {
                file.createNewFile();

                InitializeSQLCipher();
            } catch (SQLiteException e) {
                // TODO: handle exception
                e.printStackTrace();
                Crashlytics.log(Log.ERROR, TAG, "Exception " + e.getMessage());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
//			InitializeSQLCipher();
            Toast.makeText(TOMLoginUserActivity.this, "Please contact Administrator - onCreate  " + e.getMessage(), Toast.LENGTH_LONG).show();
//			onClickLogOut();
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(Log.ERROR, TAG, err.getMessage());
//    		onClickLogOut();
        } finally {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("", "onResume Called");
    }

    @Override
    public void onPause() {
        Log.e("", "onPause Called");
        super.onPause();
    }


    //deleting from /DCIM/Camera folder

    private void delete_oldest() {
        // TODO Auto-generated method stub
//        File f = new File(Environment.getExternalStorageDirectory() + "/DCIM/Camera" );
        File f = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.traconmobi.parekh/signatures2015-08-03");

        //Log.i("Log", "file name in delete folder :  "+f.toString());
        File[] files = f.listFiles();

        Crashlytics.log(Log.ERROR, TAG, "print filenames " + f.lastModified());
        //Log.i("Log", "List of files is: " +files.toString());
        Arrays.sort(files, new Comparator<Object>() {
            public int compare(Object o1, Object o2) {

                if (((File) o1).lastModified() > ((File) o2).lastModified()) {
                    Log.i("Log", "Going -1");
                    return -1;
                } else if (((File) o1).lastModified() < ((File) o2).lastModified()) {
                    Log.i("Log", "Going +1");
                    return 1;
                } else {
                    Log.i("Log", "Going 0");
                    return 0;
                }
            }

        });

        Crashlytics.log(Log.ERROR, TAG, "print filenames count" + files[0].length());
        //Log.i("Log", "Count of the FILES AFTER DELETING ::"+files[0].length());
//        files[0].delete();
    }


    private String usingDateFormatterWithTimeZone(long input) {
        Date date = new Date(input);
//        Calendar ci = Calendar.getInstance();
        Calendar cal = Calendar.getInstance();//(TimeZone.getTimeZone("GMT"));
        cal.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MMM/dd hh:mm:ss z");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setCalendar(cal);
        cal.setTime(date);
        Crashlytics.log(Log.ERROR, TAG, "Today converted date " + sdf.format(date).toString() + "\n" + "date" + date.toString());
        return sdf.format(date);
    }

    private boolean isServiceRunning() {

        boolean flag = true;
        try {
            ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
            for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (!("LocationLoggerService.class".equals(service.service.getClassName()))) {
                    flag = false;
                } else {
                    flag = true;
                }
            }
        } catch (Exception e) {
            Crashlytics.log(Log.ERROR, TAG, "Exception isServiceRunning" + e.getMessage());
        }
        return flag;
    }

    //***********************CODE TO CREATE DATABASE TABLES**********************/

    private void InitializeSQLCipher() {
        try {
            db = openOrCreateDatabase(file.toString(), SQLiteDatabase.CREATE_IF_NECESSARY, null);
            final String CREATE_TABLE_USER =
                    "create table IF NOT EXISTS TOM_Users (_id integer primary key autoincrement, "
                            + "T_U_ID TEXT not null unique,"
                            + "T_username TEXT not null,"
                            + "T_password TEXT not null,"
                            + "T_IMEI TEXT not null,"
                            + "D_User_last_login DATETIME,"
                            + "D_User_last_login_time DATETIME,"
                            + "T_Cust_Acc_No TEXT,"
                            + "T_CompanyName TEXT,"
                            + "T_Email TEXT,"
                            + "D_ServerDate DATETIME,"
                            + "T_Battery_usage TEXT,"
                            + "T_Loc_Id TEXT,"
                            + "T_Loc_Cd TEXT);";
            db.execSQL(CREATE_TABLE_USER);

            db.execSQL("CREATE INDEX IF NOT EXISTS IDX_TOM_Users ON  TOM_Users(_id,T_U_ID,T_username,T_Cust_Acc_No)");

            final String CREATE_TABLE_TOM_Assignments =
                    "CREATE TABLE IF NOT EXISTS TOM_Assignments ("
                            + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                            + "T_Assignment_Number TEXT, "
                            + "T_Assignment_Id TEXT unique,"
                            + "T_Assignment_Type TEXT,"
                            + "T_Consignee_Name TEXT,"
                            + "T_Address_Line1 TEXT,"
                            + "T_Address_Line2 TEXT,"
                            + "T_City TEXT,"
                            + "T_Pincode TEXT,"
                            + "T_Contact_number TEXT,"
                            + "B_is_Amountable BOOL,"
//                            + "F_Amount FLOAT," commented on 10dec2015
                            + "F_Amount TEXT,"
                            + "T_Amount_Type TEXT,"
                            + "T_Shipper_Acc_No TEXT,"
                            + "T_NoOfPcs TEXT,"
                            + "T_Pickup_Time TEXT,"
                            + "T_Shipper_name TEXT,"
                            + "T_Shipper_contact_number TEXT,"
                            + "T_Shipper_email TEXT,"
                            + "B_is_Completed BOOL,"
                            + "D_Completed_Date DATETIME,"
                            + "D_Completed_time DATETIME,"
                            + "T_Received_by_Collected_from TEXT,"
                            + "T_Receiver_Contact_Num TEXT,"
                            + "T_Relationship TEXT,"
                            + "T_Signature TEXT,"
                            + "T_Photo TEXT,"
                            + "T_Photo_dt DATETIME,"
                            + "T_Photo_tm DATETIME,"
                            + "T_Signature_dt DATETIME,"
                            + "F_Amount_Collected TEXT,"
                            + "D_OutScan_Date DATETIME,"
                            + "T_Out_Scan_Location TEXT,"
                            + "T_OUT_Scan_U_ID TEXT,"
                            + "T_Cust_Acc_NO TEXT,"
                            + "T_AcquiredLat TEXT,"
                            + "T_AccquiredLon TEXT,"
                            + "D_Assignment_preferred_time DATETIME,"
                            + "T_Payment_Mode TEXT,"
                            + "T_Particular TEXT,"
                            + "T_Receipt_no TEXT,"
                            + "T_Cheque_DD_no TEXT,"
                            + "T_Cheque_DD_Dt DATETIME,"
                            + "T_Bank_name TEXT,"
                            + "T_Bank_loc TEXT,"
                            + "T_Filter TEXT,"
                            + "T_Filter_Reason TEXT,"
                            + "T_trnsfr_usr_id TEXT,"
//          				+ "T_trnsfr_usr_id TEXT UNIQUE ON CONFLICT REPLACE,"
                            + "T_trnsfr_usr_nme TEXT,"
                            + "T_trnsfr_sync CHAR,"
                            + "C_Handover_sync CHAR,"
                            + "T_Trip_MOT TEXT,"
                            + "C_Scanned_flg CHAR,"
                            + "C_is_Sync CHAR )";
            db.execSQL(CREATE_TABLE_TOM_Assignments);

            db.execSQL("CREATE INDEX IF NOT EXISTS IDX_TOM_Assignments ON  TOM_Assignments  (_id,T_Assignment_Id,T_Assignment_Number,T_OUT_Scan_U_ID,T_Cust_Acc_NO,D_OutScan_Date)");


            final String CREATE_TABLE_Tom_Assignments_In_Complete =
                    "CREATE TABLE IF NOT EXISTS Tom_Assignments_In_Complete ("
                            + "_id integer primary key autoincrement, "
                            + "T_U_ID TEXT,"
                            + "T_Assignment_Type TEXT,"
                            + "T_Assignment_Number TEXT, "
                            + "T_Assignment_Id TEXT, "
                            + "T_Remarks TEXT, "
                            + "T_Photo TEXT,"
                            + "T_Photo_dt DATETIME,"
                            + "T_Photo_tm DATETIME,"
                            + "T_Assignment_In_Complete_Reason TEXT, "
                            + "T_Assignment_In_Complete_Reason_Id TEXT, "
                            + "D_In_Complete_Date DATETIME ,"
                            + "D_In_Complete_Time DATETIME , "
                            + "D_Last_Updated_date_time DATETIME,"
                            + "T_Out_Scan_Location TEXT,"
                            + "T_AccquiredLat TEXT,"
                            + "T_AccquiredLon TEXT,"
                            + "T_Cust_Acc_NO TEXT,"
                            + "T_Status TEXT,"
                            + "C_is_Sync CHAR )";
            db.execSQL(CREATE_TABLE_Tom_Assignments_In_Complete);

            db.execSQL("CREATE INDEX IF NOT EXISTS IDX_Tom_Assignments_In_Complete ON  Tom_Assignments_In_Complete  (_id,T_U_ID,T_Assignment_Id)");

            final String CREATE_TABLE_NOTIFICATION =
                    "CREATE TABLE IF NOT EXISTS TOM_NOTIFICATION_TRN ("
                            + "_id integer primary key autoincrement, "
                            + "T_U_ID Text,"
                            + "T_Cust_Acc_NO TEXT,"
                            + "T_Assignment_Id TEXT, "
                            + "T_Assignment_Number TEXT, "
                            + "T_Assignment_Type TEXT,"
                            + "T_Message TEXT, "
                            + "D_Trans_Date_Time DATETIME, "
                            + "T_Notifier_Name TEXT, "
                            + "T_Notifier_Loc TEXT, "
                            + "T_Notifier_Dept TEXT, "
                            + "D_Sync_Date DATETIME, "
                            + "D_Sync_Time DATETIME,"
                            + "T_Notify_Code TEXT unique, "
                            + "C_is_Read_Status CHAR, "
                            + "C_is_Notify_Sync_Status CHAR )";

            db.execSQL(CREATE_TABLE_NOTIFICATION);

            db.execSQL("CREATE INDEX IF NOT EXISTS IDX_TOM_NOTIFICATION_TRN ON  TOM_NOTIFICATION_TRN  (_id,T_U_ID,T_Cust_Acc_NO,T_Assignment_Id,T_Notify_Code)");

            final String CREATE_TABLE_Relation_mstr =
                    "CREATE TABLE  IF NOT EXISTS TBL_Relation_mstr(_id integer primary key autoincrement, "
                            + "T_RELATION_ID TEXT UNIQUE ON CONFLICT REPLACE,"
                            + "T_RELATION_TYP TEXT);";

            db.execSQL(CREATE_TABLE_Relation_mstr);

            /***************PRAGMA***************/
            db.execSQL("PRAGMA automatic_index = false;");
            db.execSQL("CREATE INDEX IF NOT EXISTS IDX_TBL_Relation_mstr ON  TBL_Relation_mstr  (_id,T_RELATION_ID,T_RELATION_TYP)");

            final String CREATE_TABLE_UD_REASONCODE_MASTER =
                    "CREATE TABLE IF NOT EXISTS UD_REASON_MASTER ("
                            + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                            + "T_UD_Reason_Id INTEGER UNIQUE ON CONFLICT REPLACE,"
                            + "T_UD_Reason_Code TEXT,"
                            + "T_UD_Reason_Desc TEXT,"
                            + "T_Cust_Acc_NO TEXT,"
                            + "D_DCREATE_DT DATETIME )";
            db.execSQL(CREATE_TABLE_UD_REASONCODE_MASTER);

            db.execSQL("CREATE INDEX IF NOT EXISTS IDX_TBL_UD_REASON_MASTER ON UD_REASON_MASTER(_id,T_UD_Reason_Id,T_UD_Reason_Code)");

            final String CREATE_TABLE_trip_User_MASTER =
                    "CREATE TABLE IF NOT EXISTS Trip_User_MASTER ("
                            + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                            + "T_usr_id TEXT UNIQUE ON CONFLICT REPLACE,"
                            + "T_Mode_transport TEXT,"
                            + "T_Start_Location TEXT,"
                            + "T_End_Location TEXT,"
                            + "T_Cust_Acc_NO TEXT,"
                            + "IMEI TEXT,"
                            + "TOL_KM TEXT NOT NULL  DEFAULT 0,"
                            + "D_Start_TM DATETIME,"
                            + "D_End_TM DATETIME,"
                            + "C_Sync CHAR)";
            db.execSQL(CREATE_TABLE_trip_User_MASTER);

            db.execSQL("CREATE INDEX IF NOT EXISTS IDX_TBL_Trip_User_MASTER ON Trip_User_MASTER(_id,T_usr_id,T_Cust_Acc_NO)");


            final String CREATE_TABLE_Transfer_User_MASTER =
                    "CREATE TABLE IF NOT EXISTS Transfer_User_MASTER ("
                            + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                            + "T_trnsfr_usr_id TEXT UNIQUE ON CONFLICT REPLACE,"
                            + "T_trnsfr_usr_nme TEXT,"
                            + "T_Cust_Acc_NO TEXT,"
                            + "T_Loc_Cd TEXT,"
                            + "D_DCREATE_DT DATETIME )";
            db.execSQL(CREATE_TABLE_Transfer_User_MASTER);

            final String CREATE_TABLE_TOM_Handover_Assignments =
                    "CREATE TABLE IF NOT EXISTS TOM_HandOver_Assignments ("
                            + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
//                				+ "T_Assignment_Number TEXT, "
                            + "T_Assignment_Id TEXT unique,"
                            + "T_Handover_Assignment_Type TEXT,"
                            + "T_Consignee_Name TEXT,"
                            + "T_Received_by TEXT,"
                            + "T_Handover_Contact_number TEXT,"
                            + "D_Handover_Date DATETIME,"
                            + "D_Handover_time DATETIME,"
                            + "T_Handover_Received_by_Collected_from TEXT,"
                            + "T_Handover_Signature TEXT,"
                            + "T_Handover_Signature_dt DATETIME,"
                            + "F_Amount_Collected FLOAT,"
//                                + "D_OutScan_Date DATETIME,"
                            + "T_Out_Scan_Location TEXT,"
                            + "T_OUT_Scan_U_ID TEXT,"
                            + "T_Cust_Acc_NO TEXT,"
                            + "T_AcquiredLat TEXT,"
                            + "T_AccquiredLon TEXT,"
                            + "T_Handover_update CHAR,"
//                                + "T_Deposit_PhotoId TEXT,"
                            + "C_is_Sync CHAR )";
            db.execSQL(CREATE_TABLE_TOM_Handover_Assignments);

            db.execSQL("CREATE INDEX IF NOT EXISTS IDX_TOM_HandOver_Assignments ON  TOM_HandOver_Assignments  (_id,T_Assignment_Id,T_OUT_Scan_U_ID,T_Cust_Acc_NO)");

            db.close();
            deleteContact();
//		 deleteLatest();
        } catch (SQLiteException e) {
            Toast.makeText(TOMLoginUserActivity.this, "Please try again !!! \n Please insert Memory card", Toast.LENGTH_LONG).show();
            onClickLogOut();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(TOMLoginUserActivity.this, "Please insert Memory card ", Toast.LENGTH_LONG).show();
            onClickLogOut();
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(Log.ERROR, TAG, "Error TOMLoginUserActivity UnsatisfiedLinkError ");
            onClickLogOut();
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }


    //deleting from /com.traconmobi.tom/signatures folder

    private void deleteLatest() {
        // TODO Auto-generated method stub
        File f = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.traconmobi.net/signatures");

        //Log.i("Log", "file name in delete folder :  "+f.toString());
        File[] files = f.listFiles();


        Log.i("Log", "List of files is: " + files.toString() + f.getName() + files.length);
        Arrays.sort(files, new Comparator<Object>() {
            public int compare(Object o1, Object o2) {

                if (((File) o1).lastModified() > ((File) o2).lastModified()) {
                    //         Log.i("Log", "Going -1");
                    return -1;
                } else if (((File) o1).lastModified() < ((File) o2).lastModified()) {
                    //     Log.i("Log", "Going +1");
                    return 1;
                } else {
                    //     Log.i("Log", "Going 0");
                    return 0;
                }
            }

        });

        //Log.i("Log", "Count of the FILES AFTER DELETING ::"+files[0].length());
        files[0].delete();

    }


    // Deleting tables data
    public void deleteContact() {
        try {
            CouchBaseDBHelper couchBaseDBHelper = new CouchBaseDBHelper(getApplicationContext());
            couchBaseDBHelper.deletePreviousDatabase();
            db_opn = SQLiteDatabase.openDatabase(file.toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
//		db_opn=SQLiteDatabase.openOrCreateDatabase(file, passphrasekey, null);//.openDatabase(TOMLoginUserActivity.DB_FULL_PATH, passphrasekey,null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
            // Get today as a Calendar
            Calendar today = Calendar.getInstance();
            // Subtract 2 days
//		today.add(Calendar.DATE, -2);
            today.add(Calendar.DATE, -1);  //modified on 09Dec 2014
//            today.add(Calendar.DATE, -3);//modified on 09Dec 2014
            // Make an SQL Date out of that
            java.sql.Date yesterday = new java.sql.Date(today.getTimeInMillis());
            Crashlytics.log(Log.ERROR, TAG, "yestday date " + yesterday + Calendar.DATE);

            //TOM_Users
            c_TOM_Users = db_opn.rawQuery("DELETE FROM TOM_Users WHERE D_User_last_login <='" + yesterday + "'", null);
            while (c_TOM_Users.moveToNext()) {

            }
            c_TOM_Users.close();

            //TOM_Assignments
//	 c_tom_del=db_opn.rawQuery("DELETE FROM TOM_Del WHERE AWB_OutScanTime='" + yesterday +"'and IS_DEL_SYNC_STATUS=1", null);
//	 c_tom_del=db_opn.rawQuery("DELETE FROM TOM_Assignments WHERE D_OutScan_Date <='" + yesterday +"' and C_is_Sync='1' ", null);//commented on 31jul2015
//            c_tom_del=db_opn.rawQuery("DELETE FROM TOM_Assignments WHERE D_OutScan_Date <='" + yesterday +"'", null);
            c_tom_del = db_opn.rawQuery("DELETE FROM TOM_Assignments WHERE D_OutScan_Date <='" + yesterday + "' AND C_is_Sync='1' ", null);
            while (c_tom_del.moveToNext()) {

            }
            c_tom_del.close();
            c_unattempt_tom_del = db_opn.rawQuery("DELETE FROM TOM_Assignments WHERE D_OutScan_Date <='" + yesterday + "' AND B_is_Completed IS NULL ", null);
            while (c_unattempt_tom_del.moveToNext()) {

            }
            c_unattempt_tom_del.close();
            //TOM_Assignments
            String type = "P";
            c_tom_del_p = db_opn.rawQuery("DELETE FROM TOM_Assignments WHERE D_OutScan_Date <='" + currentdate + "' AND T_Assignment_Type='" + type + "'", null);
            while (c_tom_del_p.moveToNext()) {

            }
            c_tom_del_p.close();


            //TOM_NOTIFICATION_TRN
// 	 c_TOM_NOTIFICATION_TRN=db_opn.rawQuery("DELETE FROM TOM_NOTIFICATION_TRN WHERE D_Sync_Date <='" + yesterday +"' and C_is_Notify_Sync_Status='1' ", null);//commented on 31jul2015
            c_TOM_NOTIFICATION_TRN = db_opn.rawQuery("DELETE FROM TOM_NOTIFICATION_TRN WHERE D_Sync_Date <='" + yesterday + "'", null);
            while (c_TOM_NOTIFICATION_TRN.moveToNext()) {

            }
            c_TOM_NOTIFICATION_TRN.close();

            //Tom_Assignments_In_Complete
//   c_TOM_UD=db_opn.rawQuery("DELETE FROM Tom_Assignments_In_Complete WHERE D_In_Complete_Date <='" + yesterday +"' and C_is_Sync='1' ", null);//commented on 31jul2015
            c_TOM_UD = db_opn.rawQuery("DELETE FROM Tom_Assignments_In_Complete WHERE D_In_Complete_Date <='" + yesterday + "'  AND C_is_Sync='1'", null);
            while (c_TOM_UD.moveToNext()) {

            }
            c_TOM_UD.close();

            // UD_REASON_MASTER
            c_TOM_ud_rsn_mstr = db_opn.rawQuery("DELETE FROM UD_REASON_MASTER WHERE D_DCREATE_DT <='" + yesterday + "'", null);
            while (c_TOM_ud_rsn_mstr.moveToNext()) {

            }
            c_TOM_ud_rsn_mstr.close();


            //Transfer_User_MASTER

            c_TOM_trnsfr_usr_mstr = db_opn.rawQuery("DELETE FROM Transfer_User_MASTER WHERE D_DCREATE_DT <='" + yesterday + "'", null);
            while (c_TOM_trnsfr_usr_mstr.moveToNext()) {

            }
            c_TOM_trnsfr_usr_mstr.close();

            //TOM_HandOver_Assignments
            c_TOM_handover = db_opn.rawQuery("DELETE FROM TOM_HandOver_Assignments WHERE D_Handover_Date <='" + yesterday + "'", null);
            while (c_TOM_handover.moveToNext()) {

            }
            c_TOM_handover.close();


            db_opn.close();
        } catch (SQLiteException e) {
            Toast.makeText(TOMLoginUserActivity.this, "Please insert Memory card!!! ", Toast.LENGTH_LONG).show();
            onClickLogOut();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(TOMLoginUserActivity.this, "Please contact respective Administrator ", Toast.LENGTH_LONG).show();
            onClickLogOut();
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(Log.ERROR, TAG, "Error TOMLoginUserActivity UnsatisfiedLinkError ");
            onClickLogOut();
        } finally {
            if (c_TOM_Users != null) {
                c_TOM_Users.close();
            }
            if (c_tom_del != null) {
                c_tom_del.close();
            }
            if (c_unattempt_tom_del != null) {
                c_unattempt_tom_del.close();
            }
            if (c_tom_del_p != null) {
                c_tom_del_p.close();
            }

            if (c_TOM_NOTIFICATION_TRN != null) {
                c_TOM_NOTIFICATION_TRN.close();
            }
            if (c_TOM_UD != null) {
                c_TOM_UD.close();
            }

            if (c_TOM_ud_rsn_mstr != null) {
                c_TOM_ud_rsn_mstr.close();
            }

            if (c_TOM_trnsfr_usr_mstr != null) {
                c_TOM_trnsfr_usr_mstr.close();
            }

            if (c_TOM_handover != null) {
                c_TOM_handover.close();
            }
            if (db_opn != null) {
                db_opn.close();
            }

        }
    }

    //********************** Function to show Datapack settings alert dialog************/
    private void showDataDisabledAlertToUser() {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Datapack is disabled in your device. Would you like to enable it?")
                    .setCancelable(false)
                    .setPositiveButton("Goto Settings Page To Enable Datapack",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent callDataSettingIntent = new Intent(
                                            Settings.ACTION_WIRELESS_SETTINGS);
                                    startActivity(callDataSettingIntent);
                                }
                            });
            AlertDialog alert = alertDialogBuilder.create();
            alert.show();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
//    		onClickLogOut();
            Toast.makeText(TOMLoginUserActivity.this, "Datapack is disabled in your device , Please try again !!! " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }


    //***********************  FUNCTION CALLED WHEN THE USER CLICKS THE LOGIN BUTTON**********************/

    public void sendMessage(View view) {
        try {
            // Session Manager
            session = new SessionManager(getApplicationContext());
            txtUserName = (EditText) findViewById(R.id.txtUsername);
            txtPassword = (EditText) findViewById(R.id.txtPassword);

            username = txtUserName.getText().toString().trim();
            password = txtPassword.getText().toString().trim();

            IMEI = deviceid.toString();
            session.setKeyIMEI(IMEI, username);
            //deviceid.toString();

            String User_last_login = currentdate;

            if (username.isEmpty() || password.isEmpty()) {
                Toast toast = Toast
                        .makeText(TOMLoginUserActivity.this, "Enter Username and password", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                if (username.isEmpty()) {
                    txtUserName.setFocusable(true);
                    txtUserName.requestFocus();
                } else if (password.isEmpty()) {
                    txtPassword.setFocusable(true);
                    txtPassword.requestFocus();
                }
            } else {
                txtUserName.setEnabled(false);
                txtPassword.setEnabled(false);
                session.setFirstInitialization(true);
                session.setDBInit(true);
                if (!session.getStartService()) {
                    session.setStartService(true);
                }
                String test = new String(password);
                SHA1 = SHA1(test);
                Log.e(TAG, "SHA1 HASH: " + SHA1);
//  			Toast.makeText(TOMLoginUserActivity.this,"SHA1 HASH: " + SHA1, Toast.LENGTH_LONG).show();
                session.createLoginSession(username, password);
                session.createAuthenticateDbSession(file.toString(), passphrasekey);
                if (session.getBaseUrl() != null || session.getBaseUrl() != "") {
                    baseUrl = session.getBaseUrl();
                }
//      	  loadSavedPreferences();
                String entityString = "suserID=" + URLEncoder.encode(username) + "&spassword=" + URLEncoder.encode(password)
                        + "&sIMEI=" + URLEncoder.encode(IMEI);

                //check if any previous task is running, if so then cancel it
                //it can be cancelled if it is not in FINISHED state
                if (loginTask != null && loginTask.getStatus() != AsyncTask.Status.FINISHED)
                    loginTask.cancel(true);
                loginTask = new LongOperation(); //every time create new object, as AsynTask will only be executed one time.
//                loginTask.execute(serverURL);
                loginTask.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.log(Log.ERROR, TAG, "Exception sendMessage " + e.getMessage());
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(Log.ERROR, TAG, err.getMessage());

        }
    }

    /**
     * Returns the SHA1 hash for the provided String
     *
     * @param text
     * @return the SHA1 hash or null if an error occurs
     */
    public static String SHA1(String text) {

        try {

            MessageDigest md;
            md = MessageDigest.getInstance("SHA-1");
            md.update(text.getBytes("UTF-8"),
                    0, text.length());
            byte[] sha1hash = md.digest();

            return toHex(sha1hash);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String toHex(byte[] buf) {

        if (buf == null) return "";

        int l = buf.length;
        StringBuffer result = new StringBuffer(2 * l);

        for (int i = 0; i < buf.length; i++) {
            appendHex(result, buf[i]);
        }

        return result.toString();

    }

    private final static String HEX = "0123456789abcdef";

    private static void appendHex(StringBuffer sb, byte b) {

        sb.append(HEX.charAt((b >> 4) & 0x0f))
                .append(HEX.charAt(b & 0x0f));

    }

    @Override
    public void TaskCompletionResult(boolean result) {

    }

    // Class which extends AsyncTask class
    private class LongOperation extends AsyncTask<String, Void, Void> {

        ProgressDialog Dialog = new ProgressDialog(TOMLoginUserActivity.this);
        private String Content;
        private String Error = null;

        @Override
        protected void onPreExecute() {
            // NOTE: You can call UI Element here.
            //UI Element
            btnstepin.setEnabled(false);
//            btnstepin.setBackgroundColor(Color.GRAY);
            progresstextview.setVisibility(View.VISIBLE);
        }

        // Call after onPreExecute method
        @Override
        protected Void doInBackground(String... urls) {
            try {
                db = SQLiteDatabase.openDatabase(file.toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                Log.e(TAG, "New date" + new_dt);
                cr_chkdb = db.rawQuery("SELECT _id,T_U_ID,T_username,T_password,T_IMEI,D_User_last_login,T_Loc_Cd,T_Cust_Acc_No FROM TOM_Users where T_username ='" + username + "' AND D_ServerDate='" + new_dt + "' AND T_password = '" + SHA1 + "' ", null);

                final int cr_unattempt_userdbcount = cr_chkdb.getCount();
                Log.e(TAG, "Count" + cr_unattempt_userdbcount);
                cr_chkdb.close();
                db.close();

                if (cr_unattempt_userdbcount == 0) {
                    //**When ever you want to check Internet Status in your application call isConnectingToInternet() function and it will return true or false***/
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());

                    // Boolean isInternetPresent = false;
                    Boolean isInternetPresent = cd.isConnectingToInternet(); // true or false

                    // get Internet status
                    isInternetPresent = cd.isConnectingToInternet();

                    // check for Internet status
                    if (isInternetPresent) {
                        final ParseAuthenticate authenticate = new ParseAuthenticate(getApplicationContext());
                        Map<String, String> data1 = new HashMap<>();
                        data1.put("UserName", username);
                        data1.put("Password", password);
                        data1.put("iemi", String.valueOf(IMEI));
                        data1.put("timezone", "Asia/Kolkata");
                        try {
                            Call<TokenParser> call = App.getRestClient().getToken().getToken(data1);
                            call.enqueue(new Callback<TokenParser>() {
                                @Override
                                public void onResponse(Response<TokenParser> apiResponse, Retrofit retrofit) {
                                    Log.e(TAG, "Successful Token ");
                                    if (apiResponse.body() != null) {
                                        if (apiResponse.body().getToken() != null) {
                                            if (session == null) {
                                                session = new SessionManager(getApplicationContext());
                                            }
                                            Log.e(TAG, apiResponse.body().getToken());
                                            tokenParser = apiResponse.body();
                                            initializeUserParser(tokenParser);
                                            serverdate = apiResponse.body().getServerDateTime().substring(0, 10);
                                            startService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
                                            session_user_resp = authenticate.storeInDb(apiResponse.body());
                                            if (session_user_resp != null && session_user_resp != " ") {
                                                int inserted_records = 0;
                                                try {
                                                    db_del = SQLiteDatabase.openDatabase(file.toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
//                                                  cr_chkdbusr= db_del.rawQuery("SELECT _id,T_U_ID,T_username,T_password,T_IMEI,D_User_last_login,T_Loc_Cd,T_Cust_Acc_No,D_ServerDate FROM TOM_Users where T_username ='"+ username +"' AND T_IMEI='"+ IMEI +"' AND T_password = '"+ SHA1 +"' AND D_User_last_login='"+ new_dt +"'", null);
                                                    cr_chkdbusr = db_del.rawQuery("SELECT _id,T_U_ID,T_username,T_password,T_IMEI,D_User_last_login,T_Loc_Cd,T_Loc_Id,T_Cust_Acc_No,T_CompanyName,D_ServerDate FROM TOM_Users where T_username ='" + username + "' AND T_password = '" + SHA1 + "' AND D_User_last_login='" + new_dt + "'", null);
                                                    while (cr_chkdbusr.moveToNext()) {
                                                        u_name = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("T_username"));
                                                        usr_id = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("T_U_ID"));
                                                        u_IMEI = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("T_IMEI"));
                                                        u_loc = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("T_Loc_Cd"));
                                                        u_loc_id = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("T_Loc_Id"));
                                                        cust_acc_no = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("T_Cust_Acc_No"));
                                                        compny_nme = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("T_CompanyName"));
                                                        u_last_login_dt = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("D_User_last_login"));
                                                        u_login_dt = u_last_login_dt.substring(0, 10);
                                                        serverdate = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("D_ServerDate"));
                                                        Log.e(TAG, "Check in table: " + u_name + ":" + u_login_dt + ":" + serverdate);
                                                    }
                                                    inserted_records = cr_chkdbusr.getCount();
                                                    Crashlytics.log(Log.ERROR, TAG, "print inserted_records" + inserted_records);
                                                    cr_chkdbusr.close();
                                                    db_del.close();

                                                } catch (Exception e) {

                                                }
                                                try {
                                                    if (inserted_records > 0) {
                                                        session.createUserDetailsSession(apiResponse.body().getLocCode(), String.valueOf(apiResponse.body().getUserId()),
                                                                apiResponse.body().getServerDateTime().substring(0, 10), String.valueOf(apiResponse.body().getCompanyID()), apiResponse.body().getUserName(),
                                                                apiResponse.body().getCompanyName(), String.valueOf(apiResponse.body().getLocID()));

                                                        /*****************************************Retrofit Api Call***************************************************************/
                                                        if (serverdate != null && new_dt != null) {
                                                            if (serverdate.equals(new_dt)) {
                                                                if (tiny == null) {
                                                                    tiny = new TinyDB(getApplicationContext());
                                                                }

                                                                strt_cliked = false;
                                                                tiny.putBoolean("FLAG_START", strt_cliked);
                                                                tiny.putString("transport_val", "SELECT TRANSPORT");

                                                                Intent i = new Intent(getApplicationContext(), InitializeCouchDb.class);
                                                                i.putExtra("parser", apiResponse.body());
                                                                i.putExtra("password", password);
                                                                startService(i);
                                                                alarm.setAlarm(TOMLoginUserActivity.this);
                                                                Intent intent = new Intent(TOMLoginUserActivity.this, HomeMainActivity.class);
                                                                // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                                String message = username.toString();
                                                                intent.putExtra(EXTRA_MESSAGE, message);
                                                                startActivity(intent);
                                                                //you may call the cancel() method but if it is not handled in doInBackground() method
                                                                if (loginTask != null && loginTask.getStatus() != AsyncTask.Status.FINISHED)
                                                                    loginTask.cancel(true);
//                                        Toast.makeText(TOMLoginUserActivity.this,"Successfully Logged In", Toast.LENGTH_LONG).show();
                                                                Toast toast = Toast
                                                                        .makeText(TOMLoginUserActivity.this, "Successfully Logged In", Toast.LENGTH_SHORT);
                                                                toast.setGravity(Gravity.CENTER, 0, 0);
                                                                toast.show();
                                                            }
                                                        } else {
                                                            runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    progresstextview.setVisibility(View.INVISIBLE);
//                                                Toast.makeText(TOMLoginUserActivity.this, "Please Check Your Mobile Date", Toast.LENGTH_LONG).show();
                                                                    Toast toast = Toast
                                                                            .makeText(TOMLoginUserActivity.this, "Please Check Your Mobile Date", Toast.LENGTH_SHORT);
                                                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                                                    toast.show();
                                                                    btnstepin.setEnabled(true);
                                                                    txtUserName.setEnabled(true);
                                                                    txtPassword.setEnabled(true);
                                                                    //you may call the cancel() method but if it is not handled in doInBackground() method
                                                                    if (loginTask != null && loginTask.getStatus() != Status.FINISHED)
                                                                        loginTask.cancel(true);
                                                                }
                                                            });
                                                        }
                                                    } else {
                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                progresstextview.setVisibility(View.INVISIBLE);
//                                                Toast.makeText(TOMLoginUserActivity.this, "Please Check Your Mobile Date", Toast.LENGTH_LONG).show();
                                                                Toast toast = Toast
                                                                        .makeText(TOMLoginUserActivity.this, "No data is assigned", Toast.LENGTH_SHORT);
                                                                toast.setGravity(Gravity.CENTER, 0, 0);
                                                                toast.show();
                                                                btnstepin.setEnabled(true);
                                                                txtUserName.setEnabled(true);
                                                                txtPassword.setEnabled(true);
                                                                //you may call the cancel() method but if it is not handled in doInBackground() method
                                                                if (loginTask != null && loginTask.getStatus() != Status.FINISHED)
                                                                    loginTask.cancel(true);
                                                            }
                                                        });
                                                    }
                                                } catch (Exception e) {
                                                    Crashlytics.log(Log.ERROR, TAG, "Exception server date and new date " + e.getMessage());
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            progresstextview.setVisibility(View.INVISIBLE);
//                                                Toast.makeText(TOMLoginUserActivity.this, "Please Check Your Mobile Date", Toast.LENGTH_LONG).show();
                                                            Toast toast = Toast
                                                                    .makeText(TOMLoginUserActivity.this, "Please Check Your Mobile Date", Toast.LENGTH_SHORT);
                                                            toast.setGravity(Gravity.CENTER, 0, 0);
                                                            toast.show();
                                                            btnstepin.setEnabled(true);
                                                            txtUserName.setEnabled(true);
                                                            txtPassword.setEnabled(true);
                                                            //you may call the cancel() method but if it is not handled in doInBackground() method
                                                            if (loginTask != null && loginTask.getStatus() != Status.FINISHED)
                                                                loginTask.cancel(true);
                                                        }
                                                    });
                                                }

                                            }


                                        } else {
                                            Error_msg = "Incorrect User Name or Password";
                                            progresstextview.setVisibility(View.INVISIBLE);
//                                Toast.makeText(TOMLoginUserActivity.this,"Incorrect Username or Password \n (OR) \n Please Check Mobile Internet Connection", Toast.LENGTH_LONG).show();
                                            Toast toast = Toast
                                                    .makeText(TOMLoginUserActivity.this, "Incorrect Username or Password", Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.CENTER, 0, 0);
                                            toast.show();
                                            //showDataDisabledAlertToUser();
                                            btnstepin.setEnabled(true);
                                            txtUserName.setEnabled(true);
                                            txtPassword.setEnabled(true);
//                                btnstepin.setBackgroundColor(getResources().getColor(R.color.opaque_color));
                                            //you may call the cancel() method but if it is not handled in doInBackground() method
                                            if (loginTask != null && loginTask.getStatus() != Status.FINISHED)
                                                loginTask.cancel(true);
                                        }
                                    } else {
                                        Error_msg = "Device is not Registered";
                                        progresstextview.setVisibility(View.INVISIBLE);
//                                Toast.makeText(TOMLoginUserActivity.this,"Incorrect Username or Password \n (OR) \n Please Check Mobile Internet Connection", Toast.LENGTH_LONG).show();
                                        Toast toast = Toast
                                                .makeText(TOMLoginUserActivity.this, "Device is not Registered", Toast.LENGTH_SHORT);
                                        toast.setGravity(Gravity.CENTER, 0, 0);
                                        toast.show();
                                        //showDataDisabledAlertToUser();
                                        btnstepin.setEnabled(true);
                                        txtUserName.setEnabled(true);
                                        txtPassword.setEnabled(true);
//                                btnstepin.setBackgroundColor(getResources().getColor(R.color.opaque_color));
                                        //you may call the cancel() method but if it is not handled in doInBackground() method
                                        if (loginTask != null && loginTask.getStatus() != Status.FINISHED)
                                            loginTask.cancel(true);

                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    Toast toast = Toast
                                            .makeText(TOMLoginUserActivity.this, "Please check mobile data", Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();
                                    Log.e(TAG, "TokenParser" + t.getLocalizedMessage());
                                    if (loginTask != null && loginTask.getStatus() != Status.FINISHED)
                                        loginTask.cancel(true);
                                }
                            });
                        } catch (Exception e) {
                            Log.e("POST", "Error : " + e.getMessage());
                        }
                        //******************ASyncronous call to the GetTomUser webservice********************/
                    } else {
                        // Internet connection is not present
                        // Ask user to connect to Internet
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progresstextview.setVisibility(View.INVISIBLE);
//                                Toast.makeText(TOMLoginUserActivity.this,"Incorrect Username or Password \n (OR) \n Please Check Mobile Internet Connection", Toast.LENGTH_LONG).show();
                                Toast toast = Toast
                                        .makeText(TOMLoginUserActivity.this, "Incorrect Username or Password (OR) \n Please Check Mobile Internet Connection", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                showDataDisabledAlertToUser();
                                btnstepin.setEnabled(true);
                                txtUserName.setEnabled(true);
                                txtPassword.setEnabled(true);
                                if (loginTask != null && loginTask.getStatus() != Status.FINISHED)
                                    loginTask.cancel(true);
                            }
                        });
                    }

                } else if (cr_unattempt_userdbcount > 0) {
                    db_del = SQLiteDatabase.openDatabase(file.toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                    cr_chkdbusr = db_del.rawQuery("SELECT _id,T_U_ID,T_username,T_password,T_IMEI,D_User_last_login,T_Loc_Cd,T_Loc_Id,T_Cust_Acc_No,T_CompanyName,D_ServerDate FROM TOM_Users where T_username ='" + username + "' AND D_User_last_login='" + new_dt + "' AND T_password = '" + SHA1 + "' ", null);
                    while (cr_chkdbusr.moveToNext()) {
                        u_name = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("T_username"));
                        usr_id = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("T_U_ID"));
                        u_IMEI = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("T_IMEI"));
                        u_loc = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("T_Loc_Cd"));
                        u_loc_id = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("T_Loc_Id"));
                        cust_acc_no = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("T_Cust_Acc_No"));
                        compny_nme = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("T_CompanyName"));
                        u_last_login_dt = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("D_User_last_login"));
                        u_login_dt = u_last_login_dt.substring(0, 10);
                        serverdate = cr_chkdbusr.getString(cr_chkdbusr.getColumnIndex("D_ServerDate"));
                        session.createUserDetailsSession(u_loc, usr_id, currentdate, cust_acc_no, u_name, compny_nme, u_loc_id);
                    }
                    int cr_chkdbusr_dbcount = cr_chkdbusr.getCount();
                    cr_chkdbusr.close();
                    db_del.close();
                    //******************Asyncronous call to the OutScan webservice********************/
                    if (cr_chkdbusr_dbcount > 0) {
//  					db_outscan=SQLiteDatabase.openOrCreateDatabase(file, passphrasekey, null);
                        db_outscan = SQLiteDatabase.openDatabase(file.toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                        cr_chkdbdel = db_outscan.rawQuery("SELECT T_Assignment_Id FROM TOM_Assignments where T_OUT_Scan_U_ID ='" + usr_id + "' and T_Cust_Acc_NO ='" + cust_acc_no + "' and D_OutScan_Date ='" + new_dt + "'", null);
                        while (cr_chkdbdel.moveToNext()) {

                        }
                        int dbdel_count = cr_chkdbdel.getCount();
                        cr_chkdbdel.close();
                        db_outscan.close();

                        if (dbdel_count == 0) {
                            //**When ever you want to check Internet Status in your application call isConnectingToInternet() function and it will return true or false***/
                            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                            Boolean isInternetPresent = cd.isConnectingToInternet(); // true or false
                            // get Internet status
                            isInternetPresent = cd.isConnectingToInternet();
                            // check for Internet status
                            if (isInternetPresent) {
                                // Internet Connection is Present
                                // make HTTPs requests
                                startService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//                                            deleteContact();
                                        // Put here the line which is changing the UI.
                                        alarm.setAlarm(TOMLoginUserActivity.this);
                                        Intent intent = new Intent(TOMLoginUserActivity.this, HomeMainActivity.class);

                                        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        String message = username.toString();
                                        intent.putExtra(EXTRA_MESSAGE, message);
                                        startActivity(intent);
                                        //you may call the cancel() method but if it is not handled in doInBackground() method
                                        if (loginTask != null && loginTask.getStatus() != Status.FINISHED)
                                            loginTask.cancel(true);
//                                            Toast.makeText(TOMLoginUserActivity.this,"Successfully Logged In", Toast.LENGTH_LONG).show();
                                        Toast toast = Toast
                                                .makeText(TOMLoginUserActivity.this, "Successfully Logged In", Toast.LENGTH_SHORT);
                                        toast.setGravity(Gravity.CENTER, 0, 0);
                                        toast.show();
                                        progresstextview.setVisibility(View.INVISIBLE);
                                    }
                                });
//                                }
                            } else {
                                // Internet connection is not present
                                // Ask user to connect to Internet
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        alarm.setAlarm(TOMLoginUserActivity.this);
                                        startService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
                                        Intent intent = new Intent(TOMLoginUserActivity.this, HomeMainActivity.class);
                                        // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        String message = username.toString();
                                        intent.putExtra(EXTRA_MESSAGE, message);
                                        startActivity(intent);
                                        //you may call the cancel() method but if it is not handled in doInBackground() method
                                        if (loginTask != null && loginTask.getStatus() != Status.FINISHED)
                                            loginTask.cancel(true);
//                                        Toast.makeText(TOMLoginUserActivity.this,"Successfully Logged In", Toast.LENGTH_LONG).show();
                                        Toast toast = Toast
                                                .makeText(TOMLoginUserActivity.this, "Successfully Logged In", Toast.LENGTH_SHORT);
                                        toast.setGravity(Gravity.CENTER, 0, 0);
                                        toast.show();
                                        progresstextview.setVisibility(View.INVISIBLE);

                                    }
                                });
                            }
                        } else if (dbdel_count > 0) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    alarm.setAlarm(TOMLoginUserActivity.this);
                                    startService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
                                    Intent intent = new Intent(TOMLoginUserActivity.this, HomeMainActivity.class);
                                    //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    String message = username.toString();
                                    intent.putExtra(EXTRA_MESSAGE, message);
                                    startActivity(intent);
                                    //you may call the cancel() method but if it is not handled in doInBackground() method
                                    if (loginTask != null && loginTask.getStatus() != Status.FINISHED)
                                        loginTask.cancel(true);
//                                    Toast.makeText(TOMLoginUserActivity.this,"Successfully Logged In", Toast.LENGTH_LONG).show();
                                    Toast toast = Toast
                                            .makeText(TOMLoginUserActivity.this, "Successfully Logged In", Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();
                                    progresstextview.setVisibility(View.INVISIBLE);
                                }
                            });
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // Put here the line which is changing the UI.
                                progresstextview.setVisibility(View.INVISIBLE);
                                //you may call the cancel() method but if it is not handled in doInBackground() method
                                if (loginTask != null && loginTask.getStatus() != Status.FINISHED)
                                    loginTask.cancel(true);
//                                Toast.makeText(TOMLoginUserActivity.this,"Sorry!! Incorrect Username or Password", Toast.LENGTH_LONG).show();
                                Toast toast = Toast
                                        .makeText(TOMLoginUserActivity.this, "Sorry!! Incorrect Username or Password", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                btnstepin.setEnabled(true);
                                txtUserName.setEnabled(true);
                                txtPassword.setEnabled(true);
//                                btnstepin.setBackgroundColor(getResources().getColor(R.color.opaque_color));
                            }
                        });
                    }

                }
            } catch (SQLiteException e) {

                //***********************CODE TO CREATE DATABASE TABLES on deletion of db**********************/
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progresstextview.setVisibility(View.INVISIBLE);
                        //you may call the cancel() method but if it is not handled in doInBackground() method
                        if (loginTask != null && loginTask.getStatus() != Status.FINISHED)
                            loginTask.cancel(true);
                        Toast.makeText(TOMLoginUserActivity.this, "Please try again !!! ", Toast.LENGTH_LONG).show();
                        btnstepin.setEnabled(true);
                        txtUserName.setEnabled(true);
                        txtPassword.setEnabled(true);
//                        btnstepin.setBackgroundColor(getResources().getColor(R.color.opaque_color));
                        onClickLogOut();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
                if (e.getMessage().toString().equals("unknown error (code 14): Could not open database") || e.getMessage().toString().equals("unable to open database file"))

                {
                    //***********************CODE TO CREATE DATABASE TABLES on deletion of db**********************/
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progresstextview.setVisibility(View.INVISIBLE);
                            //you may call the cancel() method but if it is not handled in doInBackground() method
                            if (loginTask != null && loginTask.getStatus() != Status.FINISHED)
                                loginTask.cancel(true);
                            Toast.makeText(TOMLoginUserActivity.this, "Please try again !!! ", Toast.LENGTH_LONG).show();
                            btnstepin.setEnabled(true);
                            txtUserName.setEnabled(true);
                            txtPassword.setEnabled(true);
//                            btnstepin.setBackgroundColor(getResources().getColor(R.color.opaque_color));
                            onClickLogOut();
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progresstextview.setVisibility(View.INVISIBLE);
                            //you may call the cancel() method but if it is not handled in doInBackground() method
                            if (loginTask != null && loginTask.getStatus() != Status.FINISHED)
                                loginTask.cancel(true);
                            Toast.makeText(TOMLoginUserActivity.this, "Sorry!! Incorrect Username or Password ", Toast.LENGTH_LONG).show();
                            btnstepin.setEnabled(true);
                            txtUserName.setEnabled(true);
                            txtPassword.setEnabled(true);
//                            btnstepin.setBackgroundColor(getResources().getColor(R.color.opaque_color));
                            onClickLogOut();
                        }
                    });
                }
            } catch (UnsatisfiedLinkError err) {
                err.getStackTrace();
                Crashlytics.log(Log.ERROR, TAG, "Error TOMLoginUserActivity UnsatisfiedLinkError ");
                onClickLogOut();
            } finally {
                if (cr_chkdb != null) {
                    cr_chkdb.close();
                }
                if (cr_chkdbusr != null) {
                    cr_chkdbusr.close();
                }
                if (cr_chkdbdel != null) {
                    cr_chkdbdel.close();
                }
                if (cr_chk_email != null) {
                    cr_chk_email.close();
                }
                if (db_del != null) {
                    db_del.close();
                }
                if (db != null) {
                    db.close();
                }
                if (db_btry_usage != null) {
                    db_btry_usage.close();
                }
                if (db_outscan != null) {
                    db_outscan.close();
                }
                if (db_email != null) {
                    db_email.close();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            // NOTE: You can call UI Element here.

        }
    }

    private void initializeUserParser(TokenParser tokenParser) {
        session.setKeyToken(tokenParser.getToken(), username);
        session.setBaseUrl(tokenParser.getBaseurl());
        session.setPickupUrl(tokenParser.getPickup());
        session.setRVPUrl(tokenParser.getRvp());
        session.setEcomUrl(tokenParser.getEcom());
        session.setUserId(String.valueOf(tokenParser.getUserId()),username);
        session.setKeyIMEI(String.valueOf(tokenParser.getIMEI()), username);
        session.setLocId(String.valueOf(tokenParser.getLocID()),username);
        session.setTimezone(tokenParser.getTimezone());
        session.setCityId(String.valueOf(tokenParser.getCityId()),username);
        session.setRegionId(String.valueOf(tokenParser.getRegionId()),username);

    }
    //*********************** THIS FUNCTION IS CALLED WHEN THE BACK BUTTON IS PRESSED**********************/

    //    @Override
    @Override
    public void onBackPressed() {
        // do something on back.
        try {
            showLogoutAlert();
        } catch (Exception e) {
            Crashlytics.log(Log.ERROR, TAG, "Exception onBackPressed " + e.getMessage());
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(Log.ERROR, TAG, "Error TOMLoginUserActivity UnsatisfiedLinkError ");
        }
    }

    @Override
    public void onDestroy() {
        try {
            //you may call the cancel() method but if it is not handled in doInBackground() method
            if (loginTask != null && loginTask.getStatus() != AsyncTask.Status.FINISHED)
                loginTask.cancel(true);
            super.onDestroy();
           /* stopService(new Intent(getApplicationContext(), LocationLoggerService.class));
            stopService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);*/
        } catch (Exception e) {
            Crashlytics.log(Log.ERROR, TAG, "Exception onDestroy " + e.getMessage());
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(Log.ERROR, TAG, "Error TOMLoginUserActivity UnsatisfiedLinkError ");
        }
    }

    public void showLogoutAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("Alert");

        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to quit TOM ?");

        // On pressing Settings button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    // Session Manager
                    session = new SessionManager(getApplicationContext());
                    tiny = new TinyDB(getApplicationContext());
                    SESSION_FLAG_START = tiny.getBoolean("FLAG_START");
                    Crashlytics.log(Log.ERROR, TAG, "SESSION_FLAG_START Onend_trip" + SESSION_FLAG_START);
                    if (SESSION_FLAG_START == true) {

                        strt_cliked = false;
                        tiny.putBoolean("FLAG_START", strt_cliked);
                        tiny.putString("transport_val", "SELECT TRANSPORT");
                        Crashlytics.log(Log.ERROR, TAG, "strt_cliked login " + strt_cliked);
                    } else {
                        strt_cliked = false;
                        tiny.putBoolean("FLAG_START", strt_cliked);
                        tiny.putString("transport_val", "SELECT TRANSPORT");
                        Crashlytics.log(Log.ERROR, TAG, "strt_cliked login else" + strt_cliked);
                    }
                } catch (Exception e) {
                    Crashlytics.log(Log.ERROR, TAG, "Exception login Onend_trip" + e.getMessage());
                }
                stopService(new Intent(getApplicationContext(), LocationLoggerService.class));
                stopService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
                if (session != null) {
                    session.setStartTrip(false);
                }
                if (alarm != null) {
                    alarm.cancelAlarm(TOMLoginUserActivity.this);
                }

//                stopService(new Intent(getApplicationContext(),SampleSchedulingService.class));
                stopService(new Intent(getApplicationContext(), Autosync.class));
                stopService(new Intent(getApplicationContext(), SampleAlarmReceiver.class));
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void logout() {
    }

    private void onClickLogOut() {
        try {
            Intent logOutActivity = new Intent(this, TOMLoginUserActivity.class);
//        logOutActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        logOutActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivity(logOutActivity);
        } catch (Exception e) {
            Crashlytics.log(Log.ERROR, TAG, "Exception onClickLogOut " + e.getMessage());

        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(Log.ERROR, TAG, "Error TOMLoginUserActivity UnsatisfiedLinkError ");

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tomlogin_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}