/*************************************************************/

/**********CREATED BY : ASHWINI NAIK ************************/
/********CREATED DATE : 17/05/2013 **************************/
/***********PURPOSE   : CLASS FILE TO PARSE THE GET_TOM_USER WEBSERVICE VALUES******/

/************************************************************/
package com.traconmobi.tom;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteConstraintException;
//import android.database.sqlite.SQLiteDatabase;

//import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteConstraintException;
import android.provider.BaseColumns;
import android.util.Log;
import android.util.Xml;
import android.util.Base64;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class ParseLoginAuthenticateXmlResponse
{
	//	public static String DB_FULL_PATH = android.os.Environment.getExternalStorageDirectory().toString()+"/TOM.sqlite";
	private static ArrayList<HashMap<String, String>> parsedLoginList;
	public String userid_value=null;
	public String username_value=null;
	public String userpwd_value=null;
	public String user_locValue=null;
	public String user_IMEI_numberValue=null;
	public String user_lastlogin_dtValue=null;
	public String user_statusValue=null;
	public String user_loc_idValue=null;

	public String c_acc_numValue=null;
	public String company_nameValue=null;
	public String c_emailValue=null;
	public String serverdateValue=null;
	public String error_id_value=null;
	public static String error_desc_value=null;

	protected SQLiteDatabase db_insert;
	private StringReader xmlReader;
	Cursor cursor;

	private String SHAHash;
	public static int NO_OPTIONS=0;

	private final String starttagList = "USER";
	private final String error_id="ERROR";
	private final String error_desc="ERROR_DESC";
	private final String userid = "ID";
	private final String user_name = "UserID";
	private final String user_pwd = "Password";
	private final String user_loc = "LocCode";
	private final String user_IMEI_number = "IMEI";
	private final String user_lastlogin_dt = "ServerDateTime";
	//	private final String user_status = "U_Status";
	private final String user_loc_id = "LocID";
	private final String c_acc_no="CompanyID";
	private final String company_name="CompanyName";
	//	private final String cust_email="CEMAIL";
	private final String serverdate="ServerDateTime";
	public static String u_status;

	// Session Manager Class
	SessionManager session;
	public String session_DB_PATH,session_DB_PWD;
	// variable to hold context
	public Context context;
	public String TAG="ParseLoginAuthenticateXmlResponse";
	//save the context received via constructor in a local variable

	public ParseLoginAuthenticateXmlResponse(String xml,Context context)
	{
		try
		{
			xmlReader = new StringReader(xml);
			this.context=context;

			//Intializing Fabric
			Fabric.with(context, new Crashlytics());
			// Session class instance
			session = new SessionManager(context);

			/**GETTING SESSION VALUES**/

			// get AuthenticateDb data from session
			HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

			// DB_PATH
			session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

			// DB_PWD
			session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);

		}
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(Log.ERROR, TAG, "Exception ParseLoginAuthenticateXmlResponse" + e.getMessage());
		}

	}

	public void parse() throws XmlPullParserException, IOException
	{
		try
		{
			TreeMap<String, String> loginListObj = null;
			XmlPullParser parser = Xml.newPullParser();
			parser.setInput(xmlReader);
			int eventType = parser.getEventType();
			loginListObj = new TreeMap<String, String>();
			parsedLoginList = new ArrayList<HashMap<String, String>>();
			while (eventType != XmlPullParser.END_DOCUMENT)
			{
				String xmlNodeName = parser.getName();
				if (XmlPullParser.START_TAG == eventType) {
					xmlNodeName = parser.getName();
				/*if (xmlNodeName.equalsIgnoreCase("ERROR"))
				{*/
					error_desc_value="null";
					if (xmlNodeName.equalsIgnoreCase(error_id))
					{
						error_id_value = parser.nextText().toString();
						loginListObj.put("error_id", error_id_value);
						Crashlytics.log(Log.ERROR, TAG, "error_id_value " + error_id_value);

					} else if (xmlNodeName.equalsIgnoreCase(error_desc))
					{

						error_desc_value = parser.nextText().toString();
						loginListObj.put("error_desc", error_desc_value);
						Crashlytics.log(Log.ERROR, TAG, "error_desc_value " + error_desc_value);
					}
				/*}
				else if (xmlNodeName.equalsIgnoreCase("ID"))
				{*/
					else if (xmlNodeName.equalsIgnoreCase(userid)) {
						userid_value = parser.nextText().toString();
						loginListObj.put("userid", userid_value);
						//				Crashlytics.log(android.util.Log.ERROR,TAG,"userid_value " + userid_value );
					} else if (xmlNodeName.equalsIgnoreCase(user_name)) {
						username_value = parser.nextText().toString();
						loginListObj.put("user_name", username_value);

					} else if (xmlNodeName.equalsIgnoreCase(user_pwd)) {
						userpwd_value = parser.nextText().toString();
						loginListObj.put("user_pwd", userpwd_value);

					} else if (xmlNodeName.equalsIgnoreCase(user_loc)) {
						user_locValue = parser.nextText().toString();
						loginListObj.put("user_loc", user_locValue);

					} else if (xmlNodeName.equalsIgnoreCase(user_IMEI_number)) {
						user_IMEI_numberValue = parser.nextText().toString();
						loginListObj.put("user_IMEI_number", user_IMEI_numberValue);
						Crashlytics.log(Log.ERROR, TAG, "print" + user_IMEI_numberValue);
					} else if (xmlNodeName.equalsIgnoreCase(user_lastlogin_dt)) {
						user_lastlogin_dtValue = parser.nextText().toString();
						loginListObj.put("user_lastlogin_dt", user_lastlogin_dtValue);

					}
					else if (xmlNodeName.equalsIgnoreCase(user_loc_id)) {
						user_loc_idValue = parser.nextText().toString();
						loginListObj.put("user_loc_id", user_loc_idValue);
					}

					else if (xmlNodeName.equalsIgnoreCase(c_acc_no)) {
						c_acc_numValue = parser.nextText();
						loginListObj.put("c_acc_no", c_acc_numValue);
					} else if (xmlNodeName.equalsIgnoreCase(company_name)) {
						company_nameValue = parser.nextText();
						loginListObj.put("company_name", company_nameValue);

					}
					//
					//			else if (xmlNodeName.equalsIgnoreCase(cust_email))
					//			{
					//			c_emailValue = parser.nextText();
					//			loginListObj.put("cust_email", c_emailValue);
					//
					//			}

					else if (xmlNodeName.equalsIgnoreCase(serverdate)) {
						serverdateValue = parser.nextText();
						//			serverdateValue=serverdateValue.substring(0, 10);
						loginListObj.put("serverdateValue", serverdateValue);
						//			serverdateValue.substring(0, 10);
					}
//				}

				} else if (XmlPullParser.END_TAG == eventType)
				{
					if (xmlNodeName.equalsIgnoreCase(starttagList))
					{

						parsedLoginList.add(new HashMap<String, String>(

								loginListObj));

						for (Entry<String, String> entry : loginListObj.entrySet())
						{
//			    String u_id =loginListObj.get("userid");
//			    String u_name=loginListObj.get("user_name");		
//			    String u_pwd=loginListObj.get("user_pwd");
//			    String u_loc=loginListObj.get("user_loc");			
//			    String u_IMEI_num=loginListObj.get("user_IMEI_number");			
//			    String u_lastlogin_dt=loginListObj.get("user_lastlogin_dt");
//			    String Cust_acc_num=loginListObj.get("c_acc_no");
//			    String Cust_name=loginListObj.get("cust_name");
//			    String Cust_email=loginListObj.get("cust_email");
//			    String usr_last_dt=u_lastlogin_dt.substring(0, 10);
//			    String user_tme=u_lastlogin_dt.substring(11, 19);
//			    u_status=loginListObj.get("user_status");			    
//				String u_id_loc=loginListObj.get("user_id_loc");	

							String u_id =loginListObj.get("userid");
							String u_name=loginListObj.get("user_name");
							String u_pwd=loginListObj.get("user_pwd");
							String u_loc=loginListObj.get("user_loc");
							String u_IMEI_num=loginListObj.get("user_IMEI_number");
							String u_lastlogin_dt=loginListObj.get("user_lastlogin_dt");
							String u_loc_id=loginListObj.get("user_loc_id");
//			    Crashlytics.log(android.util.Log.ERROR,TAG,"u_lastlogin_dt " + u_lastlogin_dt );
							String Cust_acc_num=loginListObj.get("c_acc_no");
							String Compny_name=loginListObj.get("company_name");
//			    String Cust_email=loginListObj.get("cust_email");
							String usr_last_dt=u_lastlogin_dt.substring(0, 10);
//			    Crashlytics.log(android.util.Log.ERROR,TAG,"usr_last_dt " + usr_last_dt);
							String user_tme=u_lastlogin_dt.substring(11, 19);

//			    String servr_dt =loginListObj.get("serverdate");
//			    String servr_last_dt=servr_dt.substring(0, 10);

//			    ContentValues cv =new ContentValues();			
//			    cv.put("U_ID",u_id);			
//			    cv.put("username",u_name);	
//			    cv.put("password",u_pwd);
//			    cv.put("Loc_cd",u_loc);	
//			    cv.put("User_last_login", usr_last_dt);
//			    cv.put("User_last_login_time",user_tme);
//			    cv.put("IMEI",u_IMEI_num);
//			    cv.put("CUST_ACC_NO",Cust_acc_num);
////			    cv.put("CNAME",Cust_name);
////			    cv.put("CEMAIL",Cust_email);
//			    cv.put("ServerDate",serverdateValue);

							//call method to compute SHA hash
//			    String pswd="sample";
//                computeSHAHash(pswd);

							ContentValues cv =new ContentValues();
							cv.put("T_U_ID",u_id);
							cv.put("T_username",u_name);
							cv.put("T_password",u_pwd);
							cv.put("T_Loc_Cd",u_loc);
							cv.put("D_User_last_login", usr_last_dt);
							cv.put("D_User_last_login_time",user_tme);
							cv.put("T_IMEI",u_IMEI_num);
							cv.put("T_Cust_Acc_No",Cust_acc_num);
							cv.put("T_CompanyName",Compny_name);
							cv.put("T_Loc_Id",u_loc_id);
							cv.put("D_ServerDate",usr_last_dt);
							Crashlytics.log(Log.ERROR, TAG, "print serverdateValue" + serverdateValue);
							try{

								if(session_DB_PATH != null && session_DB_PWD != null)
								{
//				    	db_insert=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
									db_insert=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
									db_insert.beginTransaction();
									db_insert.insertWithOnConflict("TOM_Users", BaseColumns._ID, cv, SQLiteDatabase.CONFLICT_REPLACE);
									db_insert.setTransactionSuccessful();
									db_insert.endTransaction();
									db_insert.close();
								}
								else
								{
									Crashlytics.log(Log.ERROR, TAG, "Error Login parser session_DB_PATH is null ");
								}
							}
							catch(SQLiteConstraintException ex){

								//what ever you want to do
								Crashlytics.log(Log.ERROR, TAG, "Exception session_DB_PATH " + ex.getMessage());
							}

							/***For Loop close****/
						}
						loginListObj.clear();

					}

				}
				eventType = parser.next();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Crashlytics.log(Log.ERROR, TAG, "Exception Treemap " + e.getMessage());
		}
		finally
		{
//		  db_insert.endTransaction();
			// This block contains statements that are ALWAYS executed
			// after leaving the try clause, regardless of whether we leave it:
			// 1) normally after reaching the bottom of the block;
			// 2) because of a break, continue, or return statement;
			// 3) with an exception handled by a catch clause above; or
			// 4) with an uncaught exception that has not been handled.
			// If the try clause calls System.exit(), however, the interpreter
			// exits before the finally clause can be run.
//			  if(cursor!=null)
//			  {
//				  cursor.close();
//			  }
//	    	if(db !=null)
//	    	{
//	    		db.close();
//	    	}


		}
	}

	@SuppressWarnings("unused")
	private String readText(XmlPullParser parser) throws IOException,XmlPullParserException
	{

		String result = "";

		try
		{
			if (parser.next() == XmlPullParser.TEXT)
			{

				result = parser.getText();

				Log.d("wat is the result",result);

				parser.nextTag();

			}
		}
		catch(Exception e)
		{
			Crashlytics.log(Log.ERROR, TAG, "Exception readText" + e.getMessage());
		}
		return result;

	}


	private static String convertToHex(byte[] data) {
		StringBuilder buf = new StringBuilder();
		for (byte b : data) {
			int halfbyte = (b >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
				halfbyte = b & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	public static String computeSHAHash(String text) {
		MessageDigest md;
		byte[] sha1hash = null;
		try {
			md = MessageDigest.getInstance("SHA-1");
			md.update(text.getBytes("iso-8859-1"), 0, text.length());
			sha1hash  = md.digest();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return convertToHex(sha1hash);
	}

/*private static String convertToHex(byte[] data) throws java.io.IOException 
{
        
        
       StringBuffer sb = new StringBuffer();
       String hex=null;
        
       hex=Base64.encodeToString(data, 0, data.length, NO_OPTIONS);
        
       sb.append(hex);
                    
       return sb.toString();
   }


public void computeSHAHash(String password)
 {
     MessageDigest mdSha1 = null;
       try
       {
         mdSha1 = MessageDigest.getInstance("SHA-1");
       } catch (NoSuchAlgorithmException e1) {
         Crashlytics.log(android.util.Log.ERROR,"myapp", "Error initializing SHA1 message digest");
       }
       try {
           mdSha1.update(password.getBytes("ASCII"));
       } catch (UnsupportedEncodingException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
       }
       byte[] data = mdSha1.digest();
       try {
           SHAHash=convertToHex(data);
       } catch (IOException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
       }
        
       Crashlytics.log(android.util.Log.ERROR,TAG,"SHA-1 hash generated is: " + " " + SHAHash);
//       result.setText("SHA-1 hash generated is: " + " " + SHAHash);
   }*/

	public ArrayList getparsedOutscanList() {

		Crashlytics.log(Log.ERROR, TAG, "print getparsedOutscanList" + parsedLoginList);
		return parsedLoginList;

	}

}
