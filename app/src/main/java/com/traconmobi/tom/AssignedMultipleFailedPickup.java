package com.traconmobi.tom;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import org.kobjects.base64.Base64;

import com.crashlytics.android.Crashlytics;



//import net.sqlcipher.database.SQLiteConstraintException;
//import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteException;

import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import io.fabric.sdk.android.Fabric;

public class AssignedMultipleFailedPickup extends Activity implements OnClickListener {

    // The minimum distance to change Updates in meters
//    private static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = (float) 11.1 ; // 10 meters
    private static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    //    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * randBetween(0, 5) ;
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    Location location; // location
    public String FORM_TYPE_VAL;
    CouchBaseDBHelper dbHelper;
    String docId;
    public String selectedItemTag, rsn_desc,sign1, rsn_id, reason_code, reason_id, consgnee, get_assgn_id, get_lat, get_lng, SESSION_TRANSPORT, new_lat, new_lon, session_COD_AMT, session_AWB_COUNT, session_RESPONSE_AWB_NUM, result, entityString_gps, imei_num;
    TextView awb_total_cnt, cod_total;
    SQLiteDatabase db, db_bkr;
    String Remarks, showdate, showtime, st, reasontext;
    private Button cancelButton, saveButton;
    int mMonth, mYear, mDay, hour, minute;
    static final String EXTRA_MESSAGE = null;
    public static String employeeId, aa, photoname = null;
    public static int shw_cnt;
    String PageName, PageName1, count_del_sync, count_del;
    Bundle bundle;
    TextView version_name, progresstextview;
    Cursor cursor, c, c_routesyn, c_chk_custmr, cr;
    String currentTime, photo_dt, photo_tme;
    public static final String PREF_NAME1 = "Pager";

    LocationManager locationManager;
    double latitude, longitude, net_lat, net_lon;
    protected static final String MULTIPLE_UNDEL_ASSGN_ID = null;
    protected static final String FORM_TYPE = null;

    private boolean clicked = false; // this is a member variable
    static File image;
    File fileimage = null;
    final int TAKE_PHOTO_CODE = 1;
    private static final int SIGNATURE_ACTIVITY = 0;
    protected Button imageview;
    String assignId;
    TableLayout tl;
    int clickcount = 0;
    int j = 1;
    View w, itemView;

    byte[] BAvalue;
    public static String getphotobytearray, s_photo;
    static ImageView iv;
    static Bitmap theImage;
    private String selectedImagePath;

    static AutoCompleteTextView et_reason;
    PopupWindow popupWindow;
    String popUpContents[];
    PopupWindow popupWindowCustmrs;
    TextView title;
    StringBuffer responseText_photonme;
    int count = 0;
    List<String> cust_labels;
    int var;
    EditText remarks;
    // Session Manager Class
    SessionManager session;
    String reasonId;
    HashMap<String, String> reasonStore = new HashMap<String, String>();
    TinyDB tiny;
    public String session_user_id, session_user_pwd, session_USER_LOC, session_USER_NUMERIC_ID, session_CURRENT_DT, session_CUST_ACC_CODE, session_USER_NAME, session_DB_PATH, session_DB_PWD;
    public static String TAG = "AssignedMultipleFailedPickup";
    int count_custmr;


    public static int randBetween(int start, int end) {
        return start + (int) Math.round(Math.random() * (end - start));
    }

    public double roundToDecimals(double d, int c) {
        int temp = (int) (d * Math.pow(10, c));
        return ((double) temp) / Math.pow(10, c);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            //Intializing Fabric
            Fabric.with(this, new Crashlytics());
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.activity_rvppickup_failed_update);
            getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, R.layout.activity_rvppickup_failed_update);

            title = (TextView) findViewById(R.id.txt_title);
            title.setText("FAILED PICKUP");


        /* Use the LocationManager class to obtain GPS locations */

            try {
                dbHelper = new CouchBaseDBHelper(getApplicationContext());
                LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                LocationListener mlocListener = new MyLocationListener();
                Crashlytics.log(android.util.Log.ERROR, TAG, "mlocListener " + mlocListener + mlocManager);
                if (location == null) {
//           locationManager.requestLocationUpdates(
//                   LocationManager.GPS_PROVIDER,
//                   MIN_TIME_BW_UPDATES,
//                   MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, mlocListener);


                    if (mlocManager != null) {
                        location = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        new_lat = String.valueOf(roundToDecimals(location.getLatitude(), 4));
                        new_lon = String.valueOf(roundToDecimals(location.getLongitude(), 4));
                        if (new_lat == null) {
                            Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lat null");
                            new_lat = "null";
                        } else {
                            Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lat not null ");
                        }
                        if (new_lon == null) {
                            Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lon null");
                            new_lon = "null";
                        } else {
                            Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lon not null ");
                        }

                        if (location != null) {
                            new_lat = String.valueOf(roundToDecimals(location.getLatitude(), 4));
                            new_lon = String.valueOf(roundToDecimals(location.getLongitude(), 4));
                            if (new_lat == null) {
                                Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lat null");
                                new_lat = "null";
                            } else {
                                Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lat not null ");
                            }
                            if (new_lon == null) {
                                Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lon null");
                                new_lon = "null";
                            } else {
                                Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lon not null ");
                            }

                        } else {
                            Crashlytics.log(android.util.Log.ERROR, TAG, "location2 not null ");
                        }
                    } else {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "mlocManager not null ");
                    }

                } else {
                    Crashlytics.log(android.util.Log.ERROR, TAG, "location1 not null ");
                }

            } catch (Exception e) {
                e.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception TOMCODUpdateActivity onCreate LocationManager " + e.getMessage() + e.getStackTrace().toString());
            }

            // Session class instance
            session = new SessionManager(getApplicationContext());
            tiny = new TinyDB(getApplicationContext());
            // get AuthenticateDb data from session
            HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

            // DB_PATH
            session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

            // DB_PWD
            session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);


            // get AssgnMultiple_num_Dts data from session
            HashMap<String, String> AssgnMultiple_num_Dts = session.getAssgnMultiple_num_Details();

            // DB_PATH
            session_RESPONSE_AWB_NUM = AssgnMultiple_num_Dts.get(SessionManager.KEY_RESPONSE_AWB_NUM);
            Crashlytics.log(android.util.Log.ERROR, TAG, "print undel session_RESPONSE_AWB_NUM" + session_RESPONSE_AWB_NUM);

            // get AuthenticateDb data from session
            HashMap<String, String> AssgnMultiple_Dts = session.getAssgnMultipleDetails();

            // DB_PATH
            session_AWB_COUNT = AssgnMultiple_Dts.get(SessionManager.KEY_AWB_COUNT);

            // DB_PWD
            session_COD_AMT = AssgnMultiple_Dts.get(SessionManager.KEY_COD_AMT);
            Intent intent = getIntent();
            assignId = intent.getStringExtra("assignId");
            FORM_TYPE_VAL = intent.getStringExtra("NAV_FORM_TYPE_VAL");

//        awb_total_cnt =(TextView)findViewById(R.id.assgncnt);
//        awb_total_cnt.setText("Assignments : " + session_AWB_COUNT);
//
            Crashlytics.log(android.util.Log.ERROR, TAG, "print + session_RESPONSE_AWB_NUM " + session_RESPONSE_AWB_NUM);

            responseText_photonme = new StringBuffer();

            String shw[] = session_RESPONSE_AWB_NUM.split(",");
            for (int i = 0; i < shw.length; i++) {
                result = shw[i];
                result = result.replace("/", "");
                result = result.replace(" ", "");
                result = result.replace("-", "");
                result = result.replace(":", "");
            }

            // get user data from session
            HashMap<String, String> login_Dts = session.getLoginDetails();

            // Userid
            session_user_id = login_Dts.get(SessionManager.KEY_UID);

            // pwd
            session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);


            // get user data from session
            HashMap<String, String> user = session.getUserDetails();

            // session_USER_LOC
            session_USER_LOC = user.get(SessionManager.KEY_USER_LOC);

            // session_USER_NUMERIC_ID
            session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);

            // session_CURRENT_DT
            session_CURRENT_DT = user.get(SessionManager.KEY_CURRENT_DT);

            // session_CUST_ACC_CODE
            session_CUST_ACC_CODE = user.get(SessionManager.KEY_CUST_ACC_CODE);

            // session_USER_NAME
            session_USER_NAME = user.get(SessionManager.KEY_USER_NAME);

            SESSION_TRANSPORT = tiny.getString("transport_val");
            Crashlytics.log(android.util.Log.ERROR, TAG, "multiple SESSION_TRANSPORT " + SESSION_TRANSPORT);


            imageview = (Button) this.findViewById(R.id.picture);
            imageview.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clicked) {
                        takePhoto();

//                    Toast.makeText(AssignedMultipleSuccessPickup.this, "You already clicked!", Toast.LENGTH_SHORT).show();
                    } else {
                        takePhoto();
//                    Toast.makeText(AssignedMultipleSuccessPickup.this, "You clicked for the first time!", Toast.LENGTH_SHORT).show();
                    }

                    clicked = true;
//        		takePhoto();

                }
            });

            cancelButton = (Button) findViewById(R.id.btncancel);
            cancelButton.setOnClickListener(this);
            saveButton = (Button) findViewById(R.id.btnsave);
            saveButton.setOnClickListener(this);

            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
            Date currentLocalTime = cal.getTime();
            DateFormat date1 = new SimpleDateFormat("HH:mm:ss z");
            date1.setTimeZone(TimeZone.getTimeZone("GMT"));
            String localTime = date1.format(currentLocalTime);
            Crashlytics.log(android.util.Log.ERROR, TAG, "undellocaltime  " + localTime);

//        Spinner s = (Spinner) findViewById(R.id.spinnerUD);
            remarks = (EditText) findViewById(R.id.txtremarks);
            et_reason = (AutoCompleteTextView) findViewById(R.id.edt_reason);


/********************$$$$$$$$$$$$$$$$$$$$*************/
            cust_labels = new ArrayList<String>();
            // show the list view as dropdown
            Crashlytics.log(android.util.Log.ERROR, TAG, "print val" + et_reason.getText().toString().trim().toUpperCase());
            db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
//			    cust_labels.add("SELECT");

            String flg = "N";
            //
            Cursor cr = db.rawQuery("select T_UD_Reason_Desc,T_UD_Reason_Id,_id from UD_REASON_MASTER where T_UD_Reason_Desc LIKE '%" + et_reason.getText().toString().trim() + "%' and T_Cust_Acc_NO='" + session_CUST_ACC_CODE + "' group by T_UD_Reason_Desc", null);
//			    Cursor cr=db.rawQuery("select T_CUST_NAME,T_ASSIGN_ID,_id from Cust_Locate_MASTER where FISGPS='" + flg + "' and T_Cust_Acc_NO='" + session_CUST_ACC_CODE + "' and T_CUST_NAME LIKE ? group by T_CUST_NAME", null);
            String qry = cr.toString();
            Crashlytics.log(android.util.Log.ERROR, TAG, "qry" + qry);
            //ORDER BY T_Hold_Reason_Code DESC
            while (cr.moveToNext()) {
                reason_code = cr.getString(cr.getColumnIndex("T_UD_Reason_Desc"));
                reason_id = cr.getString(cr.getColumnIndex("T_UD_Reason_Id"));
                Log.e(TAG, "Reason id" + reason_id);
//				cust_name=cr.getString(cr.getColumnIndex("T_UD_Reason_Desc"));
//				locte_assgn_id=cr.getString(cr.getColumnIndex("T_ASSIGN_ID"));
                reasonStore.put(reason_code.toString().trim(), reason_id);
                Crashlytics.log(android.util.Log.ERROR, TAG, "reason_code " + reason_code);
//			    	cust_labels.add(cust_name+"::"+locte_assgn_id);
                cust_labels.add(reason_code);

            }
            int count = cr.getCount();
            Crashlytics.log(android.util.Log.ERROR, TAG, "print count" + count);
            db.close();

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, cust_labels);

            et_reason.setAdapter(adapter);
            et_reason.setThreshold(1);

            et_reason.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
//					reasonText[0] = " ";
                    if (cust_labels.contains(et_reason.getText().toString().trim())) {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "in list");
                    } else {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Not in list");
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception AssignedMultipleFailedPickup onCreate " + e.getMessage());
            //onClickGoToHomePage();
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleFailedPickup UnsatisfiedLinkError ");
            //onClickGoToHomePage();
//			finish();
        }
    }

    public void OnBtnClear(View v) {
        et_reason.setText("");

    }

    public void shw_list() {
        try {
            et_reason.setFocusable(true);
            et_reason.requestFocus();
            et_reason.setEnabled(true);
            et_reason.setFocusableInTouchMode(true);
            List<String> cust_labels = new ArrayList<String>();
            // show the list view as dropdown
            Crashlytics.log(android.util.Log.ERROR, TAG, "print val" + et_reason.getText().toString().trim().toUpperCase());
            db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
//    cust_labels.add("SELECT");

            String flg = "N";
//
            Cursor cr = db.rawQuery("select T_UD_Reason_Desc,T_UD_Reason_Id,_id from UD_REASON_MASTER where T_UD_Reason_Desc LIKE '%" + et_reason.getText().toString().trim().toUpperCase() + "%' group by T_UD_Reason_Desc", null);
//    Cursor cr=db.rawQuery("select T_CUST_NAME,T_ASSIGN_ID,_id from Cust_Locate_MASTER where FISGPS='" + flg + "' and T_Cust_Acc_NO='" + session_CUST_ACC_CODE + "' and T_CUST_NAME LIKE ? group by T_CUST_NAME", null);
            String qry = cr.toString();
            Crashlytics.log(android.util.Log.ERROR, TAG, "qry" + qry);
            //ORDER BY T_Hold_Reason_Code DESC
            while (cr.moveToNext()) {
                rsn_desc = cr.getString(cr.getColumnIndex("T_UD_Reason_Desc"));
                rsn_id = cr.getString(cr.getColumnIndex("T_UD_Reason_Id"));

                Crashlytics.log(android.util.Log.ERROR, TAG, "rsn_desc " + rsn_desc + rsn_id);
                cust_labels.add(rsn_desc + "::" + rsn_id);

            }
            db.close();


            // convert to simple array
            popUpContents = new String[cust_labels.size()];
            cust_labels.toArray(popUpContents);

            // initialize pop up window
            popupWindowCustmrs = popupWindowCustmrs();

//case R.id.edt_custname:
            popupWindowCustmrs.showAsDropDown(et_reason);
            popupWindowCustmrs.showAtLocation(et_reason, Gravity.BOTTOM, 50, 50);
// popupWindowCustmrs.setFocusable(false);
// popupWindowCustmrs.update();
// popupWindow.setOutsideTouchable(true);
            Crashlytics.log(android.util.Log.ERROR, TAG, "Gravity.BOTTOM,50, 50");

//nm.setFocusable(true);
//	nm.requestFocus();
//	nm.setEnabled(true);
//	nm.setFocusableInTouchMode(true);
//   break;
// nm.setFocusable(true);
//nm.requestFocus();
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception customer popup" + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
//			 nm.setFocusable(true);
//	       		nm.requestFocus();
//	       		nm.setEnabled(true);
//	       		nm.setFocusableInTouchMode(true);
            Crashlytics.log(android.util.Log.ERROR, TAG, "finally block");
        }
    }


    public PopupWindow popupWindowCustmrs() {

        // initialize a pop up window type
        popupWindow = new PopupWindow(this);

        // the drop down list is a list view
        ListView listViewCustmrs = new ListView(this);

        // set our adapter and pass our pop up window contents
        listViewCustmrs.setAdapter(CustmrsAdapter(popUpContents));
        Crashlytics.log(android.util.Log.ERROR, TAG, "list entry 1 ");
//        nm.setFocusable(true);
//     		nm.requestFocus();
//     		nm.setEnabled(true);
//     		nm.setFocusableInTouchMode(true);

        Crashlytics.log(android.util.Log.ERROR, TAG, "list entry 2 ");

        // set the item click listener
        listViewCustmrs.setOnItemClickListener(new CustmrsDropdownOnItemClickListener());

        Crashlytics.log(android.util.Log.ERROR, TAG, "list entry 3 ");
        // some other visual settings

        popupWindow.setFocusable(true);
//  commented       popupWindow.setOutsideTouchable(false);
//   commented
//  commented     //The soft keyboard does not block popupwindow
//   commented      popupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);


//        popupWindow.setFocusable(false);
//        listViewCustmrs.setItemsCanFocus(true);
        popupWindow.setWidth(400);
        popupWindow.setHeight(LayoutParams.WRAP_CONTENT);
        Crashlytics.log(android.util.Log.ERROR, TAG, "list entry 4 ");
//        commented   listViewCustmrs.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
//        nm.setFocusable(true);
// 		nm.requestFocus();
// 		nm.setEnabled(true);
// 		nm.setFocusableInTouchMode(true);
        // set the list view as pop up window content
        popupWindow.setContentView(listViewCustmrs);
        //Close the event menu
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                Crashlytics.log(android.util.Log.ERROR, TAG, "list entry 5.5 ");
//      				nm.requestFocus();
                et_reason.setFocusable(true);
                et_reason.requestFocus();
                et_reason.setEnabled(true);

//          	 		nm.setFocusableInTouchMode(true);
//      				InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                // only will trigger it if no physical keyboard is open
//      			inputMethodManager.showSoftInput(nm,InputMethodManager.SHOW_IMPLICIT);
                if (inputMethodManager != null) {
                    Crashlytics.log(android.util.Log.ERROR, TAG, "list entry 5.5 if loop");
                    inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                }

            }
        });
        //Monitoring the touch event
        popupWindow.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                Crashlytics.log(android.util.Log.ERROR, TAG, "list entry 5 ");
                return false;
            }

//				public boolean onTouch1(View v, MotionEvent event) {
//					// TODO Auto-generated method stub
//					return false;
//				}
        });
//        listViewCustmrs.setFocusable(true);
//        listViewCustmrs.requestFocus();
//        listViewCustmrs.setEnabled(true);
//        listViewCustmrs.setFocusableInTouchMode(true);

//        nm.setFocusable(true);
// 		nm.requestFocus();
// 		nm.setEnabled(true);
// 		nm.setFocusableInTouchMode(true);

        return popupWindow;
    }


    public static void showSoftKeyboard(Activity activity, View focusedView) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(et_reason, InputMethodManager.SHOW_IMPLICIT);
    }

    /*
     * adapter where the list values will be set
     */
    private ArrayAdapter<String> CustmrsAdapter(String CustmrsArray[]) {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, CustmrsArray) {


            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                // setting the ID and text for every items in the list
                String item = getItem(position);
                String[] itemArr = item.split("::");
//                String[] itemArr = item;
                String text = itemArr[0];
//                String text=item;
                String id = itemArr[1];

                // visual settings for the list item
                TextView listItem = new TextView(AssignedMultipleFailedPickup.this);
                Crashlytics.log(android.util.Log.ERROR, TAG, "list entry 6 ");
                listItem.setText(text);
                listItem.setTag(id);
                listItem.setTextSize(12);
                listItem.setPadding(10, 10, 10, 10);
                listItem.setTextColor(Color.WHITE);
//                listItem.setFocusable(true);
                Crashlytics.log(android.util.Log.ERROR, TAG, "list entry 7 ");
                popupWindow.setFocusable(false);
                et_reason.setFocusable(true);

                et_reason.requestFocus();
                et_reason.setEnabled(true);
                et_reason.setFocusableInTouchMode(true);
                return listItem;
            }
        };

        Crashlytics.log(android.util.Log.ERROR, TAG, "list entry 8 ");
//     		nm.setFocusable(true);
        et_reason.requestFocus();
        et_reason.setEnabled(true);
        et_reason.setFocusableInTouchMode(true);
        return adapter;

    }


//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.customer_locate_main, menu);
//		return true;
//	}

    public class CustmrsDropdownOnItemClickListener implements AdapterView.OnItemClickListener {

        String TAG = "CustmrsDropdownOnItemClickListener.java";

        @Override
        public void onItemClick(AdapterView<?> arg0, View v, int arg2, long arg3) {


            // get the context and main activity to access variables
            Context mContext = v.getContext();
            AssignedMultipleFailedPickup undelActivity = ((AssignedMultipleFailedPickup) mContext);

            // add some animation when a list item was clicked
            Animation fadeInAnimation = AnimationUtils.loadAnimation(v.getContext(), android.R.anim.fade_in);
            fadeInAnimation.setDuration(10);
            v.startAnimation(fadeInAnimation);

            // dismiss the pop up
            undelActivity.popupWindowCustmrs.dismiss();

            // get the text and set it as the button text
            String selectedItemText = ((TextView) v).getText().toString();
            AssignedMultipleFailedPickup.et_reason.setText(selectedItemText);

            // get the id
            selectedItemTag = ((TextView) v).getText().toString();
            et_reason.setFocusable(true);
            et_reason.requestFocus();
            et_reason.setEnabled(true);
            et_reason.setFocusableInTouchMode(true);
            Crashlytics.log(android.util.Log.ERROR, TAG, "Custmr ID  selectedItemTag is " + selectedItemTag);
//    	        Toast.makeText(mContext, "Custmr ID is: " + selectedItemTag, Toast.LENGTH_SHORT).show();

        }
    }

    public class MyLocationListener implements LocationListener

    {

        @Override

        public void onLocationChanged(Location loc)

        {

            new_lat = String.valueOf(roundToDecimals(loc.getLatitude(), 4));

            new_lon = String.valueOf(roundToDecimals(loc.getLongitude(), 4));

            if (new_lat == null) {
                Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lat null");
                new_lat = "null";
            } else {
                Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lat not null ");
            }
            if (new_lon == null) {
                Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lon null");
                new_lon = "null";
            } else {
                Crashlytics.log(android.util.Log.ERROR, TAG, "return new_lon not null ");
            }


        }

        @Override

        public void onProviderDisabled(String provider)

        {

        }

        @Override

        public void onProviderEnabled(String provider)

        {

//   	Toast.makeText( getApplicationContext(),"Gps Enabled",Toast.LENGTH_SHORT).show();

        }

        @Override

        public void onStatusChanged(String provider, int status, Bundle extras)

        {

        }

    }/* End of Class MyLocationListener */


    /**
     * Listener Implementation of Spinner For Selecting Room
     */

    public static class Listener_Of_Selecting_Room_Spinner implements OnItemSelectedListener {
        static String RoomType;

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            try {
                /** By using this you can get the position of item which you
                 have selected from the dropdown     **/
                RoomType = (parent.getItemAtPosition(pos)).toString();
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
            } catch (Exception e) {
                Crashlytics.log(android.util.Log.ERROR, TAG, "Listener_Of_Selecting_Room_Spinner" + e.getMessage());
//				AssignedMultipleFailedPickup hmepge = new AssignedMultipleFailedPickup();
//	    		hmepge.onClickGoToHomePage();
            } catch (UnsatisfiedLinkError err) {
                err.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Listener_Of_Selecting_Room_Spinner" + err.getMessage());
//    		    AssignedMultipleFailedPickup hmepge = new AssignedMultipleFailedPickup();
//    		    hmepge.onClickGoToHomePage();
//    			finish();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing.
        }

    }


    /**
     * FUNCTION TO CAPTURE IMAGE/PHOTO*
     */

    private void takePhoto() {
        try {
            new SoapAccessTask().execute();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleDelActivity takePhoto ");

        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleDelActivity takePhoto UnsatisfiedLinkError ");

        }
    }


    private File getFile(Context context) {
        try {
//    		delete_older();
//	       final File path = new File( Environment.getExternalStorageDirectory(), "Image keeper" );
            final File path = new File(Environment.getExternalStorageDirectory()
                    + "/Android/data/com.traconmobi.net/", "Image keeper");
            photoname = null;
            if (!path.exists()) {
                path.mkdir();
            }
            try {
//	    	   File sdcard = Environment.getExternalStorageDirectory();
                File sdcard = new File(Environment.getExternalStorageDirectory()
                        + "/Android/data/com.traconmobi.net");
                File mymirFolder = new File(sdcard.getAbsolutePath() + "/Image keeper/");
                String shw[] = session_RESPONSE_AWB_NUM.split(",");
                for (int i = 0; i < shw.length; i++) {
                    result = shw[i];

                    result = result.replace("/", "");
                    result = result.replace(" ", "");
                    result = result.replace("-", "");
                    result = result.replace(":", "");
                    photoname = result + ".png";
                    Log.e(TAG, "print multi photoname" + photoname);
//			       photoname.replace("/", "");
                    fileimage = new File(path, photoname);
                }
                if (!mymirFolder.exists()) {
                    File noMedia = new File(mymirFolder.getAbsolutePath() + "/.nomedia");
                    noMedia.mkdirs();
                    noMedia.createNewFile();
                }
            } catch (IOException e) {
                Log.e(TAG, "Exception getFile  SQLiteException" + e.getMessage());
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "getFile " + e.getMessage());
            //onClickGoToHomePage();
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "getFile UnsatisfiedLinkError" + err.getMessage());
            //onClickGoToHomePage();
            //        	    			finish();
        }
        return fileimage;


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
//    	super.onActivityResult(requestCode, resultCode, data);
            switch (requestCode) {
                //********************************Activity Result for Photo/Image **************************//
                case TAKE_PHOTO_CODE:

                    if (requestCode == TAKE_PHOTO_CODE) {
                        if (resultCode == RESULT_OK) {

                            try {
//		        	        	 String shw[] = session_RESPONSE_AWB_NUM.split(",");
//		        			        for(int i = 0 ; i< shw.length ; i++)
//		        			        {
//		        			         result = shw[i];
//		        			        }
                                final Calendar c = Calendar.getInstance();
                                String month = c.get(Calendar.MONTH) + 1 + "";
                                if (month.length() < 2) {
                                    month = "0" + month;
                                }
                                String date = c.get(Calendar.DAY_OF_MONTH) + "";
                                if (date.length() < 2) {
                                    date = "0" + date;
                                }
                                mYear = c.get(Calendar.YEAR);
                                mMonth = c.get(Calendar.MONTH);
                                mDay = c.get(Calendar.DAY_OF_MONTH);
                                hour = c.get(Calendar.HOUR_OF_DAY);
                                minute = c.get(Calendar.MINUTE);
                                photo_dt = "" + c.get(Calendar.YEAR) + "-" + month + "-" +
                                        date;
                                showtime = " " +
                                        c.get(Calendar.HOUR) + ":" +
                                        c.get(Calendar.MINUTE) + ":" +
                                        c.get(Calendar.SECOND);

                                photo_tme = (c.get(Calendar.HOUR_OF_DAY)) + ":" +
                                        (c.get(Calendar.MINUTE)) + ":" +
                                        (c.get(Calendar.SECOND));
                                Log.w("TIME:", String.valueOf(photo_tme) + currentTime);
                                if (session_DB_PATH != null && session_DB_PWD != null) {
//		        	            	db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                    db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                    String shw[] = session_RESPONSE_AWB_NUM.split(",");
                                    for (int i = 0; i < shw.length; i++) {
                                        result = shw[i];

                                        result = result.replace("/", "");
                                        result = result.replace(" ", "");
                                        result = result.replace("-", "");
                                        result = result.replace(":", "");
                                        String strFilter = "T_Assignment_Number='" + result + "' ";
//		        	            	String strFilter = "T_Assignment_Number=" + result;.

                                        ContentValues cv = new ContentValues();
                                        photoname = result + ".png";
                                        cv.put("T_Photo", photoname);
                                        db.update("TOM_Assignments", cv, strFilter, null);

                                    }
                                    db.close();
                                } else {
                                    Log.e(TAG, "session_DB_PATH is null ");
                                    //onClickGoToHomePage();
                                }

                                if (photoname != null && photoname != "" && !(photoname.equals("null"))) {
//		    				    image = new File(Environment.getExternalStorageDirectory(), "Image keeper/" +photo_path.trim());
//		    				    selectedImagePath =android.os.Environment.getExternalStorageDirectory().toString()+"/Image keeper/"+ photo_path.trim();
                                    image = new File(Environment.getExternalStorageDirectory()
                                            + "/Android/data/com.traconmobi.net/", "Image keeper/" + photoname.trim());
                                    selectedImagePath = Environment.getExternalStorageDirectory()
                                            + "/Android/data/com.traconmobi.net" + "/Image keeper/" + photoname.trim();
                                    Bitmap bm = reduceImageSize(selectedImagePath);

                                    if (bm != null) {

                                        ByteArrayOutputStream BAO = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 40, BAO);

                                        BAvalue = BAO.toByteArray();
                                        getphotobytearray = Base64.encode(BAvalue);
                                        Log.e(TAG, "Photo Byte array value:" + getphotobytearray);
                                        s_photo = getphotobytearray.replace("+", "%2B");
//		    				    Log.e(TAG,"s_photo " + s_photo);
                                    } else if (bm == null) {
                                        getphotobytearray = "null";
                                    }
                                } else if (photoname == null && photoname == "" && photoname.equals("null")) {
                                    getphotobytearray = "null";
                                }


                                ByteArrayOutputStream bao = new ByteArrayOutputStream();

                                byte[] ba2 = Base64.decode(getphotobytearray);

                                ByteArrayInputStream imageStream_sign = new ByteArrayInputStream(ba2);
                                theImage = BitmapFactory.decodeStream(imageStream_sign);
                                Log.e(TAG, "The image: " + theImage );
                                iv = (ImageView) findViewById(R.id.photo);
                                iv.setImageBitmap(theImage);
                                iv.setScaleType(ImageView.ScaleType.FIT_XY);
                                iv.setAdjustViewBounds(true);
                                Toast toast = Toast
                                        .makeText(this, "Photo capture Successfull", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.BOTTOM, 105, 50);
                                toast.show();
                            } catch (SQLiteException e) {
                                // TODO: handle exception
                                e.printStackTrace();
                                Crashlytics.log(android.util.Log.ERROR, TAG, "onActivityResult " + e.getMessage());

                            } catch (UnsatisfiedLinkError err) {
                                err.getStackTrace();
                                Crashlytics.log(android.util.Log.ERROR, TAG, "onActivityResult " + err.getMessage());

                            } catch (OutOfMemoryError o) {
                                o.getStackTrace();
                                Crashlytics.log(android.util.Log.ERROR, TAG, "onActivityResult " + o.getMessage());

                            } catch (Exception e) {
                                Crashlytics.log(android.util.Log.ERROR, TAG, "onActivityResult " + e.getMessage());
                            } finally {
                                if (db != null) {
                                    db.close();
                                }
                                // This block contains statements that are ALWAYS executed
                                // after leaving the try clause, regardless of whether we leave it:
                                // 1) normally after reaching the bottom of the block;
                                // 2) because of a break, continue, or return statement;
                                // 3) with an exception handled by a catch clause above; or
                                // 4) with an uncaught exception that has not been handled.
                                // If the try clause calls System.exit(), however, the interpreter
                                // exits before the finally clause can be run.
                            }
                            break;

                        } else if (resultCode == RESULT_CANCELED) {
                            photoname = "null";
//              Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
                            Toast toast = Toast
                                    .makeText(this, "Picture was not taken", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.BOTTOM, 105, 50);
                            toast.show();

                        } else {
                            photoname = "null";
//              Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
                            Toast toast = Toast
                                    .makeText(this, "Picture was not taken", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.BOTTOM, 105, 50);
                            toast.show();

                        }
                    } else {
                        photoname = "null";
                        Toast toast = Toast
                                .makeText(this, "No Photo captured", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.BOTTOM, 105, 50);
                        toast.show();
                    }
                    //********************************Activity Result for signature **************************//
                case SIGNATURE_ACTIVITY:
                    if (resultCode == RESULT_OK) {

                        Bundle bundle = data.getExtras();
                        String status = bundle.getString("status");
                        @SuppressWarnings("unused")
                        String sign = bundle.getString("getsign");
                        sign1 = CaptureSignature.s1;

                        if (sign1 != null) {

                            if (status.equalsIgnoreCase("done")) {

                                @SuppressWarnings("unused")
                                ByteArrayOutputStream bao = new ByteArrayOutputStream();

                                byte[] ba2 = Base64.decode(sign1);

                                ByteArrayInputStream imageStream_sign = new ByteArrayInputStream(ba2);
                                theImage = BitmapFactory.decodeStream(imageStream_sign);
                                iv = (ImageView) findViewById(R.id.sign);
                                iv.setImageBitmap(theImage);
                                iv.setScaleType(ImageView.ScaleType.FIT_XY);
                                iv.setAdjustViewBounds(true);
                                Toast toast = Toast.makeText(this, "Signature capture successful!", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.BOTTOM, 105, 50);
                                toast.show();
                                //mGetDelivered.setEnabled(true);
//            		mGetDelivered.setBackgroundResource(R.drawable.button);
                                //mGetDelivered.setBackgroundResource(R.drawable.button_bgdark_stroke);
                            } else if (status.equalsIgnoreCase("cancel")) {
                                sign1 = null;
                                theImage = null;
                                iv = null;
                                iv = (ImageView) findViewById(R.id.sign);
                                iv.setImageBitmap(theImage);
                                iv.setScaleType(ImageView.ScaleType.FIT_XY);
                                iv.setAdjustViewBounds(true);

                                Toast toast = Toast.makeText(this, "Signature not captured !", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.BOTTOM, 105, 50);
                                toast.show();
                            }
                        } else // if(sign1 == null)
                        {
                            sign1 = null;
                            theImage = null;
                            iv = null;
                            iv = (ImageView) findViewById(R.id.sign);
                            iv.setImageBitmap(theImage);
                            iv.setScaleType(ImageView.ScaleType.FIT_XY);
                            iv.setAdjustViewBounds(true);

                            Toast toast = Toast
                                    .makeText(this, "Please capture Signature!", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.BOTTOM, 105, 50);
                            toast.show();
                        }

                        break;
                    }
            }
        } catch (Exception e) {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "onActivityResult " + e.getMessage());

        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "onActivityResult " + err.getMessage());
        }
    }

    public Bitmap reduceImageSize(String selectedImagePath) {
        Bitmap m = null;
        try {
            File f = new File(selectedImagePath);
            Log.e(TAG, "File to reduce bitmap" + f);

            if(f.exists()) {
                System.out.println("!!!!!!!!!!!!!!!!>>>>>>>>>>>>> About entering the decode file");
                System.out.println(f.getAbsolutePath()+" <<<<<<<<<I AM HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" +
                        "!!!!!!!!!!!!!!!" + f.toString());
            }else{
                Toast.makeText(getApplicationContext(), "Wrong Capture please try again!", Toast.LENGTH_LONG).show();
                System.out.println("!!!!!!!!!!!!!!!!!!!!!!>>>> FILE1 FAILED");
            }

            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            Log.e(TAG, "File to reduce bitmap for" + o);
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            Log.e(TAG, "File1 to reduce bitmap for" + f);

            //The new size we want to scale to
            final int REQUIRED_SIZE = 150;

            //Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;

            m = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
            Log.e(TAG, "File to reduce" + m);
        } catch (FileNotFoundException e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "Image File not found in your phone. Please select another image. " + e.getMessage());
//			  Toast.makeText(getApplicationContext(), "Image File not found in your phone. Please select another image.", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "reduceImageSize" + e.getMessage());
        }
        return m;
    }


    //starting asynchronus task
    class SoapAccessTask extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            //if you want, start progress dialog here
            // NOTE: You can call UI Element here.

        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                final Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

                image = getFile(AssignedMultipleFailedPickup.this);
                intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, Uri.fromFile(image));
                startActivityForResult(intent, TAKE_PHOTO_CODE);
            } catch (Exception e) {
                e.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception AssignedMultipleDelActivity doInBackground " + e.getMessage());
            } catch (UnsatisfiedLinkError err) {
                err.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleDelActivity doInBackground UnsatisfiedLinkError ");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            // NOTE: You can call UI Element here.
            // Close progress dialog

        }
    }

    public void toggleMenu(View v) {
        finish();

  /*      if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_ASSIGN")) {
            //        Intent homeActivity = new Intent(this, AssignedListMainActivity.class);
//        startActivity(homeActivity);
            pref = getSharedPreferences(PREF_NAME1, Context.MODE_PRIVATE);
            editor = pref.edit();
//                        editor.clear();
            editor.putString("position", String.valueOf(1));
            // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
            editor.commit();
            Intent homeActivity = new Intent(getApplicationContext(), AssignedListMainActivity.class);
            startActivity(homeActivity);
        } else if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_PICKUP_COMPLETE")) {
            //        Intent homeActivity = new Intent(this, AssignedListMainActivity.class);
//        startActivity(homeActivity);
            pref = getSharedPreferences(PREF_NAME1, Context.MODE_PRIVATE);
            editor = pref.edit();
//                        editor.clear();
            editor.putString("position", String.valueOf(1));
            // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
            editor.commit();
            Intent homeActivity = new Intent(getApplicationContext(), AssignmentCompleteStatus.class);
            startActivity(homeActivity);
        } else if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_PICKUP_INCOMPLETE")) {
            //        Intent homeActivity = new Intent(this, AssignedListMainActivity.class);
//        startActivity(homeActivity);
            pref = getSharedPreferences(PREF_NAME1, Context.MODE_PRIVATE);
            editor = pref.edit();
//                        editor.clear();
            editor.putString("position", String.valueOf(1));
            // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
            editor.commit();
            Intent homeActivity = new Intent(getApplicationContext(), AssignmentCompleteStatus.class);
            startActivity(homeActivity);
        }*/
    }

    private boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);

        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable)drawable).getBitmap() != null;
        }

        return hasImage;
    }

    @Override
    public void onClick(View v) {
        try {
            if (v.getId() == R.id.btncancel) {
                finish();

            } else if (v.getId() == R.id.btnsave) {
                session.setPosition(1);
                final Calendar c = Calendar.getInstance();
                String month = c.get(Calendar.MONTH) + 1 + "";
                if (month.length() < 2) {
                    month = "0" + month;
                }
                String date = c.get(Calendar.DAY_OF_MONTH) + "";
                if (date.length() < 2) {
                    date = "0" + date;
                }
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                hour = c.get(Calendar.HOUR_OF_DAY);
                minute = c.get(Calendar.MINUTE);
                showdate = "" + c.get(Calendar.YEAR) + "-" + month + "-" +
                        date;
                showtime = " " +
                        c.get(Calendar.HOUR) + ":" +
                        c.get(Calendar.MINUTE) + ":" +
                        c.get(Calendar.SECOND);

                currentTime = (c.get(Calendar.HOUR_OF_DAY)) + ":" +
                        (c.get(Calendar.MINUTE)) + ":" +
                        (c.get(Calendar.SECOND));
                Log.w("TIME:", String.valueOf(currentTime) + currentTime);

                Remarks = remarks.getText().toString().trim();

                if (et_reason.getText().toString().isEmpty() || et_reason.getText().toString() == null) {

                    Toast toast = Toast
                            .makeText(this, "Please Select the Reason !!", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    saveButton.setEnabled(true);

                    return;
                } else if (!et_reason.getText().toString().isEmpty()) {
                    if (!remarks.getText().toString().trim().isEmpty()) {
                        Remarks = remarks.getText().toString().trim();
                    } else {
                        Remarks = "null";
                    }
                    if (cust_labels.contains(et_reason.getText().toString().trim())) {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "in list");
                        if (hasImage((ImageView)findViewById(R.id.photo))) {

                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

                            // Setting Dialog Title
                            alertDialog.setTitle("Alert");

                            // Setting Dialog Message
                            alertDialog.setMessage("Are you sure you want to confirm ?");

                            // On pressing Settings button
                            alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        if (session_DB_PATH != null && session_DB_PWD != null) {
                                            if (SESSION_TRANSPORT.equals("SELECT TRANSPORT") || SESSION_TRANSPORT.equals("")) {
                                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(AssignedMultipleFailedPickup.this);

                                                // Setting Dialog Title
                                                alertDialog.setTitle("Alert");

                                                // Setting Dialog Message
                                                alertDialog.setMessage("Please start the trip");

                                                // On pressing Settings button
                                                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        Intent goToNextActivity = new Intent(getApplicationContext(), Start_End_TripMainActivity.class);
//								Toast.makeText(AssignedMultipleFailedPickup.this,"Please locate the customer", Toast.LENGTH_LONG).show();
                                                        saveButton.setEnabled(true);
//						 saveButton.setBackgroundResource(R.drawable.button);
                                                        startActivity(goToNextActivity);
                                                    }
                                                });

                                                // Showing Alert Message
                                                alertDialog.show();
                                            } else {
                                                db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                                Cursor cr = db.rawQuery("select T_UD_Reason_Desc,T_UD_Reason_Id,_id from UD_REASON_MASTER where T_UD_Reason_Desc='" + et_reason.getText().toString().trim() + "'", null);
                                                while (cr.moveToNext()) {
                                                    reason_code = cr.getString(cr.getColumnIndex("T_UD_Reason_Desc"));
                                                    reason_id = cr.getString(cr.getColumnIndex("T_UD_Reason_Id"));
                                                    Crashlytics.log(android.util.Log.ERROR, TAG, "reason_code " + reason_code + ":" + reason_id);
                                                }
                                                Crashlytics.log(android.util.Log.ERROR, TAG, "print save SESSION_TRANSPORT" + SESSION_TRANSPORT);
                                                if (SESSION_TRANSPORT.equals("BIKER")) {
                                                    Crashlytics.log(android.util.Log.ERROR, TAG, "cust get_assgn_id" + get_assgn_id + get_lat + get_lng);
                                                    startService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
                                                    docId = assignId + "_" + session.getAssignmentid(assignId) + "_" + session.getUserDetails().get(SessionManager.KEY_CUST_ACC_CODE);
                                                    Log.e(TAG, "DocId:" + docId);
                                                    dbHelper.updateRVPFailedData(docId, session.getWayBillNo(assignId), reasonStore.get(et_reason.getText().toString().trim()), Remarks, session.getUserDetails().get(SessionManager.KEY_CUST_ACC_CODE), session.getLocId(),
                                                            session.getuserId(), assignId, getphotobytearray, session.getItemDescription(assignId), session.getReasonOfReturn(assignId), session.getDescribedValue(assignId), session.getNoPcs(assignId));

                                                    saveButton.setEnabled(false);
                                                    finish();
                                                } else if (SESSION_TRANSPORT.equals("NON-BIKER")) {
                                                    startService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
                                                    docId = assignId + "_" + session.getAssignmentid(assignId) + "_" + session.getUserDetails().get(SessionManager.KEY_CUST_ACC_CODE);
                                                    Log.e(TAG, "DocId:" + docId);
                                                    dbHelper.updateRVPFailedData(docId, session.getWayBillNo(assignId), reasonStore.get(et_reason.getText().toString().trim()), Remarks, session.getUserDetails().get(SessionManager.KEY_CUST_ACC_CODE), session.getLocId(),
                                                            session.getuserId(), assignId, getphotobytearray, session.getItemDescription(assignId), session.getReasonOfReturn(assignId), session.getDescribedValue(assignId), session.getNoPcs(assignId));

                                                    Crashlytics.log(android.util.Log.ERROR, TAG, "imei_num " + imei_num);
                                                    saveButton.setEnabled(false);
                                                    finish();
                                                }
                                            }
                                        } else {
                                            Crashlytics.log(android.util.Log.ERROR, TAG, "Error TOMUndeliveredActivity TOMLoginUserActivity.file is null ");
                                        }
                                    } catch (SQLException s) {
                                        Crashlytics.log(android.util.Log.ERROR, TAG, "Exception db path SQLException " + s.getMessage());
                                    } catch (Exception e) {
                                        Crashlytics.log(android.util.Log.ERROR, TAG, "Exception db path  " + e.getMessage());
                                    } finally {

                                    }

                                }
                            });

                            // on pressing cancel button
                            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    session.setPosition(1);
//		            	Intent goToNextActivity = new Intent(getApplicationContext(),TOMDel_pickup_screenActivity.class);
//		    			 startActivity(goToNextActivity);
                                    saveButton.setEnabled(true);
//							 saveButton.setBackgroundResource(R.drawable.button);
                                    dialog.cancel();
                                }
                            });

                            // Showing Alert Message
                            alertDialog.show();

                        }else {
                            Toast.makeText(getApplicationContext(), "Please capture the image", Toast.LENGTH_LONG).show();
                        }
                    }else {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Not in list");
                        Toast.makeText(getApplicationContext(), "Please select reason from the list", Toast.LENGTH_LONG).show();
                    }
                }

            }
        } catch (SQLiteException e) {
            //onClickGoToHomePage();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleFailedPickup onclick SQLiteException ");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleFailedPickup onclick Exception ");
            //onClickGoToHomePage();	
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleFailedPickup onclick UnsatisfiedLinkError ");
            //onClickGoToHomePage();
//			finish();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            if (db != null) {
                db.close();
            }
        }
    }

//    } 


    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    //**********************modification for menu options(Home & Logout)**************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home:
                onClickGoToHomePage();
                return true;
            case R.id.menu_log_out:
                onClickLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onClickLogOut() {
        try {
            Intent logOutActivity = new Intent(this, TOMLoginUserActivity.class);
            logOutActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(logOutActivity);
        } catch (SQLiteConstraintException ex) {
            //what ever you want to do
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleFailedPickup onClickLogOut ");

        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleFailedPickup UnsatisfiedLinkError ");

        }
    }

    private void onClickGoToHomePage() {
        try {
            Intent homePageActivity = new Intent(this, HomeMainActivity.class);
//        homePageActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homePageActivity);
        } catch (SQLiteConstraintException ex) {
            //what ever you want to do
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleFailedPickup onClickGoToHomePage ");

        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error AssignedMultipleFailedPickup UnsatisfiedLinkError ");

        }
    }

}
