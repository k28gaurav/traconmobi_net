package com.traconmobi.tom.rest;

import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.traconmobi.tom.SessionManager;
import com.traconmobi.tom.TOMLoginUserActivity;
import com.traconmobi.tom.rest.service.EcomParserService;
import com.traconmobi.tom.rest.service.EcomService;
import com.traconmobi.tom.rest.service.LocationParserService;
import com.traconmobi.tom.rest.service.PickUpList;
import com.traconmobi.tom.rest.service.PickUpService;
import com.traconmobi.tom.rest.service.TokenService;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by kumargaurav on 12/8/15.
 */
public class RestClient
{
    private static final String BASE_URL1 = "http://traconmobi.net";
    private static final String BASE_URL = "http://navatech.cloudapp.net:8092";
    private PickUpService apiService;
    private TokenService tokenService;
    private PickUpList apiService1;
    private EcomService ecomService;
    private EcomParserService ecomParserService;
    private LocationParserService locationParserService;
    SessionManager session = null;
    String TAG = "RestClient";
    public RestClient()
    {
        try {
            Retrofit restAdapter, restAdapter1;
            restAdapter = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            restAdapter1 = new Retrofit.Builder()
                    .baseUrl(BASE_URL1)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            apiService = restAdapter.create(PickUpService.class);
            apiService1 = restAdapter.create(PickUpList.class);
            ecomService = restAdapter1.create(EcomService.class);
            ecomParserService = restAdapter.create(EcomParserService.class);
            tokenService = restAdapter1.create(TokenService.class);
            locationParserService = restAdapter1.create(LocationParserService.class);
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, e.getMessage());
        }
    }

    public PickUpService getPickUpService()
    {
        return apiService;
    }
    public PickUpList getPickUpList()
    {
        return apiService1;
    }
    public EcomService getEcomService() {
        return ecomService;
    }
    public EcomParserService getEcomParserService() {
        return ecomParserService;
    }
    public LocationParserService getLocationParserService() {
        return locationParserService;
    }

    public TokenService getToken() {
        Log.e(TAG, "TokenService Called");
        return tokenService;
    }
}

