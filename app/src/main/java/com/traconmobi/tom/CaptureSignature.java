package com.traconmobi.tom;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

import org.kobjects.base64.Base64;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
//import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Images.Media;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class CaptureSignature extends Activity {
	public String particular_val,SESSION_TRANSPORT,consgnee,get_assgn_id,get_lat,get_lng,entityString_gps,session_user_id,session_user_pwd,session_USER_LOC,session_USER_NUMERIC_ID,session_CURRENT_DT,session_CUST_ACC_CODE,session_USER_NAME,Session_single_cod_amt;
	// Session Manager Class
    SessionManager session;
    TinyDB tiny;
	SQLiteDatabase db;
    LinearLayout mContent;
    public static String session_DB_PATH,Assignment_typ;
	public static String session_DB_PWD;
    static signature mSignature;
    static Button mClear, mGetSign, mCancel;
    public static int mYear,mMonth,mDay,hour,minute;
	String showdate,showtime;
    public static String tempDir;
    public int count = 1;
    public static String current = null,bb,s1,s1_datetime;
    private Bitmap mBitmap;
    View mView;
//    File mypath;
    static File mypath;	
    byte [] HRHK;
    private String uniqueId;
    public static String employeeId,aa,awb,emp_indx,get_del_assgn_num,get_handover_sgn,recvr_nme,Assgn_typ;
    public String TAG="CaptureSignature";
    File tempdir;
    File fileimage = null;
    
    @SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState)
    {
    	try
		{
        super.onCreate(savedInstanceState);
        //Intializing Fabric
        Fabric.with(this, new Crashlytics());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.signature);
        getWindow().setFeatureInt(Window.FEATURE_NO_TITLE,R.layout.signature);       
        
        
        try
        {
        	// Session class instance
            session = new SessionManager(getApplicationContext());
            
            tiny =new TinyDB(getApplicationContext());
            
         // get AuthenticateDb data from session
            HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();
             
            // DB_PATH
            session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);
           Crashlytics.log(android.util.Log.ERROR,TAG, "print db path session_DB_PATH" + session_DB_PATH);
          
            // DB_PWD
            session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);    
                        
            // get user data from session
            HashMap<String, String> login_Dts = session.getLoginDetails();
             
            // Userid
            session_user_id = login_Dts.get(SessionManager.KEY_UID);
             
            // pwd
            session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);
        	
         // get user data from session
            HashMap<String, String> user = session.getUserDetails();
             
            // session_USER_LOC
            session_USER_LOC= user.get(SessionManager.KEY_USER_LOC);
             
            // session_USER_NUMERIC_ID
            session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);
            
            // session_CURRENT_DT
            session_CURRENT_DT= user.get(SessionManager.KEY_CURRENT_DT);
             
            // session_CUST_ACC_CODE
            session_CUST_ACC_CODE= user.get(SessionManager.KEY_CUST_ACC_CODE);
            
            // session_USER_NAME
            session_USER_NAME= user.get(SessionManager.KEY_USER_NAME);
            
            
        Intent intent = getIntent();
//		 employeeId = intent.getStringExtra(TOMCODUpdateActivity.EXTRA_MESSAGE);
//		 employeeId = intent.getStringExtra(Payment_Cust_DetailsActivity.EXTRA_MESSAGE);
//		 employeeId = intent.getStringExtra(Payment_UpdateMainActivity.EXTRA_MESSAGE);
		 
		 Assgn_typ = intent.getStringExtra(AssignedMultipleDelActivity.EXTRA_MESSAGE2);
		 
//		 if(Assgn_typ.equals("D"))
//		 {
		 
		 get_del_assgn_num = intent.getStringExtra(AssignedMultipleDelActivity.EXTRA_MESSAGE1);
		 
//		 }
//		 get_del_assgn_num = get_del_assgn_num + "D";
//		 if(Assgn_typ.equals("P"))
//		 {
//		 employeeId = intent.getStringExtra(AssignedMultiplePaymentActivity.EXTRA_MESSAGE);
//		 }
//		 employeeId = employeeId + "P";
//		 employeeId = intent.getStringExtra(AssignedMultipleDelActivity.EXTRA_MESSAGE1);
		Crashlytics.log(android.util.Log.ERROR,TAG, "shw multiple get_assgn_num" + get_del_assgn_num);
//		 if(Assgn_typ.equals("D"))
//		 {
		 employeeId=intent.getStringExtra(HandOver_DtlsActivity.EXTRA_MESSAGE);
//		 }
//		 employeeId = employeeId + "H";
		 final Calendar c = Calendar.getInstance();
         String month=c.get(Calendar.MONTH)+1+"";
         if(month.length()<2){
              month="0"+month;   
         }  
         String date=c.get(Calendar.DAY_OF_MONTH)+"";
         if(date.length()<2)
         {
         	date="0"+date;
         }
         mYear= c.get(Calendar.YEAR);
         mMonth = c.get(Calendar.MONTH);
         mDay = c.get(Calendar.DAY_OF_MONTH);
         hour = c.get(Calendar.HOUR_OF_DAY);
         minute = c.get(Calendar.MINUTE);

         showdate =""+ c.get(Calendar.YEAR)+ "-" +month+ "-" +
                 date;
         showtime =" " +
                 c.get(Calendar.HOUR_OF_DAY) + ":" +
                 c.get(Calendar.MINUTE) +  ":" +
                 c.get(Calendar.SECOND);  
         
//		 if(recvr_nme !=null)
//		 {
//			 
//		 }
		 if(employeeId == null)
	        {
	        	employeeId = get_del_assgn_num;
	        	get_del_assgn_num=null;
			 }
		 employeeId=employeeId.replaceAll("null", "");	
	 
 
        }
        catch(Exception e)
        {
        	Crashlytics.log(android.util.Log.ERROR,TAG,"Exception signature "+ e.getMessage());
        }
        mContent = (LinearLayout) findViewById(R.id.linearLayout);
        mSignature = new signature(this, null);
        mSignature.setBackgroundColor(Color.WHITE);
        mContent.addView(mSignature, LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
        mClear = (Button)findViewById(R.id.clear);
        mGetSign = (Button)findViewById(R.id.getsign);
        mGetSign.setEnabled(false);
        mCancel = (Button)findViewById(R.id.cancel);
        mView = mContent;
 
        mClear.setOnClickListener(new OnClickListener()
        {       
            @Override
			public void onClick(View v)
            {              
                mSignature.clear();
                mGetSign.setEnabled(false);
            }
        });
 
        mGetSign.setOnClickListener(new OnClickListener()
        {       
            @Override
			public void onClick(View v)
            {               
                mGetSign.setEnabled(false);
                boolean error = captureSignature();
                if(!error){
                    try {
                        mView.setDrawingCacheEnabled(true);
                        mSignature.save(mView);
                    }catch (OutOfMemoryError e) {
                        
                    }catch (Exception e) {
                        
                    }
                    Bundle b = new Bundle();
                    b.putString("status", "done");
                    b.putString("getsign", bb); //.putStringArray("sign", mView);
                    Intent intent = new Intent();
                    intent.putExtras(b);
                    setResult(RESULT_OK,intent);  
                    finish();
                }
            }
        });
 
        mCancel.setOnClickListener(new OnClickListener()
        {       
            @Override
			public void onClick(View v)
            {               
                Bundle b = new Bundle();
                b.putString("status", "cancel");
                Intent intent = new Intent();
                intent.putExtras(b);
                setResult(RESULT_OK,intent); 
                finish();
            }
        });
//        db.close();
    }
	catch(Exception e)
	{
		e.getStackTrace();
		Crashlytics.log(android.util.Log.ERROR,TAG,"Exception CaptureSignature " + e.getMessage());
		
	}
	catch(UnsatisfiedLinkError err)
	{
		err.getStackTrace();
		Crashlytics.log(android.util.Log.ERROR,TAG,"Error CaptureSignature UnsatisfiedLinkError ");
//		finish();
	}
    }
 
    @Override
    protected void onDestroy()
    {
        Log.w("GetSignature", "onDestory");
        super.onDestroy();
    }
 
    private boolean captureSignature() {
 
        boolean error = false;
        String errorMessage = "";
        try
		{
        if(error){
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, 105, 50);
            toast.show();
        }
	    }
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR,TAG,"Exception captureSignature errorMessage " + e.getMessage());
		}
        return error;
    }
 
    private String getTodaysDate() {
 
        final Calendar c = Calendar.getInstance();
        int todaysDate =     (c.get(Calendar.YEAR) * 10000) +
        ((c.get(Calendar.MONTH) + 1) * 100) +
        (c.get(Calendar.DAY_OF_MONTH));
        Log.w("DATE:",String.valueOf(todaysDate));
        return(String.valueOf(todaysDate));
 
    }
 
    private String getCurrentTime() {
 
        final Calendar c = Calendar.getInstance();
        int currentTime =     (c.get(Calendar.HOUR_OF_DAY) * 10000) +
        (c.get(Calendar.MINUTE) * 100) +
        (c.get(Calendar.SECOND));
        Log.w("TIME:",String.valueOf(currentTime));
        return(String.valueOf(currentTime));
 
    } 
 
    private boolean prepareDirectory()
    {
        try
        {
            if (makedirs())
            {
                return true;
            } else {
                return false;
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            Toast.makeText(this, "Could not initiate File System.. Is Sdcard mounted properly?", Toast.LENGTH_LONG).show();
            return false;
        }
    }
 
    private boolean makedirs()
    {
    	 try {
//        File tempdir = new File(Environment.getExternalStorageDirectory(), "signatures" );
    	tempdir = new File(Environment.getExternalStorageDirectory()
	               + "/Android/data/com.traconmobi.net/", "signatures" );
        if (!tempdir.exists())
        	 tempdir.mkdir();
//            tempdir.mkdirs();
 
//        File nomedia_file = new File(Environment.getExternalStorageDirectory()
//	               + "/Android/data/com.traconmobi.net/signatures/", ".nomedia");
//        try {
        	try {
        		File sdcard = new File(Environment.getExternalStorageDirectory()
 		               + "/Android/data/com.traconmobi.net");
 	    	    File mymirFolder = new File(sdcard.getAbsolutePath() + "/signatures/");
 	    	   if(mymirFolder.exists())
	    	    {
	    	        File noMedia = new File(mymirFolder.getAbsolutePath() + "/.nomedia");
	    	        noMedia.mkdir();
	    	        noMedia.createNewFile();
	    	    }
//				nomedia_file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
//        	}             
//        catch (SQLiteException e) {
//            // TODO: handle exception
//            e.printStackTrace();
//           Crashlytics.log(android.util.Log.ERROR,TAG,"Exception "+ e.getMessage());
//        }
        
//        try {
////	    	   File sdcard = Environment.getExternalStorageDirectory();
//        	 File sdcard =  new File(Environment.getExternalStorageDirectory()
//		               + "/Android/data/com.traconmobi.net");
////	    	    File mymirFolder = new File(Environment.getExternalStorageDirectory(), "signatures" );
//        	  File mymirFolder = new File(sdcard.getAbsolutePath()+ "/signatures/" );  
//        	 if(!mymirFolder.exists())
//	    	    {
//	    	        File noMedia = new File(mymirFolder.getAbsolutePath() + "/.nomedia");
//	    	        noMedia.mkdirs();
//	    	        noMedia.createNewFile();
//	    	    }
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
////        if (tempdir.isDirectory())
////        {
////        	try {
//////  	    	   File sdcard = Environment.getExternalStorageDirectory();
////        		 File sdcard =  new File(Environment.getExternalStorageDirectory()
////  		               + "/Android/data/com.traconmobi.net");
//////  	    	    File mymirFolder = new File(Environment.getExternalStorageDirectory(), "signatures" );
////        		 File mymirFolder = new File(sdcard.getAbsolutePath()+ "/signatures/" );  
////  	    	    if(!mymirFolder.exists())
////  	    	    {
////  	    	        File noMedia = new File(mymirFolder.getAbsolutePath() + "/.nomedia");
////  	    	        noMedia.mkdirs();
////  	    	        noMedia.createNewFile();
////  	    	    }
////  		} catch (IOException e) {
////  			// TODO Auto-generated catch block
////  			e.printStackTrace();
////  		}
////            File[] files = tempdir.listFiles();
////            try {
////// 	    	   File sdcard = Environment.getExternalStorageDirectory();
////// 	    	    File mymirFolder = new File(Environment.getExternalStorageDirectory(), "signatures");
////            	File sdcard =  new File(Environment.getExternalStorageDirectory()
////   		               + "/Android/data/com.traconmobi.net");
//////   	    	    File mymirFolder = new File(Environment.getExternalStorageDirectory(), "signatures" );
////         		 File mymirFolder = new File(sdcard.getAbsolutePath()+ "/signatures/" );  
////            	if(!mymirFolder.exists())
//// 	    	    {
//// 	    	        File noMedia = new File(mymirFolder.getAbsolutePath() + "/.nomedia");
//// 	    	        noMedia.mkdirs();
//// 	    	        noMedia.createNewFile();
//// 	    	    }
//// 		} catch (IOException e) {
//// 			// TODO Auto-generated catch block
//// 			e.printStackTrace();
//// 		}
////            for (@SuppressWarnings("unused") File file : files) 
////            {
//////                if (!file.delete())
//////                {
//////                   Crashlytics.log(android.util.Log.ERROR,TAG,"Failed to delete " + file);
//////                }
////            }
////        }
        }             
        catch (SQLiteException e) {
            // TODO: handle exception
            e.printStackTrace();
           Crashlytics.log(android.util.Log.ERROR,TAG, "Exception " + e.getMessage());
        }
        return (tempdir.isDirectory());
    }
 
    public class signature extends View
    {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();
 
        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();
 
        public signature(Context context, AttributeSet attrs)
        {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }
 
        private File getFile(Context context)
        {  
        	try
        	{
//    	       final File path = new File( Environment.getExternalStorageDirectory(), "Image keeper" );	   
        		final File path = new File(Environment.getExternalStorageDirectory()
     	               + "/Android/data/com.traconmobi.net/", "signatures" );
//        		photoname=null;	       
    	       if(!path.exists()){  
    	         path.mkdir(); 
    	       }
    	       try {
//    	    	   File sdcard = Environment.getExternalStorageDirectory();
    	    	   File sdcard = new File(Environment.getExternalStorageDirectory()
    		               + "/Android/data/com.traconmobi.net");
    	    	    File mymirFolder = new File(sdcard.getAbsolutePath() + "/signatures/");
    	    	    String shw[] = employeeId.split(",");
    	            for(int i = 0 ; i< shw.length ; i++)
    	            {
    	            String result = shw[i];
//    	            employeeId=result;//commented on 7aug2015
    	            awb=result;
//    	           Crashlytics.log(android.util.Log.ERROR,TAG,"print employeeId"+ employeeId);
//    	            db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
//    	        	Cursor cursor = db.rawQuery("SELECT emp._id,emp.T_Assignment_Number,emp.T_Assignment_Type,emp.T_Consignee_Name , emp.T_Address_Line1, emp .T_Address_Line2, emp .T_City , emp.T_Contact_number , emp .T_Pincode,emp.B_is_Amountable,emp.F_Amount,emp.T_Signature FROM TOM_Assignments  emp LEFT OUTER JOIN TOM_Assignments  mgr ON emp._id = mgr._id  where emp.T_Assignment_Number='"+ employeeId +"'",null); 
////    	        			new String[]{""+employeeId});
//    	    	
//    	    		while (cursor.moveToNext()){
//    	    			awb=cursor.getString(cursor.getColumnIndex("T_Assignment_Number"));
//    	    			=cursor.getString(cursor.getColumnIndex("T_Assignment_Type"));
//    	    			Crashlytics.log(android.util.Log.ERROR,TAG,"shw the cod page waybill" + awb);
//    	    			emp_indx=cursor.getString(cursor.getColumnIndex("_id"));
//    	    			
//    	    		}
//    	    		cursor.close();
//    	    		db.close();
    	    		if(awb !=null && awb!="")
    	    		{
    	            uniqueId = getTodaysDate() + "_" + getCurrentTime() + "_" + awb;   // + "_" + Math.random();
    	            awb=awb.replace("/", "");
    	            awb=awb.replace(" ", "");
    	            awb=awb.replace("-", "");
    	            awb=awb.replace(":", "");
//    	            current = awb + ".data";
    	            current = awb + ".png";
    	           Crashlytics.log(android.util.Log.ERROR,TAG, "print current" + current);
    	            fileimage = new File(path, current);
//    	            mypath= new File( Environment.getExternalStorageDirectory(), "signatures" + "/" + current);//(tempDir,current);  
    	     
//    	            mypath= new File( Environment.getExternalStorageDirectory()
//    	    	               + "/Android/data/com.traconmobi.net/", "signatures" + "/" + current);//(tempDir,current);  
//    	           Crashlytics.log(android.util.Log.ERROR,TAG,"print path"+mypath);
    	    		}
    	            }
//    	    	    String shw[] = session_RESPONSE_AWB_NUM.split(",");
//    		        for(int i = 0 ; i< shw.length ; i++)
//    		        {
//    		         result = shw[i];
//    		       
//    		        result=result.replace("/", "");
//    		         result=result.replace(" ", "");
//    		         result=result.replace("-", "");
//    		         result=result.replace(":", "");
//    			       photoname= result + ".png";
//    			      Crashlytics.log(android.util.Log.ERROR,TAG,"print multi photoname"+photoname);
////    			       photoname.replace("/", "");
//    			       fileimage = new File(path, photoname);	
//    		        }
    	    	    if(!mymirFolder.exists())
    	    	    {
    	    	        File noMedia = new File(mymirFolder.getAbsolutePath() + "/.nomedia");
    	    	        noMedia.mkdirs();
    	    	        noMedia.createNewFile();
    	    	    }
    				} catch (IOException e)
    				{
    					Crashlytics.log(android.util.Log.ERROR,TAG,"Exception getFile  SQLiteException" + e.getMessage());
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}
        		}
    	       catch(Exception e) 
    		     {
    		    	 e.getStackTrace();
    		    	Crashlytics.log(android.util.Log.ERROR,TAG, "Exception AssignedMultipleDelActivity getFile " + e.getMessage());
    		    	 //onClickGoToHomePage();
    		     }
    	    	catch(UnsatisfiedLinkError err)
    			{
    				err.getStackTrace();
    				Crashlytics.log(android.util.Log.ERROR,TAG,"Error AssignedMultipleDelActivity UnsatisfiedLinkError ");
    				//onClickGoToHomePage();
    	//        	    			finish();
    			}
    			return fileimage; 
    			
    	      
    	     }
        
        //deleting from /Android/data/com.traconmobi.net/Image keeper folder
        
        private void delete_older() 
        {
        	try
        	{
            // TODO Auto-generated method stub
            File f = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.traconmobi.net/signatures" );

            //Log.i("Log", "file name in delete folder :  "+f.toString());
            File [] files = f.listFiles();

            Log.i("Log", "delete_older List of files is: " +files.toString());
            
           Crashlytics.log(android.util.Log.ERROR,TAG, "print date " + new Date().getDate());
        	
            for(int i=0; i <= files.length; i++)
            {
        	long diff = new Date().getTime() - files[i].lastModified();
        	int x = 3;
        	
        	long tme = x * 24 * 60 * 60 * 1000;
        	Crashlytics.log(android.util.Log.ERROR,TAG, "print diff " + diff + "\n" + tme);
        	 
        	 
        	 
        	if(diff > x * 24 * 60 * 60 * 1000)
        	{
        		files[i].delete();
        	}
            }
            Arrays.sort( files, new Comparator<Object>()
                    {
                public int compare(Object o1, Object o2) {
                	
                	

                    if (((File)o1).lastModified() > ((File)o2).lastModified()) {
                                 Log.i("Log", "Going -1");
                        return -1;
                    } else if (((File)o1).lastModified() < ((File)o2).lastModified()) {
                             Log.i("Log", "Going +1");
                        return 1;
                    } else {
                             Log.i("Log", "Going 0");
                        return 0;
                    }
                }

                    });

            //Log.i("Log", "Count of the FILES AFTER DELETING ::"+files[0].length());
//            files[0].delete();
        	}
        	catch(Exception e)
        	{
        		Crashlytics.log(android.util.Log.ERROR,TAG,"Exception delete_older" +  e.getMessage());
        	}
        }
        
        //deleting from /DCIM/Camera folder
        
        private void deleteLatest() 
        {
        	try
        	{
            // TODO Auto-generated method stub
            File f = new File(Environment.getExternalStorageDirectory() + "/DCIM/Camera" );

            //Log.i("Log", "file name in delete folder :  "+f.toString());
            File [] files = f.listFiles();

            //Log.i("Log", "List of files is: " +files.toString());
            Arrays.sort( files, new Comparator<Object>()
                    {
                public int compare(Object o1, Object o2) {

                    if (((File)o1).lastModified() > ((File)o2).lastModified()) {
                        //         Log.i("Log", "Going -1");
                        return -1;
                    } else if (((File)o1).lastModified() < ((File)o2).lastModified()) {
                        //     Log.i("Log", "Going +1");
                        return 1;
                    } else {
                        //     Log.i("Log", "Going 0");
                        return 0;
                    }
                }

                    });

            //Log.i("Log", "Count of the FILES AFTER DELETING ::"+files[0].length());
            files[0].delete();
        }
    	catch(Exception e)
    	{
    		Crashlytics.log(android.util.Log.ERROR,TAG,"Exception deleteLatest" +  e.getMessage());
    	}
        }
        
        
        
        public void save(View v)
        {
        	try
            {
            if(mBitmap == null)
            {
                mBitmap =  Bitmap.createBitmap (mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);;
            }
            Canvas canvas = new Canvas(mBitmap);
            	mypath = getFile(CaptureSignature.this);
                FileOutputStream mFileOutStream = new FileOutputStream(mypath); 
                v.draw(canvas);
//                mBitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
                mBitmap.compress(Bitmap.CompressFormat.PNG, 100,mFileOutStream);    
                
                
// 		       intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, Uri.fromFile(image) ); 
                try {
                    Bitmap bitmapOrg = Media.getBitmap(getContentResolver(), Uri.fromFile(mypath));
//                Bitmap bitmapOrg =Media.getBitmap(android.provider.MediaStore.EXTRA_OUTPUT, Uri.fromFile(mypath));
                    ByteArrayOutputStream hrhk = new ByteArrayOutputStream();
                    bitmapOrg.compress(Bitmap.CompressFormat.PNG, 100, hrhk);
                    HRHK = hrhk.toByteArray();
                    bb = Base64.encode(HRHK);

                    byte[] converttobyte = bb.getBytes();
                    s1 = bb.replace("\n", "").replace("\r", "").replaceAll(" ", "");
                    s1_datetime = showdate + showtime;
                    mFileOutStream.flush();
                    mFileOutStream.close();
                    deleteLatest();
                    delete_older();
                }catch (Exception e) {
                    
                }
//                String url = Images.Media.insertImage(getContentResolver(), mBitmap, "title", null);
               
                //In case you want to delete the file
                //boolean deleted = mypath.delete();
                //Log.v("log_tag","deleted: " + mypath.toString() + deleted);
                //If you want to convert the image to string use base64 converter
 
            }
            catch(Exception e)
            {
//                Log.v("log_tag", e.toString());
            	Crashlytics.log(android.util.Log.ERROR,TAG,"Exception captureSignature save " + e.getMessage());
            }
        	catch(UnsatisfiedLinkError err)
        	{
        		err.getStackTrace();
        		Crashlytics.log(android.util.Log.ERROR,TAG,"Error CaptureSignature UnsatisfiedLinkError ");
//        		finish();
        	}
        }
 
        public void clear()
        {
            path.reset();
            invalidate();
        }
 
        @Override
        protected void onDraw(Canvas canvas)
        {
            canvas.drawPath(path, paint);
        }
 
        @Override
        public boolean onTouchEvent(MotionEvent event)
        {
            float eventX = event.getX();
            float eventY = event.getY();
            mGetSign.setEnabled(true);
 
            switch (event.getAction())
            {
            case MotionEvent.ACTION_DOWN:
                path.moveTo(eventX, eventY);
                lastTouchX = eventX;
                lastTouchY = eventY;
                return true;
 
            case MotionEvent.ACTION_MOVE:
 
            case MotionEvent.ACTION_UP:
 
                resetDirtyRect(eventX, eventY);
                int historySize = event.getHistorySize();
                for (int i = 0; i < historySize; i++)
                {
                    float historicalX = event.getHistoricalX(i);
                    float historicalY = event.getHistoricalY(i);
                    expandDirtyRect(historicalX, historicalY);
                    path.lineTo(historicalX, historicalY);
                }
                path.lineTo(eventX, eventY);
                break;
 
            default:
                debug("Ignored touch event: " + event.toString());
                return false;
            }
 
            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));
 
            lastTouchX = eventX;
            lastTouchY = eventY;
 
            return true;
        }
 
        private void debug(String string){
        }
 
        private void expandDirtyRect(float historicalX, float historicalY)
        {
            if (historicalX < dirtyRect.left)
            {
                dirtyRect.left = historicalX;
            }
            else if (historicalX > dirtyRect.right)
            {
                dirtyRect.right = historicalX;
            }
 
            if (historicalY < dirtyRect.top)
            {
                dirtyRect.top = historicalY;
            }
            else if (historicalY > dirtyRect.bottom)
            {
                dirtyRect.bottom = historicalY;
            }
        }
 
        private void resetDirtyRect(float eventX, float eventY)
        {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }
}
