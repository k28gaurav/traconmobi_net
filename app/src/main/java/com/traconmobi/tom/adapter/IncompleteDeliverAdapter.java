package com.traconmobi.tom.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.couchbase.lite.util.Log;
import com.traconmobi.tom.AssgnDtlsMainActivity;
import com.traconmobi.tom.AssignedMultipleDelActivity;
import com.traconmobi.tom.PhoneCall;
import com.traconmobi.tom.R;
import com.traconmobi.tom.SessionManager;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

/**
 * Created by kumargaurav on 6/27/16.
 */
public class IncompleteDeliverAdapter extends BaseAdapter {
    Context mContext;
    LayoutInflater inflater;
    Activity activity;
    private List<ItemRow> numberlist = null;
    protected int count;
    SessionManager session;
    String cod=null;
    String str_amt = null ;
    double total_cod =0;
    int total_awbs;

    public IncompleteDeliverAdapter(Activity activity,Context ctx,List<ItemRow> rows) {
        mContext = ctx;
        numberlist = rows;
        this.activity = activity;
        session = new SessionManager(ctx);
    }

    public class ViewHolder {
        TextView txtordrval,txtrs,txtrsval,txtName,cod,txtADDR,txtreason;
        ImageView map, call, email;

    }
    @Override
    public int getCount() {
        return numberlist.size();
    }

    @Override
    public ItemRow getItem(int position) {
        return numberlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.undelivery_list_items, null);

            // Locate the TextView in listview_item.xml
            holder.txtordrval = (TextView) view.findViewById(R.id.txtordrval);
            holder.txtADDR = (TextView) view.findViewById(R.id.txtADDR);
            holder.cod = (TextView) view.findViewById(R.id.cod);
            holder.txtName= (TextView) view.findViewById(R.id.txtName);
            holder.txtrs = (TextView) view.findViewById(R.id.txtrs);
            holder.txtrsval = (TextView) view.findViewById(R.id.txtrsval);
            holder.map = (ImageView)view.findViewById(R.id.btnmap);
            holder.call = (ImageView)view.findViewById(R.id.btncall);
            holder.email = (ImageView)view.findViewById(R.id.btnmsg);
            holder.txtreason = (TextView)view.findViewById(R.id.txtreason);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextView
        holder.txtordrval.setText(numberlist.get(position).getAssignmentNo());
        holder.txtADDR.setText(numberlist.get(position).getAddressLine1());
        holder.txtrsval.setText(numberlist.get(position).getAmount());
        holder.cod.setText(numberlist.get(position).getAmountType());
        holder.txtName.setText(numberlist.get(position).getConsigneeName());
        holder.txtreason.setText(numberlist.get(position).getReason());

        // Listen for ListView Item Click
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // Send single item click data to SingleItemView Class
                Intent intent = new Intent(mContext, AssignedMultipleDelActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("EXTRA_MESSAGE", numberlist.get(position).getAssignmentNo());
                cod=numberlist.get(position).getAmount();
                intent.putExtra("FORM_TYPE4_VAL","FORM_MULTIPLE_UNDEL");
                session.createAssgnMultiple_num_Session(numberlist.get(position).getAssignmentNo());
                total_cod = total_cod + Double.valueOf(cod);
                NumberFormat formatter = new DecimalFormat("###.00");
                str_amt = formatter.format(total_cod);
                total_awbs = numberlist.size();
                session.createAssgnMultipleSession(Integer.toString(total_awbs), str_amt);
                mContext.startActivity(intent);
            }
        });

        holder.map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://maps.google.com/maps?daddr=" + numberlist.get(position).getAddressLine1()
                        + numberlist.get(position).getAddressLine2() + numberlist.get(position).getCity() + "&dirflg=r"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);

            }
        });

        holder.email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhoneCall phcl = new PhoneCall();
                phcl.call(numberlist.get(position).getContactNo(), activity);
            }
        });

        if (position % 2 == 0) {
            view.setBackgroundColor(Color.rgb(255, 255, 255));
        } else {
            view.setBackgroundColor(Color.rgb(255, 255, 255));
        }

        return view;
    }
}
