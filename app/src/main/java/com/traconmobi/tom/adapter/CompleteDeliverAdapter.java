package com.traconmobi.tom.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.traconmobi.tom.R;

import java.util.List;

/**
 * Created by kumargaurav on 7/13/16.
 */
public class CompleteDeliverAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    Activity activity;
    private List<ItemRow> numberlist = null;
    protected int count;

    public CompleteDeliverAdapter(Activity activity, Context ctx, List<ItemRow> rows) {
        mContext = ctx;
        numberlist = rows;
        this.activity = activity;
    }

    public class ViewHolder {
        TextView tv_awb,tv_firstName,tv_lastName,tv_address,tv_city,tv_pincode,tv_COD_Amount;
    }

    @Override
    public int getCount() {
        return numberlist.size();
    }

    @Override
    public ItemRow getItem(int position) {
            return numberlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;

        if (view == null) {
            holder = new ViewHolder();
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.delivered_cardview_layout, null);

            // Locate the TextView in listview_item.xml
            holder.tv_awb = (TextView) view.findViewById(R.id.tv_awb);
            holder.tv_firstName = (TextView) view.findViewById(R.id.tv_firstName);
            holder.tv_lastName = (TextView) view.findViewById(R.id.tv_lastName);
            holder.tv_address= (TextView) view.findViewById(R.id.tv_address);
            holder.tv_city = (TextView) view.findViewById(R.id.tv_city);
            holder.tv_pincode = (TextView) view.findViewById(R.id.tv_pincode);
            holder.tv_COD_Amount = (TextView)view.findViewById(R.id.tv_COD_Amount);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextView
        holder.tv_awb.setText(numberlist.get(position).getAssignmentNo());
        holder.tv_address.setText(numberlist.get(position).getAddressLine1());
        holder.tv_firstName.setText(numberlist.get(position).getFirstname());
        holder.tv_lastName.setText(numberlist.get(position).getLastname());
        holder.tv_city.setText(numberlist.get(position).getCity());
        holder.tv_pincode.setText(numberlist.get(position).getPincode());
        holder.tv_COD_Amount.setText(numberlist.get(position).getAmount());
        return view;
    }
}
