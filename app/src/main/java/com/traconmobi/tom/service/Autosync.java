package com.traconmobi.tom.service;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.traconmobi.tom.ConnectionDetector;
import com.traconmobi.tom.ParseOutscanXmlResponse;
import com.traconmobi.tom.ParsePickUpReason_XmlResponse;
import com.traconmobi.tom.ParsePickupOutscanXML;
import com.traconmobi.tom.ParseUDReason_XmlResponse;
import com.traconmobi.tom.Parse_CompleteXmlResponse;
import com.traconmobi.tom.RequestCompanyReason;
import com.traconmobi.tom.SessionManager;
import com.traconmobi.tom.app.App;
import com.traconmobi.tom.http.OkHttpHandlerPost;
import com.traconmobi.tom.rest.model.Escan;
import com.traconmobi.tom.PostRequest;
import com.traconmobi.tom.rest.model.TokenParser;
import com.traconmobi.tom.singeleton.UserParser;

import org.kobjects.base64.Base64;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import io.fabric.sdk.android.Fabric;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static com.traconmobi.tom.HomeMainActivity.session_CUST_ACC_CODE;
import static com.traconmobi.tom.HomeMainActivity.session_USER_NAME;

/**
 * Created by kumargaurav on 6/3/16.
 */

public class Autosync extends IntentService {
    private static final String TAG = "Autosync";
    private SessionManager session;
    private Boolean isInternetPresent;
    //ResultReceiver resultReceiver;

    static File image;
    private String selectedImagePath;
    byte [] BAvalue;
    public String getphotobytearray,s_photo;

    private final String URL_outscan = "http://traconmobi.net/Assignment-Details";
    private final String URL_get_ud_resn = "http://traconmobi.net/Company-Reasons";
    protected SQLiteDatabase db_undel,db_undel_resp1,db_undel_resp2,db_del_complete,db_del_complete_resp1,
            db_del_complete_resp2;
    public String rel_id,cod_amt,ud_reason_id,ud_loc_id;

    long dt,dt_del;

    protected Cursor cursor_undel,c_undel_sync_resp1,c_undelsync_resp1,c_undel_sync_resp2,
            c_undelsync_resp2,cursor_del_complete,c_del_complete_sync_resp1,c_del_complete_sync_resp2;
    public String session_DB_PATH,
            session_DB_PWD,session_user_id,session_user_pwd,session_USER_LOC,
            session_USER_NUMERIC_ID,session_CURRENT_DT,session_CUST_ACC_CODE,session_USER_NAME;
    public static String awb_id,awb_delnum,isdel,del_dt,del_tm,rec_by,rec_status,
            sign_path,get_id,get_awbid,acquired_lat,acquired_long;

    public static String strFilter,awb_numb,
            photo_path;
    public static String ud_reason,ud_date,ud_time,awb_id1,
            remarks,rmks,Acq_lat,Acq_long;

    public String del_awb_id,collectd_amt,assigned_amt,cod_amt_coltd;
    public String u_reason,u_time,Assgn_type,t_u_id,Assgn_flag;
    public String session_USER_PSWD,undel_awbid,undel_awb_id;
    public String session_sync_undel_resp,session_sync_del_resp;

    public String undel_cust_acc , photo_id_dt ,photo_id_tme,sign_dt_tm,del_cust_acc;
    int syncdelcnt =0,syncundelcnt= 0;

    private final String URL_UDDtls = "http://traconmobi.net/podupdate";
    private final String URL_DELDtls = "http://traconmobi.net/podupdate";
    private final String URL_DEL_IMAGES_Dtls = "http://traconmobi.net/Upload-Image";



    public Autosync(String name) {
        super(name);
    }

    public Autosync() {
        super("");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Log.e(TAG, "onCreate Called");
        if(session == null) {
            session = new SessionManager(getApplicationContext());
        }

        //**When ever you want to check Internet Status in your application call isConnectingToInternet()
        // function and it will return true or false***/
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
//    	   		Boolean isInternetPresent = false;
        isInternetPresent = cd.isConnectingToInternet(); // true or false
        // get AuthenticateDb data from session
        HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

        // DB_PATH
        session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

        // DB_PWD
        session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);


        // get user data from session
        HashMap<String, String> login_Dts = session.getLoginDetails();
        HashMap<String, String> user = session.getUserDetails();

        // Userid
        session_user_id = login_Dts.get(SessionManager.KEY_UID);

        // pwd
        session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);

        try {
            session_CUST_ACC_CODE = user.get(SessionManager.KEY_CUST_ACC_CODE);
            session_USER_NAME = user.get(SessionManager.KEY_USER_NAME);
            session_CURRENT_DT = user.get(SessionManager.KEY_CURRENT_DT);
            session_USER_LOC = session.getLocId();
            session_USER_NUMERIC_ID = session.getuserId();
            session_USER_PSWD = login_Dts.get(SessionManager.KEY_PWD);
        }catch(NullPointerException ex) {

        }catch (Exception e) {

        }

    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        Log.e(TAG, "onStart Called");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand Called");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /*Bundle bundle = new Bundle();
        bundle.putString("end", "Shipment list created");
        resultReceiver.send(200, bundle);*/
        Log.e(TAG, "onDestroy Called");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if(isInternetPresent) {
            try {

                //   if (parser != null) {
                try {
                    RequestBody formBody = new FormBody.Builder()
                            .add("username", session_USER_NAME)
                            .add("password", session_USER_PSWD)
                            .add("iemi", session.getKeyIMEI())
                            .add("timezone", session.getTimezone())
                            .add("company_id", session_CUST_ACC_CODE)
                            .add("token", session.getKeyToken())
                            .build();

                    new PostRequest(new PostRequest.AsyncResponse() {
                        @Override
                        public void processFinish(String output) {

                            if (output != null && output != "") {
                                /**parser used for del outscan**/
                                ParseOutscanXmlResponse flightListParser = new ParseOutscanXmlResponse(output, getApplicationContext());
                                try {
                                    flightListParser.parse();
                                } catch (XmlPullParserException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                ParsePickupOutscanXML pickupListParser = new ParsePickupOutscanXML(output.replaceAll("[^\\x20-\\x7e]", ""),
                                        getApplicationContext());
                                try {
                                    pickupListParser.parseXml(output.replaceAll("[^\\x20-\\x7e]", ""), getApplicationContext());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }, formBody, URL_outscan).doInBackground();

                } catch (Exception e) {
                }

                try {
                    RequestBody formBody = new FormBody.Builder()
                            .add("iemi", session.getKeyIMEI())
                            .add("timezone", session.getTimezone())
                            .add("company_id", session_CUST_ACC_CODE)
                            .add("token", session.getKeyToken())
                            .build();
                    new RequestCompanyReason(new RequestCompanyReason.AsyncResponse() {
                        @Override
                        public void processFinish(String output) {
                            if (output != null && output != "") {
                                /**parser used for ud reasons **/

                                ParseUDReason_XmlResponse HoldReasonListParser = new ParseUDReason_XmlResponse(output,
                                        getApplicationContext());
                                try {
                                    HoldReasonListParser.parse();

                                } catch (XmlPullParserException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                                /*----------------- ----parser used for PickUp cancel reason----------------*/
                                ParsePickUpReason_XmlResponse PickUpReasonListParser = new
                                        ParsePickUpReason_XmlResponse(output, getApplicationContext());
                                try {
                                    PickUpReasonListParser.parse();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }, formBody, URL_get_ud_resn).doInBackground();
                } catch (Exception e) {
                }

                try {
                    Date curDate = new Date();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                    final String mLastUpdateTime = dateFormat.format(curDate).toString();
                    Map<String, String> data1 = new HashMap<>();
                    data1.put("username", session_USER_NAME);
                    data1.put("password", session_user_pwd);
                    data1.put("iemi", session.getKeyIMEI());
                    data1.put("timezone", session.getTimezone());
                    data1.put("company_id", session_CUST_ACC_CODE);
                    data1.put("token", session.getKeyToken());
                    Call<Escan> call = App.getRestClient().getEcomService().getEcomService(data1);

                    call.enqueue(new Callback<Escan>() {
                        @Override
                        public void onResponse(Response<Escan> escan, Retrofit retrofit) {
                            if(escan.body() != null) {
                                int responseSize = escan.body().getData().size();
                                Set<String> pickUpset;

                                for (int i = 0; i < responseSize; i++) {
                                    if (escan.body().getData().get(i).getORDERDATE().contains(mLastUpdateTime) &&
                                            String.valueOf(escan.body().getData().get(i).getCOMPANYID()).contains
                                                    (session_CUST_ACC_CODE)
                                            && String.valueOf(escan.body().getData().get(i).getUSERID()).
                                            equals(session.getuserId())
                                            && String.valueOf(escan.body().getData().get(i).getLOCATION()).
                                            equals(session.getLocId())) {
                                        pickUpset = new HashSet<String>(escan.body().getData().get(i).getSCANITEM());
                                        session.setScanRef(escan.body().getData().get(i).getASSIGNMENTNO(), pickUpset);
                                        session.setDocType(escan.body().getData().get(i).getASSIGNMENTNO(),
                                                escan.body().getData().get(i).getDOCTYPE());
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            Log.e("Error : ", "" + t.getLocalizedMessage());
                        }
                    });
                } catch (Exception e) {
                }


                //***********************STEP 2 GETTING ALL THE DELIVERIES TO SYNC TO CENTRAL DB FROM LOCAL DB **********************/
                //***********************OPEN DATABASE TO PERFORM READ/WRITE OPERATION***********************/
                try {
                    if (session_DB_PATH != null && session_DB_PWD != null) {
//    		db_del_complete=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                        db_del_complete = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                        Assgn_flag = "TRUE";
                        Assgn_type = "D";
//    		 cursor_del_complete = db_del_complete.rawQuery("SELECT T_OUT_Scan_U_ID,T_Assignment_Id,T_Assignment_Number,B_is_Completed,F_Amount_Collected,D_Completed_Date,D_Completed_time,T_Received_by_Collected_from,T_Relationship,T_Signature,T_Photo,T_AcquiredLat,T_AccquiredLon,C_is_Sync FROM TOM_Assignments where B_is_Completed='" + Assgn_flag + "' and T_Assignment_Type = '" + Assgn_type + "' and C_is_Sync IS NOT 1 LIMIT 100",null);
                        cursor_del_complete = db_del_complete.rawQuery("SELECT emp.T_OUT_Scan_U_ID,emp.T_Out_Scan_Location,emp.T_Receiver_Contact_Num ,emp.T_Assignment_Id, emp.T_Assignment_Number, emp.B_is_Completed,emp.F_Amount ,emp.F_Amount_Collected, emp.D_Completed_Date, emp.D_Completed_time, emp.T_Received_by_Collected_from, emp.T_Relationship, emp.T_Signature, emp.T_Photo, emp.T_AcquiredLat, emp.T_AccquiredLon, emp.T_Signature_dt, emp.T_Cust_Acc_NO, emp.C_is_Sync,mgr.T_RELATION_TYP,mgr.T_RELATION_ID  FROM  TOM_Assignments  emp JOIN TBL_Relation_mstr  mgr ON mgr.T_RELATION_TYP = emp.T_Relationship where  emp.B_is_Completed='" + Assgn_flag + "' and  emp.T_Assignment_Type = '" + Assgn_type + "' and C_is_Sync IS NOT 1", null);
                        //	new String[]{""+employeeId});

                        syncdelcnt = cursor_del_complete.getCount();
                        while (cursor_del_complete.moveToNext()) {
                            t_u_id = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Out_Scan_Location"));
                            awb_id = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Assignment_Id"));
                            awb_delnum = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Assignment_Number"));
                            isdel = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("B_is_Completed"));
//							is_cod_coltd="0";
                            cod_amt = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("F_Amount"));
                            assigned_amt = cod_amt.replace(".", "_");
                            cod_amt_coltd = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("F_Amount_Collected"));

                            collectd_amt = cod_amt_coltd.replace(".", "_");
                            Crashlytics.log(android.util.Log.ERROR, TAG, "print amount" + cod_amt_coltd + "collectd_amt" + collectd_amt + "assigned_amt" + assigned_amt);
                            del_dt = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("D_Completed_Date"));

                            del_tm = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("D_Completed_time"));
                            String del_tme = del_tm.replace(":", "_");
                            rec_by = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Received_by_Collected_from"));
                            String recvd_by = rec_by.replace(" ", "%20");
                            String rvr_contact_numbr = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Receiver_Contact_Num"));
                            rec_status = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Relationship"));
                            rel_id = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_RELATION_ID"));
                            sign_path = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Signature"));
                            photo_path = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Photo"));
                            acquired_lat = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_AcquiredLat"));
                            acquired_long = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_AccquiredLon"));
                            sign_dt_tm = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Signature_dt"));
                            del_cust_acc = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Cust_Acc_NO"));

                            if (acquired_lat != null && acquired_lat != "" && !acquired_lat.equals("null")) {

                                acquired_lat = acquired_lat;
                            } else if (acquired_lat == null && acquired_lat == "" && acquired_lat.equals("null")) {
                                acquired_lat = "null";
                            }
                            if (acquired_long != null && acquired_long != "" && !acquired_long.equals("null")) {

                                acquired_long = acquired_long;
                            } else if (acquired_long == null && acquired_long == "" && acquired_long.equals("null")) {
                                acquired_long = "null";
                            }
                            if (photo_id_dt != null && photo_id_dt != "" && !(photo_id_dt.equals("null"))) {
                                photo_id_dt = photo_id_dt;
                            } else if (photo_id_dt == null || photo_id_dt == "" || photo_id_dt.equals("null")) {
                                photo_id_dt = "null";
                            }

                            if (photo_id_tme != null && photo_id_tme != "" && !(photo_id_tme.equals("null"))) {
                                photo_id_tme = photo_id_tme;
                            } else if (photo_id_tme == null || photo_id_tme == "" || photo_id_tme.equals("null")) {
                                photo_id_tme = "null";
                            }

                            if (photo_path != null && photo_path != "" && !(photo_path.equals("null"))) {
                                image = new File(Environment.getExternalStorageDirectory(), "Image keeper/" + photo_path.trim());
//				    selectedImagePath =android.os.Environment.getExternalStorageDirectory().toString()+"/Image keeper/"+ photo_path.trim();
                                image = new File(Environment.getExternalStorageDirectory()
                                        + "/Android/data/com.traconmobi.net/", "Image keeper/" + photo_path.trim());
                                selectedImagePath = android.os.Environment.getExternalStorageDirectory()
                                        + "/Android/data/com.traconmobi.net" + "/Image keeper/" + photo_path.trim();
                                Bitmap bm = reduceImageSize(selectedImagePath);

                                if (bm != null) {

                                    ByteArrayOutputStream BAO = new ByteArrayOutputStream();

                                    bm.compress(Bitmap.CompressFormat.JPEG, 40, BAO);

                                    BAvalue = BAO.toByteArray();
                                    getphotobytearray = Base64.encode(BAvalue);
                                    Log.e(TAG, "getphotobytearray:" + getphotobytearray);
                                    s_photo = getphotobytearray.replace("+", "%2B");
//				    Crashlytics.log(android.util.Log.ERROR,TAG,"s_photo " + s_photo);
                                } else if (bm == null) {
                                    getphotobytearray = "null";
                                }
                            } else if (photo_path == null && photo_path == "" && photo_path.equals("null")) {
                                photo_path = "null";
                            }
                            try {
                                DateFormat formatter_date = new SimpleDateFormat("yyyy-MM-dd");
                                formatter_date.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                                Date date1 = (Date) formatter_date.parse(del_dt);
                                long dt = date1.getTime() / 1000;
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Today is " + date1.getTime() / 1000 + "dt" + dt);
                                int reqId_DEL = 104;
//									String requestParameter_del="0";//"/"+opsggnpc+"/"+sample+"/"+a2lfrq40kdh0nrut1vl0ani4a2+"/"+142+"/"+1000002292+"/"+UNDEL+"/"+4670+"/"+0+"/"+0+"/"+1447612200+"/"+15_58_9+"/"+null+"/"+null+"/"+null+"/"+5850+"/"+U+"/"+null+"/"+ URLEncoder.encode(token);
                                String status = "DEL";
                                String requestParameter_del = "/" + session.getuserId() + "/" + session_user_pwd + "/" + session.getKeyToken() + "/" + session_CUST_ACC_CODE + "/" + awb_id + "/" + status + "/" + "null" + "/" + assigned_amt + "/" + collectd_amt + "/" + dt + "/" + del_tme + "/" + recvd_by + "/" + rel_id + "/" + rvr_contact_numbr + "/" + t_u_id + "/" + "D" + "/" + acquired_lat + "/" + acquired_long + "/" + "null" + "/" + session.getKeyIMEI();
                                Crashlytics.log(android.util.Log.ERROR, TAG, "print requestParameter_del" + requestParameter_del);
                                try {

                                    RequestBody formBody = new FormBody.Builder()
                                            .add("user_id", session.getuserId())
                                            .add("timezone", session.getTimezone())
                                            .add("imei", session.getKeyIMEI())
                                            .add("company_id", session_CUST_ACC_CODE)
                                            .add("token", session.getKeyToken())
                                            .add("assignmentid", awb_id)
                                            .add("status",status)
                                            .add("reason_id","NA")
                                            .add("cod_amount", assigned_amt )
                                            .add("cod_amount_collected", collectd_amt)
                                            .add("date",String.valueOf(dt))
                                            .add("time", del_tme)
                                            .add("received_by",recvd_by)
                                            .add("relationship", rel_id)
                                            .add("contact_no", rvr_contact_numbr.trim())
                                            .add("location_id", t_u_id)
                                            .add("transaction_for", "D")
                                            .add("latitude", (acquired_lat != null) ? acquired_lat:"NA")
                                            .add("longitude", (acquired_long != null) ? acquired_long:"NA")
                                            .add("remarks", "NA")
                                            .build();
                                    OkHttpHandlerPost handler_UD = new OkHttpHandlerPost(formBody, URL_DELDtls, status, session_DB_PATH, session_DB_PWD);
                                    session_sync_undel_resp = handler_UD.sendRequest();
                                }catch (Exception e) {
                                    Log.e(TAG, "Exception in fom body"  + e.getMessage());
                                }

                                status = "photo";
                                //uploading Image and signature
                                RequestBody formdata = new FormBody.Builder()
                                        .add("Assign_ID", awb_id)
                                        .add("Signature", sign_path)
                                        .add("photo", (getphotobytearray != null) ? getphotobytearray:"NA")
                                        .add("locid", t_u_id)
                                        .add("imei",session.getKeyIMEI())
                                        .add("companyid", del_cust_acc)
                                        .add("token", session.getKeyToken())
                                        .add("timezone", session.getTimezone())
                                        .build();

                                OkHttpHandlerPost handler_photo = new OkHttpHandlerPost(formdata, URL_DEL_IMAGES_Dtls, status, session_DB_PATH, session_DB_PWD);
                                String session_sync_del_photoresp = handler_photo.sendRequest();

                                if (session_sync_del_photoresp != null && session_sync_del_photoresp != "") {
//    				            	ParsePOD_UDXmlResponse POD_UD_parser = new ParsePOD_UDXmlResponse(session_sync_del_resp);
                                    Parse_CompleteXmlResponse Complete_parser = new Parse_CompleteXmlResponse(session_sync_del_photoresp);
                                    try {
                                        Complete_parser.parse();
                                    } catch (XmlPullParserException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }

                                    if (Complete_parser.getparsedCompleteList().isEmpty()) {
                                        try {
                                            if (session_DB_PATH != null && session_DB_PWD != null) {
                                                //    				            		   db_del_complete_resp1=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                                db_del_complete_resp1 = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                                del_awb_id = Parse_CompleteXmlResponse.ASSIGNID;
                                                String sync_status = Parse_CompleteXmlResponse.IS_SYNCED;
                                                if (del_awb_id != null || del_awb_id != "") {
                                                    c_del_complete_sync_resp1 = db_del_complete_resp1.rawQuery("UPDATE TOM_Assignments SET C_is_Sync='" + sync_status + "' WHERE T_Assignment_Id = ?", new String[]{"" + del_awb_id});
                                                    while (c_del_complete_sync_resp1.moveToNext()) {
                                                    }
                                                    //    					          		 	if(c_del_complete_sync_resp1 != null)
                                                    //    					          		 	{
                                                    //    					          		 		c_del_complete_sync_resp1.close();
                                                    //    					          		 	}
                                                }
                                                //    					            	   	db_del_complete_resp1.close();
                                            } else {
                                                Crashlytics.log(android.util.Log.ERROR, TAG, "error : step4 HomeMainActivity delsync");
                                                //onClickLogOut();
                                            }
                                        } catch (Exception e) {
                                            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Expense " + e.getMessage());
                                        } finally {
                                            if (c_del_complete_sync_resp1 != null && !c_del_complete_sync_resp1.isClosed()) {
                                                c_del_complete_sync_resp1.close();
                                            }
                                            if (db_del_complete_resp1 != null && db_del_complete_resp1.isOpen()) {
                                                db_del_complete_resp1.close();
                                            }
                                        }
//    				          		 syncdelcnt=cursor.getCount();
                                    } else if (!(Complete_parser.getparsedCompleteList().isEmpty())) {
                                        try {
                                            if (session_DB_PATH != null && session_DB_PWD != null) {
                                                //    				            		   db_del_complete_resp2=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                                db_del_complete_resp2 = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                                del_awb_id = Parse_CompleteXmlResponse.ASSIGNID;
                                                String sync_status = Parse_CompleteXmlResponse.IS_SYNCED;
                                                //    					            	   Crashlytics.log(android.util.Log.ERROR,TAG,"delsync "+ pod_awb);
                                                if (del_awb_id != null || del_awb_id != "") {
                                                    c_del_complete_sync_resp2 = db_del_complete_resp2.rawQuery("UPDATE TOM_Assignments SET C_is_Sync='" + sync_status + "'  WHERE T_Assignment_Id = ?", new String[]{"" + del_awb_id});
                                                    while (c_del_complete_sync_resp2.moveToNext()) {
                                                    }
//	    					            	   if(c_del_complete_sync_resp2 != null)
//	    					            	   {
//	    					            		   c_del_complete_sync_resp2.close();
//	    					            	   }
                                                }
//	    					            	 db_del_complete_resp2.close();
                                            } else {
                                                Crashlytics.log(android.util.Log.ERROR, TAG, "error : step5 HomeMainActivity delsync ");
                                                //onClickLogOut();
                                            }
                                        } catch (Exception e) {
                                            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Expense " + e.getMessage());
                                        } finally {
                                            if (c_del_complete_sync_resp2 != null && !c_del_complete_sync_resp2.isClosed()) {
                                                c_del_complete_sync_resp2.close();
                                            }
                                            if (db_del_complete_resp2 != null && db_del_complete_resp2.isOpen()) {
                                                db_del_complete_resp2.close();
                                            }
                                        }
//    				       		    syncdelcnt=cursor.getCount();
                                    }

                                    // ret = ret + syncdelcnt;
                                } else {
                                    //do nothing
                                    Crashlytics.log(android.util.Log.ERROR, TAG, "session_sync_del_resp is null");
                                }
//								}
                            } catch (Exception e) {
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception session_sync_del_resp is null" + e.getMessage());
                            }
//    						}
//    					 }).start();

                        }
                    } else {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "HomeMainActivity delivery session_DB_PATH is null");
                        //onClickLogOut();
                    }
                } catch (Exception e) {
                    Crashlytics.log(android.util.Log.ERROR, TAG, "HomeMainActivity delivery Exception" + e.getMessage());
                } finally {
                    if (cursor_del_complete != null && !(cursor_del_complete.isClosed())) {
                        cursor_del_complete.close();
                    }
                    if (db_del_complete != null && db_del_complete.isOpen()) {
                        db_del_complete.close();
                    }
                }


                //*********************** STEP 3 GETTING ALL THE UNDELIVERIES TO SYNC TO CENTRAL DB FROM LOCAL DB **********************/
                try {
                    if (session_DB_PATH != null && session_DB_PWD != null) {

                        db_undel = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                        Assgn_type = "D";
//		cursor_undel = db_undel.rawQuery("SELECT T_U_ID,T_Assignment_Id,T_Assignment_Number,T_Assignment_Type,T_Remarks,T_Photo,T_Assignment_In_Complete_Reason,D_In_Complete_Date,D_In_Complete_Time,T_AccquiredLat,T_AccquiredLon,C_is_Sync FROM Tom_Assignments_In_Complete where T_Assignment_Type = '" + Assgn_type + "' AND  C_is_Sync IS NOT 1 LIMIT 100",null);
                        cursor_undel = db_undel.rawQuery("SELECT T_U_ID,T_Assignment_Id,T_Assignment_Number,T_Assignment_Type,T_Remarks,T_Photo,T_Photo_dt,T_Photo_tm,T_Assignment_In_Complete_Reason,T_Assignment_In_Complete_Reason_Id,D_In_Complete_Date,D_In_Complete_Time,T_AccquiredLat,T_AccquiredLon,T_Cust_Acc_NO,T_Out_Scan_Location,C_is_Sync FROM Tom_Assignments_In_Complete where T_Assignment_Type = '" + Assgn_type + "' AND  C_is_Sync IS NOT 1", null);
                        syncundelcnt = cursor_undel.getCount();
                        while (cursor_undel.moveToNext()) {
                            t_u_id = cursor_undel.getString(cursor_undel.getColumnIndex("T_U_ID"));
                            awb_id1 = cursor_undel.getString(cursor_undel.getColumnIndex("T_Assignment_Id"));
                            awb_numb = cursor_undel.getString(cursor_undel.getColumnIndex("T_Assignment_Number"));
                            ud_reason = cursor_undel.getString(cursor_undel.getColumnIndex("T_Assignment_In_Complete_Reason"));
                            ud_reason_id = cursor_undel.getString(cursor_undel.getColumnIndex("T_Assignment_In_Complete_Reason_Id"));
                            u_reason = ud_reason.replace(" ", "%20");
                            ud_date = cursor_undel.getString(cursor_undel.getColumnIndex("D_In_Complete_Date"));
                            ud_time = cursor_undel.getString(cursor_undel.getColumnIndex("D_In_Complete_Time"));
//							u_time=ud_time.replace(" ", "%20");
                            u_time = ud_time.replace(":", "_");
                            ud_loc_id = cursor_undel.getString(cursor_undel.getColumnIndex("T_Out_Scan_Location"));
                            remarks = cursor_undel.getString(cursor_undel.getColumnIndex("T_Remarks"));
                            rmks = remarks.replace(" ", "%20");
                            photo_path = cursor_undel.getString(cursor_undel.getColumnIndex("T_Photo"));
                            undel_cust_acc = cursor_undel.getString(cursor_undel.getColumnIndex("T_Cust_Acc_NO"));
                            photo_id_dt = cursor_undel.getString(cursor_undel.getColumnIndex("T_Photo_dt"));
                            photo_id_tme = cursor_undel.getString(cursor_undel.getColumnIndex("T_Photo_tm"));

                            Acq_lat = cursor_undel.getString(cursor_undel.getColumnIndex("T_AccquiredLat"));
                            Acq_long = cursor_undel.getString(cursor_undel.getColumnIndex("T_AccquiredLon"));
                            if (Acq_lat == null && Acq_lat == "" && Acq_lat.equals("null")) {
                                Acq_lat = "null";
                            }
                            if (Acq_long == null && Acq_long == "" && Acq_long.equals("null")) {
                                Acq_long = "null";
                            }

                            if (photo_id_dt != null && photo_id_dt != "" && !(photo_id_dt.equals("null"))) {
                                photo_id_dt = photo_id_dt;
                            } else if (photo_id_dt == null || photo_id_dt == "" || photo_id_dt.equals("null")) {
                                photo_id_dt = "null";
                            }

                            if (photo_id_tme != null && photo_id_tme != "" && !(photo_id_tme.equals("null"))) {
                                photo_id_tme = photo_id_tme;
                            } else if (photo_id_tme == null || photo_id_tme == "" || photo_id_tme.equals("null")) {
                                photo_id_tme = "null";
                            }

                            if (photo_path != null && photo_path != "" && !(photo_path.equals("null"))) {
                                photo_path = photo_path;
                            } else if (photo_path == null && photo_path == "" && photo_path.equals("null")) {
                                photo_path = "null";
                            }

                            try {
                                /******************ASyncronous call of the webservice********************/
                                DateFormat formatter_date = new SimpleDateFormat("yyyy-MM-dd");
                                formatter_date.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                                Date date1 = (Date) formatter_date.parse(ud_date);
                                long dt = date1.getTime() / 1000;
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Today is " + date1.getTime() / 1000 + "dt" + dt);

                                int reqId_UD = 105;
                                String status = "UNDEL";
                                String requestParameter = "/" + t_u_id + "/" + session_user_pwd + "/" + session.getKeyToken() + "/" + session_CUST_ACC_CODE + "/" + awb_id1 + "/" + status + "/" + ud_reason_id + "/" + "0" + "/" + "0" + "/" + dt + "/" + u_time + "/" + "null" + "/" + "null" + "/" + "null" + "/" + ud_loc_id + "/" + "U" + "/" + Acq_lat + "/" + Acq_long + "/" + rmks + "/" + session.getKeyIMEI();

                                Crashlytics.log(android.util.Log.ERROR, TAG, "print undel parameter" + requestParameter);

                                RequestBody formBody = new FormBody.Builder()
                                        .add("user_id", t_u_id)
                                        .add("timezone", session.getTimezone())
                                        .add("imei", session.getKeyIMEI())
                                        .add("company_id", session_CUST_ACC_CODE)
                                        .add("token", session.getKeyToken())
                                        .add("assignmentid", awb_id1)
                                        .add("status",status)
                                        .add("reason_id",ud_reason_id)
                                        .add("cod_amount", String.valueOf(0))
                                        .add("cod_amount_collected", String.valueOf(0))
                                        .add("date",String.valueOf(dt))
                                        .add("time",u_time)
                                        .add("received_by","NA")
                                        .add("relationship", "NA")
                                        .add("contact_no", "NA")
                                        .add("location_id", ud_loc_id)
                                        .add("transaction_for", "U")
                                        .add("latitude", (Acq_lat != null) ? Acq_lat:"NA")
                                        .add("longitude", (Acq_long != null) ? Acq_long:"NA")
                                        .add("remarks", rmks)
                                        .build();

                                OkHttpHandlerPost handler_UD = new OkHttpHandlerPost(formBody, URL_UDDtls, status, session_DB_PATH, session_DB_PWD);
                                session_sync_undel_resp = handler_UD.sendRequest();


                                Crashlytics.log(android.util.Log.ERROR, TAG, "print session_sync_undel_resp" + session_sync_undel_resp);
                                if (session_sync_undel_resp != null && session_sync_undel_resp != "") {

//	        	ParsePOD_UDXmlResponse POD_UD_parser = new ParsePOD_UDXmlResponse(session_sync_undel_resp);
                                    Parse_CompleteXmlResponse UD_parser = new Parse_CompleteXmlResponse(session_sync_undel_resp);
                                    try {
                                        UD_parser.parse();
                                    } catch (XmlPullParserException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }

                                    if (UD_parser.getparsedCompleteList().isEmpty()) {
                                        if (session_DB_PATH != null && session_DB_PWD != null) {
                                            try {
//			                	db_del_incomplete_resp1=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                                db_undel_resp1 = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                                undel_awb_id = Parse_CompleteXmlResponse.ASSIGNID;
                                                String sync_status = Parse_CompleteXmlResponse.IS_SYNCED;
                                                Crashlytics.log(android.util.Log.ERROR, TAG, "print undel_awb_id empty" + undel_awb_id);
                                                if (undel_awb_id != null || undel_awb_id != "") {
                                                    c_undel_sync_resp1 = db_undel_resp1.rawQuery("UPDATE Tom_Assignments_In_Complete SET C_is_Sync='" + sync_status + "' WHERE T_Assignment_Id='" + undel_awbid + "'", null);
//			                			new String[]{""+undel_awb_id});
                                                    while (c_undel_sync_resp1.moveToNext()) {

                                                    }
                                                    c_undelsync_resp1 = db_undel_resp1.rawQuery("UPDATE TOM_Assignments SET C_is_Sync='" + sync_status + "' WHERE T_Assignment_Id='" + undel_awbid + "'", null);
//			                			new String[]{""+undel_awb_id});
                                                    while (c_undelsync_resp1.moveToNext()) {

                                                    }
//			        	        if(c_undel_sync_resp1 !=null && !(c_undel_sync_resp1.isClosed()))
//			        	        {
//			        	        	c_undel_sync_resp1.close();
//			        	        }

                                                }
//			                	db_undel_resp1.close();
                                            } catch (Exception e) {
                                                Crashlytics.log(android.util.Log.ERROR, TAG, "HomeMainActivity undelsync isEmpty Exception" + e.getMessage());
                                            } finally {
                                                if (c_undel_sync_resp1 != null && !(c_undel_sync_resp1.isClosed())) {
                                                    c_undel_sync_resp1.close();
                                                }
                                                if (c_undelsync_resp1 != null && !(c_undelsync_resp1.isClosed())) {
                                                    c_undelsync_resp1.close();
                                                }
                                                if (db_undel_resp1 != null && db_undel_resp1.isOpen()) {
                                                    db_undel_resp1.close();
                                                }
                                            }
                                        } else {
                                            Crashlytics.log(android.util.Log.ERROR, TAG, "HomeMainActivity db_undel_resp1 session_DB_PATH is null");
                                            //onClickLogOut();
                                        }
//			        	        syncundelcnt=cr.getCount();
                                    } else if (!(UD_parser.getparsedCompleteList().isEmpty())) {
                                        if (session_DB_PATH != null && session_DB_PWD != null) {
                                            try {
//			                		db_del_incomplete_resp2=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                                db_undel_resp2 = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                                undel_awb_id = Parse_CompleteXmlResponse.ASSIGNID;
                                                String sync_status = Parse_CompleteXmlResponse.IS_SYNCED;
                                                Crashlytics.log(android.util.Log.ERROR, TAG, "print undel_awb_id not empty" + undel_awb_id);
                                                if (undel_awb_id != null || undel_awb_id != "") {
                                                    c_undel_sync_resp2 = db_undel_resp2.rawQuery("UPDATE Tom_Assignments_In_Complete SET C_is_Sync='" + sync_status + "' WHERE T_Assignment_Id='" + undel_awb_id + "'", null);
//				                			new String[]{""+undel_awb_id});
                                                    while (c_undel_sync_resp2.moveToNext()) {

                                                    }
                                                    c_undelsync_resp2 = db_undel_resp2.rawQuery("UPDATE TOM_Assignments SET C_is_Sync='" + sync_status + "' WHERE T_Assignment_Id='" + undel_awb_id + "'", null);
//			                			new String[]{""+undel_awb_id});
                                                    while (c_undelsync_resp2.moveToNext()) {

                                                    }
//				        	        if(c_undel_sync_resp2 !=null && !(c_undel_sync_resp2.isClosed()))
//				        	        {
//				        	        	c_undel_sync_resp2.close();
//				        	        }
                                                }
//				                	db_undel_resp2.close();
                                            } catch (Exception e) {
                                                Crashlytics.log(android.util.Log.ERROR, TAG, "HomeMainActivity undelsync isnotEmpty Exception" + e.getMessage());
                                            } finally {
                                                if (c_undel_sync_resp2 != null && !(c_undel_sync_resp2.isClosed())) {
                                                    c_undel_sync_resp2.close();
                                                }
                                                if (c_undelsync_resp2 != null && !(c_undelsync_resp2.isClosed())) {
                                                    c_undelsync_resp2.close();
                                                }
                                                if (db_undel_resp2 != null && db_undel_resp2.isOpen()) {
                                                    db_undel_resp2.close();
                                                }
                                            }
                                        } else {
                                            Crashlytics.log(android.util.Log.ERROR, TAG, "HomeMainActivity db_undel_resp2 session_DB_PATH is null ");
                                            //onClickLogOut();
                                        }
//			        	        syncundelcnt=cr.getCount();
                                    }

                                    // ret = ret + syncundelcnt;
                                } else {
                                    //do nothing
                                    Crashlytics.log(android.util.Log.ERROR, TAG, "HomeMainActivity session_sync_undel_resp is null");
                                }
                            } catch (Exception e) {
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception HomeMainActivity session_sync_undel_resp is null" + e.getMessage());
                            }
                        }

                    }

                } catch (Exception e) {
                    Crashlytics.log(android.util.Log.ERROR, TAG, "HomeMainActivity undelivry Exception" + e.getMessage());
                } finally {
                    if (cursor_undel != null && !(cursor_undel.isClosed())) {
                        cursor_undel.close();
                    }
                    if (db_undel != null && db_undel.isOpen()) {
                        db_undel.close();
                    }
                }
            } catch (SQLiteException e) {
                Crashlytics.log(android.util.Log.ERROR, TAG, "Error HomeMainActivity autosync SQLiteException ");

            }catch (Exception e) {
                // TODO Auto-generated catch block
                e.getMessage();
                e.printStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Error HomeMainActivity autosync Exception ");
            } catch (UnsatisfiedLinkError err) {
                err.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Error HomeMainActivity autosync UnsatisfiedLinkError ");
//    		onClickGoToHomePage();
            }



           /* }catch(Exception e) {
                Log.e(TAG, e.getMessage());
            }
            finally
            {
            }*/

        }
    }

   /* public void onEvent(TokenParser event){
        Log.e("", "onEvent Called");
        this.parser = event;

        // get AuthenticateDb data from session
        HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

        // DB_PATH
        session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

        // DB_PWD
        session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);


        // get user data from session
        HashMap<String, String> login_Dts = session.getLoginDetails();
        HashMap<String, String> user = session.getUserDetails();

        // Userid
        session_user_id = login_Dts.get(SessionManager.KEY_UID);

        // pwd
        session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);

        session_CURRENT_DT=user.get(SessionManager.KEY_CURRENT_DT);
        session_CUST_ACC_CODE=String.valueOf(event.getCompanyID());
        session_USER_LOC=event.getLocCode();
        session_USER_NUMERIC_ID= String.valueOf(event.getUserId());
        session_USER_PSWD=login_Dts.get(SessionManager.KEY_PWD);;
    }*/

    public Bitmap reduceImageSize(String selectedImagePath){

        Bitmap m = null;
        try {
            File f = new File(selectedImagePath);

            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            //The new size we want to scale to
            final int REQUIRED_SIZE=150;

            //Find the correct scale value. It should be the power of 2.
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2 < REQUIRED_SIZE || height_tmp/2 < REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale*=2;
            }

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            m = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
//            Toast.makeText(getApplicationContext(), "Image File not found in your phone. Please select another image.", Toast.LENGTH_LONG).show();
            Crashlytics.log(android.util.Log.ERROR, TAG, "FileNotFoundException reduceImageSize " + e.getMessage());
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception reduceImageSize" + e.getMessage());
        }
        return  m;
    }
}
