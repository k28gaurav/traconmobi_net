package com.traconmobi.tom.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.traconmobi.tom.CouchBaseDBHelper;
import com.traconmobi.tom.ParseOutscanXmlResponse;
import com.traconmobi.tom.ParsePickUpReason_XmlResponse;
import com.traconmobi.tom.ParsePickupOutscanXML;
import com.traconmobi.tom.ParseRelation_XmlResponse;
import com.traconmobi.tom.ParseUDReason_XmlResponse;
import com.traconmobi.tom.PostRelationRequest;
import com.traconmobi.tom.PostRequest;
import com.traconmobi.tom.RequestCompanyReason;
import com.traconmobi.tom.SessionManager;
import com.traconmobi.tom.app.App;
import com.traconmobi.tom.http.OkHttpHandlerPost;
import com.traconmobi.tom.model.Row;
import com.traconmobi.tom.model.Row1;
import com.traconmobi.tom.model.Row2;
import com.traconmobi.tom.model.Value;
import com.traconmobi.tom.model.Value1;
import com.traconmobi.tom.model.Value2;
import com.traconmobi.tom.rest.model.ApiResponse;
import com.traconmobi.tom.rest.model.ApiResponse1;
import com.traconmobi.tom.rest.model.ECOMParser;
import com.traconmobi.tom.rest.model.Escan;
import com.traconmobi.tom.rest.model.LocationParser;
import com.traconmobi.tom.rest.model.TokenParser;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.fabric.sdk.android.Fabric;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by kumargaurav on 6/5/16.
 */

public class InitializeCouchDb extends IntentService {
    private static final String TAG = "InitializeCouchDb";
    private TokenParser parser;
    private SessionManager session;
    private String password;
    private final String URL_get_relations_dtls = "http://traconmobi.net/Relations";
    private final String URL_get_ud_resn = "http://traconmobi.net/Company-Reasons";
    private final String URL_outscan = "http://traconmobi.net/Assignment-Details";
    private final String URL_gps_log = "http://traconmobi.net/UserTrack-History";
    public String session_DB_PATH, session_DB_PWD;

    public InitializeCouchDb(String name) {
        super(name);
    }

    public InitializeCouchDb() {
        super("");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Log.e(TAG, "onCreate Called");
        if(session == null) {
            session = new SessionManager(getApplicationContext());
        }
        HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

        // DB_PATH
        session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

        // DB_PWD
        session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        Log.e(TAG, "onStart Called");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand Called");

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy Called");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        parser = (TokenParser) intent.getSerializableExtra("parser");
        password = intent.getStringExtra("password");
        Log.e(TAG, password);
        if(parser != null) {

            final CouchBaseDBHelper dbHelper = new CouchBaseDBHelper(getApplicationContext());
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            Map<String, String> data = new HashMap<>();
            final String mLastUpdateTime = dateFormat.format(curDate).toString();
            data.put("startkey", "\"" + mLastUpdateTime + " 00:00:00" + "\"");

            try {
                Call<ApiResponse> call = App.getRestClient().getPickUpService().getPickUp(parser.getPickup(), data);
                call.enqueue(new Callback<ApiResponse>() {
                    @Override
                    public void onResponse(Response<ApiResponse> apiResponse, Retrofit retrofit) {
                        Log.e(TAG, "Parsing successful for Pickup");

                        if (!apiResponse.body().getRows().isEmpty()) {
                            Set<String> barcodeScan = new HashSet<>();
                            Set<String> setScanData = new HashSet<>();
                            int responseSize = apiResponse.body().getRows().size();
                            Log.e(TAG, "Response Size: " + responseSize);

                            for (int i = 0; i < responseSize; i++) {
                                if (apiResponse.body().getRows().get(i).getId() != null && apiResponse.body().getRows().get(i).getValue().getCompanyId() != null && apiResponse.body().getRows().get(i).getValue().getUID() != null && apiResponse.body().getRows().get(i).getValue().getLocID() != null) {
                                    Log.e(TAG, "Pickup Response body :" + apiResponse.body().getRows().get(i).getId() + ":"+ apiResponse.body().getRows().get(i).getValue().getCompanyId() + "," + String.valueOf(parser.getCompanyID()) + ":" + apiResponse.body().getRows().get(i).getValue().getUID() + "," + String.valueOf(parser.getUserId()) + ":" + apiResponse.body().getRows().get(i).getValue().getLocID() + "," + String.valueOf(parser.getLocID()) + ":" + apiResponse.body().getRows().get(i).getValue().getCompleteStatus());

                                    if (apiResponse.body().getRows().get(i).getId().contains(mLastUpdateTime) && apiResponse.body().getRows().get(i).getValue().getCompanyId().equals(String.valueOf(parser.getCompanyID())) && apiResponse.body().getRows().get(i).getValue().getUID().equals(String.valueOf(parser.getUserId()))
                                            && apiResponse.body().getRows().get(i).getValue().getLocID().contains(String.valueOf(parser.getLocID()))) {

                                        Log.e(TAG, "Pickup Response body :" + apiResponse.body().getRows().get(i).getValue().getCompleteStatus());
                                        List<String> save = new ArrayList<String>();
                                        Row row = apiResponse.body().getRows().get(i);
                                        //Key rowValue = row.getKey();
                                        Value rowValue = row.getValue();
                                        save = rowValue.getScanItem();
                                        barcodeScan.addAll(save);
                                        dbHelper.setScanItems(rowValue.getAssignmentNo(), barcodeScan, row.getId());
                                        //setScanData = session.getScannedData(rowValue.getAssignmentNo());
                                       /* String[] dataId;
                                        if (setScanData != null) {
                                            dataId = setScanData.toArray(new String[setScanData.size()]);
                                            save.addAll(Arrays.asList(dataId));
                                        }*/

                                       /* save.addAll(rowValue.getScanItem());
                                        barcodeScan.addAll(save);
                                        dataId = barcodeScan.toArray(new String[barcodeScan.size()]);
                                        save.clear();
                                        save.addAll(Arrays.asList(dataId));*/

                                        // session.setScannedData(barcodeScan, rowValue.getAssignmentNo());
                                        session.setReason(rowValue.getAssignmentNo(), rowValue.getPickupReason());
                                        session.setReason(rowValue.getAssignmentNo(), rowValue.getPickupReason());
                                        if(save.size() > 0) {
                                            Log.e(TAG, "Response1: for complete" + rowValue.getAssignmentNo() + ":" + rowValue.getCompleteStatus());
                                            dbHelper.updateInitialStatus(row.getId(), save, "Y", apiResponse.body().getRows().get(i).getKey(),
                                                    rowValue.getPickupReason(), rowValue.getPickupRemark(), rowValue.getCompanyId(), rowValue.getLocID(), rowValue.getUID(), rowValue.getAssignmentNo(), rowValue.getReasonId());


                                        }else {
                                            Log.e(TAG, "Response1: " + rowValue.getAssignmentNo() + ":" + rowValue.getCompleteStatus());
                                            dbHelper.updateInitialStatus(row.getId(), save, rowValue.getCompleteStatus(), apiResponse.body().getRows().get(i).getKey(),
                                                    rowValue.getPickupReason(), rowValue.getPickupRemark(), rowValue.getCompanyId(), rowValue.getLocID(), rowValue.getUID(), rowValue.getAssignmentNo(), rowValue.getReasonId());
                                        }

                                    }
                                    barcodeScan.clear();
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.e("Error : ", "" + t.getLocalizedMessage());

                    }
                });
            } catch (Exception e) {
                Log.e(TAG, "Exception in ApiResponse api" + e.getMessage());
            }

            try {
                Call<ApiResponse1> call = App.getRestClient().getPickUpList().getRVPList(parser.getRvp(), data);
                call.enqueue(new Callback<ApiResponse1>() {
                    @Override
                    public void onResponse(Response<ApiResponse1> apiResponse1, Retrofit retrofit) {
                        Log.e(TAG, "Parsing successful for RVP");
                        List<String> pickUpData = new ArrayList<String>();
                        Set<String> setpickUpList = new HashSet<>();
                        Set<String> pickUpset = new HashSet<>();
                        String[] dataId;
                        if (!apiResponse1.body().getRows().isEmpty()) {
                            Set<String> barcodeScan = new HashSet<>();
                            int responseSize = apiResponse1.body().getRows().size();
                            for (int i = 0; i < responseSize; i++) {
                                if (apiResponse1.body().getRows().get(i).getId() != null && apiResponse1.body().getRows().get(i).getValue().getCompanyId() != null && apiResponse1.body().getRows().get(i).getValue().getUID() != null && apiResponse1.body().getRows().get(i).getValue().getLocID() != null) {
                                    if (apiResponse1.body().getRows().get(i).getId().contains(mLastUpdateTime) && apiResponse1.body().getRows().get(i).getValue().getCompanyId().equals(String.valueOf(parser.getCompanyID())) && apiResponse1.body().getRows().get(i).getValue().getUID().equals(String.valueOf(parser.getUserId()))
                                            && apiResponse1.body().getRows().get(i).getValue().getLocID().contains(String.valueOf(parser.getLocID()))) {
                                        Row1 row = apiResponse1.body().getRows().get(i);
                                        Value1 rowValue = row.getValue();

                                        session.setReason(rowValue.getAssignmentNo(), rowValue.getPickupReason());
                                        Log.e(TAG, "Response2: " + rowValue.getAssignmentNo() + ":" + rowValue.getCompleteStatus());
                                        dbHelper.updateinitialRVPStatus(row.getId(), rowValue.getCompleteStatus(), rowValue.getCreatedOn(), rowValue.getWaybillNo(), rowValue.getSignature(), rowValue.getCustName(), rowValue.getCompanyId(), rowValue.getLocID(), rowValue.getUID(), rowValue.getAssignmentNo(), rowValue.getImage(), rowValue.getItemDescription(), rowValue.getReturnReason(), rowValue.getDescribedValue(), rowValue.getRelationship(), rowValue.getCustNo(), rowValue.getPickupReason(), rowValue.getPickupRemark(),
                                                rowValue.getLatitude(),rowValue.getLongitude());
                                    }
                                    barcodeScan.clear();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.e("Error : in RVP ", "" + t.getLocalizedMessage());

                    }
                });
            } catch (Exception e) {
                Log.e(TAG, "Exception in ApiResponse1 api " + e.getMessage());
            }
            try {
                Call<ECOMParser> call = App.getRestClient().getEcomParserService().getEcomParserservice(parser.getEcom(), data);
                call.enqueue(new Callback<ECOMParser>() {
                    @Override
                    public void onResponse(Response<ECOMParser> apiResponse, Retrofit retrofit) {
                        Log.e(TAG, "Parsing successful for ECOM");
                        if (!apiResponse.body().getRows().isEmpty()) {
                            Set<String> barcodeScan = new HashSet<String>();
                            Set<String> setScanData = new HashSet<>();
                            int responseSize = apiResponse.body().getRows().size();
                            for (int i = 0; i < responseSize; i++) {
                                if (apiResponse.body().getRows().get(i).getId() != null && apiResponse.body().getRows().get(i).getValue().getCompanyId() != null && apiResponse.body().getRows().get(i).getValue().getUID() != null && apiResponse.body().getRows().get(i).getValue().getLocID() != null) {
                                    if (apiResponse.body().getRows().get(i).getId().contains(mLastUpdateTime) && apiResponse.body().getRows().get(i).getValue().getCompanyId().equals(String.valueOf(parser.getCompanyID())) && apiResponse.body().getRows().get(i).getValue().getUID().equals(String.valueOf(parser.getUserId()))
                                            && apiResponse.body().getRows().get(i).getValue().getLocID().contains(String.valueOf(parser.getLocID()))) {
                                        List<String> save = new ArrayList<String>();
                                        Row2 row = apiResponse.body().getRows().get(i);
                                        Value2 rowValue = row.getValue();
                                        setScanData = session.getScannedData(rowValue.getAssignmentNo());
                                        String[] dataId;
                                        if (setScanData != null) {
                                            dataId = setScanData.toArray(new String[setScanData.size()]);
                                            save.addAll(Arrays.asList(dataId));
                                        }

                                        save.addAll(rowValue.getScanItem());
                                        barcodeScan.addAll(save);

                                        session.setScannedData(barcodeScan, rowValue.getAssignmentNo());
                                        session.setReason(rowValue.getAssignmentNo(), rowValue.getPickupReason());
                                        Log.e(TAG, "Response4: " + rowValue.getAssignmentNo() + ":" + rowValue.getScanItem());
                                        dbHelper.updateInitialEcomData(row.getId(), save, rowValue.getCompleteStatus(), rowValue.getCreatedOn(),
                                                rowValue.getPickupReason(), rowValue.getPickupRemark(), rowValue.getCompanyId(), rowValue.getLocID(), rowValue.getUID(), rowValue.getAssignmentNo(), rowValue.getDoctype(), rowValue.getReasonid());

                                    }
                                    barcodeScan.clear();
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.e("Error : in ECOM", "" + t.getLocalizedMessage());

                    }
                });

            } catch (Exception e) {
                Log.e(TAG, "Exception in EcomParser api " + e.getMessage());
            }

            try {
                Map<String, String> data1 = new HashMap<>();
                data1.put("username", parser.getUserName());
                data1.put("password", password);
                data1.put("iemi", String.valueOf(parser.getIMEI()));
                data1.put("timezone", parser.getTimezone());
                data1.put("company_id", String.valueOf(parser.getCompanyID()));
                data1.put("token", parser.getToken());
                Call<Escan> call = App.getRestClient().getEcomService().getEcomService(data1);

                call.enqueue(new Callback<Escan>() {
                    @Override
                    public void onResponse(Response<Escan> escan, Retrofit retrofit) {
                        if(escan.body() != null) {
                            int responseSize = escan.body().getData().size();
                            Set<String> pickUpset;

                            for (int i = 0; i < responseSize; i++) {
                                if (escan.body().getData().get(i).getORDERDATE().contains(mLastUpdateTime) &&
                                        String.valueOf(escan.body().getData().get(i).getCOMPANYID()).contains
                                                (String.valueOf(parser.getCompanyID()))
                                        && String.valueOf(escan.body().getData().get(i).getUSERID()).
                                        equals(session.getuserId())
                                        && String.valueOf(escan.body().getData().get(i).getLOCATION()).
                                        equals(session.getLocId())) {
                                    pickUpset = new HashSet<String>(escan.body().getData().get(i).getSCANITEM());
                                    session.setScanRef(escan.body().getData().get(i).getASSIGNMENTNO(), pickUpset);
                                    session.setDocType(escan.body().getData().get(i).getASSIGNMENTNO(),
                                            escan.body().getData().get(i).getDOCTYPE());
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.e("Error : ", "" + t.getLocalizedMessage());
                    }
                });
            } catch (Exception e) {
            }

            try {
                try {
                    RequestBody formBody = new FormBody.Builder()
                            .add("iemi", String.valueOf(parser.getIMEI()))
                            .add("timezone", parser.getTimezone())
                            .add("token", parser.getToken())
                            .build();
                    new PostRelationRequest(new PostRelationRequest.AsyncResponse() {
                        @Override
                        public void processFinish(String output) {
                            if (output != null && output != "") {
                                /**parser used for outscan**/
                                ParseRelation_XmlResponse relationListParser = new ParseRelation_XmlResponse(output, getApplicationContext());
                                try {
                                    relationListParser.parse();
                                } catch (XmlPullParserException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                        }
                    },formBody, URL_get_relations_dtls).doInBackground();
                } catch (Exception e) {
                    Log.e(TAG, "Error in post connection" + e);
                }

                try {
                    RequestBody formBody = new FormBody.Builder()
                            .add("iemi", String.valueOf(parser.getIMEI()))
                            .add("timezone", parser.getTimezone())
                            .add("company_id", String.valueOf(parser.getCompanyID()))
                            .add("token", session.getKeyToken())
                            .build();
                    new RequestCompanyReason(new RequestCompanyReason.AsyncResponse() {
                        @Override
                        public void processFinish(String output) {
                            if (output != null && output != "") {
                                /**parser used for ud reasons **/
                                ParseUDReason_XmlResponse HoldReasonListParser = new ParseUDReason_XmlResponse(output,
                                        getApplicationContext());
                                try {
                                    HoldReasonListParser.parse();

                                } catch (XmlPullParserException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                                /*----------------- ----parser used for PickUp cancel reason----------------*/
                                ParsePickUpReason_XmlResponse PickUpReasonListParser = new
                                        ParsePickUpReason_XmlResponse(output, getApplicationContext());
                                try {
                                    PickUpReasonListParser.parse();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }, formBody, URL_get_ud_resn).doInBackground();
                } catch (Exception e) {
                }

            } catch (Exception e) {
                Crashlytics.log(Log.ERROR, "Error in Api Calling", e.getStackTrace() + "");
                Crashlytics.logException(e);
            }
        }

    }

}
