package com.traconmobi.tom;

import android.app.Activity;
//import android.app.FragmentManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
//import org.eclipse.jdt.annotation.Nullable;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class DeliveryMultipleFragment extends Fragment implements BaseFragment {

    View v;
    Button Onclick_single;
    private String mTitle;
    ViewPager pager;
    private AssignedListAdapter mAdapter;
    CharSequence Titles[] = {"Delivery", "PickUp"};
    int Numboftabs = 2,scn_count;
    public static String employeeId = "";
    // Session Manager Class
    SessionManager session;
    public String SESSION_TRANSPORT,session_user_id,session_user_pwd,session_USER_LOC_ID,session_USER_NUMERIC_ID,session_CURRENT_DT,session_CUST_ACC_CODE,session_USER_NAME,session_DB_PATH,session_DB_PWD;
    //    session_USER_LOC
    TinyDB tiny;
    Button myButton ,btn_del,btn_undel;
    protected ListView filterList;
    protected SQLiteDatabase db;
    public String Assignment_Type,get_scan_val,scanned_flg,srch_text;
    Cursor cursor_consignee_list,cursor_shippr_list;
    public static int total_awbs;
    MyCustomAdapter dataAdapter = null;
    EditText searchText;
    TextView count;
    StringBuffer responseText,scan_responseText;
    HashMap<String, String> scan_items;
    ArrayList<HashMap<String, String>> shwList;
    ArrayList<String> scanitemList;
    private static final String TAG_REF_ID="";
    ArrayList<Country> countryList;
    boolean mShowingChild;
    public String TAG="DeliveryMultipleFragment";
    private OnFragmentInteractionListener mListener1;
    RadioButton rb_shipper,rb_congsnee;
    RadioGroup radiogrp;
    private RadioButton radioButton;

    String str_amt = null;
    String result;
    int j = 0;
    double total_cod = 0;
    String cod = null;
    public DeliveryMultipleFragment()
    {
    }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {
        try
        {

            v = inflater.inflate(R.layout.fragment_multipledelivery, container, false);

            //Intializing Fabric
            Fabric.with(getActivity(), new Crashlytics());

            // Session class instance
            session = new SessionManager(getActivity().getApplicationContext());

            // Session class instance
            tiny = new TinyDB(getActivity().getApplicationContext());

            // get AuthenticateDb data from session
            HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

            // DB_PATH
            session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

            // DB_PWD
            session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);


            // get user data from session
            HashMap<String, String> login_Dts = session.getLoginDetails();

            // Userid
            session_user_id = login_Dts.get(SessionManager.KEY_UID);

            // pwd
            session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);


            // get user data from session
            HashMap<String, String> user = session.getUserDetails();

            // session_USER_LOC
//        session_USER_LOC = user.get(SessionManager.KEY_USER_LOC);

            //session_USER_LOC_ID
            session_USER_LOC_ID=user.get(SessionManager.KEY_USER_LOC_ID);

            // session_USER_NUMERIC_ID
            session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);

            // session_CURRENT_DT
            session_CURRENT_DT = user.get(SessionManager.KEY_CURRENT_DT);

            // session_CUST_ACC_CODE
            session_CUST_ACC_CODE = user.get(SessionManager.KEY_CUST_ACC_CODE);

            // session_USER_NAME
            session_USER_NAME = user.get(SessionManager.KEY_USER_NAME);



            filterList = (ListView) v.findViewById(R.id.listView1);
//            filterList = (ListView) v.findViewById(R.id.list);
            searchText = (EditText) v.findViewById(R.id.searchText);
            srch_text = searchText.getText().toString().trim();
            radiogrp = (RadioGroup)v.findViewById(R.id.radioGroup1);

            rb_shipper = (RadioButton) v.findViewById(R.id.shipper);
            rb_congsnee = (RadioButton) v.findViewById(R.id.consignee);
            shwList = new ArrayList<HashMap<String, String>>();
            scanitemList = new ArrayList<String>();
            scan_responseText = new StringBuffer();
//        count = (TextView) v.findViewById(R.id.textView);
            scan_items = new HashMap<String, String>();
            myButton = (Button)v.findViewById(R.id.findSelected);
            btn_del = (Button) v.findViewById(R.id.btn_del);
            btn_undel = (Button) v.findViewById(R.id.btn_undel);
            Onclick_single = (Button) v.findViewById(R.id.singleButton);
        }
        catch(Exception e)

        {
            Crashlytics.log(Log.ERROR, TAG, "Exception oncreate" + e.getMessage());
        }
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        SESSION_TRANSPORT = tiny.getString("transport_val");
        Crashlytics.log(Log.ERROR, TAG, " Appleactivity SESSION_TRANSPORT " + SESSION_TRANSPORT);

        rb_shipper.setChecked(true);
        rb_congsnee.setChecked(false);

        myButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    srch_text = searchText.getText().toString().trim();
                    if(srch_text.isEmpty())
                    {
                        Toast.makeText(getActivity(),"Please enter name", Toast.LENGTH_LONG).show();
                        filterList.setVisibility(View.INVISIBLE);
                    }
                    else if(!(srch_text.isEmpty()))
                    {
                        hideKeyboard(getActivity());
                        Boolean flag = false;

                        if(rb_shipper.isChecked())
                        {

                            flag = shwshipperlist();
                        }
                        else if(rb_congsnee.isChecked())
                        {

                            flag = shwconsigneelist();

                        }
                        StringBuffer responseText = new StringBuffer();

                        if(flag == true)
                        {
                            ArrayList<Country> countryList = dataAdapter.countryList;
//		    use for loop i=0 to < countryList.size()
                            for(int i=0;i<countryList.size();i++)
                            {
                                Country country = countryList.get(i);
                                if(country.isSelected())
                                {
                                    responseText.append(country.getAssign_num()+"/");
                                }
                            }
                            session.createAssgnMultiple_num_Session(responseText.toString());
                        }
                        else if(flag == false)
                        {
                            filterList.setVisibility(View.INVISIBLE);
                            Toast.makeText(getActivity(),"No Data Found", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                catch(Exception e)
                {
                    e.getStackTrace();
                    System.out.println("Exception AssignedMultipleActivity myButton " + e.getMessage());
                }
            }
        });

            /* Attach CheckedChangeListener to radio group */
        radiogrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {
//                        Toast.makeText(getActivity(), rb.getText(), Toast.LENGTH_SHORT).show();
                    if(rb.getText().equals("Shipper"))
                    {
                        // shipper radiobutton checked
                        searchText.setText("");
                        filterList.setVisibility(View.INVISIBLE);
                    }
                    if(rb.getText().equals("Consignee"))
                    {
                        // shipper radiobutton checked
                        searchText.setText("");

                        filterList.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });
        if(rb_shipper.isChecked())
        {
            if(srch_text.isEmpty()){

                // shipper radiobutton checked
                searchText.setText("");

                filterList.setVisibility(View.INVISIBLE);
            }
            else if(srch_text != null && !(srch_text.isEmpty()))
            {
                shwshipperlist();
            }
            else
            {
                filterList.setVisibility(View.INVISIBLE);
            }
        }
        else if(rb_congsnee.isChecked())
        {
            //do nothing
            if(srch_text.isEmpty())
            {
                // consignee radiobutton checked
                searchText.setText("");
                filterList.setVisibility(View.INVISIBLE);
            }
            else if(srch_text != null && !(srch_text.isEmpty()))
            {
                shwconsigneelist();
            }
            else
            {
                filterList.setVisibility(View.INVISIBLE);
            }
        }

        Onclick_single.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mListener1 != null) {
                    mListener1.onFragmentInteraction(true);
                }

            }
        });

        btn_undel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (dataAdapter.countryList != null) {
                        try {
                            String str_amt = null;
                            String result;
                            int j = 0;
                            double total_cod = 0;
                            String cod = null;
                            responseText = new StringBuffer();
//                            ArrayList<Country> countryList = dataAdapter.countryList;
//					    use for loop i=0 to < countryList.size()
                            for (int i = 0; i < dataAdapter.countryList.size(); i++) {
                                Country country = dataAdapter.countryList.get(i);
                                if (country.isSelected()) {
                                    responseText.append(country.getawb_num() + ",");
                                    j = j + 1;
                                }
                            }
                            Crashlytics.log(Log.ERROR, TAG, "shw accepted assigned list" + responseText.toString());
                            session.createAssgnMultiple_num_Session(responseText.toString());
                            if (responseText.toString().isEmpty() || responseText.toString() == null) {
                                Toast.makeText(getActivity(), "Please select data", Toast.LENGTH_LONG).show();
                            } else if (!(responseText.toString().isEmpty()) && responseText.toString() != null) {
                                try {
                                    if (session_DB_PATH != null && session_DB_PWD != null) {
                                        if (SESSION_TRANSPORT.equals("SELECT TRANSPORT") || SESSION_TRANSPORT.equals("")) {
                                    /*Toast.makeText(getActivity(),"Please start the trip", Toast.LENGTH_LONG).show();
                                    btn_del.setEnabled(true);
//                                    btn_del.setBackgroundResource(R.drawable.button);*/
                                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                            // Setting Dialog Title
                                            alertDialog.setTitle("Alert");
                                            alertDialog.setCancelable(false);
                                            // Setting Dialog Message
                                            alertDialog.setMessage("Please start the trip");
                                            // On pressing Settings button
                                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), Start_End_TripMainActivity.class);
                                                    goToNextActivity.putExtra("NAV_FORM_TYPE_VAL", "FORM_UNDEL");
//								Toast.makeText(AssignedMultipleUndelActivity.this,"Please locate the customer", Toast.LENGTH_LONG).show();
                                                    btn_del.setEnabled(true);
                                                    dialog.dismiss();
//						 saveButton.setBackgroundResource(R.drawable.button);
                                                    startActivity(goToNextActivity);
                                                }
                                            });

                                            // Showing Alert Message
                                            alertDialog.create().show();
                                        } else {
                                            if (SESSION_TRANSPORT.equals("BIKER")) {
                                                Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), AssignedMultipleUndelActivity.class);
                                                session.createAssgnMultiple_num_Session(responseText.toString());
                                                total_awbs = j;
                                                str_amt = "0";
                                                session.createAssgnMultipleSession(Integer.toString(total_awbs), str_amt);

                                                startActivity(goToNextActivity);
                                            } else if (SESSION_TRANSPORT.equals("NON-BIKER")) {
                                                Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), AssignedMultipleUndelActivity.class);
                                                session.createAssgnMultiple_num_Session(responseText.toString());
                                                total_awbs = j;
                                                str_amt = "0";
                                                session.createAssgnMultipleSession(Integer.toString(total_awbs), str_amt);

                                                startActivity(goToNextActivity);
                                            }
                                        }
                                    }
                                } catch (Exception e) {

                                }
                            }
                        } catch (Exception e) {

                        }
                    }
                } catch (Exception e) {

                }
            }
        });

        btn_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Crashlytics.log(Log.ERROR, TAG, "print dataAdapter.countryList" + dataAdapter.countryList);
                    if (dataAdapter.countryList != null) {
                        try {
                            responseText = new StringBuffer();
//                            ArrayList<Country> countryList = dataAdapter.countryList;
//					    use for loop i=0 to < countryList.size()
                            for (int i = 0; i < dataAdapter.countryList.size(); i++) {
                                Country country = dataAdapter.countryList.get(i);
                                Crashlytics.log(Log.ERROR, TAG, "  country.isSelected()" + country.isSelected());
                                if (country.isSelected()) {
                                    responseText.append(country.getawb_num() + ",");
                                    j = j + 1;
                                }
                            }
                            Crashlytics.log(Log.ERROR, TAG, "shw accepted assigned list" + responseText.toString());
                            session.createAssgnMultiple_num_Session(responseText.toString());
                            if (responseText.toString().isEmpty() || responseText.toString() == null) {
                                Toast.makeText(getActivity(), "Please select data", Toast.LENGTH_LONG).show();
                            } else if (!(responseText.toString().isEmpty()) && responseText.toString() != null) {
                                try {
                                    btn_del.setEnabled(false);
//                            btn_del.setBackgroundColor(Color.GRAY);
                                    if (session_DB_PATH != null && session_DB_PWD != null) {
                                        if (SESSION_TRANSPORT.equals("SELECT TRANSPORT") || SESSION_TRANSPORT.equals("")) {

                                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

                                            // Setting Dialog Title
                                            alertDialog.setTitle("Alert");

                                            alertDialog.setCancelable(false);
                                            // Setting Dialog Message
                                            alertDialog.setMessage("Please start the trip");

                                            // On pressing Settings button
                                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), Start_End_TripMainActivity.class);
                                                    goToNextActivity.putExtra("NAV_FORM_TYPE_VAL", "FORM_UNDEL");
//								Toast.makeText(AssignedMultipleUndelActivity.this,"Please locate the customer", Toast.LENGTH_LONG).show();
                                                    btn_del.setEnabled(true);
//						 saveButton.setBackgroundResource(R.drawable.button);
                                                    startActivity(goToNextActivity);
                                                }
                                            });

                                            // Showing Alert Message
                                            alertDialog.show();
                                        } else {
                                            new CursorTask().execute();
                                        }
                                    }
                                } catch (Exception e) {
                                    Crashlytics.log(Log.ERROR, TAG, "Assigned_Delivery_MainActivity customer locate " + e.getMessage());
                                } finally {
                                    if (db != null && db.isOpen()) {
                                        db.close();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Crashlytics.log(Log.ERROR, TAG, "dataAdapter" + e.getMessage());
                        }
                    }
                } catch (Exception e) {
                    Crashlytics.log(Log.ERROR, TAG, "onclickdelivred" + e.getMessage());
                }
            }
        });
    }


    public Boolean shwshipperlist()
    {
        Boolean flag = false;
        //Generate list View from ArrayList
        try
        {
            flag = displayListView();
        }
        catch(Exception e)
        {
            e.getStackTrace();
            System.out.println("Exception AssignedMultipleActivity shwshipperlist " + e.getMessage());
        }
        catch(UnsatisfiedLinkError err)
        {
            err.getStackTrace();
            System.out.println("Error AssignedMultipleActivity UnsatisfiedLinkError ");
        }
        return flag;

    }

    public Boolean shwconsigneelist()
    {
        Boolean flag = false;
        try
        {
            flag = displayConsigneeListView();
        }
        catch(Exception e)
        {
            e.getStackTrace();
            System.out.println("Exception AssignedMultipleActivity shwconsigneelist " + e.getMessage());
        }
        catch(UnsatisfiedLinkError err)
        {
            err.getStackTrace();
            System.out.println("Error AssignedMultipleActivity UnsatisfiedLinkError ");
        }
        return flag;
    }

    /***************Getting shipper details************************/
    private Boolean displayListView()
    {
        Boolean flag = false;
        try
        {
            if(session_DB_PATH != null && session_DB_PWD != null)
            {
                if(srch_text != null && !(srch_text.isEmpty()))
                {
                    double total_cod =0;
                    String str_amt = null ;
                    db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                    ArrayList<Country> countryList = new ArrayList<Country>();
                    Assignment_Type="D";
                    cursor_shippr_list=db.rawQuery("SELECT _id ,T_Assignment_Number,T_Consignee_Name,F_Amount,T_Shipper_name FROM TOM_Assignments WHERE T_Shipper_name LIKE  '%"+ searchText.getText().toString().trim() +"%' AND T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed IS NULL AND T_Out_Scan_Location='"+ session.getLocId() +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
                    while(cursor_shippr_list.moveToNext())
                    {
                        String awb_num=cursor_shippr_list.getString(cursor_shippr_list.getColumnIndex("T_Assignment_Number"));
                        String cod=cursor_shippr_list.getString(cursor_shippr_list.getColumnIndex("F_Amount"));
                        String shpr_name=cursor_shippr_list.getString(cursor_shippr_list.getColumnIndex("T_Consignee_Name"));

                        total_cod = total_cod + Double.valueOf(cod);
                        NumberFormat formatter = new DecimalFormat("###.00");
                        str_amt = formatter.format(total_cod);

//                        Country country = new Country(awb_num,cod,shpr_name,true);
                        Country country = new Country(awb_num,shpr_name,true);
                        countryList.add(country);
                    }
                    total_awbs = cursor_shippr_list.getCount();
                    cursor_shippr_list.close();
                    if(total_awbs > 0)
                    {
                        session.createAssgnMultipleSession(Integer.toString(total_awbs), str_amt);

                        //create an ArrayAdaptar from the String Array
                        dataAdapter = new MyCustomAdapter(getActivity(),
                                R.layout.multiple_list, countryList);
                        // Assign adapter to ListView
                        filterList.setAdapter(dataAdapter);
                        filterList.setVisibility(View.VISIBLE);
                        filterList.setCacheColorHint(Color.WHITE);
                        btn_del.setEnabled(true);
                        btn_undel.setEnabled(true);
                        flag = true;
                    }
                    else if(total_awbs <= 0)
                    {
                        filterList.setVisibility(View.INVISIBLE);
                        btn_del.setEnabled(false);
                        btn_undel.setEnabled(false);
                        flag = false;
                    }
                    db.close();
                }
                else
                {
                    filterList.setVisibility(View.INVISIBLE);
                    btn_del.setEnabled(false);
                    btn_undel.setEnabled(false);
                    flag = false;
                }
            }
            else
            {
                flag = false;
                System.out.println("Error AssignedMultipleActivity TOMLoginUserActivity.file is null ");
                //onClickGoToHomePage();
            }
        }
        catch(Exception e)
        {
            e.getStackTrace();
            System.out.println("Exception AssignedMultipleActivity displayListView " + e.getMessage());
        }
        catch(UnsatisfiedLinkError err)
        {
            err.getStackTrace();
            System.out.println("Error AssignedMultipleActivity UnsatisfiedLinkError ");
            //onClickGoToHomePage();
//	            	    			finish();
        }
        return flag;
    }


    /***************Getting consignee details************************/

    private Boolean displayConsigneeListView()
    {
        Boolean flag = false;
        try
        {
            if(session_DB_PATH != null && session_DB_PWD != null)
            {
                if(srch_text != null && !(srch_text.isEmpty()))
                {

                    double total_cod =0;
                    String str_amt = null ;
                    db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                    ArrayList<Country> countryList = new ArrayList<Country>();
                    Assignment_Type="D";
                    cursor_consignee_list=db.rawQuery("SELECT _id ,T_Assignment_Number,T_Consignee_Name,F_Amount FROM TOM_Assignments WHERE T_Consignee_Name LIKE  '%"+ searchText.getText().toString().trim() +"%' AND T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session.getLocId() +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
                    while(cursor_consignee_list.moveToNext())
                    {
                        String awb_num=cursor_consignee_list.getString(cursor_consignee_list.getColumnIndex("T_Assignment_Number"));
                        String cod=cursor_consignee_list.getString(cursor_consignee_list.getColumnIndex("F_Amount"));
                        String cnsgn_name =cursor_consignee_list.getString(cursor_consignee_list.getColumnIndex("T_Consignee_Name"));

                        total_cod = total_cod + Double.valueOf(cod);
                        NumberFormat formatter = new DecimalFormat("###.00");
                        str_amt = formatter.format(total_cod);

//                        Country country = new Country(awb_num,cod,cnsgn_name,true);
                        Country country = new Country(awb_num,cnsgn_name,true);
                        countryList.add(country);
                    }
                    total_awbs = cursor_consignee_list.getCount();
                    cursor_consignee_list.close();
                    if(total_awbs > 0)
                    {
                        session.createAssgnMultipleSession(Integer.toString(total_awbs), str_amt);

                        //create an ArrayAdaptar from the String Array
                        dataAdapter = new MyCustomAdapter(getActivity(),
                                R.layout.multiple_list, countryList);
                        // Assign adapter to ListView
                        filterList.setAdapter(dataAdapter);
                        filterList.setVisibility(View.VISIBLE);
                        filterList.setCacheColorHint(Color.WHITE);
                        btn_del.setEnabled(true);
                        btn_undel.setEnabled(true);
                        flag = true;
                    }
                    else if(total_awbs <= 0)
                    {
                        filterList.setVisibility(View.INVISIBLE);
                        btn_del.setEnabled(false);
                        btn_undel.setEnabled(false);
                        flag = false;
                    }
                    db.close();
                }
                else
                {
                    filterList.setVisibility(View.INVISIBLE);
                    btn_del.setEnabled(false);
                    btn_undel.setEnabled(false);
                    flag = false;
                }
            }
            else
            {
                System.out.println("Error AssignedMultipleActivity TOMLoginUserActivity.file is null ");
            }
        }
        catch(SQLiteException e )
        {
            e.getStackTrace();
            System.out.println("Exception AssignedMultipleActivity SQLiteException " + e.getMessage());
            //onClickGoToHomePage();
        }
        catch(Exception e)
        {
            e.getStackTrace();
            System.out.println("Exception AssignedMultipleActivity displayConsigneeListView " + e.getMessage());
        }
        catch(UnsatisfiedLinkError err)
        {
            err.getStackTrace();
            System.out.println("Error AssignedMultipleActivity UnsatisfiedLinkError ");
        }
        return flag;
    }

    @Override
    public boolean isShowingChild() {
        return mShowingChild;
    }

    @Override
    public void setShowingChild(boolean showingChild) {
        mShowingChild = showingChild;
    }


    private class MyCustomAdapter extends ArrayAdapter<Country> {

        private ArrayList<Country> originalList;
        private ArrayList<Country> countryList;
//        private CountryFilter filter;

        public MyCustomAdapter(Context context, int textViewResourceId,
                               ArrayList<Country> countryList) {
            super(context, textViewResourceId, countryList);
            this.countryList = new ArrayList<Country>();
            this.countryList.addAll(countryList);
            this.originalList = new ArrayList<Country>();
            this.originalList.addAll(countryList);
        }

        public int getCount(){
            this.countryList.size();
            Crashlytics.log(Log.ERROR, TAG, "print count " + this.countryList.size());
            return this.countryList.size();
        }
        /* public void removeElementAtPosition(int position) {

         }*/
        public void removeElementAtPosition(int position){
            this.countryList.remove(position);
            dataAdapter.notifyDataSetChanged();
        }
        private class ViewHolder {
            TextView code;
            TextView name;
            CheckBox select;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            if (convertView == null) {

                LayoutInflater vi = (LayoutInflater)getActivity().getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.multiple_list, null);

                holder = new ViewHolder();
                holder.code = (TextView) convertView.findViewById(R.id.awb);
                holder.name = (TextView) convertView.findViewById(R.id.firstName);
                holder.select = (CheckBox) convertView.findViewById(R.id.checkBox1);

                convertView.setTag(holder);

                holder.select.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v;
                        Country country = (Country) cb.getTag();
                        country.setSelected(cb.isChecked());
                    }
                });

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Country country = countryList.get(position);
            holder.name.setText(country.getcname());
            holder.code.setText(country.getawb_num());
            holder.select.setChecked(country.isSelected());
            holder.select.setTag(country);
            return convertView;
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(boolean status);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener1 = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener1 = null;
    }

    public void replaceChild(BaseFragment oldFrg, int position) {
        mAdapter.replaceChildFragment(oldFrg, position);
    }


    public static DeliveryMultipleFragment newInstance()
    {
        return new DeliveryMultipleFragment();
    }

    public void onclick_single()
    {

    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private class CursorTask extends AsyncTask<Void, Void, Void> {
        final ProgressDialog dialog = new ProgressDialog((AssignedListMainActivity)getActivity());


        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Please wait for COD amount Calculation...");
            this.dialog.show();
            this.dialog.setCancelable(true);
        }


        CursorTask() {
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                if (SESSION_TRANSPORT.equals("BIKER")) {
                    if (session_DB_PATH != null && session_DB_PWD != null) {
//		    			    		db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                        db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                        String shw_res[] = responseText.toString().split(",");
                        for (int k = 0; k < shw_res.length; k++) {
                            result = shw_res[k];
                            Crashlytics.log(Log.ERROR, TAG, "prnt reslt " + result + "responseText.toString() " + responseText.toString());
                            Assignment_Type = "D";
//		    					        	cursor_shippr_list=db.rawQuery("SELECT _id ,T_Assignment_Number,T_Consignee_Name,F_Amount FROM TOM_Assignments WHERE T_Assignment_Number ='" + result +"' AND T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
//		    					        	 cursor_shippr_list=db.rawQuery("SELECT _id ,T_Assignment_Number,T_Consignee_Name,F_Amount FROM TOM_Assignments WHERE T_Assignment_Number ='" + result +"' AND T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' ", null);
                            cursor_shippr_list = db.rawQuery("SELECT _id ,T_Assignment_Number,T_Consignee_Name,F_Amount FROM TOM_Assignments WHERE T_Assignment_Number ='" + result + "' AND T_Assignment_Type='" + Assignment_Type + "' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session.getLocId() +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID + "' AND D_OutScan_Date='" + session_CURRENT_DT +"' ", null);
                            while (cursor_shippr_list.moveToNext()) {
                                String awb_num = cursor_shippr_list.getString(cursor_shippr_list.getColumnIndex("T_Assignment_Number"));
                                cod = cursor_shippr_list.getString(cursor_shippr_list.getColumnIndex("F_Amount"));
                                String get_consgnee = cursor_shippr_list.getString(cursor_shippr_list.getColumnIndex("T_Consignee_Name"));
                            }
                            int cnt = cursor_shippr_list.getCount();
                            total_awbs = cursor_shippr_list.getCount();
                            cursor_shippr_list.close();
                            total_cod = total_cod + Double.valueOf(cod);
                            NumberFormat formatter = new DecimalFormat("###.00");
                            str_amt = formatter.format(total_cod);
                            Crashlytics.log(Log.ERROR, TAG, "print amount " + str_amt);
                        }


                        db.close();
                       /* Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), AssignedMultipleDelActivity.class);
                        goToNextActivity.putExtra("FORM_TYPE4_VAL", "FORM_MULTIPLE_DEL");
                        session.createAssgnMultiple_num_Session(responseText.toString());

                        total_awbs = j;

                        session.createAssgnMultipleSession(Integer.toString(total_awbs), str_amt);
                        startActivity(goToNextActivity);*/

                    } else {
                        Crashlytics.log(Log.ERROR, TAG, "Error Assigned_Delivery_MainActivity session_DB_PATH is null ");
                        //onClickGoToHomePage();
                    }
                } else if (SESSION_TRANSPORT.equals("NON-BIKER")) {
                    if (session_DB_PATH != null && session_DB_PWD != null) {
//		    			    		db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                        db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                        String shw_res[] = responseText.toString().split(",");
                        for (int k = 0; k < shw_res.length; k++) {
                            result = shw_res[k];
                            Crashlytics.log(Log.ERROR, TAG, "prnt reslt " + result + "responseText.toString() " + responseText.toString());
                            Assignment_Type = "D";
//		    					        	cursor_shippr_list=db.rawQuery("SELECT _id ,T_Assignment_Number,T_Consignee_Name,F_Amount FROM TOM_Assignments WHERE T_Assignment_Number ='" + result +"' AND T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
                            cursor_shippr_list = db.rawQuery("SELECT _id ,T_Assignment_Number,T_Consignee_Name,F_Amount FROM TOM_Assignments WHERE T_Assignment_Number ='" + result + "' AND T_Assignment_Type='" + Assignment_Type + "' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session.getLocId() +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID + "' AND D_OutScan_Date='" + session_CURRENT_DT +"'", null);
                            while (cursor_shippr_list.moveToNext()) {
                                String awb_num = cursor_shippr_list.getString(cursor_shippr_list.getColumnIndex("T_Assignment_Number"));
                                cod = cursor_shippr_list.getString(cursor_shippr_list.getColumnIndex("F_Amount"));
                                String get_consgnee = cursor_shippr_list.getString(cursor_shippr_list.getColumnIndex("T_Consignee_Name"));
                            }
                            int cnt = cursor_shippr_list.getCount();
                            total_awbs = cursor_shippr_list.getCount();
                            cursor_shippr_list.close();
                            total_cod = total_cod + Double.valueOf(cod);
                            Crashlytics.log(Log.ERROR, TAG, "print amount total_cod " + total_cod);
                            NumberFormat formatter = new DecimalFormat("###.00");
                            str_amt = formatter.format(total_cod);
                            Crashlytics.log(Log.ERROR, TAG, "print amount " + str_amt);
                        }
                        db.close();
                    } else {
                        Crashlytics.log(Log.ERROR, TAG, "Error Assigned_Delivery_MainActivity session_DB_PATH is null ");
                        //onClickGoToHomePage();
                    }
                }
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e.getMessage());
                db.close();

            } catch (SQLException e) {
                Log.e(TAG, e.getMessage());
                db.close();

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                db.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void reult) {
            try {
                Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), AssignedMultipleDelActivity.class);
                goToNextActivity.putExtra("FORM_TYPE4_VAL", "FORM_MULTIPLE_DEL");
                session.createAssgnMultiple_num_Session(responseText.toString());
                total_awbs = j;
                session.createAssgnMultipleSession(Integer.toString(total_awbs), str_amt);
                startActivity(goToNextActivity);
            }catch(NullPointerException e) {

            }catch(Exception e){

            }
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

        }
    }

}


