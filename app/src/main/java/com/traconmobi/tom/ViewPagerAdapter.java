package com.traconmobi.tom;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

/**
 * Created by ashwini on 29-09-2015.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created
    String ord_no;
    public String TAG="ViewPagerAdapter";


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPagerAdapter(FragmentManager fm,CharSequence mTitles[], int mNumbOfTabsumb,String ordrno) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;
        this.ord_no = ordrno;
    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if(position == 0) // if the position is 0 we are returning the First tab
        {

            DeliveryDtlsFragment tab1 = new DeliveryDtlsFragment();
            try
            {
            Bundle bundle = new Bundle();
            bundle.putString("orderid", ord_no);
            tab1.setArguments(bundle);
            }
            catch(Exception e)
            {
                Log.e(TAG, "EXTRA_MESSAGE" + e.getMessage());
            }
            return tab1;
        }
     /*else if(position == 1)              // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
        {
            AppleActivity tab2 = new AppleActivity();
            return tab2;
        }*/
       else            // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
        {
            DeliveryMultipleFragment tab2 = new DeliveryMultipleFragment();
            return tab2;
//            WindowsActivity tab3 = new WindowsActivity();
//            Bundle bundle =new Bundle();
//            bundle.putString("orderid",ord_no);
//            tab3.setArguments(bundle);
//            return tab3;
        }

    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }

}