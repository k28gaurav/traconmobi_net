package com.traconmobi.tom;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;

import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
//import android.support.v7.widget.Toolbar;

public class AssignmentInCompleteStatus extends FragmentActivity {

    // Declaring Your View and Variables
    //    Toolbar toolbar;

    ViewPager pager;
    AssignmentInCompleteAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[] = {"Delivery", "PickUp"};
    int Numboftabs = 2;
    public static String employeeId = "";
    SharedPreferences pref;
    public static final String PREF_NAME = "Pager";

    TextView title;
    public String TAG = "AssignmentInCompleteStatus";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            super.onCreate(savedInstanceState);
            //Intializing Fabric
            Fabric.with(this, new Crashlytics());
            setContentView(R.layout.layout_incomplete_assign_status);
            getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, R.layout.layout_incomplete_assign_status);

            title = (TextView) findViewById(R.id.txt_title);
            title.setText("INCOMPLETE LIST");
//        ActionBar bar = getSupportActionBar();
            //for color
//        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#010E76")));

            // Creating The Toolbar and setting it as the Toolbar for the activity

            //  toolbar = (Toolbar) findViewById(R.id.tool_bar);
            //  setSupportActionBar(toolbar);

            // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
            adapter = new AssignmentInCompleteAdapter(getSupportFragmentManager(), Titles, Numboftabs, employeeId);
            // Assigning ViewPager View and setting the adapter
            pager = (ViewPager) findViewById(R.id.pager);
            pager.setAdapter(adapter);

            // Assiging the Sliding Tab Layout View
            tabs = (SlidingTabLayout) findViewById(R.id.tabs);
            tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

            // Setting Custom Color for the Scroll bar indicator of the Tab View
            tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
                @Override
                public int getIndicatorColor(int position) {
                    return getResources().getColor(R.color.tabsScrollColor);
                }
            });

            // Setting the ViewPager For the SlidingTabsLayout
            tabs.setViewPager(pager);
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "oncreate" + e.getMessage());
        }
    }

    public void toggleMenu(View v) {
        finish();
    }

    public void onclk_noti(View v) {
        Intent homeActivity = new Intent(this, NotificationActivity.class);
        startActivity(homeActivity);
    }

    public void onclk_trip(View v) {
        Intent homeActivity = new Intent(this, Start_End_TripMainActivity.class);
        startActivity(homeActivity);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.assgn_dtls_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

    }
}


