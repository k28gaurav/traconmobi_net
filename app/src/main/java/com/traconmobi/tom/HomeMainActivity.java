package com.traconmobi.tom;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.traconmobi.tom.app.App;
import com.traconmobi.tom.http.OkHttpHandlerPost;
import com.traconmobi.tom.model.Row;
import com.traconmobi.tom.model.Row1;
import com.traconmobi.tom.model.Row2;
import com.traconmobi.tom.model.Value;
import com.traconmobi.tom.model.Value1;
import com.traconmobi.tom.model.Value2;
import com.traconmobi.tom.rest.model.ApiResponse;
import com.traconmobi.tom.rest.model.ApiResponse1;
import com.traconmobi.tom.rest.model.ECOMParser;
import com.traconmobi.tom.rest.model.Escan;

import com.traconmobi.tom.singeleton.UserParser;
import com.traconmobi.tom.slidingmenu.adapter.NavDrawerListAdapter;
import com.traconmobi.tom.slidingmenu.model.NavDrawerItem;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;


import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Fragment;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;


import android.location.Location;

import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;


import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.Settings;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.kobjects.base64.Base64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import io.fabric.sdk.android.Fabric;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class HomeMainActivity extends ActionBarActivity implements android.location.LocationListener, TaskDelegate {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    // nav drawer title
    private CharSequence mDrawerTitle;
    public ProgressDialog progress = null;
    private UserParser parser;
    MyResultReceiver resultReceiver;

    // Initializing
    TinyDB tiny;
    List<String> savedList = new ArrayList<String>();
    List<String> waypoints;
    ArrayList<LatLng> markerPoints;
    MarkerOptions options = new MarkerOptions();
    String companyId;
    boolean SESSION_START = false;

    // used to store app title
    private CharSequence mTitle;
    Location mlocation;
    double distance = 0.0;
    double srcLatitude = 0.0;
    double srcLongitude = 0.0;
    double cDistance = 0.0;
    double accDistance = 0.0;
    LatLng srclatlng;
    boolean assignmentResponse = false;
    boolean transferResponse = false;
    boolean companyReason = false;
    boolean relationships = false;
    boolean ecomAssignment = false;
    private static final String TAG = "HomeActivity";

//	private AutosyncSoapAccessTask AutosyncTask;
    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;


    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // Keys for storing activity state in the Bundle.
    protected final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";
    protected final static String LOCATION_KEY = "location-key";
    protected final static String LAST_UPDATED_TIME_STRING_KEY = "last-updated-time-string-key";

    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    protected Boolean mRequestingLocationUpdates;
    private final String URL_outscan = "http://traconmobi.net/Assignment-Details";
    private final String URL_get_ud_resn = "http://traconmobi.net/Company-Reasons";
    private final String URL_get_transfer_users = "http://traconmobi.net/Transfer-User";
    //	private final String URL_UDDtls = "http://traconmobi.net/POD-Update";
//	private final String URL_DELDtls = "http://traconmobi.net/POD-Update";
    /*private final String URL_UDDtls = "http://traconmobi.net/POD-Update_New";
    private final String URL_DELDtls = "http://traconmobi.net/POD-Update_New";*///commented on 23dec2015
    private final String URL_UDDtls = "http://traconmobi.net/podupdate";
    private final String URL_DELDtls = "http://traconmobi.net/podupdate";
    private final String URL_DEL_IMAGES_Dtls = "http://traconmobi.net/Upload-Image";

    SoapObject request = null, objMessages = null;
    SoapSerializationEnvelope envelope;
    HttpTransportSE httpTransport;

    //	public static String DB_FULL_PATH = android.os.Environment.getExternalStorageDirectory().toString()+"/TOM.sqlite";
    static final String EXTRA_MESSAGE = null;
    private static final String SOAPRequestXML = null;
    protected SQLiteDatabase db6_del, db8_undel, db, db1, db2, db3, db4, db5, db6, db7, db8, db9, db_noti, db_unattmpt_cnt2, db_unattmpt_cnt1;
    protected Cursor c_notiupdate, c, cc, cursor, crs, cr, cursor_chkdb, cr_chkdel, c_undelsyn, c_syn_update, cr_unattempt_del, cr_del_status, cursor_outscan_dt, cur_sync_dt, c_syn, c_syn_updater, cursor_outscan_dt_val, cr_unattempt_del_val;
    public static String message;

    int syncdelcnt = 0, syncnoticnt = 0, syncundelcnt = 0, syncunattemptcnt = 0, synclocatecnt = 0, syncexpnsecnt = 0, sync_paymnt_failurecnt = 0, syncfiltr_typcnt = 0, syncpaymnt_updtcnt = 0, synchandovr_updtcnt = 0;
    public static String bnk_locn, bnk_name, chque_dt, chque_no_val, particular_val, msg, myString, strFilter, awb_id, awb_delnum, awb_numb, isdel, is_cod_coltd, del_dt, del_tm, rec_by, rec_status, sign_path, photo_path, get_id, get_awbid, acquired_lat, acquired_long;
    public static String ud_reason, ud_reason_id, ud_date, ud_time, awb_id1, isdel1, cod_amt, remarks, rmks, sgn, l_sign, s_photo, l_photo, awb_outscantime, awb_outscan_dt_val, awb_outscan_dt, awb_outscandt, get_sync_dt, get_sync_date, respstr, Acq_lat, Acq_long;
    final String user_loc = TOMLoginUserActivity.u_loc;
    final String user_id = TOMLoginUserActivity.usr_id;
    final String curnt_dt = TOMLoginUserActivity.currentdate;
    final String cust_acc_code = TOMLoginUserActivity.cust_acc_no;
    final String usr_name = TOMLoginUserActivity.u_name;

    //String variables
    public String fail_Photo_dt, fail_Photo_tm, fail_cust_acc, recpt_no, sign_dt_tme, session_sync_undel_resp_PHOTO_ID, filter_awbid, expense_awbid, locate_awbid, Payment_Mode, Particular, Cheque_DD, Cheque_DD_Dt, Bank_name, Bank_loc, filtr_type, assgn_type, trnfr_uid, cust_locate_assgn_id, asgn_typ, sb_string, scn_usr_id, complete_resp_awbid, complete_resp_awb_id, Incomplete_resp_awbid, Incomplete_resp_awb_id, del_awbid, del_awb_id,
            undel_awbid, undel_awb_id, assgn_num_val, scan_num, scn_CUST_ACC_NO, sync_scan_num, get_payment_failure_response, collectd_amt, assigned_amt, cod_amt_coltd;

    //Integer variables
    public int syncscancnt;

    //StringBuilder
    StringBuilder sb, stringBuilder;

    //session objects
    public String rel_id, undel_cust_acc, photo_id_dt, photo_id_tme, sign_dt_tm, del_cust_acc, session_cust_locate_resp, get_paymnt_response, session_trnsfr_usr_resp, session_paymentreason_resp, session_udreason_resp, session_paymnt_update_resp, session_filtr_dtls, session_Payment_failure, session_get_sync_complete_pickup_response, session_get_sync_incomplete_pickup_response, session_sync_scanitem_resp, session_outscan_pickupscanresp,
            get_pickupscanresp, get_sync_complete_pickup_response, get_sync_undelpickup_response, getscanpickup_response, session_hndovr_resp;

    //Database objects
    protected SQLiteDatabase db_handovr_updt, db_paymnt_update_resp2, db_paymnt_update_resp1, db_paymnt_updt, db_filter_resp2, db_filter_resp1, db_filtr_typ, db_paymnt_failure_resp2, db_paymnt_failure_resp1, db_paymnt_failure, db_cust_locte, db_expnse, db_btry_usage, db_pickup_complete, db_pickup_complete_resp1, db_pickup_complete_resp2, db_pickup_incomplete,
            db_pickup_Incomplete_resp1, db_pickup_Incomplete_resp2, db_pickup_scan_item, db_pickup_scan_resp1,
            db_pickup_scan_resp2, db_opn_noti, db_get_noti_value, db_syn_noti, db_del_complete, db_del_complete_resp1,
            db_del_complete_resp2;

    //*******Expense_trn db and cursor connections******/
    protected SQLiteDatabase db_expense_update_syn1, db_expense_update_syn2;

    protected Cursor c_expense_update_syn1, c_expense_update_syn2;

    public String sb_val, session_expense_resp, get_expense_sync_response;
    /**
     * ****Expense_trn db and cursor connections*****
     */

    //*******customer locate db and cursor connections******/
    protected SQLiteDatabase db_loc_update_syn1, db_loc_update_syn2;

    protected Cursor c_loc_update_syn1, c_loc_update_syn2;

    public String session_updte_locate_resp, get_cust_locate_response;
    /**
     * ****customer locate db and cursor connections*****
     */

    //**********Undelivery db and cursor connections**********************//
    protected SQLiteDatabase db_undel, db_undel_resp1, db_undel_resp2;

    protected Cursor cursor_undel, c_undel_sync_resp1, c_undelsync_resp1, c_undel_sync_resp2, c_undelsync_resp2;

    /**
     * *******Undelivery db and cursor connections*********************
     */

    //Cursor objects
    protected Cursor cursor_handovr_updt, c_paymnt_update_sync_resp1, cursor_paymnt_updt, c_paymnt_update_sync_resp2, c_filter_sync_resp2, c_filter_sync_resp1, cursor_filtr_typ, c_paymnt_failure_sync_resp2, c_paymnt_failure_sync_resp1, cursor_paymnt_failure, cursor_expnse, cursor_cust_locte, crs_batry_usage, cursor_pickup_complete, c_pickup_complete_sync_resp1, c_pickup_complete_sync_resp2,
            cursor_pickup_incomplete, c_pickup_Incomplete_sync_resp1, c_pickup_Incomplete_sync_resp2, cursor_pickup_scanitem,
            c_pickup_scan_sync_resp1, c_pickup_scan_sync_resp2, cursor_opn_noti, cursor_get_noti_value, cursor_syn_noti,
            cursor_del_complete, c_del_complete_sync_resp1, c_del_complete_sync_resp2;


    static File image;
    private String selectedImagePath;
    byte[] BAvalue;
    public static String getphotobytearray;

    String pod_awb;
    String ud_awb;
    int ret = 0, val = 0, cr_unattempt_delcount;
    int outscn = 0;
    Button btnsyn;
    TextView rpt_cnt, sync_cnt, trck_cnt, shwcount, textView, version_name, cnt_unattempt_del, cnt_del_status, cnt_ud, prgtextview, optimizer_cnt;

    // Session Manager Class
    SessionManager session;
    public String paymnt_failure_awbid, expnse_assgn_id, expnse_mode, expnse_amt, expnse_dt, expnse_frm_loc, expnse_to_loc, cust_lng, cust_lat, cust_locate_id, usr_btry_name, usr_imei, usr_batry_usage, u_reason, u_time, ud_loc_id, Assgn_type, fltr_type, t_u_id, Assgn_flag;
    public String notawb, msg2, nname, nloc, ndept, isread, not_id;
    public static String session_DB_PATH, session_DB_PWD, session_user_pwd, get_filtr_response, get_sync_scan_response, session_USER_PSWD, session_user_id, session_USER_LOC, session_USER_LOC_ID, session_USER_NUMERIC_ID, session_CURRENT_DT, session_CUST_ACC_CODE, session_USER_NAME, session_COMPANY_NAME, gettomnoti_response, session_noti_resp, session_outscan_resp, getoutscan_response;
    public String get_sync_noti_response, session_sync_noti_resp, get_sync_undel_response, session_sync_undel_resp, get_sync_del_response, session_sync_del_resp;

    public String get_sync_fail_photoid_response, get_sync_undel_photoid_response, get_paymentreason_resp, get_trnsfr_usr_resp, get_cust_locate_resp, get_udreasons_resp, handovr_type, hndovr_awb_id, handovr_congn_nme, handovr_consgn_rcvr_by, handovr_contct_num, hndovr_cod_amt_coltd, hndovr_del_dt;
    public String hndovr_del_tm, hndovr_rec_by, hndovr_sign_path, hndovr_loc, hndovr_uid, hndovr_cust_accno, hndovr_acquired_lat, hndovr_acquired_long, get_hndovr_response;


    private AutosyncSoapAccessTask AutosyncTask;


    String entityString_del;

    Button btnFusedLocation;
    TextView tvLocation;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;
    String mLastUpdateTime;
    Map<String, Object> docProperties = null;
    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    //private String PREF_NAME = "MapPref";
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    private SupportMapFragment map;
    GoogleMap googleMap;
    CouchBaseDBHelper couchDb;
    TextView distanceMap;
    String docId = "";
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    boolean firstInitialization = true;
    boolean flag = false;
    String userName;
    String passWord;
    String date;
    public String gps;
    public static final String PREF_NAME = "ScanPref";
    //******DB PATH******/
    static File DB_FULL_PATH = new File(Environment.getExternalStorageDirectory()
            + "/Android/data/com.traconmobi.net/app_database");

    public String token = "a2lfrq40kdh0nrut1vl0ani4a2";

    TextView username, cmpny_nme, loc;

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setSmallestDisplacement(10);
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            //Intializing Fabric
            Fabric.with(this, new Crashlytics());
            setContentView(R.layout.activity_home_main);

            ActionBar bar = getSupportActionBar();
            //for color
            bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#002E83")));

            // Session class instance
            session = new SessionManager(getApplicationContext());
            progress = new ProgressDialog(this);

            // get AuthenticateDb data from session
            HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

            // DB_PATH
            session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

            // DB_PWD
            session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);


            // get user data from session
            HashMap<String, String> login_Dts = session.getLoginDetails();

            // Userid
            session_user_id = login_Dts.get(SessionManager.KEY_UID);

            // pwd
            session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);


            // get user data from session
            HashMap<String, String> user = session.getUserDetails();

            // session_USER_LOC
            session_USER_LOC = user.get(SessionManager.KEY_USER_LOC);

            //session_USER_LOC_ID
            session_USER_LOC_ID = user.get(SessionManager.KEY_USER_LOC_ID);

            // session_USER_NUMERIC_ID
            session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);

            // session_CURRENT_DT
            session_CURRENT_DT = user.get(SessionManager.KEY_CURRENT_DT);

            // session_CUST_ACC_CODE
            session_CUST_ACC_CODE = user.get(SessionManager.KEY_CUST_ACC_CODE);

            // session_USER_NAME
            session_USER_NAME = user.get(SessionManager.KEY_USER_NAME);

            //session_COMPANY_NAME
            session_COMPANY_NAME = user.get(SessionManager.KEY_COMPANY_NAME);

            resultReceiver = new MyResultReceiver(null);

			

			/*distanceMap = (TextView) findViewById(R.id.distance);
			distanceMap.setText("Distance Travelled:" + roundToDecimals(distance, 3) / 1000);*/
            distanceMap = (TextView) findViewById(R.id.distance);
            float dis = session.getDistance();
            if (dis > 0) {

                distanceMap.setText("Distance Travelled:" + round(dis / 1000, 3) + "KM");
            } else {
                distanceMap.setText("Distance Travelled:" + round(dis / 1000, 3) + "KM");
            }

            if (session.getFirstInitialization()) {
                flag = displayGpsStatus();
                if (flag) {
                    userName = session_USER_NAME;
                    companyId = session_CUST_ACC_CODE;
                    Crashlytics.log(android.util.Log.ERROR, TAG, "CompanyCheck" + companyId + "Username :" + userName);
                } else {
                    alertbox("Gps Status!!", "Your GPS is: OFF");
                }
                session.setFirstInitialization(false);
            }


            Crashlytics.log(android.util.Log.ERROR, TAG, "Session start service before start service: " + session.getStartTrip());
            //if(session.getStartService()) {
           /* if (session.getStartTrip()) {
                if (!isServiceRunning()) {
                    Intent myIntent = new Intent(getApplicationContext(), LocationLoggerService.class);
                    startService(myIntent);
                    session.setStartService(false);
                }
                //Toast.makeText(getApplicationContext(), "LATLNG"+ session.getText(), Toast.LENGTH_LONG).show();
            }*/

            markerPoints = new ArrayList<LatLng>();
            waypoints = new ArrayList<>();
            setUpMapIfNeeded();
            /***************************Logged in UserDetails*************************************/
            username = (TextView) findViewById(R.id.usernme);
            cmpny_nme = (TextView) findViewById(R.id.compny_nme);
            loc = (TextView) findViewById(R.id.loc);

            username.setText(session_USER_NAME);
            cmpny_nme.setText(session_COMPANY_NAME);
            loc.setText("Location : " + session_USER_LOC);
            /***************************Logged in UserDetails*************************************/
            mTitle = mDrawerTitle = getTitle();

            // load slide menu items
            navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

            // nav drawer icons from resources
            navMenuIcons = getResources()
                    .obtainTypedArray(R.array.nav_drawer_icons);

            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

            navDrawerItems = new ArrayList<NavDrawerItem>();

            // adding nav drawer items to array
            // Home
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
            // Find People
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
            // Photos
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));

        /*// Communities, Will add a counter here
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1), true, "22"));
		// Pages
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
		// What's hot, We  will add a counter here
        // navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1), true, "50+"));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1)))*/
            ;
            // Recycle the typed array
            navMenuIcons.recycle();
            mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

            // setting the nav drawer list adapter
            adapter = new NavDrawerListAdapter(getApplicationContext(),
                    navDrawerItems);
            mDrawerList.setAdapter(adapter);

            //CouchBaseDBHelper.getDatabaseList(this);
            // enabling action bar app icon and behaving it as toggle button
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

            mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                    R.string.app_name, // nav drawer open - description for accessibility
                    R.string.app_name // nav drawer close - description for accessibility
            ) {
                public void onDrawerClosed(View view) {
                    getSupportActionBar().setTitle(mTitle);
//				 calling onPrepareOptionsMenu() to show action bar icons
                    invalidateOptionsMenu();
                }

                public void onDrawerOpened(View drawerView) {
                    try {
                        getSupportActionBar().setTitle(mDrawerTitle);
                        // calling onPrepareOptionsMenu() to hide action bar icons
                        invalidateOptionsMenu();
                        float dist = session.getDistance();
                        if (dist > 0) {
                            distanceMap.setText("Distance Travelled:" + round(dist / 1000, 3) + "KM");
                        }
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Start strip status" + session.getStartTrip());
                        if (session.getStartTrip()) {
                            googleMap.clear();
                            //String batteryUse = pref.getString("battery", "");
                            // if (sessionManager.getStartTrip()) {
                            gps = session.getText();
                            if (gps != null && gps != "") {
                                String[] mapData = gps.split(":");
                                if (mapData.length > 2) {
                                    if (pref != null) {
                                        boolean flag = pref.getBoolean("status", false);
                                        if (!flag) {
                                            srclatlng = new LatLng(Double.parseDouble(mapData[0]), Double.parseDouble(mapData[1]));
                                            markerPoints.add(srclatlng);

                                            // Setting the position of the marker
                                            options.position(srclatlng);
                                            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

                                            // Add new marker to the Google Map Android API V2
                                            // Showing the current location in Google Map
                                            googleMap.moveCamera(CameraUpdateFactory.newLatLng(srclatlng));

                                            // Zoom in the Google Map
                                            // googleMap.animateCamera(CameraUpdateFactory.zoomTo(15.5f));
                                            changeCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(srclatlng)
                                                    .zoom(15.5f)
                                                    .bearing(0)
                                                    .tilt(25)
                                                    .build()
                                            ), new GoogleMap.CancelableCallback() {
                                                @Override
                                                public void onFinish() {
                                                    // Your code here to do something after the Map is rendered
                                                }

                                                @Override
                                                public void onCancel() {
                                                    // Your code here to do something after the Map rendering is cancelled
                                                }
                                            });
                                            googleMap.addMarker(options);

                                            //distanceMap.setText(String.valueOf(roundToDecimals(Double.parseDouble(mapData[0]) / 1000, 3)) + "Km");
                                            srcLatitude = Double.parseDouble(mapData[0]);
                                            srcLongitude = Double.parseDouble(mapData[1]);
                                            editor = pref.edit();
//          editor.clear();
                                            editor.putBoolean("status", true);
                                            editor.putString("lat", mapData[0]);
                                            editor.putString("lng", mapData[1]);
                                            // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
                                            editor.commit();
                                            Crashlytics.log(android.util.Log.ERROR, TAG, "Initial LatLng" + Double.parseDouble(mapData[0]) + "::" + Double.parseDouble(mapData[1]));
                                        } else {
                                            //waypoints.clear();
                                            String pLat = pref.getString("lat", "");
                                            String pLng = pref.getString("lng", "");
                                            srclatlng = new LatLng(Double.parseDouble(pLat), Double.parseDouble(pLng));
                                            double lat = Double.parseDouble(mapData[0]);
                                            double lng = Double.parseDouble(mapData[1]);
                                            //   waypoints.add(String.valueOf(lat) + ":" + String.valueOf(lng));
                                            //docId = userName + "_" + companyId;
                                            float dis = Float.parseFloat(mapData[2]);
                                            Crashlytics.log(android.util.Log.ERROR, TAG, "Previous Latitude : " + pLat);
                                            if (!pLat.isEmpty() && !pLat.equals(String.valueOf(lat)) && !pLng.isEmpty() && !pLng.equals(String.valueOf(lng))) {

                                                //distanceMap.setText(String.valueOf(roundToDecimals(dis / 1000, 3)) + "Km");

                                                // Setting the position of the marker
                                                LatLng destLng = new LatLng(lat, lng);
                                                options.position(destLng);
                                                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                                                googleMap.addMarker(options);
                                                //drawPoly(srclatlng, destLng);
                                                editor = pref.edit();
                                                //           editor.clear();
                                                editor.putString("lat", String.valueOf(lat));
                                                editor.putString("lng", String.valueOf(lng));
                                                // Showing the current location in Google Map
                                                googleMap.moveCamera(CameraUpdateFactory.newLatLng(destLng));

                                                // Zoom in the Google Map
                                                // googleMap.animateCamera(CameraUpdateFactory.zoomTo(15.5f));
                                                changeCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(destLng)
                                                        .zoom(15.5f)
                                                        .bearing(0)
                                                        .tilt(25)
                                                        .build()
                                                ), new GoogleMap.CancelableCallback() {
                                                    @Override
                                                    public void onFinish() {
                                                        // Your code here to do something after the Map is rendered
                                                    }

                                                    @Override
                                                    public void onCancel() {
                                                        // Your code here to do something after the Map rendering is cancelled
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                            // }

                            // Crashlytics.log(android.util.Log.ERROR,TAG,"print gps" + gps);

                        }
                    } catch (Exception e) {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Exception OnDrawer open" + e.getMessage());
                    }
                }
            };

            mDrawerLayout.setDrawerListener(mDrawerToggle);
            if (savedInstanceState == null) {
                // on first time display view for first nav item
                displayView(0);
            }
/**********************************Map**************************************************/

            // Session class instance
            tiny = new TinyDB(this);
            waypoints = new ArrayList<>();
//			distanceMap = (TextView) findViewById(R.id.distance);
            if (firstInitialization) {
                flag = displayGpsStatus();
                if (flag) {
                    pref = getSharedPreferences(session.PREF_NAME, Context.MODE_PRIVATE);
                    userName = pref.getString("name", null);
                    companyId = pref.getString("companyId", null);
                    userName = session_USER_NAME;
                    companyId = session_CUST_ACC_CODE;
                } else {
                    alertbox("Gps Status!!", "Your GPS is: OFF");
                }
                firstInitialization = false;
            }

            markerPoints = new ArrayList<LatLng>();
            setUpMapIfNeeded();
            SESSION_START = tiny.getBoolean("FLAG_START");
            pref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
            editor = pref.edit();
            editor.putBoolean("startTrip", SESSION_START);
            editor.commit();
            Crashlytics.log(android.util.Log.ERROR, TAG, "SESSION_START " + SESSION_START);

			/*gps = LocationLoggerService.Text;
			Crashlytics.log(android.util.Log.ERROR,TAG, "Print gps at home" + gps);
			if (gps != null && gps != "") {
				String[] mapData = gps.split(":");
				if (mapData.length > 2) {

					double lat = Double.parseDouble(mapData[0]);
					double lng = Double.parseDouble(mapData[1]);
					float dis = Float.parseFloat(mapData[2]);
					pref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
					editor = pref.edit();
//				editor.clear();
					editor.putFloat("dis", dis);
					// editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
					editor.commit();
				}
			}*/
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception OnCreate HomeMainActivity" + e.getMessage());
        }
    }

    private boolean isServiceRunning() {
        boolean flag = true;
        try {
            ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (!("LocationLoggerService".equals(service.service.getClassName()))) {
                    flag = false;
                } else {
                    flag = true;
                }
            }
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception isServiceRunning" + e.getMessage());
        }
        return flag;
    }

    private void setUpMapIfNeeded() {
        try {
            if (googleMap == null) {
                // Try to obtain the map from the SupportMapFragment.
                googleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                        .getMap();
            }
            // Check if we were successful in obtaining the map.
            if (googleMap != null) {
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                // Getting reference to the SupportMapFragment of activity_main.xml
                SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

                // Getting GoogleMap object from the fragment
                googleMap = fm.getMap();

                // Enabling MyLocation Layer of Google Map
                googleMap.setMyLocationEnabled(true);

                LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
                        0, this);
				/*Criteria criteria = new Criteria();
				criteria.setAccuracy(Criteria.ACCURACY_FINE);
				criteria.setPowerRequirement(Criteria.POWER_LOW);
				mlocManager.requestLocationUpdates(0,                            //Commented on 29dec2015
						0, criteria, this, null);*/
                mlocation = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                srclatlng = new LatLng(mlocation.getLatitude(), mlocation.getLongitude());

                // Add new marker to the Google Map Android API V2
                // Showing the current location in Google Map
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(srclatlng));

                // Zoom in the Google Map
                // googleMap.animateCamera(CameraUpdateFactory.zoomTo(15.5f));
                changeCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(srclatlng)
                        .zoom(15.5f)
                        .bearing(0)
                        .tilt(25)
                        .build()
                ), new GoogleMap.CancelableCallback() {
                    @Override
                    public void onFinish() {
                        // Your code here to do something after the Map is rendered
                    }

                    @Override
                    public void onCancel() {
                        // Your code here to do something after the Map rendering is cancelled
                    }
                });
            }

        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception setUpMapIfNeeded" + e.getMessage());
        }
    }

    //	@Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void TaskCompletionResult(boolean result) {
        if (result) {
            //you may call the cancel() method but if it is not handled in doInBackground() method
            if (progress != null) {
                if (progress.isShowing()) {
                    progress.dismiss();
                }
            }
            if (AutosyncTask != null && AutosyncTask.getStatus() != AsyncTask.Status.FINISHED)
                AutosyncTask.cancel(true);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(HomeMainActivity.this, "DATA SYNC IS SUCCESSFULLY DONE", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * Slide menu item click listener
     */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            displayView(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem settingsItem = menu.findItem(R.id.notification);

        if (session.getNotify()) {
            settingsItem.setIcon(getResources().getDrawable(R.drawable.notification_small));
        } else {
            settingsItem.setIcon(getResources().getDrawable(R.drawable.notification_small));
        }
        // set your desired icon here based on a flag if you like

        return super.onPrepareOptionsMenu(menu);
    }

	/*@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		// Save the user's current game state
		savedInstanceState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY, mRequestingLocationUpdates);
		savedInstanceState.putParcelable(LOCATION_KEY, mCurrentLocation);
		savedInstanceState.putString(LAST_UPDATED_TIME_STRING_KEY, mLastUpdateTime);

		// Always call the superclass so it can save the view hierarchy state
		super.onSaveInstanceState(savedInstanceState);
	}*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            case R.id.trip_started:
                Intent intent = new Intent(HomeMainActivity.this, Start_End_TripMainActivity.class);
                intent.putExtra("NAV_FORM_TYPE_VAL", "FORM_HOME");
                startActivity(intent);
                return true;
            case R.id.notification:

                Intent i = new Intent(HomeMainActivity.this, Notification.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     */
    private void displayView(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new HomeFragment();
                break;
            case 1:
                OnClickSync();
                break;
            case 2:
                showLogoutAlert();
                break;
            default:
                break;
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.mainContent, fragment).commit();

            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(navMenuTitles[position]);
            mDrawerLayout.closeDrawer(Gravity.LEFT);
//			mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Crashlytics.log(android.util.Log.ERROR, "MainActivity", "Error in creating fragment");
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public void OnClickSync() {
        try {
            startService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
            //**When ever you want to check Internet Status in your application call isConnectingToInternet() function and it will return true or false***/
            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
//    			   		Boolean isInternetPresent = false;
            Boolean isInternetPresent = cd.isConnectingToInternet(); // true or false
            // get Internet status
            isInternetPresent = cd.isConnectingToInternet();

            // check for Internet status
            if (isInternetPresent) {
                // Internet Connection is Present
                // make HTTP requests
//				btnsyn.setEnabled(false);
                OnAutoSync();
//    			         		new SoapAccessTask().execute();
//				btnsyn.setEnabled(false);

            } else {
                // Internet connection is not present
                // Ask user to connect to Internet
                Toast.makeText(HomeMainActivity.this, "Please Check Your Mobile Internet Connection ", Toast.LENGTH_LONG).show();
//				btnsyn.setEnabled(true);
            }
        }/***End of try***/ catch (SQLiteException e) {
//			Toast.makeText(TOMDel_pickup_screenActivity.this,"Please try Login again !!! " , Toast.LENGTH_LONG).show();
//			onClickLogOut();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
//			onClickLogOut();
//			Toast.makeText(TOMDel_pickup_screenActivity.this,"Please try Login again !!! " , Toast.LENGTH_LONG).show();
        }
    }

    ;

    public void OnAutoSync() {
        try {
            //**When ever you want to check Internet Status in your application call isConnectingToInternet() function and it will return true or false***/
            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
//    	   		Boolean isInternetPresent = false; 
            Boolean isInternetPresent = cd.isConnectingToInternet(); // true or false
            // get Internet status
            isInternetPresent = cd.isConnectingToInternet();

            // check for Internet status
            if (isInternetPresent) {
                //check if any previous task is running, if so then cancel it
                //it can be cancelled if it is not in FINISHED state
                if (AutosyncTask != null && AutosyncTask.getStatus() != AsyncTask.Status.FINISHED)
                    AutosyncTask.cancel(true);
                AutosyncTask = new AutosyncSoapAccessTask(this, progress); //every time create new object, as AsynTask will only be executed one time.
                AutosyncTask.execute();


            } else {
//            	 Toast.makeText(HomeMainActivity.this,"Please Check Your Mobile Internet Connection", Toast.LENGTH_LONG).show();
            }

            /****Commented on 12NOV2014 12:40pm****/
            //    	new AutosyncSoapAccessTask().execute();
        } catch (Exception e) {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception HomeMainActivity OnAutoSync " + e.getMessage());
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error HomeMainActivity UnsatisfiedLinkError ");

        }
    }

    //starting asynchronus task
    class AutosyncSoapAccessTask extends AsyncTask<String, Integer, Integer> {

        TaskDelegate taskDelegate;
        ProgressDialog Dialog;


        AutosyncSoapAccessTask(TaskDelegate taskDelegate, ProgressDialog Dialog) {
            this.taskDelegate = taskDelegate;
            this.Dialog = Dialog;
        }

        @Override
        protected void onPreExecute() {

            //if you want, start progress dialog here
            // NOTE: You can call UI Element here.
            Dialog.setMessage("Please wait..");
            Dialog.show();
            Dialog.setCancelable(true);
        }

        @Override
        protected Integer doInBackground(String... params) {
            try {
                ret = 0;
                Crashlytics.log(android.util.Log.ERROR, TAG, "print session_USER_NAME " + session_USER_NAME + session_user_pwd + session_CUST_ACC_CODE + session_USER_LOC + session_USER_NUMERIC_ID + session_CURRENT_DT + "Get token" +
                        session.getKeyToken() + ": GetIMEI"
                        + session.getKeyIMEI());

                if ((session_USER_NAME != null && session_USER_NAME != "null") && (session_user_pwd != null && session_user_pwd != "null") && (session_CUST_ACC_CODE != null && session_CUST_ACC_CODE != "null")) {
                    String entityString_del = "/" + URLEncoder.encode(session_USER_NAME) + "/" + URLEncoder.encode(session_user_pwd)
                            + "/" + URLEncoder.encode(session_CUST_ACC_CODE) + "/" + session.getKeyToken();

                    Crashlytics.log(android.util.Log.ERROR, TAG, "print entityString_del" + entityString_del);

                    try {

                     /*   runOnUiThread(new Runnable() {
                            @Override
                            public void run() {*/
                        RequestBody formBody = new FormBody.Builder()
                                .add("username", session_USER_NAME)
                                .add("password", session_user_pwd)
                                .add("iemi", session.getKeyIMEI())
                                .add("timezone", session.getTimezone())
                                .add("company_id", session_CUST_ACC_CODE)
                                .add("token", session.getKeyToken())
                                .build();
                        new PostRequest(new PostRequest.AsyncResponse() {
                            @Override
                            public void processFinish(String output) {
                                session_outscan_resp = output;
                                if (output != null && output != "") {
                                    /**parser used for del outscan**/

                                    ParseOutscanXmlResponse flightListParser = new ParseOutscanXmlResponse(output, getApplicationContext());
                                    try {
                                        flightListParser.parse();
                                    } catch (XmlPullParserException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                    ParsePickupOutscanXML pickupListParser = new ParsePickupOutscanXML(session_outscan_resp.replaceAll("[^\\x20-\\x7e]", ""), getApplicationContext());
                                    try {
                                        //pickUpList = flightListParser.parseXml();
                                        pickupListParser.parseXml(session_outscan_resp.replaceAll("[^\\x20-\\x7e]", ""), getApplicationContext());
                                        assignmentResponse = true;
                                        //dataDoc = flightListParser.getStoredDocId();
                                        //Crashlytics.log(android.util.Log.ERROR,TAG,"DocStored" + dataDoc);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    if (assignmentResponse && companyReason) {
                                        taskDelegate.TaskCompletionResult(true);
                                    }
                                }
                            }
                        }, formBody, URL_outscan).doInBackground();

                          /*  }
                        });
*/
                    } catch (Exception e) {
                        Log.e(TAG, "Error in post connection" + e);
                    }
                          /*  }
                        });*/

                    try {
                        final CouchBaseDBHelper dbHelper = new CouchBaseDBHelper(getApplicationContext());
                        Date curDate = new Date();
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                        Map<String, String> data = new HashMap<>();
                        final String mLastUpdateTime = dateFormat.format(curDate).toString();
                        data.put("startkey", "\"" + mLastUpdateTime + " 00:00:00" + "\"");
                        if (session == null) {
                            session = new SessionManager(getApplicationContext());
                        }

                        try {
                            Call<ApiResponse> call = App.getRestClient().getPickUpService().getPickUp(session.getPickupUrl(), data);
                            call.enqueue(new Callback<ApiResponse>() {
                                @Override
                                public void onResponse(Response<ApiResponse> apiResponse, Retrofit retrofit) {
                                    Log.e(TAG, "Parsing successful for Pickup");

                                    if (!apiResponse.body().getRows().isEmpty()) {
                                        Set<String> barcodeScan = new HashSet<>();
                                        Set<String> setScanData = new HashSet<>();
                                        int responseSize = apiResponse.body().getRows().size();
                                        for (int i = 0; i < responseSize; i++) {
                                            if (apiResponse.body().getRows().get(i).getId() != null && apiResponse.body().getRows().get(i).getValue().getCompanyId() != null && apiResponse.body().getRows().get(i).getValue().getUID() != null && apiResponse.body().getRows().get(i).getValue().getLocID() != null) {
                                                if (apiResponse.body().getRows().get(i).getId().contains(mLastUpdateTime) && apiResponse.body().getRows().get(i).getValue().getCompanyId().equals(session_CUST_ACC_CODE) && apiResponse.body().getRows().get(i).getValue().getUID().equals(session_USER_NUMERIC_ID)
                                                        && apiResponse.body().getRows().get(i).getValue().getLocID().contains(String.valueOf(session.getLocId()))) {
                                                    Log.e(TAG, "Pickup Response body :" + apiResponse.body().getRows().get(i).getValue().getCompleteStatus());
                                                    List<String> save = new ArrayList<String>();
                                                    Row row = apiResponse.body().getRows().get(i);
                                                    //Key rowValue = row.getKey();
                                                    Value rowValue = row.getValue();
                                                    save = rowValue.getScanItem();
                                                    barcodeScan.addAll(save);
                                                    dbHelper.setScanItems(rowValue.getAssignmentNo(), barcodeScan, row.getId());
                                                    //setScanData = session.getScannedData(rowValue.getAssignmentNo());
                                       /* String[] dataId;
                                        if (setScanData != null) {
                                            dataId = setScanData.toArray(new String[setScanData.size()]);
                                            save.addAll(Arrays.asList(dataId));
                                        }*/

                                       /* save.addAll(rowValue.getScanItem());
                                        barcodeScan.addAll(save);
                                        dataId = barcodeScan.toArray(new String[barcodeScan.size()]);
                                        save.clear();
                                        save.addAll(Arrays.asList(dataId));*/

                                                    // session.setScannedData(barcodeScan, rowValue.getAssignmentNo());
                                                    session.setReason(rowValue.getAssignmentNo(), rowValue.getPickupReason());
                                                    session.setReason(rowValue.getAssignmentNo(), rowValue.getPickupReason());
                                                    if (save.size() > 0) {
                                                        Log.e(TAG, "Response1: " + rowValue.getAssignmentNo() + ":" + rowValue.getCompleteStatus());
                                                        dbHelper.updateInitialStatus(row.getId(), save, "Y", apiResponse.body().getRows().get(i).getKey(),
                                                                rowValue.getPickupReason(), rowValue.getPickupRemark(), rowValue.getCompanyId(), rowValue.getLocID(), rowValue.getUID(), rowValue.getAssignmentNo(), rowValue.getReasonId());


                                                    } else {
                                                        Log.e(TAG, "Response1: " + rowValue.getAssignmentNo() + ":" + rowValue.getCompleteStatus());
                                                        dbHelper.updateInitialStatus(row.getId(), save, rowValue.getCompleteStatus(), apiResponse.body().getRows().get(i).getKey(),
                                                                rowValue.getPickupReason(), rowValue.getPickupRemark(), rowValue.getCompanyId(), rowValue.getLocID(), rowValue.getUID(), rowValue.getAssignmentNo(), rowValue.getReasonId());
                                                    }
                                                   /* Log.e(TAG, "Response1: " + rowValue.getAssignmentNo() + ":" + rowValue.getCompleteStatus());
                                                    dbHelper.updateInitialStatus(row.getId(), save, rowValue.getCompleteStatus(), apiResponse.body().getRows().get(i).getKey(),
                                                            rowValue.getPickupReason(), rowValue.getPickupRemark(), rowValue.getCompanyId(), rowValue.getLocID(), rowValue.getUID(), rowValue.getAssignmentNo(), rowValue.getReasonId());
*/
                                                }
                                                barcodeScan.clear();
                                            }
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    Log.e("Error :in Pickup ", "" + t.getLocalizedMessage());

                                }
                            });
                        } catch (Exception e) {
                            Log.e(TAG, "Exception in ApiResponse api" + e.getMessage());
                        }

                        try {
                            Call<ApiResponse1> call = App.getRestClient().getPickUpList().getRVPList(session.getRVPUrl(), data);
                            call.enqueue(new Callback<ApiResponse1>() {
                                @Override
                                public void onResponse(Response<ApiResponse1> apiResponse1, Retrofit retrofit) {
                                    if (!apiResponse1.body().getRows().isEmpty()) {
                                        Set<String> barcodeScan = new HashSet<>();
                                        int responseSize = apiResponse1.body().getRows().size();
                                        for (int i = 0; i < responseSize; i++) {
                                            if (apiResponse1.body().getRows().get(i).getId() != null && apiResponse1.body().getRows().get(i).getValue().getCompanyId() != null && apiResponse1.body().getRows().get(i).getValue().getUID() != null && apiResponse1.body().getRows().get(i).getValue().getLocID() != null) {
                                                if (apiResponse1.body().getRows().get(i).getId().contains(mLastUpdateTime) && apiResponse1.body().getRows().get(i).getValue().getCompanyId().equals(session_CUST_ACC_CODE) && apiResponse1.body().getRows().get(i).getValue().getUID().equals(session_USER_NUMERIC_ID)
                                                        && apiResponse1.body().getRows().get(i).getValue().getLocID().contains(String.valueOf(session.getLocId()))) {
                                                    Row1 row = apiResponse1.body().getRows().get(i);
                                                    Value1 rowValue = row.getValue();

                                                    session.setReason(rowValue.getAssignmentNo(), rowValue.getPickupReason());
                                                    Log.e(TAG, "Response2: " + rowValue.getAssignmentNo() + ":" + rowValue.getCompleteStatus());
                                                    dbHelper.updateinitialRVPStatus(row.getId(), rowValue.getCompleteStatus(), rowValue.getCreatedOn(), rowValue.getWaybillNo(), rowValue.getSignature(), rowValue.getCustName(), rowValue.getCompanyId(), rowValue.getLocID(), rowValue.getUID(), rowValue.getAssignmentNo(), rowValue.getImage(), rowValue.getItemDescription(), rowValue.getReturnReason(), rowValue.getDescribedValue(), rowValue.getRelationship(), rowValue.getCustNo(), rowValue.getPickupReason(), rowValue.getPickupRemark(),
                                                            rowValue.getLatitude(),rowValue.getLongitude());
                                                }
                                                barcodeScan.clear();
                                            }
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    Log.e("Error : in RVP", "" + t.getLocalizedMessage());

                                }
                            });
                        } catch (Exception e) {
                            Log.e(TAG, "Exception in ApiResponse1 api " + e.getMessage());
                        }
                        try {
                            Call<ECOMParser> call = App.getRestClient().getEcomParserService().getEcomParserservice(session.getEcomUrl(), data);
                            call.enqueue(new Callback<ECOMParser>() {
                                @Override
                                public void onResponse(Response<ECOMParser> apiResponse, Retrofit retrofit) {
                                    if (!apiResponse.body().getRows().isEmpty()) {
                                        Set<String> barcodeScan = new HashSet<String>();
                                        Set<String> setScanData = new HashSet<>();
                                        int responseSize = apiResponse.body().getRows().size();
                                        for (int i = 0; i < responseSize; i++) {
                                            if (apiResponse.body().getRows().get(i).getId() != null && apiResponse.body().getRows().get(i).getValue().getCompanyId() != null && apiResponse.body().getRows().get(i).getValue().getUID() != null && apiResponse.body().getRows().get(i).getValue().getLocID() != null) {
                                                if (apiResponse.body().getRows().get(i).getId().contains(mLastUpdateTime) && apiResponse.body().getRows().get(i).getValue().getCompanyId().equals(session_CUST_ACC_CODE) && apiResponse.body().getRows().get(i).getValue().getUID().equals(session_USER_NUMERIC_ID)
                                                        && apiResponse.body().getRows().get(i).getValue().getLocID().contains(String.valueOf(session.getLocId()))) {
                                                    List<String> save = new ArrayList<String>();
                                                    Row2 row = apiResponse.body().getRows().get(i);
                                                    //Key3 rowValue = row.getKey();
                                                    Value2 rowValue = row.getValue();
                                                    setScanData = session.getScannedData(rowValue.getAssignmentNo());
                                                    String[] dataId;
                                                    if (setScanData != null) {
                                                        dataId = setScanData.toArray(new String[setScanData.size()]);
                                                        save.addAll(Arrays.asList(dataId));
                                                    }

                                                    save.addAll(rowValue.getScanItem());
                                                    barcodeScan.addAll(save);

                                                    session.setScannedData(barcodeScan, rowValue.getAssignmentNo());
                                                    session.setReason(rowValue.getAssignmentNo(), rowValue.getPickupReason());
                                                    Log.e(TAG, "Response4: " + rowValue.getAssignmentNo() + ":" + rowValue.getScanItem());
                                                    dbHelper.updateInitialEcomData(row.getId(), save, rowValue.getCompleteStatus(), rowValue.getCreatedOn(),
                                                            rowValue.getPickupReason(), rowValue.getPickupRemark(), rowValue.getCompanyId(), rowValue.getLocID(), rowValue.getUID(), rowValue.getAssignmentNo(), rowValue.getDoctype(), rowValue.getReasonid());

                                                }
                                                barcodeScan.clear();
                                            }
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    Log.e("Error : in ECOM", "" + t.getLocalizedMessage());

                                }
                            });
                        } catch (Exception e) {
                            Crashlytics.log(Log.DEBUG, TAG, "Exception in Ecom api " + e.getMessage());
                        }

                        try {
                            Map<String, String> data1 = new HashMap<>();
                            data1.put("username", session_USER_NAME);
                            data1.put("password", session_user_pwd);
                            data1.put("company_id", session_CUST_ACC_CODE);
                            data1.put("iemi", session.getKeyIMEI());
                            data1.put("timezone", session.getTimezone());
                            data1.put("token", session.getKeyToken());

                            Call<Escan> call = App.getRestClient().getEcomService().getEcomService(data1);
                            call.enqueue(new Callback<Escan>() {
                                @Override
                                public void onResponse(Response<Escan> escan, Retrofit retrofit) {
                                    if (escan != null) {
                                        if (escan.body() != null) {
                                            if (!escan.body().getData().isEmpty()) {
                                                int responseSize = escan.body().getData().size();
                                                Set<String> pickUpset;

                                                for (int i = 0; i < responseSize; i++) {

                                                    if (escan.body().getData().get(i).getORDERDATE().contains(mLastUpdateTime) && String.valueOf(escan.body().getData().get(i).getCOMPANYID()).contains(session_CUST_ACC_CODE) && String.valueOf(escan.body().getData().get(i).getUSERID()).equals(session_USER_NUMERIC_ID) && String.valueOf(escan.body().getData().get(i).getLOCATION()).equals(session.getLocId())) {
                                                        pickUpset = new HashSet<String>(escan.body().getData().get(i).getSCANITEM());
                                                        session.setScanRef(escan.body().getData().get(i).getASSIGNMENTNO(), pickUpset);
                                                        session.setDocType(escan.body().getData().get(i).getASSIGNMENTNO(), escan.body().getData().get(i).getDOCTYPE());
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    Log.e("Error : in ESCAN", "" + t.getLocalizedMessage());

                                }
                            });
                        } catch (Exception e) {
                            Crashlytics.log(Log.DEBUG, TAG, "Exception in Ecom api " + e.getMessage());
                        }

                    } catch (Exception e) {
                        Crashlytics.log(Log.ERROR, "ApiResponse", e.getStackTrace() + "");
                        Crashlytics.logException(e);
                    }
                }
                /*******************UD reason code*****************************/

                if (session_CUST_ACC_CODE != null && session_CUST_ACC_CODE != "null") {
                    try {

                      /*  runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                       */
                        RequestBody formBody = new FormBody.Builder()
                                .add("iemi", session.getKeyIMEI())
                                .add("timezone", session.getTimezone())
                                .add("company_id", session_CUST_ACC_CODE)
                                .add("token", session.getKeyToken())
                                .build();
                        new RequestCompanyReason(new RequestCompanyReason.AsyncResponse() {
                            @Override
                            public void processFinish(String output) {
                                session_udreason_resp = output;
                                if (session_udreason_resp != null && session_udreason_resp != "") {
                                    /**parser used for ud reasons **/
                                    ParseUDReason_XmlResponse HoldReasonListParser = new ParseUDReason_XmlResponse(session_udreason_resp, getApplicationContext());
                                    try {
                                        HoldReasonListParser.parse();

                                    } catch (XmlPullParserException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                                /*----------------- ----parser used for PickUp cancel reason---------------------------------------------*/
                                    ParsePickUpReason_XmlResponse PickUpReasonListParser = new ParsePickUpReason_XmlResponse(session_udreason_resp, getApplicationContext());
                                    try {
                                        PickUpReasonListParser.parse();
                                        companyReason = true;
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    if (assignmentResponse && companyReason) {
                                        taskDelegate.TaskCompletionResult(true);
                                    }
                                }
                            }
                        }, formBody, URL_get_ud_resn).doInBackground();
                       /*     }
                        });*/

                    } catch (Exception e) {
                        Log.e(TAG, "Error in post connection" + e);
                    }
                }

                /*******************Transfer Users*****************************/

                //***********************STEP 2 GETTING ALL THE DELIVERIES TO SYNC TO CENTRAL DB FROM LOCAL DB **********************/
                //***********************OPEN DATABASE TO PERFORM READ/WRITE OPERATION***********************/
                try {
                    if (session_DB_PATH != null && session_DB_PWD != null) {
//    		db_del_complete=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                        db_del_complete = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                        Assgn_flag = "TRUE";
                        Assgn_type = "D";
//    		 cursor_del_complete = db_del_complete.rawQuery("SELECT T_OUT_Scan_U_ID,T_Assignment_Id,T_Assignment_Number,B_is_Completed,F_Amount_Collected,D_Completed_Date,D_Completed_time,T_Received_by_Collected_from,T_Relationship,T_Signature,T_Photo,T_AcquiredLat,T_AccquiredLon,C_is_Sync FROM TOM_Assignments where B_is_Completed='" + Assgn_flag + "' and T_Assignment_Type = '" + Assgn_type + "' and C_is_Sync IS NOT 1 LIMIT 100",null);
                        cursor_del_complete = db_del_complete.rawQuery("SELECT emp.T_OUT_Scan_U_ID,emp.T_Out_Scan_Location,emp.T_Receiver_Contact_Num ,emp.T_Assignment_Id, emp.T_Assignment_Number, emp.B_is_Completed,emp.F_Amount ,emp.F_Amount_Collected, emp.D_Completed_Date, emp.D_Completed_time, emp.T_Received_by_Collected_from, emp.T_Relationship, emp.T_Signature, emp.T_Photo, emp.T_AcquiredLat, emp.T_AccquiredLon, emp.T_Signature_dt, emp.T_Cust_Acc_NO, emp.C_is_Sync,mgr.T_RELATION_TYP,mgr.T_RELATION_ID  FROM  TOM_Assignments  emp JOIN TBL_Relation_mstr  mgr ON mgr.T_RELATION_TYP = emp.T_Relationship where  emp.B_is_Completed='" + Assgn_flag + "' and  emp.T_Assignment_Type = '" + Assgn_type + "' and C_is_Sync IS NOT 1", null);
                        //	new String[]{""+employeeId});

                        syncdelcnt = cursor_del_complete.getCount();
                        while (cursor_del_complete.moveToNext()) {
                            t_u_id = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Out_Scan_Location"));
                            awb_id = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Assignment_Id"));
                            awb_delnum = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Assignment_Number"));
                            isdel = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("B_is_Completed"));
//							is_cod_coltd="0";
                            cod_amt = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("F_Amount"));
                            assigned_amt = cod_amt.replace(".", "_");
                            cod_amt_coltd = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("F_Amount_Collected"));

                            collectd_amt = cod_amt_coltd.replace(".", "_");
                            Crashlytics.log(android.util.Log.ERROR, TAG, "print amount" + cod_amt_coltd + "collectd_amt" + collectd_amt + "assigned_amt" + assigned_amt);
                            del_dt = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("D_Completed_Date"));

                            del_tm = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("D_Completed_time"));
                            String del_tme = del_tm.replace(":", "_");
                            rec_by = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Received_by_Collected_from"));
                            String recvd_by = rec_by.replace(" ", "%20");
                            String rvr_contact_numbr = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Receiver_Contact_Num"));
                            rec_status = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Relationship"));
                            rel_id = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_RELATION_ID"));
                            sign_path = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Signature"));
                            photo_path = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Photo"));
                            acquired_lat = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_AcquiredLat"));
                            acquired_long = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_AccquiredLon"));
                            sign_dt_tm = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Signature_dt"));
                            del_cust_acc = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Cust_Acc_NO"));

                            if (acquired_lat != null && acquired_lat != "" && !acquired_lat.equals("null")) {

                                acquired_lat = acquired_lat;
                            } else if (acquired_lat == null && acquired_lat == "" && acquired_lat.equals("null")) {
                                acquired_lat = "null";
                            }
                            if (acquired_long != null && acquired_long != "" && !acquired_long.equals("null")) {

                                acquired_long = acquired_long;
                            } else if (acquired_long == null && acquired_long == "" && acquired_long.equals("null")) {
                                acquired_long = "null";
                            }
                            if (photo_id_dt != null && photo_id_dt != "" && !(photo_id_dt.equals("null"))) {
                                photo_id_dt = photo_id_dt;
                            } else if (photo_id_dt == null || photo_id_dt == "" || photo_id_dt.equals("null")) {
                                photo_id_dt = "null";
                            }

                            if (photo_id_tme != null && photo_id_tme != "" && !(photo_id_tme.equals("null"))) {
                                photo_id_tme = photo_id_tme;
                            } else if (photo_id_tme == null || photo_id_tme == "" || photo_id_tme.equals("null")) {
                                photo_id_tme = "null";
                            }

                            if (photo_path != null && photo_path != "" && !(photo_path.equals("null"))) {
                                image = new File(Environment.getExternalStorageDirectory(), "Image keeper/" + photo_path.trim());
//				    selectedImagePath =android.os.Environment.getExternalStorageDirectory().toString()+"/Image keeper/"+ photo_path.trim();
                                image = new File(Environment.getExternalStorageDirectory()
                                        + "/Android/data/com.traconmobi.net/", "Image keeper/" + photo_path.trim());
                                selectedImagePath = android.os.Environment.getExternalStorageDirectory()
                                        + "/Android/data/com.traconmobi.net" + "/Image keeper/" + photo_path.trim();
                                Bitmap bm = reduceImageSize(selectedImagePath);

                                if (bm != null) {

                                    ByteArrayOutputStream BAO = new ByteArrayOutputStream();

                                    bm.compress(Bitmap.CompressFormat.JPEG, 40, BAO);

                                    BAvalue = BAO.toByteArray();
                                    getphotobytearray = Base64.encode(BAvalue);
                                    s_photo = getphotobytearray.replace("+", "%2B");
//				    Crashlytics.log(android.util.Log.ERROR,TAG,"s_photo " + s_photo);
                                } else if (bm == null) {
                                    getphotobytearray = "null";
                                }
                            } else if (photo_path == null && photo_path == "" && photo_path.equals("null")) {
                                photo_path = "null";
                            }
                            try {
                                DateFormat formatter_date = new SimpleDateFormat("yyyy-MM-dd");
                                formatter_date.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                                Date date1 = (Date) formatter_date.parse(del_dt);
                                long dt = date1.getTime() / 1000;
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Today is " + date1.getTime() / 1000 + "dt" + dt);
                                int reqId_DEL = 104;
//									String requestParameter_del="0";//"/"+opsggnpc+"/"+sample+"/"+a2lfrq40kdh0nrut1vl0ani4a2+"/"+142+"/"+1000002292+"/"+UNDEL+"/"+4670+"/"+0+"/"+0+"/"+1447612200+"/"+15_58_9+"/"+null+"/"+null+"/"+null+"/"+5850+"/"+U+"/"+null+"/"+ URLEncoder.encode(token);
                                String status = "DEL";
                                String requestParameter_del = "/" + session.getuserId() + "/" + session_user_pwd + "/" + session.getKeyToken() + "/" + session_CUST_ACC_CODE + "/" + awb_id + "/" + status + "/" + "null" + "/" + assigned_amt + "/" + collectd_amt + "/" + dt + "/" + del_tme + "/" + recvd_by + "/" + rel_id + "/" + rvr_contact_numbr + "/" + t_u_id + "/" + "D" + "/" + acquired_lat + "/" + acquired_long + "/" + "null" + "/" + session.getKeyIMEI();
                                Crashlytics.log(android.util.Log.ERROR, TAG, "print requestParameter_del" + requestParameter_del);
                                try {

                                    RequestBody formBody = new FormBody.Builder()
                                            .add("user_id", session.getuserId())
                                            .add("timezone", session.getTimezone())
                                            .add("imei", session.getKeyIMEI())
                                            .add("company_id", session_CUST_ACC_CODE)
                                            .add("token", session.getKeyToken())
                                            .add("assignmentid", awb_id)
                                            .add("status", status)
                                            .add("reason_id", "NA")
                                            .add("cod_amount", assigned_amt)
                                            .add("cod_amount_collected", collectd_amt)
                                            .add("date", String.valueOf(dt))
                                            .add("time", del_tme)
                                            .add("received_by", recvd_by)
                                            .add("relationship", rel_id)
                                            .add("contact_no", rvr_contact_numbr.trim())
                                            .add("location_id", t_u_id)
                                            .add("transaction_for", "D")
                                            .add("latitude", (acquired_lat != null) ? acquired_lat : "NA")
                                            .add("longitude", (acquired_long != null) ? acquired_long : "NA")
                                            .add("remarks", "NA")
                                            .build();
                                    OkHttpHandlerPost handler_UD = new OkHttpHandlerPost(formBody, URL_DELDtls, status, session_DB_PATH, session_DB_PWD);
                                    session_sync_undel_resp = handler_UD.sendRequest();
                                } catch (Exception e) {
                                    Log.e(TAG, "Exception in fom body" + e.getMessage());
                                }

                                // get user data from session
                               /* HashMap<String, String> resp_Dts8 = session.gethttpsrespDetails();

                                // Userid
                                session_sync_del_resp = resp_Dts8.get(SessionManager.KEY_RESPONSE);*/
                                Crashlytics.log(android.util.Log.ERROR, TAG, "print session_sync_del_resp" + session_sync_undel_resp);
                                status = "photo";
                                //uploading Image and signature
                                RequestBody formdata = new FormBody.Builder()
                                        .add("Assign_ID", awb_id)
                                        .add("Signature", sign_path)
                                        .add("photo", getphotobytearray)
                                        .add("locid", t_u_id)
                                        .add("imei", session.getKeyIMEI())
                                        .add("companyid", del_cust_acc)
                                        .add("token", session.getKeyToken())
                                        .add("timezone", session.getTimezone())
                                        .build();

                                int reqId_photo = 109;
                                OkHttpHandlerPost handler_photo = new OkHttpHandlerPost(formdata, URL_DEL_IMAGES_Dtls, status, session_DB_PATH, session_DB_PWD);
                                String session_sync_del_photoresp = handler_photo.sendRequest();

                                if (session_sync_del_resp != null && session_sync_del_resp != "") {
//    				            	ParsePOD_UDXmlResponse POD_UD_parser = new ParsePOD_UDXmlResponse(session_sync_del_resp);
                                    Parse_CompleteXmlResponse Complete_parser = new Parse_CompleteXmlResponse(session_sync_del_resp);
                                    try {
                                        Complete_parser.parse();
                                    } catch (XmlPullParserException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }

                                    if (Complete_parser.getparsedCompleteList().isEmpty()) {
                                        try {
                                            if (session_DB_PATH != null && session_DB_PWD != null) {
                                                //    				            		   db_del_complete_resp1=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                                db_del_complete_resp1 = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                                del_awb_id = Parse_CompleteXmlResponse.ASSIGNID;
                                                String sync_status = Parse_CompleteXmlResponse.IS_SYNCED;
                                                if (del_awb_id != null || del_awb_id != "") {
                                                    c_del_complete_sync_resp1 = db_del_complete_resp1.rawQuery("UPDATE TOM_Assignments SET C_is_Sync='" + sync_status + "' WHERE T_Assignment_Id = ?", new String[]{"" + del_awb_id});
                                                    while (c_del_complete_sync_resp1.moveToNext()) {
                                                    }
                                                    //    					          		 	if(c_del_complete_sync_resp1 != null)
                                                    //    					          		 	{
                                                    //    					          		 		c_del_complete_sync_resp1.close();
                                                    //    					          		 	}
                                                }
                                                //    					            	   	db_del_complete_resp1.close();
                                            } else {
                                                Crashlytics.log(android.util.Log.ERROR, TAG, "error : step4 HomeMainActivity delsync");
                                                //onClickLogOut();
                                            }
                                        } catch (Exception e) {
                                            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Expense " + e.getMessage());
                                        } finally {
                                            if (c_del_complete_sync_resp1 != null && !c_del_complete_sync_resp1.isClosed()) {
                                                c_del_complete_sync_resp1.close();
                                            }
                                            if (db_del_complete_resp1 != null && db_del_complete_resp1.isOpen()) {
                                                db_del_complete_resp1.close();
                                            }
                                        }
//    				          		 syncdelcnt=cursor.getCount();
                                    } else if (!(Complete_parser.getparsedCompleteList().isEmpty())) {
                                        try {
                                            if (session_DB_PATH != null && session_DB_PWD != null) {
                                                //    				            		   db_del_complete_resp2=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                                db_del_complete_resp2 = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                                del_awb_id = Parse_CompleteXmlResponse.ASSIGNID;
                                                String sync_status = Parse_CompleteXmlResponse.IS_SYNCED;
                                                //    					            	   Crashlytics.log(android.util.Log.ERROR,TAG,"delsync "+ pod_awb);
                                                if (del_awb_id != null || del_awb_id != "") {
                                                    c_del_complete_sync_resp2 = db_del_complete_resp2.rawQuery("UPDATE TOM_Assignments SET C_is_Sync='" + sync_status + "'  WHERE T_Assignment_Id = ?", new String[]{"" + del_awb_id});
                                                    while (c_del_complete_sync_resp2.moveToNext()) {
                                                    }
//	    					            	   if(c_del_complete_sync_resp2 != null)
//	    					            	   {
//	    					            		   c_del_complete_sync_resp2.close();
//	    					            	   }
                                                }
//	    					            	 db_del_complete_resp2.close();
                                            } else {
                                                Crashlytics.log(android.util.Log.ERROR, TAG, "error : step5 HomeMainActivity delsync ");
                                                //onClickLogOut();
                                            }
                                        } catch (Exception e) {
                                            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Expense " + e.getMessage());
                                        } finally {
                                            if (c_del_complete_sync_resp2 != null && !c_del_complete_sync_resp2.isClosed()) {
                                                c_del_complete_sync_resp2.close();
                                            }
                                            if (db_del_complete_resp2 != null && db_del_complete_resp2.isOpen()) {
                                                db_del_complete_resp2.close();
                                            }
                                        }
//    				       		    syncdelcnt=cursor.getCount();
                                    }

                                    ret = ret + syncdelcnt;
                                } else {
                                    //do nothing
                                    Crashlytics.log(android.util.Log.ERROR, TAG, "session_sync_del_resp is null");
                                }
//								}
                            } catch (Exception e) {
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception session_sync_del_resp is null" + e.getMessage());
                            }
//    						}
//    					 }).start();

                        }
                    } else {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "HomeMainActivity delivery session_DB_PATH is null");
                        //onClickLogOut();
                    }
                } catch (Exception e) {
                    Crashlytics.log(android.util.Log.ERROR, TAG, "HomeMainActivity delivery Exception" + e.getMessage());
                } finally {
                    if (cursor_del_complete != null && !(cursor_del_complete.isClosed())) {
                        cursor_del_complete.close();
                    }
                    if (db_del_complete != null && db_del_complete.isOpen()) {
                        db_del_complete.close();
                    }
                }

                //*********************** STEP 3 GETTING ALL THE UNDELIVERIES TO SYNC TO CENTRAL DB FROM LOCAL DB **********************/
                try {
                    if (session_DB_PATH != null && session_DB_PWD != null) {

                        db_undel = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                        Assgn_type = "D";
//		cursor_undel = db_undel.rawQuery("SELECT T_U_ID,T_Assignment_Id,T_Assignment_Number,T_Assignment_Type,T_Remarks,T_Photo,T_Assignment_In_Complete_Reason,D_In_Complete_Date,D_In_Complete_Time,T_AccquiredLat,T_AccquiredLon,C_is_Sync FROM Tom_Assignments_In_Complete where T_Assignment_Type = '" + Assgn_type + "' AND  C_is_Sync IS NOT 1 LIMIT 100",null);
                        cursor_undel = db_undel.rawQuery("SELECT T_U_ID,T_Assignment_Id,T_Assignment_Number,T_Assignment_Type,T_Remarks,T_Photo,T_Photo_dt,T_Photo_tm,T_Assignment_In_Complete_Reason,T_Assignment_In_Complete_Reason_Id,D_In_Complete_Date,D_In_Complete_Time,T_AccquiredLat,T_AccquiredLon,T_Cust_Acc_NO,T_Out_Scan_Location,C_is_Sync FROM Tom_Assignments_In_Complete where T_Assignment_Type = '" + Assgn_type + "' AND  C_is_Sync IS NOT 1", null);
                        syncundelcnt = cursor_undel.getCount();
                        while (cursor_undel.moveToNext()) {
                            t_u_id = cursor_undel.getString(cursor_undel.getColumnIndex("T_U_ID"));
                            awb_id1 = cursor_undel.getString(cursor_undel.getColumnIndex("T_Assignment_Id"));
                            awb_numb = cursor_undel.getString(cursor_undel.getColumnIndex("T_Assignment_Number"));
                            ud_reason = cursor_undel.getString(cursor_undel.getColumnIndex("T_Assignment_In_Complete_Reason"));
                            ud_reason_id = cursor_undel.getString(cursor_undel.getColumnIndex("T_Assignment_In_Complete_Reason_Id"));
                            u_reason = ud_reason.replace(" ", "%20");
                            ud_date = cursor_undel.getString(cursor_undel.getColumnIndex("D_In_Complete_Date"));
                            ud_time = cursor_undel.getString(cursor_undel.getColumnIndex("D_In_Complete_Time"));
//							u_time=ud_time.replace(" ", "%20");
                            u_time = ud_time.replace(":", "_");
                            ud_loc_id = cursor_undel.getString(cursor_undel.getColumnIndex("T_Out_Scan_Location"));
                            remarks = cursor_undel.getString(cursor_undel.getColumnIndex("T_Remarks"));
                            rmks = remarks.replace(" ", "%20");
                            photo_path = cursor_undel.getString(cursor_undel.getColumnIndex("T_Photo"));
                            undel_cust_acc = cursor_undel.getString(cursor_undel.getColumnIndex("T_Cust_Acc_NO"));
                            photo_id_dt = cursor_undel.getString(cursor_undel.getColumnIndex("T_Photo_dt"));
                            photo_id_tme = cursor_undel.getString(cursor_undel.getColumnIndex("T_Photo_tm"));

                            Acq_lat = cursor_undel.getString(cursor_undel.getColumnIndex("T_AccquiredLat"));
                            Acq_long = cursor_undel.getString(cursor_undel.getColumnIndex("T_AccquiredLon"));
                            if (Acq_lat == null && Acq_lat == "" && Acq_lat.equals("null")) {
                                Acq_lat = "null";
                            }
                            if (Acq_long == null && Acq_long == "" && Acq_long.equals("null")) {
                                Acq_long = "null";
                            }

                            if (photo_id_dt != null && photo_id_dt != "" && !(photo_id_dt.equals("null"))) {
                                photo_id_dt = photo_id_dt;
                            } else if (photo_id_dt == null || photo_id_dt == "" || photo_id_dt.equals("null")) {
                                photo_id_dt = "null";
                            }

                            if (photo_id_tme != null && photo_id_tme != "" && !(photo_id_tme.equals("null"))) {
                                photo_id_tme = photo_id_tme;
                            } else if (photo_id_tme == null || photo_id_tme == "" || photo_id_tme.equals("null")) {
                                photo_id_tme = "null";
                            }

                            if (photo_path != null && photo_path != "" && !(photo_path.equals("null"))) {
                                photo_path = photo_path;
                            } else if (photo_path == null && photo_path == "" && photo_path.equals("null")) {
                                photo_path = "null";
                            }

                            try {
                                /******************ASyncronous call of the webservice********************/
                                DateFormat formatter_date = new SimpleDateFormat("yyyy-MM-dd");
                                formatter_date.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                                Date date1 = (Date) formatter_date.parse(ud_date);
                                long dt = date1.getTime() / 1000;
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Today is " + date1.getTime() / 1000 + "dt" + dt);

                                int reqId_UD = 105;
                                String status = "UNDEL";
                                String requestParameter = "/" + t_u_id + "/" + session_user_pwd + "/" + session.getKeyToken() + "/" + session_CUST_ACC_CODE + "/" + awb_id1 + "/" + status + "/" + ud_reason_id + "/" + "0" + "/" + "0" + "/" + dt + "/" + u_time + "/" + "null" + "/" + "null" + "/" + "null" + "/" + ud_loc_id + "/" + "U" + "/" + Acq_lat + "/" + Acq_long + "/" + rmks + "/" + session.getKeyIMEI();

                                Crashlytics.log(android.util.Log.ERROR, TAG, "print undel parameter" + requestParameter);

                                RequestBody formBody = new FormBody.Builder()
                                        .add("user_id", t_u_id)
                                        .add("timezone", session.getTimezone())
                                        .add("imei", session.getKeyIMEI())
                                        .add("company_id", session_CUST_ACC_CODE)
                                        .add("token", session.getKeyToken())
                                        .add("assignmentid", awb_id1)
                                        .add("status", status)
                                        .add("reason_id", ud_reason_id)
                                        .add("cod_amount", String.valueOf(0))
                                        .add("cod_amount_collected", String.valueOf(0))
                                        .add("date", String.valueOf(dt))
                                        .add("time", u_time)
                                        .add("received_by", "NA")
                                        .add("relationship", "NA")
                                        .add("contact_no", "NA")
                                        .add("location_id", ud_loc_id)
                                        .add("transaction_for", "U")
                                        .add("latitude", (Acq_lat != null) ? Acq_lat : "NA")
                                        .add("longitude", (Acq_long != null) ? Acq_long : "NA")
                                        .add("remarks", rmks)
                                        .build();

                                OkHttpHandlerPost handler_UD = new OkHttpHandlerPost(formBody, URL_UDDtls, status, session_DB_PATH, session_DB_PWD);
                                session_sync_undel_resp = handler_UD.sendRequest();


                                Crashlytics.log(android.util.Log.ERROR, TAG, "print session_sync_undel_resp" + session_sync_undel_resp);
                                if (session_sync_undel_resp != null && session_sync_undel_resp != "") {

//	        	ParsePOD_UDXmlResponse POD_UD_parser = new ParsePOD_UDXmlResponse(session_sync_undel_resp);
                                    Parse_CompleteXmlResponse UD_parser = new Parse_CompleteXmlResponse(session_sync_undel_resp);
                                    try {
                                        UD_parser.parse();
                                    } catch (XmlPullParserException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }

                                    if (UD_parser.getparsedCompleteList().isEmpty()) {
                                        if (session_DB_PATH != null && session_DB_PWD != null) {
                                            try {
//			                	db_del_incomplete_resp1=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                                db_undel_resp1 = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                                undel_awb_id = Parse_CompleteXmlResponse.ASSIGNID;
                                                String sync_status = Parse_CompleteXmlResponse.IS_SYNCED;
                                                Crashlytics.log(android.util.Log.ERROR, TAG, "print undel_awb_id empty" + undel_awb_id);
                                                if (undel_awb_id != null || undel_awb_id != "") {
                                                    c_undel_sync_resp1 = db_undel_resp1.rawQuery("UPDATE Tom_Assignments_In_Complete SET C_is_Sync='" + sync_status + "' WHERE T_Assignment_Id='" + undel_awbid + "'", null);
//			                			new String[]{""+undel_awb_id});
                                                    while (c_undel_sync_resp1.moveToNext()) {

                                                    }
                                                    c_undelsync_resp1 = db_undel_resp1.rawQuery("UPDATE TOM_Assignments SET C_is_Sync='" + sync_status + "' WHERE T_Assignment_Id='" + undel_awbid + "'", null);
//			                			new String[]{""+undel_awb_id});
                                                    while (c_undelsync_resp1.moveToNext()) {

                                                    }
//			        	        if(c_undel_sync_resp1 !=null && !(c_undel_sync_resp1.isClosed()))
//			        	        {
//			        	        	c_undel_sync_resp1.close();
//			        	        }			        	        

                                                }
//			                	db_undel_resp1.close();
                                            } catch (Exception e) {
                                                Crashlytics.log(android.util.Log.ERROR, TAG, "HomeMainActivity undelsync isEmpty Exception" + e.getMessage());
                                            } finally {
                                                if (c_undel_sync_resp1 != null && !(c_undel_sync_resp1.isClosed())) {
                                                    c_undel_sync_resp1.close();
                                                }
                                                if (c_undelsync_resp1 != null && !(c_undelsync_resp1.isClosed())) {
                                                    c_undelsync_resp1.close();
                                                }
                                                if (db_undel_resp1 != null && db_undel_resp1.isOpen()) {
                                                    db_undel_resp1.close();
                                                }
                                            }
                                        } else {
                                            Crashlytics.log(android.util.Log.ERROR, TAG, "HomeMainActivity db_undel_resp1 session_DB_PATH is null");
                                            //onClickLogOut();
                                        }
//			        	        syncundelcnt=cr.getCount();
                                    } else if (!(UD_parser.getparsedCompleteList().isEmpty())) {
                                        if (session_DB_PATH != null && session_DB_PWD != null) {
                                            try {
//			                		db_del_incomplete_resp2=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                                db_undel_resp2 = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                                undel_awb_id = Parse_CompleteXmlResponse.ASSIGNID;
                                                String sync_status = Parse_CompleteXmlResponse.IS_SYNCED;
                                                Crashlytics.log(android.util.Log.ERROR, TAG, "print undel_awb_id not empty" + undel_awb_id);
                                                if (undel_awb_id != null || undel_awb_id != "") {
                                                    c_undel_sync_resp2 = db_undel_resp2.rawQuery("UPDATE Tom_Assignments_In_Complete SET C_is_Sync='" + sync_status + "' WHERE T_Assignment_Id='" + undel_awb_id + "'", null);
//				                			new String[]{""+undel_awb_id});
                                                    while (c_undel_sync_resp2.moveToNext()) {

                                                    }
                                                    c_undelsync_resp2 = db_undel_resp2.rawQuery("UPDATE TOM_Assignments SET C_is_Sync='" + sync_status + "' WHERE T_Assignment_Id='" + undel_awb_id + "'", null);
//			                			new String[]{""+undel_awb_id});
                                                    while (c_undelsync_resp2.moveToNext()) {

                                                    }
//				        	        if(c_undel_sync_resp2 !=null && !(c_undel_sync_resp2.isClosed()))
//				        	        {
//				        	        	c_undel_sync_resp2.close();
//				        	        }
                                                }
//				                	db_undel_resp2.close();
                                            } catch (Exception e) {
                                                Crashlytics.log(android.util.Log.ERROR, TAG, "HomeMainActivity undelsync isnotEmpty Exception" + e.getMessage());
                                            } finally {
                                                if (c_undel_sync_resp2 != null && !(c_undel_sync_resp2.isClosed())) {
                                                    c_undel_sync_resp2.close();
                                                }
                                                if (c_undelsync_resp2 != null && !(c_undelsync_resp2.isClosed())) {
                                                    c_undelsync_resp2.close();
                                                }
                                                if (db_undel_resp2 != null && db_undel_resp2.isOpen()) {
                                                    db_undel_resp2.close();
                                                }
                                            }
                                        } else {
                                            Crashlytics.log(android.util.Log.ERROR, TAG, "HomeMainActivity db_undel_resp2 session_DB_PATH is null ");
                                            //onClickLogOut();
                                        }
//			        	        syncundelcnt=cr.getCount();
                                    }

                                    ret = ret + syncundelcnt;
                                } else {
                                    //do nothing
                                    Crashlytics.log(android.util.Log.ERROR, TAG, "HomeMainActivity session_sync_undel_resp is null");
                                }
                            } catch (Exception e) {
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception HomeMainActivity session_sync_undel_resp is null" + e.getMessage());
                            }
                        }

                    }

                } catch (Exception e) {
                    Crashlytics.log(android.util.Log.ERROR, TAG, "HomeMainActivity undelivry Exception" + e.getMessage());
                } finally {
                    if (cursor_undel != null && !(cursor_undel.isClosed())) {
                        cursor_undel.close();
                    }
                    if (db_undel != null && db_undel.isOpen()) {
                        db_undel.close();
                    }
                }
            } catch (SQLiteException e) {
                Crashlytics.log(android.util.Log.ERROR, TAG, "Error HomeMainActivity autosync SQLiteException ");

            }
//        catch(DatabaseObjectNotClosedException d)
//        {
//      	  Crashlytics.log(android.util.Log.ERROR,TAG,"Error HomeMainActivity DatabaseObjectNotClosedException autosync ");
//        }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.getMessage();
                e.printStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Error HomeMainActivity autosync Exception ");
            } catch (UnsatisfiedLinkError err) {
                err.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Error HomeMainActivity autosync UnsatisfiedLinkError ");
//    		onClickGoToHomePage();
            }

            return ret;

        }

        @Override
        protected void onPostExecute(Integer ret2) {
            // NOTE: You can call UI Element here.
            // Close progress dialog
        }
    }

    public Bitmap reduceImageSize(String selectedImagePath) {

        Bitmap m = null;
        try {
            File f = new File(selectedImagePath);

            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //The new size we want to scale to
            final int REQUIRED_SIZE = 150;

            //Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            m = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
//            Toast.makeText(getApplicationContext(), "Image File not found in your phone. Please select another image.", Toast.LENGTH_LONG).show();
            Crashlytics.log(android.util.Log.ERROR, TAG, "FileNotFoundException reduceImageSize " + e.getMessage());
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception reduceImageSize" + e.getMessage());
        }
        return m;
    }

    public void showLogoutAlert() {
        try {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

            // Setting Dialog Title
            alertDialog.setTitle("Alert");

            // Setting Dialog Message
            alertDialog.setMessage("Are you sure you want to logout ?");

            // On pressing Settings button
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //            	onClickLogOut();
                    signout();
                }
            });

            // on pressing cancel button
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            // Showing Alert Message
            alertDialog.show();
        } catch (Exception e) {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception TOMDel_pickup_screenActivity showLogoutAlert " + e.getMessage());
            //onClickLogOut();
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error TOMDel_pickup_screenActivity UnsatisfiedLinkError showLogoutAlert ");
            //onClickLogOut();
        }
    }

    public void signout() {
        try {
            Intent logOutActivity = new Intent(this, TOMLoginUserActivity.class);

            logOutActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(logOutActivity);
        } catch (Exception e) {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception TOMDel_pickup_screenActivity signout " + e.getMessage());
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error TOMDel_pickup_screenActivity UnsatisfiedLinkError signout ");
        }
    }


    private void changeCamera(CameraUpdate update, GoogleMap.CancelableCallback callback) {
        googleMap.moveCamera(update);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume called");
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private static double distanceCal(double lat1, double lon1, double lat2, double lon2, String unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == "K") {
            dist = dist * 1.609344;
        } else if (unit == "N") {
            dist = dist * 0.8684;
        }

        return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::	This function converts decimal degrees to radians						 :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::	This function converts radians to decimal degrees						 :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    private Boolean displayGpsStatus() {
        ContentResolver contentResolver = getBaseContext()
                .getContentResolver();
        boolean gpsStatus = Settings.Secure
                .isLocationProviderEnabled(contentResolver,
                        LocationManager.GPS_PROVIDER);
        if (gpsStatus) {
            return true;

        } else {
            return false;
        }
    }

    protected void alertbox(String title, String mymessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your Device's GPS is Disable")
                .setCancelable(false)
                .setTitle("** Gps Status **")
                .setPositiveButton("Gps On",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // finish the current activity
                                // AlertBoxAdvance.this.finish();
                                Intent myIntent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(myIntent);
                                dialog.cancel();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // cancel the dialog box
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static double roundToDecimals(double d, int c) {
        int temp = (int) (d * Math.pow(10, c));
        return ((double) temp) / Math.pow(10, c);
    }


    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    public void drawPoly(LatLng src, LatLng dest) {
        googleMap.addPolyline(new PolylineOptions()
                .add(new LatLng(src.latitude, src.longitude),
                        new LatLng(dest.latitude, dest.longitude))
                .width(5).color(Color.BLUE).geodesic(true));
    }

    //    @Override
    @Override
    public void onBackPressed() {
        // do something on back.
    }

    class UpdateUI implements Runnable {
        String updateString;

        public UpdateUI(String updateString) {
            this.updateString = updateString;
        }

        public void run() {
            Toast.makeText(getApplicationContext(), updateString, Toast.LENGTH_LONG).show();
        }
    }

    @SuppressLint("ParcelCreator")
    class MyResultReceiver extends ResultReceiver {
        public MyResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            if (resultCode == 200) {
                runOnUiThread(new UpdateUI(resultData.getString("end")));
            }
        }
    }
}
