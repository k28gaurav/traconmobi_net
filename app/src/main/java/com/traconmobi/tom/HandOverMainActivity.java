package com.traconmobi.tom;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class HandOverMainActivity extends Activity {

	Spinner s_assgnmnt;
	public String get_assgnmnt;
	// Session Manager Class
    SessionManager session;
    public String session_user_id,session_user_pwd,session_USER_LOC,session_USER_NUMERIC_ID,session_CURRENT_DT,session_CUST_ACC_CODE,session_USER_NAME,session_DB_PATH,session_DB_PWD;
    TextView version_name;
    protected SQLiteDatabase db;
    protected ListView filterList;
    public static String flag,Assignment_Type;
    Cursor cursor,cursor_paymnt;
    MyCustomAdapter dataAdapter = null;
    RelativeLayout rl1,rl2;
    static final String EXTRA_MESSAGE1 = null;
	TextView title;
	public String TAG="HandOverMainActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		try
		{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		//Intializing Fabric
		Fabric.with(this, new Crashlytics());
		setContentView(R.layout.activity_hand_over_main);
		getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, R.layout.activity_hand_over_main);


		title=(TextView)findViewById(R.id.txt_title);
		title.setText("HANDOVER LIST");
		//**********************Getting the version name************//
      	
        	// Session class instance
            session = new SessionManager(getApplicationContext());
            
            /**GETTING SESSION VALUES**/
            
         // get AuthenticateDb data from session
	        HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();
	         
	        // DB_PATH
	        session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);
	     
	        // DB_PWD
	        session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);
	    	
            
            // get user data from session
            HashMap<String, String> login_Dts = session.getLoginDetails();
             
            // Userid
            session_user_id = login_Dts.get(SessionManager.KEY_UID);
             
            // pwd
            session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);
            
            
         // get user data from session
            HashMap<String, String> user = session.getUserDetails();
             
            // session_USER_LOC
            session_USER_LOC= user.get(SessionManager.KEY_USER_LOC);
             
            // session_USER_NUMERIC_ID
            session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);
            
            // session_CURRENT_DT
            session_CURRENT_DT= user.get(SessionManager.KEY_CURRENT_DT);
             
            // session_CUST_ACC_CODE
            session_CUST_ACC_CODE= user.get(SessionManager.KEY_CUST_ACC_CODE);
            
            // session_USER_NAME
            session_USER_NAME= user.get(SessionManager.KEY_USER_NAME);
		
		
//		s_assgnmnt=(Spinner)findViewById(R.id.select_assgnmnt);
		filterList = (ListView) findViewById (R.id.list);
		rl1= (RelativeLayout)findViewById(R.id.rl_btn1);
		rl2= (RelativeLayout)findViewById(R.id.rl_btn2);
		
		rl1.setVisibility(View.VISIBLE);
		rl2.setVisibility(View.INVISIBLE);

		get_dellist();

	}
	catch(Exception e)
	{
		Crashlytics.log(android.util.Log.ERROR, TAG, "oncreate" + e.getMessage());
	}
	}

	public void Onclick_handover(View v)
	{
		/*if(get_assgnmnt.equals("SELECT ASSIGNMENT"))
		{
			 Toast.makeText(HandOverMainActivity.this,"Please select assignment to handover !!", Toast.LENGTH_SHORT).show();  
		}
		else if(get_assgnmnt.equals("DELIVERY"))
	     {*/
			rl1.setVisibility(View.VISIBLE);
			rl2.setVisibility(View.INVISIBLE);
	    	send_dellist(); 
	    	
	   /*  }
		else if(get_assgnmnt.equals("PAYMENT"))
	     {
			rl1.setVisibility(View.INVISIBLE);
    		rl2.setVisibility(View.VISIBLE);
	    	send_paymentlist(); 
	     }*/
	}
	
	public void Onclick_deposit(View v)
	{
		send_depositlist();
	}
	
	
	public void get_dellist()
	{
		try
		{
		db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
		 ArrayList<Country> countryList = new ArrayList<Country>();
		Assignment_Type="D";
    	flag="TRUE";
//    	 cursor = db.rawQuery("SELECT _id , T_Assignment_Number,T_Assignment_Type,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount_Collected,T_Received_by_Collected_from FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed='"+ flag +"' AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
    	cursor = db.rawQuery("SELECT _id , T_Assignment_Number,T_Assignment_Type,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount,F_Amount_Collected,T_Received_by_Collected_from FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed='"+ flag +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' AND D_OutScan_Date='" + session_CURRENT_DT + "' AND C_Handover_sync IS NULL", null);
    	
    	 while(cursor.moveToNext())
    	 {
    		 String awb_numbr=cursor.getString(cursor.getColumnIndex("T_Assignment_Number"));
    		 String congsnee_name=cursor.getString(cursor.getColumnIndex("T_Consignee_Name"));
	    	 String cod_collected=cursor.getString(cursor.getColumnIndex("F_Amount_Collected"));
	    	 String recvr_name=cursor.getString(cursor.getColumnIndex("T_Received_by_Collected_from"));
	    	 
	    	 Country country = new Country(awb_numbr,congsnee_name,cod_collected,recvr_name,false);
	 		  countryList.add(country);
    	 }
    	 cursor.close();
    	 db.close();
    	 //create an ArrayAdaptar from the String Array
		  dataAdapter = new MyCustomAdapter(this,
		    R.layout.assgnmnt_del_list, countryList);
		  // Assign adapter to ListView
		  filterList.setAdapter(dataAdapter);
		  filterList.setVisibility(View.VISIBLE);
		  filterList.setCacheColorHint(Color.WHITE);
		  filterList.setOnItemClickListener(new OnItemClickListener() {
		   @Override
		public void onItemClick(AdapterView parent, View view,
		     int position, long id) {
		    // When clicked, show a toast with the TextView text
		    Country country = (Country) parent.getItemAtPosition(position);

		   }
		  });
		}
		catch(Exception e)
		{
			Crashlytics.log(android.util.Log.ERROR,TAG,"Exception get_dellist "+ e.getMessage());
		}
		finally
		{
			if(cursor !=null)
			{
				cursor.close();
			}
			if(db!=null)
			{
				db.close();
			}
		}
	}
	
	public void get_paymentlist()
	{
		try
		{
		db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
		 ArrayList<Country> countryList = new ArrayList<Country>();
		Assignment_Type="C";
    	flag="TRUE";
//    	 cursor_paymnt = db.rawQuery("SELECT _id , T_Assignment_Number,T_Assignment_Type,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount_Collected,T_Received_by_Collected_from FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed='"+ flag +"' AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
//commented on 29mar2015//    	cursor_paymnt = db.rawQuery("SELECT _id , T_Assignment_Number,T_Assignment_Type,T_Consignee_Name, T_Address_Line1, T_Address_Line2 ,T_City ,T_Contact_number,T_Pincode,F_Amount_Collected,T_Received_by_Collected_from FROM TOM_Assignments WHERE T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed='"+ flag +"' AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and C_Handover_sync IS NULL", null);
    	cursor_paymnt = db.rawQuery("SELECT p._id , p.T_Assignment_Number,p.T_Assignment_Type,p.T_Consignee_Name,p.T_Address_Line1, p.T_Address_Line2 ,p.T_City ,p.T_Contact_number,p.T_Pincode,SUM(s.F_Amount_Collected)F_Amount_Collected,s.T_Received_by_Collected_from FROM TOM_Assignments p left outer join Payment_Collection_Complete s on p.T_Assignment_Number = s.T_Assignment_Number WHERE p.T_Assignment_Type='"+ Assignment_Type  +"' AND p.B_is_Completed='"+ flag +"' AND p.T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and p.C_Handover_sync IS NULL group by p._id , p.T_Assignment_Number,p.T_Assignment_Type,p.T_Consignee_Name, p.T_Address_Line1, p.T_Address_Line2 ,p.T_City ,p.T_Contact_number,p.T_Pincode ", null);
    	 while(cursor_paymnt.moveToNext())
    	 {
    		 String awb_numbr=cursor_paymnt.getString(cursor_paymnt.getColumnIndex("T_Assignment_Number"));
    		 String congsnee_name=cursor_paymnt.getString(cursor_paymnt.getColumnIndex("T_Consignee_Name"));
	    	 String cod_collected=cursor_paymnt.getString(cursor_paymnt.getColumnIndex("F_Amount_Collected"));
	    	 String recvr_name=cursor_paymnt.getString(cursor_paymnt.getColumnIndex("T_Received_by_Collected_from"));
	    	 
	    	 Country country = new Country(awb_numbr,congsnee_name,cod_collected,recvr_name,false);
	 		  countryList.add(country);
    	 }
    	 cursor_paymnt.close();
    	 db.close();
    	 //create an ArrayAdaptar from the String Array
		  dataAdapter = new MyCustomAdapter(this,
		    R.layout.assgnmnt_del_list, countryList);
		  // Assign adapter to ListView
		  filterList.setAdapter(dataAdapter);
		  filterList.setVisibility(View.VISIBLE);
		  filterList.setCacheColorHint(Color.WHITE);
		  filterList.setOnItemClickListener(new OnItemClickListener() {
		   @Override
		public void onItemClick(AdapterView parent, View view,
		     int position, long id) {
		    // When clicked, show a toast with the TextView text
		    Country country = (Country) parent.getItemAtPosition(position);

		   }
		  });
	}
	catch(Exception e)
	{
		Crashlytics.log(android.util.Log.ERROR,TAG,"Exception get_dellist "+ e.getMessage());
	}
	finally
	{
		if(cursor_paymnt !=null)
		{
			cursor_paymnt.close();
		}
		if(db!=null)
		{
			db.close();
		}
	}
	}
	
	 private class MyCustomAdapter extends ArrayAdapter<Country> 
	 {
		 
		  private ArrayList<Country> countryList;
		 
		  public MyCustomAdapter(Context context, int textViewResourceId,
		    ArrayList<Country> countryList) 
		  {
			  
		   super(context, textViewResourceId, countryList);
		     try
		      {
			   this.countryList = new ArrayList<Country>();
			   this.countryList.addAll(countryList);
			  }
				catch(Exception e)
				{
					e.getStackTrace();
					Crashlytics.log(android.util.Log.ERROR,TAG,"Exception AssignedMultipleActivity ArrayList " + e.getMessage());
				}
		     catch(UnsatisfiedLinkError err)
		    	{
		    		err.getStackTrace();
		    		Crashlytics.log(android.util.Log.ERROR,TAG,"Error AssignedMultipleActivity UnsatisfiedLinkError ");
		    		//onClickGoToHomePage();
//		            	    			finish();
		    	}
		  }
		  
		  private class ViewHolder {
		   TextView awb_code,cname,pncde,cty,shpr_name,amt;
		   CheckBox name;
		  }
		  
		  @Override
		  public View getView(int position, View convertView, ViewGroup parent) {
		  
		   ViewHolder holder = null;
		   try
		   {
		   Log.v("ConvertView", String.valueOf(position));
		  
		   if (convertView == null) {
		   LayoutInflater vi = (LayoutInflater)getSystemService(
		     Context.LAYOUT_INFLATER_SERVICE);
		   convertView = vi.inflate(R.layout.assgnmnt_del_list, null);
		  
		   holder = new ViewHolder();
		   holder.awb_code = (TextView) convertView.findViewById(R.id.awb);
		   holder.cname = (TextView) convertView.findViewById(R.id.firstName);
//		   holder.cty = (TextView) convertView.findViewById(R.id.city);
//		   holder.pncde = (TextView) convertView.findViewById(R.id.pincode);
		   holder.amt = (TextView) convertView.findViewById(R.id.COD_Amount);
		   holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
		   holder.shpr_name = (TextView) convertView.findViewById(R.id.shpr_nme);
		   convertView.setTag(holder);
		  
		    holder.name.setOnClickListener( new View.OnClickListener() {
		     @Override
			public void onClick(View v) {
		      CheckBox cb = (CheckBox) v ;
		      Country country = (Country) cb.getTag();
		      country.setSelected(cb.isChecked());
		     }
		    });
		   }
		   else {
		    holder = (ViewHolder) convertView.getTag();
		   }		  
		   Country country = countryList.get(position);
		   holder.amt.setText(country.getcod_collected());
		   holder.cname.setText(country.getcongsnee_name());
//		   holder.cty.setText(country.getcty());
//		   holder.pncde.setText(country.getpncode());
		   holder.shpr_name.setText("Received by: " + country.getrecvr_name());
//		   holder.code.setText(" (" +  country.getcod() + ")" );
//		   holder.code.setTextColor(Color.RED);
		   holder.awb_code.setText(country.getawb_numbr());
		   holder.name.setChecked(country.isSelected());
		   holder.name.setTag(country);
		  
		  }
			catch(Exception e)
			{
				e.getStackTrace();
				Crashlytics.log(android.util.Log.ERROR,TAG,"Exception AssignedMultipleActivity MyCustomAdapter " + e.getMessage());
			}
		   catch(UnsatisfiedLinkError err)
	    	{
	    		err.getStackTrace();
	    		Crashlytics.log(android.util.Log.ERROR,TAG,"Error AssignedMultipleActivity UnsatisfiedLinkError ");	    	
	    	}
		   return convertView;
		  
		  }
	 
		 }
	 
	 public void send_dellist()
	 {
			try
			{
			    
				if(dataAdapter.countryList != null)
			    {
			    	try
				    {

					    String str_amt = null ;
					    String result;
					    int j=0;
					    double total_cod =0;
					    String cod=null;
					    StringBuffer responseText = new StringBuffer();
					    StringBuffer responseText_awb = new StringBuffer();
						 ArrayList<Country> countryList = dataAdapter.countryList;
//						    use for loop i=0 to < countryList.size()
						    for(int i=0;i<countryList.size();i++)
						    {
					    	 Country country = countryList.get(i);
						     if(country.isSelected())
						     {
						    	 responseText.append(country.getawb_numbr()+ "," +country.getcongsnee_name()+"," + country.getrecvr_name() +"," + country.getcod_collected()+"::");
						    	 responseText_awb.append(country.getawb_numbr()+"_H"+",");
						     }
						    }
//						    Crashlytics.log(android.util.Log.ERROR,TAG,"shw handoverlist selected" + responseText.toString() + responseText_awb.toString() );
//							    session.createAssgnMultiple_num_Session(responseText.toString());

			    if(responseText.toString().isEmpty() || responseText.toString() == null)
			    {
			    	Toast.makeText(HandOverMainActivity.this,"Please select assignment to handover !!", Toast.LENGTH_LONG).show();
			    }
			    else if(!(responseText.toString().isEmpty()) && responseText.toString() != null )
			    {
			    
			    	if(session_DB_PATH != null && session_DB_PWD != null)
			    	{
//			    		db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
//			    		db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
				    	 String shw[] = responseText.toString().split(",");
					        for(int k = 0 ; k< shw.length ; k++)
					        {
					        	 result = shw[k];
					        	Crashlytics.log(android.util.Log.ERROR,TAG,"prnt reslt "+ result + "responseText.toString() " + responseText.toString());
					        	
//					        	 String flg="R";
//					        	 Assignment_Type="D";
//					        	cursor_reject_list=db.rawQuery("update TOM_Assignments set T_Filter='" + flg + "' WHERE T_Assignment_Number ='" + result +"' AND T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
//								 while(cursor_reject_list.moveToNext())
//								 {
//
//								}
////								 int cnt = cursor_shippr_list.getCount();
//								 cursor_reject_list.close();

					        }

						 
//						 db.close();
			    	}
			    	else
					{
						Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMDel_FilterMainActivity session_DB_PATH is null ");
						//onClickGoToHomePage();
					}
			    	session.createAssgnMultiple_num_Session(responseText.toString());
			    	session.createAssgnMultiple_recevr_Session(responseText_awb.toString());
			    Intent goToNextActivity = new Intent(getApplicationContext(),HandOver_DtlsActivity.class);
			    goToNextActivity.putExtra(EXTRA_MESSAGE1,get_assgnmnt);
				 startActivity(goToNextActivity);
			    }
			    }
			    catch(SQLiteException ex)
			    {
			    	ex.getStackTrace();
			    	//onClickGoToHomePage();
			    }
		    	catch(UnsatisfiedLinkError err)
		    	{
		    		err.getStackTrace();
		    		Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMDel_FilterMainActivity UnsatisfiedLinkError ");
		    		//onClickGoToHomePage();
//			            	    			finish();
		    	}
			    	finally
			    	{
//			    		if(cursor_reject_list != null)
//			    		{
//			    			cursor_reject_list.close();
//			    		}
//			    		if(db!=null)
//			    		{
//			    			db.close();
//			    		}
			    	}
			    }
			    else
			    {
			    	
				    
				    	Toast.makeText(HandOverMainActivity.this,"No delivered data to handover", Toast.LENGTH_LONG).show();
				    
			    }
		}
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR,TAG,"Exception TOMDel_FilterMainActivity submitClicked " + e.getMessage());
			//onClickGoToHomePage();
		}
		catch(UnsatisfiedLinkError err)
		{
			err.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMDel_FilterMainActivity UnsatisfiedLinkError ");
			//onClickGoToHomePage();
//		        	    			finish();
		}
	 }
	 
	 
	 public void send_paymentlist()
	 {
			try
			{
			    
				if(dataAdapter.countryList != null)
			    {
			    	try
				    {

					    String str_amt = null ;
					    String result;
					    int j=0;
					    double total_cod =0;
					    String cod=null;
					    StringBuffer responseText = new StringBuffer();
					    StringBuffer responseText_awb = new StringBuffer();
						 ArrayList<Country> countryList = dataAdapter.countryList;
//						    use for loop i=0 to < countryList.size()
						    for(int i=0;i<countryList.size();i++)
						    {
					    	 Country country = countryList.get(i);
						     if(country.isSelected())
						     {
//						    	 responseText.append(country.getawb_numbr()+",");
						    	 responseText.append(country.getawb_numbr()+ "," +country.getcongsnee_name()+"," + country.getrecvr_name() +"," + country.getcod_collected()+"::");
						    	 responseText_awb.append(country.getawb_numbr()+"_H"+",");
						     }
						    }
						    Crashlytics.log(android.util.Log.ERROR,TAG,"shw handover paymnt list selected" + responseText.toString());
							    session.createAssgnMultiple_num_Session(responseText.toString());

			    if(responseText.toString().isEmpty() || responseText.toString() == null)
			    {
			    	Toast.makeText(HandOverMainActivity.this,"Please select data", Toast.LENGTH_LONG).show();
			    }
			    else if(!(responseText.toString().isEmpty()) && responseText.toString() != null )
			    {
			    
			    	if(session_DB_PATH != null && session_DB_PWD != null)
			    	{
//			    		db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
//			    		db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
				    	 String shw[] = responseText.toString().split(",");
					        for(int k = 0 ; k< shw.length ; k++)
					        {
					        	 result = shw[k];
					        	Crashlytics.log(android.util.Log.ERROR,TAG,"prnt reslt "+ result + "responseText.toString() " + responseText.toString());
//					        	 String flg="R";
//					        	 Assignment_Type="D";
//					        	cursor_reject_list=db.rawQuery("update TOM_Assignments set T_Filter='" + flg + "' WHERE T_Assignment_Number ='" + result +"' AND T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
//								 while(cursor_reject_list.moveToNext())
//								 {
//
//								}
////								 int cnt = cursor_shippr_list.getCount();
//								 cursor_reject_list.close();

					        }

						 
//						 db.close();
			    	}
			    	else
					{
						Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMDel_FilterMainActivity session_DB_PATH is null ");
						//onClickGoToHomePage();
					}
			    	session.createAssgnMultiple_num_Session(responseText.toString());
			    	session.createAssgnMultiple_recevr_Session(responseText_awb.toString());
			    Intent goToNextActivity = new Intent(getApplicationContext(),HandOver_DtlsActivity.class);
			    goToNextActivity.putExtra(EXTRA_MESSAGE1,get_assgnmnt);
				 startActivity(goToNextActivity);
//			    Intent goToNextActivity = new Intent(getApplicationContext(),TOMDel_pickup_screenActivity.class);
//				 startActivity(goToNextActivity);
			    }
			    }
			    catch(SQLiteException ex)
			    {
			    	ex.getStackTrace();
			    	//onClickGoToHomePage();
			    }
		    	catch(UnsatisfiedLinkError err)
		    	{
		    		err.getStackTrace();
		    		Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMDel_FilterMainActivity UnsatisfiedLinkError ");
		    		//onClickGoToHomePage();
//			            	    			finish();
		    	}
			    	finally
			    	{
//			    		if(cursor_reject_list != null)
//			    		{
//			    			cursor_reject_list.close();
//			    		}
//			    		if(db!=null)
//			    		{
//			    			db.close();
//			    		}
			    	}
			    }
			    else
			    {
			    	
				    
				    	Toast.makeText(HandOverMainActivity.this,"No payment data to handover", Toast.LENGTH_LONG).show();
				    
			    }
		}
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR,TAG,"Exception TOMDel_FilterMainActivity submitClicked " + e.getMessage());
			//onClickGoToHomePage();
		}
		catch(UnsatisfiedLinkError err)
		{
			err.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMDel_FilterMainActivity UnsatisfiedLinkError ");
			//onClickGoToHomePage();
//		        	    			finish();
		}
	 }
	 
	 public void send_depositlist()
	 {
			try
			{
			    
				if(dataAdapter.countryList != null)
			    {
			    	try
				    {

					    String str_amt = null ;
					    String result;
					    int j=0;
					    double total_cod =0;
					    String cod=null;
					    StringBuffer responseText = new StringBuffer();
					    StringBuffer responseText_awb = new StringBuffer();
						 ArrayList<Country> countryList = dataAdapter.countryList;
//						    use for loop i=0 to < countryList.size()
						    for(int i=0;i<countryList.size();i++)
						    {
					    	 Country country = countryList.get(i);
						     if(country.isSelected())
						     {
//						    	 responseText.append(country.getawb_numbr()+",");
						    	 responseText.append(country.getawb_numbr()+ "," +country.getcongsnee_name()+"," + country.getrecvr_name() +"," + country.getcod_collected()+"::");
						    	 responseText_awb.append(country.getawb_numbr()+"_H"+",");
						     }
						    }
						    Crashlytics.log(android.util.Log.ERROR,TAG,"shw handover paymnt list selected" + responseText.toString());
							    session.createAssgnMultiple_num_Session(responseText.toString());

			    if(responseText.toString().isEmpty() || responseText.toString() == null)
			    {
			    	Toast.makeText(HandOverMainActivity.this,"Please select data", Toast.LENGTH_LONG).show();
			    }
			    else if(!(responseText.toString().isEmpty()) && responseText.toString() != null )
			    {
			    
			    	if(session_DB_PATH != null && session_DB_PWD != null)
			    	{
//			    		db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
//			    		db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
				    	 String shw[] = responseText.toString().split(",");
					        for(int k = 0 ; k< shw.length ; k++)
					        {
					        	 result = shw[k];
					        	Crashlytics.log(android.util.Log.ERROR,TAG,"prnt reslt "+ result + "responseText.toString() " + responseText.toString());
//					        	 String flg="R";
//					        	 Assignment_Type="D";
//					        	cursor_reject_list=db.rawQuery("update TOM_Assignments set T_Filter='" + flg + "' WHERE T_Assignment_Number ='" + result +"' AND T_Assignment_Type='"+ Assignment_Type  +"' AND B_is_Completed IS NULL  AND T_Out_Scan_Location='"+ session_USER_LOC +"' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID +"' and D_OutScan_Date='" + session_CURRENT_DT +"'", null);
//								 while(cursor_reject_list.moveToNext())
//								 {
//
//								}
////								 int cnt = cursor_shippr_list.getCount();
//								 cursor_reject_list.close();

					        }

						 
//						 db.close();
			    	}
			    	else
					{
						Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMDel_FilterMainActivity session_DB_PATH is null ");
						//onClickGoToHomePage();
					}
			    	session.createAssgnMultiple_num_Session(responseText.toString());
			    	session.createAssgnMultiple_recevr_Session(responseText_awb.toString());
//			    Intent goToNextActivity = new Intent(getApplicationContext(),DepositMainActivity.class);
//			    goToNextActivity.putExtra(EXTRA_MESSAGE1,"DEPOSIT");
//				 startActivity(goToNextActivity);
//			    Intent goToNextActivity = new Intent(getApplicationContext(),TOMDel_pickup_screenActivity.class);
//				 startActivity(goToNextActivity);
			    }
			    }
			    catch(SQLiteException ex)
			    {
			    	ex.getStackTrace();
			    	//onClickGoToHomePage();
			    }
		    	catch(UnsatisfiedLinkError err)
		    	{
		    		err.getStackTrace();
		    		Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMDel_FilterMainActivity UnsatisfiedLinkError ");
		    		//onClickGoToHomePage();
//			            	    			finish();
		    	}
			    	finally
			    	{
//			    		if(cursor_reject_list != null)
//			    		{
//			    			cursor_reject_list.close();
//			    		}
//			    		if(db!=null)
//			    		{
//			    			db.close();
//			    		}
			    	}
			    }
			    else
			    {
			    	
				    
				    	Toast.makeText(HandOverMainActivity.this,"No payment data to handover", Toast.LENGTH_LONG).show();
				    
			    }
		}
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR,TAG,"Exception TOMDel_FilterMainActivity submitClicked " + e.getMessage());
			//onClickGoToHomePage();
		}
		catch(UnsatisfiedLinkError err)
		{
			err.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR,TAG,"Error TOMDel_FilterMainActivity UnsatisfiedLinkError ");
			//onClickGoToHomePage();
//		        	    			finish();
		}
	 }

	public void toggleMenu(View v)
	{
		Intent homeActivity = new Intent(this, HomeMainActivity.class);
		startActivity(homeActivity);
	}

	public void onclk_noti(View v)
	{
		Intent homeActivity = new Intent(this, NotificationActivity.class);
		startActivity(homeActivity);
	}

	public void onclk_trip(View v)
	{
		Intent homeActivity = new Intent(this, Start_End_TripMainActivity.class);
		startActivity(homeActivity);
	}

	 @Override
		public void onBackPressed() {
	        // do something on back.
	    	try
			{
//	    		mGetDelivered.setEnabled(true);
//	   		 mGetDelivered.setBackgroundColor(Color.GRAY);
	    	/*Intent goToNextActivity = new Intent(getApplicationContext(), TOMDel_pickup_screenActivity.class);
//	    	goToNextActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(goToNextActivity);*/
		    }
			catch(Exception e)
			{
				e.getStackTrace();
				Crashlytics.log(android.util.Log.ERROR,TAG,"Exception HandOver_DtlsActivity onBackPressed " + e.getMessage());
//				//onClickGoToHomePage();	
			}
	    	catch(UnsatisfiedLinkError err)
	    	{
	    		err.getStackTrace();
	    		Crashlytics.log(android.util.Log.ERROR,TAG,"Error HandOver_DtlsActivity UnsatisfiedLinkError ");
//	    		//onClickGoToHomePage();	
	    	}
	    }
	 
	 
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.hand_over_main, menu);
		return true;
	}

}
