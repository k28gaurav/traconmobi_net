package com.traconmobi.tom;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.internal.InterfaceAudience;
import com.couchbase.lite.util.Log;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.model.Polyline;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.fabric.sdk.android.Fabric;

public class PickUpScanList extends Activity {
    /**
     * Items entered by the user is stored in this ArrayList variable
     */
    List<String> list = new ArrayList<String>();
    List<String> savedList = new ArrayList<String>();
    Set<String> barcodeScan  = new HashSet<>();

    /**
     * Declaring an ArrayAdapter to set items to ListView
     */
    private final String TAG = "PickUpScanList";
    ArrayAdapter adapter;
    String pickUpId = "";
    String assignId = "";
    String companyId = "";
    String locationId = "";
    String getDocId;
    CouchBaseDBHelper dbHelper;
    SessionManager sessionManager;
    String docId;
    int size = 0;
    //ArrayList<String> listData;
    //HashMap<String, String> docs;
    ListView listView;
    ArrayList<HashMap<String, String>> shwList;
    private static final String TAG_REF_ID="";
    HashMap<String, String> scan_items;
    EditText scan = null;
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    public static final String PREF_NAME = "ScanPref";
    public static final String PREF_NAME1 = "Pager";
    public int scn_cnt=0;
    public String FORM_TYPE_VAL;
    TextView tv_scn_count;
    Button completeButton;
    TextView title;
    String username;
    Set<String> setScanData = new HashSet<>();
    Date curDate = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    String doctime = dateFormat.format(curDate).toString();
   /* PickUpScanList(DeliveryPicupNavigation nav) {
        this.navigation = nav;
    }*/

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        try
        {
            super.onCreate(savedInstanceState);
            //Intializing Fabric
            Fabric.with(this, new Crashlytics());
            setContentView(R.layout.activity_pick_up_scan_list);
            dbHelper = new CouchBaseDBHelper(getApplicationContext());

            title = (TextView) findViewById(R.id.txt_title);
            title.setText("SCAN LIST");
            sessionManager = new SessionManager(getApplicationContext());
            username = sessionManager.getUserDetails().get(sessionManager.KEY_USER_NAME);
            Intent intnt = getIntent();
            FORM_TYPE_VAL = intnt.getStringExtra("NAV_FORM_TYPE_VAL");
            pickUpId = getIntent().getStringExtra("pickId");
            assignId = getIntent().getStringExtra("assignId");
            companyId = getIntent().getStringExtra("companyId");
            getDocId = pickUpId + "_" + assignId + "_" + companyId + "_" + doctime;
            locationId = sessionManager.getLocId();

            listView = (ListView) findViewById(R.id.scanList);
            tv_scn_count = (TextView) findViewById(R.id.pickup_count);
            adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
            TextView pick = (TextView) findViewById(R.id.pickupId);
            pick.setText(pickUpId);
            List<String> scanitems = null;
            try {
                scanitems  = dbHelper.getScanItems(getDocId, pickUpId);
                Log.e(TAG, "Scanitems: " + scanitems);
            }catch (NullPointerException e) {
                Log.e(TAG, "Nullpointer" + e.getMessage());
            }

        /*    String[] dataId;
            if (setScanData != null) {
                dataId = setScanData.toArray(new String[setScanData.size()]);
                savedList = Arrays.asList(dataId);
                barcodeScan.addAll(savedList);
                Crashlytics.log(android.util.Log.ERROR,TAG, "PickUpLis insde" + savedList + "size" + setScanData.size() + "barcodeScan" + barcodeScan.size());
                scn_cnt = scn_cnt + barcodeScan.size();
                tv_scn_count.setText(Integer.toString(scn_cnt));
            }*/
            if(scanitems != null) {
                barcodeScan.addAll(scanitems);
                scn_cnt = scn_cnt + barcodeScan.size();
                tv_scn_count.setText(Integer.toString(scn_cnt));
                Log.e(TAG, "Barcode scanItems: " + barcodeScan);
            }

            listView.setAdapter(adapter);

            completeButton = (Button) findViewById(R.id.complete);

            scan = (EditText) findViewById(R.id.scanId);
            scan.requestFocus();
            final Handler handler = new Handler();

            scan.setOnKeyListener(new android.view.View.OnKeyListener() {
                @Override
                public boolean onKey(android.view.View v, int keyCode, KeyEvent event) {
                    if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                            (keyCode == KeyEvent.KEYCODE_ENTER)) {

                        String scannedData = scan.getText().toString().trim();
                        Crashlytics.log(android.util.Log.ERROR,"Barcode value", scannedData);
                        scan.setFocusable(true);
                        scan.requestFocus();

                        if (scannedData.isEmpty() && scannedData.trim().length() < 0) {
                            Toast.makeText(getApplicationContext(), "Please scan AWB's", Toast.LENGTH_LONG).show();
                        }
                        else if (!(scannedData.isEmpty()) && scannedData.trim().length() > 0) {
//				       adding contact to contact list
                            if (barcodeScan.contains(scannedData)) {
                                //ignore duplicate values
                                Crashlytics.log(android.util.Log.ERROR,TAG, "print duplicate");
                                scan.setText("");
                            } else {
                                list.add(0, scannedData);
                            }
                            adapter.notifyDataSetChanged();
                            barcodeScan.addAll(list);
                            scn_cnt = barcodeScan.size();
                            tv_scn_count.setText(Integer.toString(scn_cnt));
                            scan.setText("");

                            dbHelper.setScanItems(pickUpId, barcodeScan, getDocId);
                            sessionManager.setScannedData(barcodeScan, pickUpId);
                        }
                    }

                    // Put focus back in the EditText after brief delay
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            scan.setFocusable(true);
                            scan.requestFocus();
                            // Select all text
//                        scan.setSelection(0, scan.getText().length());
                        }
                    }, 200);
//            return true;
//        }
                    return false;
                }
            });


            completeButton.setOnClickListener(new View.OnClickListener()

            {
                @Override
                public void onClick(View v) {
                    try {

                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                PickUpScanList.this);
                        alert.setTitle("Alert!!");
                        alert.setMessage("Do you want to complete or Go back");
                        alert.setPositiveButton("Complete", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                //do your work here
                                if (barcodeScan.size() > 0) {
                                    Log.e( TAG, "Sending updated scan item" + list + "PicUpId:" + pickUpId + ":" + assignId);
                                    try {
                                        if (savedList.size() > 0) {
                                            setScanData.addAll(list);
                                            List<String> newList = new ArrayList<String>(barcodeScan);
                                            // sessionManager.setScannedDataSize(newList.size(), pickUpId);
                                            dbHelper.updateDoc(newList, pickUpId, assignId, companyId);
                                        } else {
                                            List<String> newList = new ArrayList<String>(barcodeScan);
                                            //sessionManager.setScannedDataSize(newList.size(), pickUpId);
                                            Log.e(TAG, "New List else:" + newList);
                                            dbHelper.updateDoc(newList, pickUpId, assignId, companyId);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), "Please scan the AWB's", Toast.LENGTH_LONG).show();
                                }
                                dialog.dismiss();
                                finish();
                                return;

                            }
                        });
                        alert.setNegativeButton("Continue", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                scan.setFocusable(true);
                                scan.requestFocus();
                                dialog.dismiss();
                            }
                        });

                        alert.create().show();
                    } catch (Exception e) {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Exception in complete button" + e.getMessage());
                    }
                }
            });

            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()

            {

                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view,
                                               final int arg2, long arg3) {
                    try {

                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                PickUpScanList.this);
                        alert.setTitle("Alert!!");
                        alert.setMessage("Are you sure to delete record");
                        alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //do your work here
                                String scn_val = listView.getItemAtPosition(arg2).toString();
                                list.remove(arg2);
//                        adapter.notifyDataSetChanged();

                                barcodeScan.remove(scn_val);
                                sessionManager.setScannedData(barcodeScan, pickUpId);
                                scn_cnt = barcodeScan.size();//listView.getAdapter().getCount();

                                tv_scn_count.setText(Integer.toString(scn_cnt));
                                adapter.notifyDataSetChanged();
                                dialog.dismiss();
                                finish();
                                return;

                            }
                        });
                        alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                                finish();
                                return;
                            }
                        });

                        alert.create().show();
                    }catch (Exception e) {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "While deleting the item" + e.getMessage());
                    }
                    return false;
                }
            });
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "oncreate" + e.getMessage());
        }
    }

    public void toggleMenu(View v)
    {

        AlertDialog.Builder alert = new AlertDialog.Builder(
                PickUpScanList.this);
        alert.setTitle("Alert!!");
        alert.setMessage("Do you want to complete or Go back");
        alert.setPositiveButton("Complete", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    //do your work here
                    if (barcodeScan.size() > 0) {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Sending updated scan item" + list + "PicUpId:" + pickUpId + ":" + assignId);
                        try {
                            if (savedList.size() > 0) {
                                setScanData.addAll(list);
                                List<String> newList = new ArrayList<String>(barcodeScan);
                                //sessionManager.setScannedDataSize(newList.size(), pickUpId);

                                // Crashlytics.log(android.util.Log.ERROR, TAG, "New List if:" + newList);
                                dbHelper.updateDoc(newList, pickUpId, assignId, companyId);
                                //dbHelper.updateCompleteDoc(newList, companyId, locationId, pickUpId, assignId);
                                //dbHelper.retrieveQueryData();
                            } else {
                                List<String> newList = new ArrayList<String>(barcodeScan);
                                // sessionManager.setScannedDataSize(newList.size(), pickUpId);
                                Log.e(TAG, "New List else:" + newList);
                                dbHelper.updateDoc(newList, pickUpId, assignId, companyId);
                                //dbHelper.updateCompleteDoc(newList, companyId, locationId, pickUpId, assignId);
                                //new LongOperation().execute();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Please scan the AWB's", Toast.LENGTH_LONG).show();
                    }
                    dialog.dismiss();
                    finish();
                    return;
                } catch (Exception e) {
                    Crashlytics.log(android.util.Log.ERROR, TAG, "onComplete button Pressed" + e.getMessage());
                }

            }
        });
        alert.setNegativeButton("Back", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    dialog.dismiss();
                    finish();
                    return;
                } catch (Exception e) {
                    Crashlytics.log(android.util.Log.ERROR, TAG, "toggleMenu" + e.getMessage());
                }
            }
        });

        alert.show();
    }
    @Override
    public void onBackPressed() {
        // do something on back.
    }
}


