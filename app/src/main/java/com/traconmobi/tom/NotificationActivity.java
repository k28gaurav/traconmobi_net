/*************************************************************/

/**********CREATED BY : ASHWINI NAIK ************************/
/********CREATED DATE : 12/03/2013 **************************/
/***********PURPOSE   : TO GET THE NOTIFICATION DETAILS ******/

/************************************************************/

package com.traconmobi.tom;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;

import org.xmlpull.v1.XmlPullParserException;

import com.crashlytics.android.Crashlytics;
import com.traconmobi.tom.http.OkHttpHandler;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
//import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteException;
import android.graphics.Color;
import android.util.Log;
import android.view.*;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import io.fabric.sdk.android.Fabric;

public class NotificationActivity extends Activity
{	
	 @SuppressWarnings("unused")

	private final String NAMESPACE = "http://tempuri.org/";
	private final String URL = "http://navatech.cloudapp.net/TrackOnMobi_WS_PAREKH_Test/TrackOnMobi_WS_V_2.asmx";
	 @SuppressWarnings("unused")
	private final String SOAP_ACTION = "http://tempuri.org/GetDownLoadNotificationFromMobile";
	private final String METHOD_NAME = "GetDownLoadNotificationFromMobile";
	final String uid =TOMLoginUserActivity.usr_id;
	 public View row;
	 protected ListView Notification_list;
	 protected Cursor cursor,cur_sync_dt,cursor_noti,c,crs_noti,crs;
	 protected ListAdapter adapter;
	 protected SQLiteDatabase db,db1,db2;	 
	 String msg,get_sync_date,msgshw,dt,dt1,get_sync_dt;	
	 final String curnt_dt =TOMLoginUserActivity.currentdate;
	 TextView version_name,title;
	public String TAG="NotificationActivity";
	// Session Manager Class
	    SessionManager session;
	    public String notawb,msg2,nname,nloc,n_dept,isread,not_id,get_id,entityString4,status,flag,Assgn_type;
	    public String session_user_id,session_user_pwd,session_USER_LOC,session_USER_NUMERIC_ID,session_CURRENT_DT,session_CUST_ACC_CODE,session_USER_NAME,get_callback_resp,session_noti_callback_resp,session_DB_PATH,session_DB_PWD;

	    
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
    	try
        {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
			//Intializing Fabric
		Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_notification);
        getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, R.layout.activity_notification);


		title=(TextView)findViewById(R.id.txt_title);
		title.setText("NOTIFICATION LIST");
        
        Notification_list =(ListView) findViewById(R.id.list);
			Log.e(TAG, "Notification is called");
     	
        	// Session class instance
            session = new SessionManager(getApplicationContext());
            
            /**GETTING SESSION VALUES**/
             
         // get AuthenticateDb data from session
	        HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();
	         
	        // DB_PATH
	        session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);
	       
	        // DB_PWD
	        session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD); 
	        
            // get user data from session
            HashMap<String, String> login_Dts = session.getLoginDetails();
             
            // Userid
            session_user_id = login_Dts.get(SessionManager.KEY_UID);
             
            // pwd
            session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);
            
         // get user data from session
            HashMap<String, String> user = session.getUserDetails();
             
            // session_USER_LOC
            session_USER_LOC= user.get(SessionManager.KEY_USER_LOC);
             
            // session_USER_NUMERIC_ID
            session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);
            
            // session_CURRENT_DT
            session_CURRENT_DT= user.get(SessionManager.KEY_CURRENT_DT);
             
            // session_CUST_ACC_CODE
            session_CUST_ACC_CODE= user.get(SessionManager.KEY_CUST_ACC_CODE);
            
            // session_USER_NAME
            session_USER_NAME= user.get(SessionManager.KEY_USER_NAME);
            
    if(session_DB_PATH != null && session_DB_PWD != null)
	  {
		  Log.e(TAG,"session_DB_PATH not null");
//        db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
    	db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
        Assgn_type="D";
        cursor = db.rawQuery("SELECT _id,T_U_ID,T_Assignment_Number,T_Assignment_Type,T_Message,T_Notifier_Name,T_Notifier_Loc,D_Trans_Date_Time,D_Sync_Date,C_is_Read_Status  FROM TOM_NOTIFICATION_TRN  WHERE T_Assignment_Type='"+ Assgn_type +"' and D_Sync_Date ='"+session_CURRENT_DT+"' and T_U_ID="+ session_USER_NUMERIC_ID +"", null); 

        final int cursor_count = cursor.getCount();
		  Log.e(TAG,"cursor count: " + cursor_count);
        
        while(cursor.moveToNext())
        {
        	get_sync_dt = cursor.getString(cursor.getColumnIndex("D_Sync_Date")); 
        	flag = cursor.getString(cursor.getColumnIndex("C_is_Read_Status")); 
//        	Crashlytics.log(android.util.Log.ERROR,TAG,"flag " + flag);
        	if(flag.equals("Y"))
        	{
        		status="READ";
        	}
        	else if(flag.equals("N"))
        	{
        		status="UNREAD";
        	}
        		
        	msg=cursor.getString(cursor.getColumnIndex("T_Message")); 
        	dt =cursor.getString(cursor.getColumnIndex("D_Trans_Date_Time"));
          	dt1=dt.substring(0, 10);
        } 
        
        if(cursor_count > 0)
        {
        final MyCursorAdapter adapter = new  MyCursorAdapter(
                        this, 
                        R.layout.notification_list, 
                        cursor, 
                        new String[] {"T_Assignment_Number","T_Message","D_Trans_Date_Time","T_Notifier_Name","T_Notifier_Loc","C_is_Read_Status"},                  
                        new int[] {R.id.awb_num,R.id.msg,R.id.date,R.id.sndr_nme,R.id.sndr_loc,R.id.status_val},0);
        
       Notification_list.setAdapter(adapter);       
       Notification_list.setCacheColorHint(Color.WHITE); 
       Notification_list.setOnItemClickListener(new OnItemClickListener()
       {         	
    	   @Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
   				long id) 
    	   {   
    		   if(session_DB_PATH != null && session_DB_PWD != null)
           	  	{
//	    		   db2=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);  
    			   db2=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
	    		   Cursor cursor = (Cursor) adapter.getItem(position);   
	    		   
	    			 int index = cursor.getInt(cursor.getColumnIndex("_id"));
	    			 String i =cursor.getString(cursor.getColumnIndex("_id"));
	    			 Crashlytics.log(android.util.Log.ERROR, TAG, "index " + index + "i" + i);
//	   	             final String employeeId = index;
		        	 String strFilter = "_id=" + index;	    
//		        	 crs.close();
		        	 Assgn_type="D";
		        	 cursor_noti = db2.rawQuery("SELECT emp._id,emp .T_Assignment_Number,emp.T_Assignment_Type,emp .T_Message FROM TOM_NOTIFICATION_TRN  emp where emp.T_Assignment_Type='"+ Assgn_type +"' and emp._id ='"+ index +"'", null);
//	            			new String[]{""+index});
		        	
			         while(cursor_noti.moveToNext())
			        {
			    	    msgshw = cursor_noti.getString(cursor_noti.getColumnIndex("T_Message"));
			    	   	ContentValues cv = new ContentValues();
		    	    	cv.put("C_is_Read_Status", "Y");
		    	    	cv.put("C_is_Notify_Sync_Status","0");
		    	    	db2.update("TOM_NOTIFICATION_TRN", cv, strFilter, null);
			        }
			         int c_count=cursor_noti.getCount();
			        cursor_noti.close();
			        db2.close();
			        
			        Crashlytics.log(android.util.Log.ERROR, TAG, "msgshw " + msgshw);
			        if(c_count > 0)
			        {
	    			AlertDialog.Builder builder = new AlertDialog.Builder(NotificationActivity.this);    			
	    		    builder.setTitle("NOTIFICATION");
	    		    builder.setMessage(msgshw);   
	    		    builder.setCancelable(false);
	    		    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() 
	    		    {
			        @Override
					public void onClick(DialogInterface dialog, int which) 
			        {
			            /** GETTING ALL THE READ NOTIFICATIONS TO SYNC TO CENTRAL DB FROM LOCAL DB */
			        	if(session_DB_PATH != null && session_DB_PWD != null)
		              	  {
//				        	db1=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
			        		db1=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
						    //String flag="N";
						    String flag1="Y";
						    if(get_sync_dt == null)
						       {
						    	   Crashlytics.log(android.util.Log.ERROR, TAG, "sync date value is null");
						       }
						    else if(get_sync_dt != null)
						    {
						     if(get_sync_dt.equals(session_CURRENT_DT))
						    {
						    	 Assgn_type="D";
						    	crs_noti = db1.rawQuery("SELECT _id,T_Assignment_Number,T_Assignment_Type,T_Message,C_is_Read_Status,T_Notifier_Name,T_Notifier_Loc,T_Notifier_Dept,C_is_Notify_Sync_Status,T_Notify_Code FROM TOM_NOTIFICATION_TRN where T_Assignment_Type='"+ Assgn_type +"' and  C_is_Read_Status='"+ flag1 +"' and  C_is_Notify_Sync_Status=0" ,null);
							    while(crs_noti.moveToNext())
							    {
							    	 get_id =crs_noti.getString(crs_noti.getColumnIndex("_id"));
			//		                isdel1 =crs.getString(crs.getColumnIndex("isDelivered"));
					                notawb=crs_noti.getString(crs_noti.getColumnIndex("T_Assignment_Number"));
					                String msg=crs_noti.getString(crs_noti.getColumnIndex("T_Message"));
					                String msg1=msg.trim().toString();
					                msg2=msg.replace(" ", "%20");
			//		                String msg="check_it";
					                isread=crs_noti.getString(crs_noti.getColumnIndex("C_is_Read_Status"));
					                nname=crs_noti.getString(crs_noti.getColumnIndex("T_Notifier_Name"));
					                nloc=crs_noti.getString(crs_noti.getColumnIndex("T_Notifier_Loc"));
					                n_dept=crs_noti.getString(crs_noti.getColumnIndex("T_Notifier_Dept"));
					                if(n_dept == null || n_dept.equals("null"))
					                {
					                	n_dept="null";
					                }
					                else
					                {
					                	n_dept=n_dept;
					                }
					                not_id=crs_noti.getString(crs_noti.getColumnIndex("T_Notify_Code"));		               
					             }
							    if(crs_noti.getCount() > 0)
							    {
//							    	 entityString4 = "CAWB_NO=" + notawb +"&CMESSAGE=" + URLEncoder.encode(msg2)
//						                        + "&NOTIFIER_NAME=" + nname + "&NOTIFIER_LOC="
//						                        + nloc +"&NOTIFIER_DEPT=" + n_dept + "&IS_Read_STATUS=" + isread +"&CNOT_CD=" + not_id;
							    	 entityString4 = "ASSIGNMENT_NO=" + notawb +"&CMESSAGE=" + URLEncoder.encode(msg2)
					                        + "&NOTIFIER_NAME=" + nname + "&NOTIFIER_LOC="
					                        + nloc +"&NOTIFIER_DEPT=" + n_dept + "&IS_Read_STATUS=" + isread +"&CNOT_CD=" + not_id;
							    }
							    crs_noti.close();
							    db1.close();
						    }
						    
					    	else
							{
								Crashlytics.log(android.util.Log.ERROR, TAG, "Error NotificationActivity step 2 TOMLoginUserActivity.file is null ");
//								onClickGoToHomePage();
							}
						    }
				              //**When ever you want to check Internet Status in your application call isConnectingToInternet() function and it will return true or false***/
				           		ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
//				           		Boolean isInternetPresent = false; 
				           		Boolean isInternetPresent = cd.isConnectingToInternet(); // true or false
				           		// get Internet status
				                     isInternetPresent = cd.isConnectingToInternet();

				                     // check for Internet status
				                 if (isInternetPresent) 
				                 {
				                     // Internet Connection is Present
				                     // make HTTP requests    
				                	
				                	 Crashlytics.log(android.util.Log.ERROR, TAG, "values gng " + notawb + msg2 + nname + nloc + n_dept + isread + not_id);
					                
//										String entityString4 = "CAWB_NO=" + notawb +"&CMESSAGE=" + URLEncoder.encode(msg2)
//							                        + "&NOTIFIER_NAME=" + nname + "&NOTIFIER_LOC="
//							                        + nloc +"&NOTIFIER_DEPT=" + n_dept + "&IS_Read_STATUS=" + isread +"&CNOT_CD=" + not_id;
										new Thread(new Runnable() 
										 {									
											@Override
											public void run() 
											{
												if(entityString4 != null && !(entityString4.isEmpty()))
												{
//							                String entityString4 = "CAWB_NO=" + notawb +"&CMESSAGE=" + URLEncoder.encode(msg2)
//							                        + "&NOTIFIER_NAME=" + nname + "&NOTIFIER_LOC="
//							                        + nloc +"&NOTIFIER_DEPT=" + n_dept + "&IS_Read_STATUS=" + isread +"&CNOT_CD=" + not_id;
							                
							                /******************Asyncronous call of the webservice********************/
					                	 	/*int reqId = 104;
					                	 	HttpHandler handler = new HttpHandler(URL+ "/" + METHOD_NAME+ "?" + entityString4,null, null, reqId);
					                		handler.addHttpLisner(NotificationActivity.this);
					                		handler.sendRequest();				                		
					                		
					                		// get user data from session
					                       HashMap<String, String> resp_Dts = session.gethttpsrespDetails();
					                        
					                       // Userid
					                       session_noti_callback_resp = resp_Dts.get(SessionManager.KEY_RESPONSE);*/

													OkHttpHandler handler_del = new OkHttpHandler(URL+ "/" + METHOD_NAME+ "?" + entityString4);
													session_noti_callback_resp = handler_del.sendRequest();
					                       if(session_noti_callback_resp!=null && session_noti_callback_resp != "")
					                       {						   				        
								                    ParseXmlResponsenoti NotiListParser = new ParseXmlResponsenoti(session_noti_callback_resp,getApplicationContext());
								                    try {
														NotiListParser.parse();
													} catch (XmlPullParserException e) {
														// TODO Auto-generated catch block
														e.printStackTrace();
													} catch (IOException e) {
														// TODO Auto-generated catch block
														e.printStackTrace();
													}

								                    if(NotiListParser.getParsedNotifyList().isEmpty())
													{
								                    	if(session_DB_PATH != null && session_DB_PWD != null)
								  	              	  {
//								                    	db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
								                    		db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
													    c=db.rawQuery("UPDATE TOM_NOTIFICATION_TRN SET C_is_Notify_Sync_Status=1 WHERE _id = ?",new String[]{""+get_id});
													    while(c.moveToNext()){}
											            c.close();
											            db.close();
								  	              	  }
												    	else
														{
															Crashlytics.log(android.util.Log.ERROR, TAG, "Error NotificationActivity step 3 TOMLoginUserActivity.file is null ");
//															onClickGoToHomePage();
														}
													}
								                    else if(!(NotiListParser.getParsedNotifyList().isEmpty()))
													{
								                    	
													}
						                 
					                       }
												}
												else
												{
													Crashlytics.log(android.util.Log.ERROR, TAG, "Entitystring4 is null ");
												}
											}
										 }).start();
				                 } 
				                 else 
				                 {
				                	 // Internet connection is not present
				                     // Ask user to connect to Internet
				                	 if(session_DB_PATH != null && session_DB_PWD != null)
					              	  {
//					                	 db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
				                		 db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
					                	 c=db.rawQuery("UPDATE TOM_NOTIFICATION_TRN SET C_is_Notify_Sync_Status=0 WHERE _id = ?",new String[]{""+get_id});
									     while(c.moveToNext()){}
							             c.close();
							             db.close();
					              	  }
							    	else
									{
										Crashlytics.log(android.util.Log.ERROR, TAG, "Error NotificationActivity step 4 TOMLoginUserActivity.file is null ");
//										onClickGoToHomePage();
									}
				                 }
					    }
					    else
					    {
					    	Crashlytics.log(android.util.Log.ERROR, TAG, "sync date value is null");
					    }
			            Intent goToNextActivity = new Intent(getApplicationContext(), NotificationActivity.class);
			            startActivity(goToNextActivity);	
			           
			            // Do nothing but close the dialog
			            dialog.dismiss();   
			        }    		        
	    		    });
	    		    AlertDialog alert = builder.create();
	    		    alert.show();  
			        }
			        
           	  	}
		    	else
				{
					Crashlytics.log(android.util.Log.ERROR, TAG, "Error NotificationActivity step 1 TOMLoginUserActivity.file is null ");
//					onClickGoToHomePage();
				}
    		   
    		}
	 });
       }
       else
       {
    	   Toast.makeText(NotificationActivity.this,"No Notifications for current day available", Toast.LENGTH_LONG).show();
       }
        }
    	else
		{
			Crashlytics.log(android.util.Log.ERROR, TAG, "Error NotificationActivity step 5 TOMLoginUserActivity.file is null ");
//			onClickGoToHomePage();
		}

    }
    catch(SQLiteException e)
    {
    	Crashlytics.log(android.util.Log.ERROR, TAG, "Error SQLiteException NotificationActivity : onCreate ");
//		   onClickGoToHomePage();
	}
	catch(UnsatisfiedLinkError err)
	{
		err.getStackTrace();
		Crashlytics.log(android.util.Log.ERROR, TAG, "Error NotificationActivity UnsatisfiedLinkError ");
//		onClickGoToHomePage();
	}
    catch (Exception e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
	if(e.getMessage().toString().equals("unknown error (code 14): Could not open database") || e.getMessage().toString().equals("unable to open database file"))
	{
		Crashlytics.log(android.util.Log.ERROR, TAG, "Error NotificationActivity unknown error  ");
//		onClickGoToHomePage();
	}
	else
	{
		Crashlytics.log(android.util.Log.ERROR, TAG, "Error NotificationActivity else ");
//		onClickGoToHomePage();
	}
}
        finally {
       
//          if (cur_sync_dt != null) 
//          {
//        	  cur_sync_dt.close();
//          }
//          if (cursor_noti != null) 
//          {
//        	  cursor_noti.close();
//          }
//          if (c != null) 
//          {
//        	  c.close();
//          }
//          if (crs != null) 
//          {
//        	  crs.close();
//          }
//          if (crs_noti != null) 
//          {
//        	  crs_noti.close();
//          }
//          if (db1 != null) 
//          {
//        	  db1.close();
//          }
//          if (db2 != null) 
//          {
//        	  db2.close();
//          }
//          
//          if (cursor != null) 
//          {
//        	  cursor.close();
//          }
//        	
//          if (db != null) 
//          {
//        	  db.close();
//          }
        }
    }

    /**extend the SimpleCursorAdapter to create a custom class where we
    can override the getView to change the row colors*/
    private class MyCursorAdapter extends SimpleCursorAdapter
    {    
     @SuppressWarnings("deprecation")
	public MyCursorAdapter(Context context, int layout, Cursor c,
       String[] from, int[] to, int flags) {
      super(context, layout, c, from, to);
     } 
    
     @Override 
     public View getView(final int position, View convertView, ViewGroup parent) 
     {     	     
    	 View view = super.getView(position, convertView, parent);
    	 try
 		{
    	   if (view == null)
    	     {
    	        LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	        view = vi.inflate(R.layout.notification_list, null);
    	        
    	     }
    	     if(position % 2 == 0)
    	     { 
    	         view.setBackgroundColor(Color.rgb(238, 233, 233));
    	     }
	        else 
	        {
	         view.setBackgroundColor(Color.rgb(255, 255, 255));
	        }
 		}
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "Exception NotificationActivity getView " + e.getMessage());
		}
    	 catch(UnsatisfiedLinkError err)
    		{
    			err.getStackTrace();
    			Crashlytics.log(android.util.Log.ERROR, TAG, "Error NotificationActivity UnsatisfiedLinkError ");
//    			onClickGoToHomePage();
    		}
	        return view; 
     } 
   }

	public void toggleMenu(View v)
	{
		Intent homeActivity = new Intent(this, HomeMainActivity.class);
		startActivity(homeActivity);
	}

	public void onclk_noti(View v)
	{
		Intent homeActivity = new Intent(this, NotificationActivity.class);
		startActivity(homeActivity);
	}

	public void onclk_trip(View v)
	{
		Intent homeActivity = new Intent(this, Start_End_TripMainActivity.class);
		startActivity(homeActivity);
	}


	@Override
    public void onBackPressed() 
    {
    	try
		{
    // do something on back.
//    	Intent goToNextActivity = new Intent(getApplicationContext(), TOMWelcomeActivity.class);
//        startActivity(goToNextActivity);
	    }
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "Exception NotificationActivity onBackPressed " + e.getMessage());
//			onClickGoToHomePage();
		}
    	catch(UnsatisfiedLinkError err)
    	{
    		err.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR, TAG, "Error NotificationActivity UnsatisfiedLinkError ");
//    		onClickGoToHomePage();
    	}
      }
    
    @Override
    public void onDestroy() 
    {
    	try
		{
    	super.onDestroy();
    	/*stopService(new Intent(getApplicationContext(), LocationLoggerService.class));
    	stopService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
    	android.os.Process.killProcess(android.os.Process.myPid());
    	System.exit(0);*/
	    }
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "Exception NotificationActivity onDestroy " + e.getMessage());

		}
    	catch(UnsatisfiedLinkError err)
    	{
    		err.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR, TAG, "Error NotificationActivity UnsatisfiedLinkError ");

    	}
    }
    
    //**********************modification for menu options(Home & Logout)**************************
   /* @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater() ;
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.menu_home:
                onClickGoToHomePage();
                return true;
            case R.id.menu_log_out:
                onClickLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

    private void onClickLogOut()
    {
    	try
		{
        Intent logOutActivity = new Intent(this, TOMLoginUserActivity.class);
        logOutActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(logOutActivity);
	    }
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "Exception NotificationActivity onClickLogOut " + e.getMessage());

		}
    	catch(UnsatisfiedLinkError err)
    	{
    		err.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR, TAG, "Error NotificationActivity UnsatisfiedLinkError ");

    	}
    }

    private void onClickGoToHomePage()
    {
    	try
		{
//        Intent homePageActivity = new Intent(this,TOMWelcomeActivity.class);
////        homePageActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(homePageActivity);
	    }
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "Exception NotificationActivity onClickGoToHomePage " + e.getMessage());

		}
    	catch(UnsatisfiedLinkError err)
    	{
    		err.getStackTrace();
    		Crashlytics.log(android.util.Log.ERROR, TAG, "Error NotificationActivity UnsatisfiedLinkError ");

    	}
    }
}
