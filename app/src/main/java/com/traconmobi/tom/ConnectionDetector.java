package com.traconmobi.tom;

import android.content.Context;
import android.net.ConnectivityManager;
 
public class ConnectionDetector {
     
    private Context _context;
     
    public ConnectionDetector(Context context){
        this._context = context;
    }
 
    public boolean isConnectingToInternet(){
//        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
//          if (connectivity != null)
//          {
//              NetworkInfo[] info = connectivity.getAllNetworkInfo();
//              if (info != null)
//                  for (int i = 0; i < info.length; i++)
//                      if (info[i].getState() == NetworkInfo.State.CONNECTED)
//                      {
//                          return true;
//                      }
// 
//          }
//          return false;
    	// get Connectivity Manager object to check connection
        ConnectivityManager connec = 
                       (ConnectivityManager)_context.getSystemService(Context.CONNECTIVITY_SERVICE);
         
           // Check for network connections
            if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                 connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                 connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                 connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
                
                // if connected with internet
                 
//                Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
                return true;
                 
            } else if (
              connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
              connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {
               
//                Toast.makeText(this, " Not Connected ", Toast.LENGTH_LONG).show();
                return false;
            }
          return false;
        }
    
}