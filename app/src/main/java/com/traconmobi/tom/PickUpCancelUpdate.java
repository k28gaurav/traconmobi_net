package com.traconmobi.tom;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import io.fabric.sdk.android.Fabric;


public class PickUpCancelUpdate extends Activity {
    private Button update, back;
    private EditText remarkText;
    private ArrayList<String> get_reasonOPtion;
    private ImageView clearSearch;
    private String[] get_reason;
    private String[] get_reasonId;
    SessionManager sManager = null;
    private final String URL_pickupscan = "http://traconmobi.net/Assignment-Details/opsggnpc/sample/142/a2lfrq40kdh0nrut1vl0ani4a2";
    String gettomuser_response = null;
    private final String METHOD_NAME_outscan = "GetTomMobileOutScanDtls";
    SharedPreferences pref;
    String pickUpId = "";
    String assignId = "";
    String companyId = "";
    CouchBaseDBHelper dbHelper;
    SharedPreferences.Editor editor;
    public static final String PREF_NAME = "ScanPref";
    public static final String PREF_Pickup = "PickupCancel";
    public static final String PREF_NAME1 = "Pager";
    public String FORM_TYPE_VAL;
    String reason=" ";
    TextView title;
    public static String TAG = "PickUpCancelUpdate";

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        try
        {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            super.onCreate(savedInstanceState);
            //Intializing Fabric
            Fabric.with(this, new Crashlytics());
            setContentView(R.layout.activity_pickup_cancel);
            getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, R.layout.activity_pickup_cancel);

            title = (TextView) findViewById(R.id.txt_title);
            title.setText("PICKUP CANCEL LIST");

            final AutoCompleteTextView autoSearch = (AutoCompleteTextView) findViewById(R.id.reason);
            remarkText = (EditText) findViewById(R.id.remark);
            update = (Button) findViewById(R.id.update);
//        back = (Button)findViewById(R.id.back);

            clearSearch = (ImageView) findViewById(R.id.clear);
            dbHelper = new CouchBaseDBHelper(getApplicationContext());

            pref = getApplicationContext().getSharedPreferences(PREF_Pickup, Context.MODE_PRIVATE);
            Set<String> reasonList = pref.getStringSet("reasonlistforcancel", null);
            FORM_TYPE_VAL = getIntent().getStringExtra("NAV_FORM_TYPE_VAL");
            pickUpId = getIntent().getStringExtra("assignmentNo");
            assignId = getIntent().getStringExtra("assignmentId");
            companyId = getIntent().getStringExtra("companyId");
            Log.e(TAG, "ReasonList" + reasonList);
            if(reasonList != null) {
                get_reason = reasonList.toArray(new String[reasonList.size()]);
                sManager = new SessionManager(getApplicationContext());
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, get_reason);
                autoSearch.setAdapter(adapter);
                autoSearch.setThreshold(1);
                final String[] reasonText = {""};
                final String[] reasonid = {""};
                autoSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                        reasonText[0] = (String) parent.getItemAtPosition(position);
                        reasonid[0] = sManager.getKeyReasonId(reasonText[0]);
                        Log.e(TAG, "Reason: " + reasonText[0] + "IS: " + reasonid[0]);
                    }
                });

                autoSearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        reasonText[0] = " ";
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                clearSearch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        autoSearch.setText("");
                    }
                });
                update.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View v) {
                                                  final String remark = remarkText.getText().toString();

                                                  final String assign_Id = assignId;
                                                  final String assignNo = pickUpId;
                                                  final String companyID = companyId;
                                                  reason = reasonText[0];

                                                  AlertDialog.Builder alertDialog = new AlertDialog.Builder(PickUpCancelUpdate.this);

                                                  // Setting Dialog Title
                                                  alertDialog.setTitle("Alert");

                                                  // Setting Dialog Message
                                                  alertDialog.setMessage("Are you sure you want to update ?");

                                                  // On pressing Settings button
                                                  alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                      @Override
                                                      public void onClick(DialogInterface dialog, int which) {
                                                          try {
                                                              if (reason.isEmpty()) {
                                                                  Toast.makeText(getApplicationContext(), "Please select reason", Toast.LENGTH_LONG).show();
                                                              } else {
                                                                  List<String> reasonList = Arrays.asList(get_reason);
                                                                  if (reasonList.contains(reason)) {
                                                                      sManager.setReason(assignNo, reason);
                                                                      dbHelper.updateDoc(reason, remark, assignNo, assign_Id, companyID, reasonid[0]);
                                                                      //PickUpCancelUpdate.this.finish();
                                                                  } else {
                                                                      Toast.makeText(getApplicationContext(), "Select reason from the list", Toast.LENGTH_LONG).show();
                                                                  }
                                                              }
                                                          } catch (Exception e) {
                                                              Crashlytics.log(Log.ERROR, TAG, "Error Exception Onend_trip " + e.getMessage());
                                                          }
                                                          dialog.dismiss();
                                                          finish();
                                                          return;
                                                      }
                                                  });

                                                  // on pressing cancel button
                                                  alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                      @Override
                                                      public void onClick(DialogInterface dialog, int which) {
                                                          dialog.dismiss();
                                                          finish();
                                                          return;
                                                      }
                                                  });

                                                  // Showing Alert Message
                                                  alertDialog.create().show();
                                              }
                                          }
                );
            }
        }
        catch(Exception e)

        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "oncreate" + e.getMessage());

        }
    }
    public void toggleMenu(View v)
    {
        finish();
    }

    @Override
    public void onBackPressed()
    {

    }
}