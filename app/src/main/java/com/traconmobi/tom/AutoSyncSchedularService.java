package com.traconmobi.tom;

/**
 * Created by LENOVO on 12/2/2015.
 */
/*public class AutoSyncSchedularService {
}*/


/*******************************************************************/
//package com.traconmobi.parekh;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import com.crashlytics.android.Crashlytics;
import com.traconmobi.tom.app.App;
import com.traconmobi.tom.http.OkHttpHandlerPost;
import com.traconmobi.tom.rest.model.Escan;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import org.kobjects.base64.Base64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import io.fabric.sdk.android.Fabric;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class AutoSyncSchedularService  {

    /*SoapObject request = null, objMessages = null;
    SoapSerializationEnvelope envelope;
    HttpTransportSE httpTransport;
    List<String> savedList = new ArrayList<String>();
    //	public static String DB_FULL_PATH = android.os.Environment.getExternalStorageDirectory().toString()+"/TOM.sqlite";
    static final String EXTRA_MESSAGE =null;
    private static final String SOAPRequestXML = null;
    protected SQLiteDatabase db6_del,db8_undel,db,db1,db2,db3,db4,db5,db6,db7,db8,db9,db_noti,db_unattmpt_cnt2,db_unattmpt_cnt1;
    protected Cursor c_notiupdate,c,cc,cursor,crs,cr,cursor_chkdb,cr_chkdel,c_undelsyn,c_syn_update,cr_unattempt_del,cr_del_status,cursor_outscan_dt,cur_sync_dt,c_syn,c_syn_updater,cursor_outscan_dt_val,cr_unattempt_del_val;
    public static String message;

    int syncdelcnt =0 ,syncnoticnt= 0,syncundelcnt= 0,syncunattemptcnt= 0,synclocatecnt=0,syncexpnsecnt=0,sync_paymnt_failurecnt=0,syncfiltr_typcnt=0,syncpaymnt_updtcnt=0,synchandovr_updtcnt=0;
    public static String bnk_locn,bnk_name,chque_dt,chque_no_val,particular_val,msg,myString,strFilter,awb_id,awb_delnum,awb_numb,isdel,is_cod_coltd,del_dt,del_tm,rec_by,rec_status,sign_path,photo_path,get_id,get_awbid,acquired_lat,acquired_long;
    public static String ud_reason,ud_date,ud_time,awb_id1,isdel1,remarks,rmks,sgn,l_sign,l_photo,awb_outscantime,awb_outscan_dt_val,awb_outscan_dt,awb_outscandt,get_sync_dt,get_sync_date,respstr,Acq_lat,Acq_long;

    final String user_loc =TOMLoginUserActivity.u_loc;
    final String user_id=TOMLoginUserActivity.usr_id;

    final String curnt_dt =TOMLoginUserActivity.currentdate;
    final String cust_acc_code =TOMLoginUserActivity.cust_acc_no;
    final String usr_name =TOMLoginUserActivity.u_name;

    //String variables
    public String  fail_Photo_dt,  fail_Photo_tm  ,fail_cust_acc ,recpt_no,sign_dt_tme,session_sync_undel_resp_PHOTO_ID,filter_awbid,expense_awbid,locate_awbid,Payment_Mode,Particular,Cheque_DD,Cheque_DD_Dt,Bank_name,Bank_loc,filtr_type,assgn_type,trnfr_uid,cust_locate_assgn_id,asgn_typ,sb_string,scn_usr_id,complete_resp_awbid,complete_resp_awb_id,Incomplete_resp_awbid,Incomplete_resp_awb_id,del_awbid,del_awb_id,
            undel_awbid,undel_awb_id,assgn_num_val,scan_num,scn_CUST_ACC_NO,sync_scan_num,get_payment_failure_response,collectd_amt,assigned_amt,cod_amt_coltd;

    //Integer variables
    public int syncscancnt;

    //StringBuilder
    StringBuilder sb,stringBuilder;

    //session objects
    public String undel_cust_acc , photo_id_dt ,photo_id_tme,sign_dt_tm,del_cust_acc,session_cust_locate_resp,get_paymnt_response,session_trnsfr_usr_resp,session_paymentreason_resp,session_udreason_resp,session_paymnt_update_resp,session_filtr_dtls,session_Payment_failure,session_get_sync_complete_pickup_response,session_get_sync_incomplete_pickup_response,session_sync_scanitem_resp,session_outscan_pickupscanresp,
            get_pickupscanresp,get_sync_complete_pickup_response,get_sync_undelpickup_response,getscanpickup_response,session_hndovr_resp;

    //Database objects
    protected SQLiteDatabase db_handovr_updt,db_paymnt_update_resp2,db_paymnt_update_resp1,db_paymnt_updt,db_filter_resp2,db_filter_resp1,db_filtr_typ,db_paymnt_failure_resp2,db_paymnt_failure_resp1,db_paymnt_failure,db_cust_locte,db_expnse,db_btry_usage,db_pickup_complete,db_pickup_complete_resp1,db_pickup_complete_resp2,db_pickup_incomplete,
            db_pickup_Incomplete_resp1,db_pickup_Incomplete_resp2,db_pickup_scan_item,db_pickup_scan_resp1,
            db_pickup_scan_resp2,db_opn_noti,db_get_noti_value,db_syn_noti,db_del_complete,db_del_complete_resp1,
            db_del_complete_resp2;

    /*//*******Expense_trn db and cursor connections******//*
    protected SQLiteDatabase db_expense_update_syn1,db_expense_update_syn2;

    protected Cursor c_expense_update_syn1,c_expense_update_syn2;

    public String sb_val,session_expense_resp,get_expense_sync_response;
    *//*******Expense_trn db and cursor connections******//*

    /*//*******customer locate db and cursor connections******//*
    protected SQLiteDatabase db_loc_update_syn1,db_loc_update_syn2;

    protected Cursor c_loc_update_syn1,c_loc_update_syn2;

    public String session_updte_locate_resp,get_cust_locate_response;
    *//*******customer locate db and cursor connections******//*

    /*//**********Undelivery db and cursor connections**********************//*/
    protected SQLiteDatabase db_undel,db_undel_resp1,db_undel_resp2;

    protected Cursor cursor_undel,c_undel_sync_resp1,c_undelsync_resp1,c_undel_sync_resp2,c_undelsync_resp2;

    *//**********Undelivery db and cursor connections**********************//*

    //Cursor objects
    protected Cursor cursor_handovr_updt,c_paymnt_update_sync_resp1,cursor_paymnt_updt,c_paymnt_update_sync_resp2,c_filter_sync_resp2,c_filter_sync_resp1,cursor_filtr_typ,c_paymnt_failure_sync_resp2,c_paymnt_failure_sync_resp1,cursor_paymnt_failure,cursor_expnse,cursor_cust_locte,crs_batry_usage,cursor_pickup_complete,c_pickup_complete_sync_resp1,c_pickup_complete_sync_resp2,
            cursor_pickup_incomplete,c_pickup_Incomplete_sync_resp1,c_pickup_Incomplete_sync_resp2,cursor_pickup_scanitem,
            c_pickup_scan_sync_resp1,c_pickup_scan_sync_resp2,cursor_opn_noti,cursor_get_noti_value,cursor_syn_noti,
            cursor_del_complete,c_del_complete_sync_resp1,c_del_complete_sync_resp2;

    public String token="a2lfrq40kdh0nrut1vl0ani4a2";

    static File image;
    private String selectedImagePath;
    byte [] BAvalue;
    public String getphotobytearray,s_photo;

    String pod_awb;
    String ud_awb;
    int ret = 0 ,val = 0,cr_unattempt_delcount;
    int outscn = 0 ;
    Button btnsyn;
    TextView rpt_cnt,sync_cnt,trck_cnt,shwcount,textView,version_name,cnt_unattempt_del,cnt_del_status,cnt_ud,prgtextview,optimizer_cnt;

    public String rel_id,cod_amt,ud_reason_id,ud_loc_id;
    public String  paymnt_failure_awbid,expnse_assgn_id,expnse_mode,expnse_amt,expnse_dt,expnse_frm_loc,expnse_to_loc,cust_lng,cust_lat,cust_locate_id,usr_btry_name,usr_imei,usr_batry_usage,u_reason,u_time,Assgn_type,fltr_type,t_u_id,Assgn_flag;
    public String notawb ,msg2,nname,nloc,ndept,isread,not_id;
    public String get_filtr_response,get_sync_scan_response,session_USER_PSWD,gettomnoti_response,session_noti_resp,session_outscan_resp,getoutscan_response;
    public String get_sync_noti_response,session_sync_noti_resp,get_sync_undel_response,session_sync_undel_resp,get_sync_del_response,session_sync_del_resp;

    public String get_sync_fail_photoid_response,get_sync_undel_photoid_response,get_paymentreason_resp,get_trnsfr_usr_resp,get_cust_locate_resp,get_udreasons_resp,handovr_type,hndovr_awb_id,handovr_congn_nme,handovr_consgn_rcvr_by,handovr_contct_num,hndovr_cod_amt_coltd,hndovr_del_dt;
    public String hndovr_del_tm,hndovr_rec_by,hndovr_sign_path,hndovr_loc,hndovr_uid,hndovr_cust_accno,hndovr_acquired_lat,hndovr_acquired_long,get_hndovr_response;

    //    private AutosyncSoapAccessTask AutosyncTask;
    // variable to hold context
    public Context context;

    long dt,dt_del;

    public String TAG="AutoSyncSchedularService";

    // Session Manager Class
    SessionManager session;
    public String session_COD_AMT,session_AWB_COUNT,session_RESPONSE_AWB_NUM,session_DB_PATH,session_DB_PWD,session_user_id,session_user_pwd,session_USER_LOC,session_USER_NUMERIC_ID,session_CURRENT_DT,session_CUST_ACC_CODE,session_USER_NAME;

    public String entityString_del,entityString_ud,entityString_transfer_users;
    private final String URL_outscan = "http://traconmobi.net/Assignment-Details";
    private final String URL_get_ud_resn = "http://traconmobi.net/Company-Reasons";
    private final String URL_get_transfer_users = "http://traconmobi.net/Transfer-User";
    //    private final String URL_UDDtls="http://traconmobi.net/POD-Update";
//    private final String URL_DELDtls="http://traconmobi.net/POD-Update";
   *//* private final String URL_UDDtls = "http://traconmobi.net/POD-Update_New";
    private final String URL_DELDtls = "http://traconmobi.net/POD-Update_New";*//*//commented on 23dec2015
    private final String URL_UDDtls = "http://traconmobi.net/podupdate";
    private final String URL_DELDtls = "http://traconmobi.net/podupdate";
    private final String URL_DEL_IMAGES_Dtls = "http://traconmobi.net/Upload-Image";

    public static int randBetween(int start, int end) {
        return start + (int)Math.round(Math.random() * (end - start));
    }

    public AutoSyncSchedularService()
    {
////	    	this.context=context;
//        super("SchedularService");

    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
//Intializing Fabric
        Fabric.with(this, new Crashlytics());
        Crashlytics.log(android.util.Log.ERROR, TAG, "MyAlarmService.onStart()");
        try  {
            session = new SessionManager(getApplicationContext());
            session_DB_PATH=HomeMainActivity.session_DB_PATH;
            session_DB_PWD=HomeMainActivity.session_DB_PWD;
            session_CURRENT_DT=HomeMainActivity.session_CURRENT_DT;
            session_CUST_ACC_CODE=HomeMainActivity.session_CUST_ACC_CODE;
            session_USER_LOC=HomeMainActivity.session_USER_LOC;
            session_USER_NUMERIC_ID=HomeMainActivity.session_USER_NUMERIC_ID;
            session_USER_NAME=HomeMainActivity.session_USER_NAME;
            session_USER_PSWD=HomeMainActivity.session_user_pwd;
            Crashlytics.log(android.util.Log.ERROR, TAG, "print AutoSyncSchedularService vals " + session_DB_PATH + session_DB_PWD + session_CURRENT_DT + session_CUST_ACC_CODE + session_USER_LOC + session_USER_NUMERIC_ID + "\n" + session_USER_NAME + session_USER_PSWD);


            // BEGIN_INCLUDE(service_onhandle)
            // The URL from which to fetch content.
//        String urlString = URL;

//        String result ="aaaa";

            // calling Asynctasks.
            try {
                /*//**When ever you want to check Internet Status in your application call isConnectingToInternet() function and it will return true or false***//*
                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
//    	   		Boolean isInternetPresent = false;
                Boolean isInternetPresent = cd.isConnectingToInternet(); // true or false
                // get Internet status
              //  isInternetPresent = cd.isConnectingToInternet();

                // check for Internet status
                if (isInternetPresent)
                {
                    Crashlytics.log(android.util.Log.ERROR, TAG, "print AutoSyncSchedularService session_USER_NAME " + session_USER_NAME + session_USER_PSWD + session_CUST_ACC_CODE + session_USER_LOC + session_USER_NUMERIC_ID + session_CURRENT_DT);
                    *//*******************Outscan Assignments*****************************//*
                    if((session_USER_NAME != null && session_USER_NAME!="null") && (session_USER_PSWD != null && session_USER_PSWD!="null") && (session_CUST_ACC_CODE !=null && session_CUST_ACC_CODE !="null"))
                    {
                        entityString_del = "/" + URLEncoder.encode(session_USER_NAME)  +"/" + URLEncoder.encode(session_USER_PSWD)
                                + "/" + URLEncoder.encode(session_CUST_ACC_CODE)+ "/" + URLEncoder.encode(session.getKeyToken());

                        Crashlytics.log(android.util.Log.ERROR, TAG, "print entityString_del" + entityString_del);
//                                          String  URL_outscan="http://traconmobi.net/Assignment-Details";
                        Handler mHandler = new Handler(getMainLooper());
                        mHandler.post(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                new Thread(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        try {
                                            RequestBody formBody = new FormBody.Builder()
                                                    .add("username", session_USER_NAME)
                                                    .add("password", session_USER_PSWD)
                                                    .add("iemi", session.getKeyIMEI())
                                                    .add("timezone", session.getTimezone())
                                                    .add("company_id", session_CUST_ACC_CODE)
                                                    .add("token", session.getKeyToken())
                                                    .build();
                                          *//*  new PostRequest(new PostRequest.AsyncResponse() {
                                                @Override
                                                public void processFinish(String output) {
                                                    session_outscan_resp = output;
                                                    if (session_outscan_resp != null && session_outscan_resp != "") {
                                                        *//**//**parser used for del outscan**//**//*
                                                        ParseOutscanXmlResponse flightListParser = new ParseOutscanXmlResponse(session_outscan_resp, getApplicationContext());
                                                        try {
                                                            flightListParser.parse();
                                                        } catch (XmlPullParserException e) {
                                                            // TODO Auto-generated catch block
                                                            e.printStackTrace();
                                                        } catch (IOException e) {
                                                            // TODO Auto-generated catch block
                                                            e.printStackTrace();
                                                        }
                                                        ParsePickupOutscanXML pickupListParser = new ParsePickupOutscanXML(session_outscan_resp.replaceAll("[^\\x20-\\x7e]", ""), getApplicationContext());
                                                        try {
                                                            //pickUpList = flightListParser.parseXml();
                                                            pickupListParser.parseXml(session_outscan_resp.replaceAll("[^\\x20-\\x7e]", ""), getApplicationContext());
                                                            //dataDoc = flightListParser.getStoredDocId();
                                                            //Crashlytics.log(android.util.Log.ERROR,TAG,"DocStored" + dataDoc);
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }
                                            }, formBody).execute(URL_outscan, session_USER_NAME, session_USER_PSWD, session.getKeyIMEI(), session_CUST_ACC_CODE, session.getKeyToken());
                                *//*        } catch (Exception e) {
                                        }

                                        try {
                                            final CouchBaseDBHelper dbHelper = new CouchBaseDBHelper(getApplicationContext());
                                            Date curDate = new Date();
                                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                                            final String mLastUpdateTime = dateFormat.format(curDate).toString();
                                            if (session == null) {
                                                session = new SessionManager(getApplicationContext());
                                            }

                                            try {
                                                Map<String, String> data1 = new HashMap<>();
                                                data1.put("username", session_USER_NAME);
                                                data1.put("password", session_USER_PSWD);
                                                data1.put("company_id", session_CUST_ACC_CODE);
                                                data1.put("iemi", session.getKeyIMEI());
                                                data1.put("timezone", "Asia/Kolkata");
                                                data1.put("token", session.getKeyToken());
                                                Call<Escan> call = App.getRestClient().getEcomService().getEcomService(data1);

                                                call.enqueue(new Callback<Escan>() {
                                                    @Override
                                                    public void onResponse(Response<Escan> escan, Retrofit retrofit) {
                                                        if(escan.body() != null) {
                                                            int responseSize = escan.body().getData().size();
                                                            Set<String> pickUpset;

                                                            for (int i = 0; i < responseSize; i++) {
                                                                if (escan.body().getData().get(i).getORDERDATE().contains(mLastUpdateTime) && String.valueOf(escan.body().getData().get(i).getCOMPANYID()).contains(session_CUST_ACC_CODE) && String.valueOf(escan.body().getData().get(i).getUSERID()).equals(session_USER_NUMERIC_ID) && String.valueOf(escan.body().getData().get(i).getLOCATION()).equals(session.getLocId())) {
                                                                    pickUpset = new HashSet<String>(escan.body().getData().get(i).getSCANITEM());
                                                                    session.setScanRef(escan.body().getData().get(i).getASSIGNMENTNO(), pickUpset);
                                                                    session.setDocType(escan.body().getData().get(i).getASSIGNMENTNO(), escan.body().getData().get(i).getDOCTYPE());
                                                                }
                                                            }
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Throwable t) {
                                                        Log.e("Error : ", "" + t.getLocalizedMessage());

                                                    }
                                                });
                                            } catch (Exception e) {
                                            }
                                        } catch (Exception e) {
                                            Crashlytics.log(Log.ERROR, "Escan api call", e.getStackTrace() + "");
                                            Crashlytics.logException(e);
                                        }
                                    }
                                }).start();
                            }
                        });
                    }
                    *//*******************UD reason code*****************************//*

                    if(session_CUST_ACC_CODE !=null && session_CUST_ACC_CODE !="null")
                    {
                        entityString_ud = "/" + URLEncoder.encode(session_CUST_ACC_CODE)+ "/" + URLEncoder.encode(token);
                        Crashlytics.log(android.util.Log.ERROR, TAG, "print entityString_ud reason" + entityString_ud);
                        Handler mHandler = new Handler(getMainLooper());
                        mHandler.post(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                new Thread(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        *//*try {
                                            RequestBody formBody = new FormBody.Builder()
                                                    .add("iemi", session.getKeyIMEI())
                                                    .add("timezone", session.getTimezone())
                                                    .add("company_id", session_CUST_ACC_CODE)
                                                    .add("token", session.getKeyToken())
                                                    .build();
                                            new RequestCompanyReason(new RequestCompanyReason.AsyncResponse() {
                                                @Override
                                                public void processFinish(String output) {
                                                    session_udreason_resp = output;
                                                    if (session_udreason_resp != null && session_udreason_resp != "") {
                                                        *//**//**parser used for ud reasons **//**//*
                                                        ParseUDReason_XmlResponse HoldReasonListParser = new ParseUDReason_XmlResponse(session_udreason_resp, getApplicationContext());
                                                        try {
                                                            HoldReasonListParser.parse();

                                                        } catch (XmlPullParserException e) {
                                                            // TODO Auto-generated catch block
                                                            e.printStackTrace();
                                                        } catch (IOException e) {
                                                            // TODO Auto-generated catch block
                                                            e.printStackTrace();
                                                        }
                                                *//**//*----------------- ----parser used for PickUp cancel reason---------------------------------------------*//**//*
                                                        ParsePickUpReason_XmlResponse PickUpReasonListParser = new ParsePickUpReason_XmlResponse(session_udreason_resp, getApplicationContext());
                                                        try {
                                                            PickUpReasonListParser.parse();
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }
                                            },formBody).execute(URL_get_ud_resn,session.getKeyIMEI(), session.getKeyToken(), session_CUST_ACC_CODE);
                                        } catch (Exception e) {
                                        }*//*
                                    }
                                }).start();
                            }
                        });
                    }

                    /*//*********************** STEP 3 GETTING ALL THE UNDELIVERIES TO SYNC TO CENTRAL DB FROM LOCAL DB **********************//*
                    try {
                        Handler mHandler = new Handler(getMainLooper());
                        mHandler.post(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                new Thread(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        if (session_DB_PATH != null && session_DB_PWD != null) {

                                            db_undel = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                            Assgn_type = "D";
                                            String userid = "";
//		cursor_undel = db_undel.rawQuery("SELECT T_U_ID,T_Assignment_Id,T_Assignment_Number,T_Assignment_Type,T_Remarks,T_Photo,T_Assignment_In_Complete_Reason,D_In_Complete_Date,D_In_Complete_Time,T_AccquiredLat,T_AccquiredLon,C_is_Sync FROM Tom_Assignments_In_Complete where T_Assignment_Type = '" + Assgn_type + "' AND  C_is_Sync IS NOT 1 LIMIT 100",null);
                                            cursor_undel = db_undel.rawQuery("SELECT T_U_ID,T_Assignment_Id,T_Assignment_Number,T_Assignment_Type,T_Remarks,T_Photo,T_Photo_dt,T_Photo_tm,T_Assignment_In_Complete_Reason,T_Assignment_In_Complete_Reason_Id,D_In_Complete_Date,D_In_Complete_Time,T_AccquiredLat,T_AccquiredLon,T_Cust_Acc_NO,T_Out_Scan_Location,C_is_Sync FROM Tom_Assignments_In_Complete where T_Assignment_Type = '" + Assgn_type + "' AND  C_is_Sync IS NOT 1", null);
                                            syncundelcnt = cursor_undel.getCount();
                                            while (cursor_undel.moveToNext()) {
                                                t_u_id = cursor_undel.getString(cursor_undel.getColumnIndex("T_U_ID"));
                                                awb_id1 = cursor_undel.getString(cursor_undel.getColumnIndex("T_Assignment_Id"));
                                                awb_numb = cursor_undel.getString(cursor_undel.getColumnIndex("T_Assignment_Number"));
                                                ud_reason = cursor_undel.getString(cursor_undel.getColumnIndex("T_Assignment_In_Complete_Reason"));
                                                ud_reason_id = cursor_undel.getString(cursor_undel.getColumnIndex("T_Assignment_In_Complete_Reason_Id"));
                                                u_reason = ud_reason.replace(" ", "%20");
                                                ud_date = cursor_undel.getString(cursor_undel.getColumnIndex("D_In_Complete_Date"));
                                                ud_time = cursor_undel.getString(cursor_undel.getColumnIndex("D_In_Complete_Time"));
//							u_time=ud_time.replace(" ", "%20");
                                                u_time = ud_time.replace(":", "_");
                                                ud_loc_id = cursor_undel.getString(cursor_undel.getColumnIndex("T_Out_Scan_Location"));
                                                remarks = cursor_undel.getString(cursor_undel.getColumnIndex("T_Remarks"));
                                                rmks = remarks.replace(" ", "%20");
                                                photo_path = cursor_undel.getString(cursor_undel.getColumnIndex("T_Photo"));
                                                undel_cust_acc = cursor_undel.getString(cursor_undel.getColumnIndex("T_Cust_Acc_NO"));
                                                photo_id_dt = cursor_undel.getString(cursor_undel.getColumnIndex("T_Photo_dt"));
                                                photo_id_tme = cursor_undel.getString(cursor_undel.getColumnIndex("T_Photo_tm"));

                                                Acq_lat = cursor_undel.getString(cursor_undel.getColumnIndex("T_AccquiredLat"));
                                                Acq_long = cursor_undel.getString(cursor_undel.getColumnIndex("T_AccquiredLon"));
                                                if (Acq_lat == null && Acq_lat == "" && Acq_lat.equals("null")) {
                                                    Acq_lat = "null";
                                                }
                                                if (Acq_long == null && Acq_long == "" && Acq_long.equals("null")) {
                                                    Acq_long = "null";
                                                }

                                                if (photo_id_dt != null && photo_id_dt != "" && !(photo_id_dt.equals("null"))) {
                                                    photo_id_dt = photo_id_dt;
                                                } else if (photo_id_dt == null || photo_id_dt == "" || photo_id_dt.equals("null")) {
                                                    photo_id_dt = "null";
                                                }

                                                if (photo_id_tme != null && photo_id_tme != "" && !(photo_id_tme.equals("null"))) {
                                                    photo_id_tme = photo_id_tme;
                                                } else if (photo_id_tme == null || photo_id_tme == "" || photo_id_tme.equals("null")) {
                                                    photo_id_tme = "null";
                                                }

                                                if (photo_path != null && photo_path != "" && !(photo_path.equals("null"))) {

                                                } else if (photo_path == null && photo_path == "" && photo_path.equals("null")) {
                                                    photo_path = "null";
                                                }
                                                try {
                                                    DateFormat formatter_date = new SimpleDateFormat("yyyy-MM-dd");
                                                    formatter_date.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                                                    Date date1 = (Date) formatter_date.parse(ud_date);
                                                    dt = date1.getTime() / 1000;

                                                    int reqId_UD = 105;
                                                    String status = "UNDEL";
                                                    RequestBody formBody = new FormBody.Builder()
                                                            .add("user_id", t_u_id)
                                                            .add("timezone", session.getTimezone())
                                                            .add("imei", session.getKeyIMEI())
                                                            .add("company_id", session_CUST_ACC_CODE)
                                                            .add("token", session.getKeyToken())
                                                            .add("assignmentid", awb_id1)
                                                            .add("status",status)
                                                            .add("reason_id",ud_reason_id)
                                                            .add("cod_amount", String.valueOf(0))
                                                            .add("cod_amount_collected", String.valueOf(0))
                                                            .add("date",String.valueOf(dt))
                                                            .add("time",u_time)
                                                            .add("received_by","NA")
                                                            .add("relationship", "NA")
                                                            .add("contact_no", "NA")
                                                            .add("location_id", ud_loc_id)
                                                            .add("transaction_for", "U")
                                                            .add("latitude", (Acq_lat != null) ? Acq_lat:"NA")
                                                            .add("longitude", (Acq_long != null) ? Acq_long:"NA")
                                                            .add("remarks", rmks)
                                                            .build();

                                                    OkHttpHandlerPost handler_UD = new OkHttpHandlerPost(formBody, URL_UDDtls,status,session_DB_PATH, session_DB_PWD);
                                                    session_sync_undel_resp = handler_UD.sendRequest();
                                                    Crashlytics.log(Log.ERROR, TAG, "print session_sync_undel_resp" + session_sync_undel_resp);
                                                    if (session_sync_undel_resp != null && session_sync_undel_resp != "") {

//	        	ParsePOD_UDXmlResponse POD_UD_parser = new ParsePOD_UDXmlResponse(session_sync_undel_resp);
                                                        Parse_CompleteXmlResponse UD_parser = new Parse_CompleteXmlResponse(session_sync_undel_resp);
                                                        try {
                                                            UD_parser.parse();
                                                        } catch (XmlPullParserException e) {
                                                            // TODO Auto-generated catch block
                                                            e.printStackTrace();
                                                        } catch (IOException e) {
                                                            // TODO Auto-generated catch block
                                                            e.printStackTrace();
                                                        }

                                                        if (UD_parser.getparsedCompleteList().isEmpty()) {
                                                            if (session_DB_PATH != null && session_DB_PWD != null) {
                                                                try {
//			                	db_del_incomplete_resp1=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                                                    db_undel_resp1 = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                                                    undel_awb_id = Parse_CompleteXmlResponse.ASSIGNID;
                                                                    String sync_status = Parse_CompleteXmlResponse.IS_SYNCED;
                                                                    Crashlytics.log(Log.ERROR, TAG, "print undel_awbid empty" + undel_awb_id);
                                                                    if (undel_awb_id != null || undel_awb_id != "") {
                                                                        c_undel_sync_resp1 = db_undel_resp1.rawQuery("UPDATE Tom_Assignments_In_Complete SET C_is_Sync='" + sync_status + "' WHERE T_Assignment_Id='" + undel_awbid + "'", null);
//			                			new String[]{""+undel_awbid});
                                                                        while (c_undel_sync_resp1.moveToNext()) {

                                                                        }
                                                                        c_undelsync_resp1 = db_undel_resp1.rawQuery("UPDATE TOM_Assignments SET C_is_Sync='" + sync_status + "' WHERE T_Assignment_Id='" + undel_awbid + "'", null);
//			                			new String[]{""+undel_awbid});
                                                                        while (c_undelsync_resp1.moveToNext()) {

                                                                        }
                                                                    }
//			                	db_undel_resp1.close();
                                                                } catch (Exception e) {
                                                                    Crashlytics.log(Log.ERROR, TAG, "SampleSchedulingService undelsync isEmpty Exception" + e.getMessage());
                                                                } finally {
                                                                    if (c_undel_sync_resp1 != null && !(c_undel_sync_resp1.isClosed())) {
                                                                        c_undel_sync_resp1.close();
                                                                    }
                                                                    if (c_undelsync_resp1 != null && !(c_undelsync_resp1.isClosed())) {
                                                                        c_undelsync_resp1.close();
                                                                    }
                                                                    if (db_undel_resp1 != null && db_undel_resp1.isOpen()) {
                                                                        db_undel_resp1.close();
                                                                    }
                                                                }
                                                            } else {
                                                                Crashlytics.log(Log.ERROR, TAG, "SampleSchedulingService db_undel_resp1 session_DB_PATH is null");
                                                                //onClickLogOut();
                                                            }
//			        	        syncundelcnt=cr.getCount();
                                                        } else if (!(UD_parser.getparsedCompleteList().isEmpty())) {
                                                            if (session_DB_PATH != null && session_DB_PWD != null) {
                                                                try {
//			                		db_del_incomplete_resp2=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                                                    db_undel_resp2 = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                                                    undel_awb_id = Parse_CompleteXmlResponse.ASSIGNID;
                                                                    String sync_status = Parse_CompleteXmlResponse.IS_SYNCED;
                                                                    Crashlytics.log(Log.ERROR, TAG, "print undel_awb_id not empty" + undel_awb_id);
                                                                    if (undel_awb_id != null || undel_awb_id != "") {
                                                                        c_undel_sync_resp2 = db_undel_resp2.rawQuery("UPDATE Tom_Assignments_In_Complete SET C_is_Sync='" + sync_status + "' WHERE T_Assignment_Id='" + undel_awb_id + "'", null);
//				                			new String[]{""+undel_awb_id});
                                                                        while (c_undel_sync_resp2.moveToNext()) {

                                                                        }
                                                                        c_undelsync_resp2 = db_undel_resp2.rawQuery("UPDATE TOM_Assignments SET C_is_Sync='" + sync_status + "' WHERE T_Assignment_Id='" + undel_awb_id + "'", null);
//			                			new String[]{""+undel_awbid});
                                                                        while (c_undelsync_resp2.moveToNext()) {

                                                                        }
                                                                    }
//				                	db_undel_resp2.close();
                                                                } catch (Exception e) {
                                                                    Crashlytics.log(Log.ERROR, TAG, "SampleSchedulingService undelsync isnotEmpty Exception" + e.getMessage());
                                                                } finally {
                                                                    if (c_undel_sync_resp2 != null && !(c_undel_sync_resp2.isClosed())) {
                                                                        c_undel_sync_resp2.close();
                                                                    }
                                                                    if (c_undelsync_resp2 != null && !(c_undelsync_resp2.isClosed())) {
                                                                        c_undelsync_resp2.close();
                                                                    }
                                                                    if (db_undel_resp2 != null && db_undel_resp2.isOpen()) {
                                                                        db_undel_resp2.close();
                                                                    }
                                                                }
                                                            } else {
                                                                Crashlytics.log(Log.ERROR, TAG, "SampleSchedulingService db_undel_resp2 session_DB_PATH is null ");
                                                                //onClickLogOut();
                                                            }
//			        	        syncundelcnt=cr.getCount();
                                                        }


                                                    } else {
                                                        //do nothing
                                                        Crashlytics.log(Log.ERROR, TAG, "SampleSchedulingService session_sync_undel_resp is null");
                                                    }

                                                } catch (Exception e) {
                                                    Crashlytics.log(Log.ERROR, TAG, "Exception SampleSchedulingService session_sync_undel_resp is null" + e.getMessage());
                                                }
                                            }
                                        }
                                    }
                                }).start();
                            }
                        });
                    }
                    catch(Exception e)
                    {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "SampleSchedulingService undelivry Exception" + e.getMessage());
                    }
                    finally
                    {
                        if(cursor_undel !=null && !(cursor_undel.isClosed()))
                        {
                            cursor_undel.close();
                        }
                        if(db_undel!=null && db_undel.isOpen())
                        {
                            db_undel.close();
                        }

                    }


                    /*//***********************STEP 2 GETTING ALL THE DELIVERIES TO SYNC TO CENTRAL DB FROM LOCAL DB **********************//*
                    /*//***********************OPEN DATABASE TO PERFORM READ/WRITE OPERATION***********************//*
                    try
                    {
                        Handler mHandler = new Handler(getMainLooper());
                        mHandler.post(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                new Thread(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        if(session_DB_PATH != null && session_DB_PWD != null)
                                        {
//    		db_del_complete=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                            db_del_complete=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
                                            Assgn_flag="TRUE";
                                            Assgn_type="D";
//    		 cursor_del_complete = db_del_complete.rawQuery("SELECT T_OUT_Scan_U_ID,T_Assignment_Id,T_Assignment_Number,B_is_Completed,F_Amount_Collected,D_Completed_Date,D_Completed_time,T_Received_by_Collected_from,T_Relationship,T_Signature,T_Photo,T_AcquiredLat,T_AccquiredLon,C_is_Sync FROM TOM_Assignments where B_is_Completed='" + Assgn_flag + "' and T_Assignment_Type = '" + Assgn_type + "' and C_is_Sync IS NOT 1 LIMIT 100",null);
                                            cursor_del_complete = db_del_complete.rawQuery("SELECT emp.T_OUT_Scan_U_ID,emp.T_Out_Scan_Location,emp.T_Receiver_Contact_Num ,emp.T_Assignment_Id, emp.T_Assignment_Number, emp.B_is_Completed,emp.F_Amount ,emp.F_Amount_Collected, emp.D_Completed_Date, emp.D_Completed_time, emp.T_Received_by_Collected_from, emp.T_Relationship, emp.T_Signature, emp.T_Photo, emp.T_AcquiredLat, emp.T_AccquiredLon, emp.T_Signature_dt, emp.T_Cust_Acc_NO, emp.C_is_Sync,mgr.T_RELATION_TYP,mgr.T_RELATION_ID  FROM  TOM_Assignments  emp JOIN TBL_Relation_mstr  mgr ON mgr.T_RELATION_TYP = emp.T_Relationship where  emp.B_is_Completed='" + Assgn_flag + "' and  emp.T_Assignment_Type = '" + Assgn_type + "' and emp.C_is_Sync IS NOT 1",null);
                                            //	new String[]{""+employeeId});
                                            syncdelcnt=cursor_del_complete.getCount();
                                            while(cursor_del_complete.moveToNext()) {
                                                // user_id = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_"));
                                                t_u_id = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Out_Scan_Location"));
                                                awb_id = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Assignment_Id"));
                                                awb_delnum = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Assignment_Number"));
                                                isdel = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("B_is_Completed"));
//							is_cod_coltd="0";
                                                cod_amt = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("F_Amount"));
                                                assigned_amt =cod_amt.replace(".", "_");
                                                cod_amt_coltd = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("F_Amount_Collected"));

                                                collectd_amt = cod_amt_coltd.replace(".", "_");
                                                Crashlytics.log(Log.ERROR, TAG, "print amount" + cod_amt_coltd + "collectd_amt" + collectd_amt + "assigned_amt" + assigned_amt);
                                                del_dt = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("D_Completed_Date"));

                                                del_tm = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("D_Completed_time"));
                                                String del_tme = del_tm.replace(":", "_");
                                                rec_by = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Received_by_Collected_from"));
                                                String recvd_by=rec_by.replace(" ", "%20");
                                                String rvr_contact_numbr = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Receiver_Contact_Num"));
                                                rec_status = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Relationship"));
                                                rel_id = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_RELATION_ID"));
                                                sign_path = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Signature"));
                                                photo_path = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Photo"));
                                                acquired_lat = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_AcquiredLat"));
                                                acquired_long = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_AccquiredLon"));
                                                sign_dt_tm = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Signature_dt"));
                                                del_cust_acc = cursor_del_complete.getString(cursor_del_complete.getColumnIndex("T_Cust_Acc_NO"));

                                                if (acquired_lat != null && acquired_lat != "" && !acquired_lat.equals("null")) {

                                                    acquired_lat = acquired_lat;
                                                } else if (acquired_lat == null && acquired_lat == "" && acquired_lat.equals("null")) {
                                                    acquired_lat = "null";
                                                }
                                                if (acquired_long != null && acquired_long != "" && !acquired_long.equals("null")) {

                                                    acquired_long = acquired_long;
                                                } else if (acquired_long == null && acquired_long == "" && acquired_long.equals("null")) {
                                                    acquired_long = "null";
                                                }
                                                if(photo_id_dt!=null && photo_id_dt!= ""  &&  !(photo_id_dt.equals("null")))
                                                {
                                                    photo_id_dt=photo_id_dt;
                                                }
                                                else if(photo_id_dt == null || photo_id_dt == ""  || photo_id_dt.equals("null"))
                                                {
                                                    photo_id_dt="null";
                                                }

                                                if(photo_id_tme!=null && photo_id_tme!= ""  &&  !(photo_id_tme.equals("null")))
                                                {
                                                    photo_id_tme=photo_id_tme;
                                                }
                                                else if(photo_id_tme == null || photo_id_tme == ""  || photo_id_tme.equals("null"))
                                                {
                                                    photo_id_tme="null";
                                                }

                                                if(photo_path!=null && photo_path!= ""  &&  !(photo_path.equals("null")))
                                                {
                                                    image = new File(Environment.getExternalStorageDirectory(), "Image keeper/" +photo_path.trim());
//				    selectedImagePath =android.os.Environment.getExternalStorageDirectory().toString()+"/Image keeper/"+ photo_path.trim();
                                                    image = new File(Environment.getExternalStorageDirectory()
                                                            + "/Android/data/com.traconmobi.in/", "Image keeper/" +photo_path.trim());
                                                    selectedImagePath = Environment.getExternalStorageDirectory()
                                                            + "/Android/data/com.traconmobi.in"+"/Image keeper/"+ photo_path.trim();
                                                    Bitmap bm = reduceImageSize(selectedImagePath);

                                                    if(bm != null)
                                                    {

                                                        ByteArrayOutputStream BAO = new ByteArrayOutputStream();

                                                        bm.compress(Bitmap.CompressFormat.JPEG, 40, BAO);

                                                        BAvalue = BAO.toByteArray();
                                                        getphotobytearray= Base64.encode(BAvalue);
                                                        s_photo=getphotobytearray.replace("+", "%2B");
                                                        Crashlytics.log(Log.ERROR,TAG,"s_photo " + s_photo);
                                                    }
                                                    else if(bm == null)
                                                    {
                                                        getphotobytearray="null";
                                                    }
                                                }
                                                else if(photo_path == null && photo_path == ""  && photo_path.equals("null"))
                                                {
                                                    photo_path="null";
                                                }
                                                try {

                                                    DateFormat formatter_date = new SimpleDateFormat("yyyy-MM-dd");
                                                    formatter_date.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                                                    Date date1 = (Date) formatter_date.parse(del_dt);
                                                    dt_del = date1.getTime() / 1000;
                                                    Crashlytics.log(Log.ERROR, TAG, "Today is " + date1.getTime() / 1000 + "dt" + dt);

                                                    int reqId_DEL = 104;
//									String requestParameter_del="0";//"/"+opsggnpc+"/"+sample+"/"+a2lfrq40kdh0nrut1vl0ani4a2+"/"+142+"/"+1000002292+"/"+UNDEL+"/"+4670+"/"+0+"/"+0+"/"+1447612200+"/"+15_58_9+"/"+null+"/"+null+"/"+null+"/"+5850+"/"+U+"/"+null+"/"+ URLEncoder.encode(token);
                                                    String status = "DEL";
                                                    String requestParameter_del = "/" + user_id + "/" + session_USER_PSWD + "/" + session.getKeyToken() + "/" + session_CUST_ACC_CODE + "/" + awb_id + "/" + status + "/" + "null" + "/" + assigned_amt + "/" + collectd_amt + "/" + dt_del + "/" + del_tme + "/" + recvd_by + "/" + rel_id + "/" + rvr_contact_numbr.trim() + "/" + t_u_id + "/" + "D" + "/" + acquired_lat + "/" + acquired_long + "/" + "null"+ "/"+ session.getKeyIMEI();
                                                    Crashlytics.log(Log.ERROR, TAG, "print requestParameter_del" + requestParameter_del);
                                                  *//*  HttpHandler handler_DEL = new HttpHandler(URL_DELDtls + requestParameter_del, null, null, reqId_DEL);
                                                    handler_DEL.addHttpLisner(AutoSyncSchedularService.this);
                                                    handler_DEL.sendRequest();


                                                    // get user data from session
                                                    HashMap<String, String> resp_Dts8 = session.gethttpsrespDetails();

                                                    // Userid
                                                    session_sync_del_resp = resp_Dts8.get(SessionManager.KEY_RESPONSE);*//*
                                                    RequestBody formBody = new FormBody.Builder()
                                                            .add("user_id", session.getuserId())
                                                            .add("timezone", session.getTimezone())
                                                            .add("imei", session.getKeyIMEI())
                                                            .add("company_id", session_CUST_ACC_CODE)
                                                            .add("token", session.getKeyToken())
                                                            .add("assignmentid", awb_id1)
                                                            .add("status",status)
                                                            .add("reason_id","NA")
                                                            .add("cod_amount", assigned_amt )
                                                            .add("cod_amount_collected", collectd_amt)
                                                            .add("date",String.valueOf(dt_del))
                                                            .add("time", del_tme)
                                                            .add("received_by",recvd_by)
                                                            .add("relationship", rel_id)
                                                            .add("contact_no", rvr_contact_numbr.trim())
                                                            .add("location_id", t_u_id)
                                                            .add("transaction_for", "D")
                                                            .add("latitude", (acquired_lat != null) ? acquired_lat:"NA")
                                                            .add("longitude", (acquired_long != null) ? acquired_long:"NA")
                                                            .add("remarks", "NA")
                                                            .build();

                                                    OkHttpHandlerPost handler_UD = new OkHttpHandlerPost(formBody, URL_DELDtls,status,session_DB_PATH, session_DB_PWD);
                                                    session_sync_undel_resp = handler_UD.sendRequest();
                                                    *//*OkHttpHandler handler_DEL = new OkHttpHandler(URL_DELDtls + requestParameter_del);
                                                    session_sync_del_resp = handler_DEL.sendRequest();*//*
                                                    Crashlytics.log(Log.ERROR, TAG, "print session_sync_del_resp" + session_sync_undel_resp);

                                                    //uploading Images and signature
                                                    *//*List<NameValuePair> requestParameter = new ArrayList<NameValuePair>();

                                                    requestParameter.add(new BasicNameValuePair("Assign_ID", awb_id));
                                                    requestParameter.add(new BasicNameValuePair("Signature", sign_path));
                                                    requestParameter.add(new BasicNameValuePair("photo", getphotobytearray));
                                                    requestParameter.add(new BasicNameValuePair("locid", t_u_id));
                                                    requestParameter.add(new BasicNameValuePair("companyid", del_cust_acc));
                                                    requestParameter.add(new BasicNameValuePair("token", token));*//*
                                                    status = "photo";
                                                    RequestBody formdata = new FormBody.Builder()
                                                            .add("Assign_ID", awb_id)
                                                            .add("Signature", sign_path)
                                                            .add("photo", getphotobytearray)
                                                            .add("locid", t_u_id)
                                                            .add("imei", session.getKeyIMEI())
                                                            .add("companyid", del_cust_acc)
                                                            .add("token", session.getKeyToken())
                                                            .add("timezone",session.getTimezone())
                                                            .build();
                                                    OkHttpHandlerPost handler_photo = new OkHttpHandlerPost(formdata, URL_DEL_IMAGES_Dtls,status,session_DB_PATH, session_DB_PWD);
                                                    String session_sync_del_photoresp = handler_photo.sendRequest();


//								requestParameter.add(new BasicNameValuePair("SIGN_DT",sign_dt_tm));
//								requestParameter.add(new BasicNameValuePair("CustomerCode",del_cust_acc));
                                                    //Crashlytics.log(android.util.Log.ERROR,TAG,"print del requestParameter" + requestParameter);
                                                    int reqId_photo = 109;
                                                    Crashlytics.log(Log.ERROR,TAG,"shw photo path" + photo_path);
                                                 *//*   HttpHandler handler_photo = new HttpHandler(URL_DEL_IMAGES_Dtls,requestParameter,null,reqId_photo);
                                                    handler_photo.addHttpLisner(AutoSyncSchedularService.this);
                                                    handler_photo.sendRequest();

                                                    // get user data from session
                                                    HashMap<String, String> resp_Dts_photo = session.gethttpsrespDetails();

                                                    // Userid
                                                    String session_sync_del_photoresp = resp_Dts_photo.get(SessionManager.KEY_RESPONSE);*//*
                                                    Crashlytics.log(Log.ERROR,TAG, "print session_sync_del_photoresp" + session_sync_del_photoresp);


                                                    if (session_sync_del_resp != null && session_sync_del_resp != "") {
//    				            	ParsePOD_UDXmlResponse POD_UD_parser = new ParsePOD_UDXmlResponse(session_sync_del_resp);
                                                        Parse_CompleteXmlResponse Complete_parser = new Parse_CompleteXmlResponse(session_sync_del_resp);
                                                        try {
                                                            Complete_parser.parse();
                                                        } catch (XmlPullParserException e) {
                                                            // TODO Auto-generated catch block
                                                            e.printStackTrace();
                                                        } catch (IOException e) {
                                                            // TODO Auto-generated catch block
                                                            e.printStackTrace();
                                                        }

                                                        //               Crashlytics.log(android.util.Log.ERROR,TAG,"getting the undelivery sync response array list value"+ POD_UD_parser.getparsedUDList());
                                                        if (Complete_parser.getparsedCompleteList().isEmpty()) {
                                                            try {
                                                                if (session_DB_PATH != null && session_DB_PWD != null) {
                                                                    //    				            		   db_del_complete_resp1=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                                                    db_del_complete_resp1 = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                                                    del_awb_id = Parse_CompleteXmlResponse.ASSIGNID;
                                                                    String sync_status = Parse_CompleteXmlResponse.IS_SYNCED;
                                                                    if (del_awb_id != null || del_awb_id != "") {
                                                                        c_del_complete_sync_resp1 = db_del_complete_resp1.rawQuery("UPDATE TOM_Assignments SET C_is_Sync='" + sync_status + "' WHERE T_Assignment_Id = ?", new String[]{"" + del_awb_id});
                                                                        while (c_del_complete_sync_resp1.moveToNext()) {
                                                                        }
                                                                        //    					          		 	if(c_del_complete_sync_resp1 != null)
                                                                        //    					          		 	{
                                                                        //    					          		 		c_del_complete_sync_resp1.close();
                                                                        //    					          		 	}
                                                                    }
                                                                    //    					            	   	db_del_complete_resp1.close();
                                                                } else {
                                                                    Crashlytics.log(Log.ERROR, TAG, "error : step4 SampleSchedulingService delsync");
                                                                    //onClickLogOut();
                                                                }
                                                            } catch (Exception e) {
                                                                Crashlytics.log(Log.ERROR, TAG, "Exception Expense " + e.getMessage());
                                                            } finally {
                                                                if (c_del_complete_sync_resp1 != null && !c_del_complete_sync_resp1.isClosed()) {
                                                                    c_del_complete_sync_resp1.close();
                                                                }
                                                                if (db_del_complete_resp1 != null && db_del_complete_resp1.isOpen()) {
                                                                    db_del_complete_resp1.close();
                                                                }
                                                            }
//    				          		 syncdelcnt=cursor.getCount();
                                                        } else if (!(Complete_parser.getparsedCompleteList().isEmpty())) {
                                                            try {
                                                                if (session_DB_PATH != null && session_DB_PWD != null) {
                                                                    //    				            		   db_del_complete_resp2=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                                                                    db_del_complete_resp2 = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                                                    del_awb_id = Parse_CompleteXmlResponse.ASSIGNID;
                                                                    String sync_status = Parse_CompleteXmlResponse.IS_SYNCED;
                                                                    //    					            	   Crashlytics.log(android.util.Log.ERROR,TAG,"delsync "+ pod_awb);
                                                                    if (del_awb_id != null || del_awb_id != "") {
                                                                        c_del_complete_sync_resp2 = db_del_complete_resp2.rawQuery("UPDATE TOM_Assignments SET C_is_Sync='" + sync_status + "'  WHERE T_Assignment_Id = ?", new String[]{"" + del_awb_id});
                                                                        while (c_del_complete_sync_resp2.moveToNext()) {
                                                                        }
//	    					            	   if(c_del_complete_sync_resp2 != null)
//	    					            	   {
//	    					            		   c_del_complete_sync_resp2.close();
//	    					            	   }
                                                                    }
//	    					            	 db_del_complete_resp2.close();
                                                                } else {
                                                                    Crashlytics.log(Log.ERROR, TAG, "error : step5 SampleSchedulingService delsync ");
                                                                    //onClickLogOut();
                                                                }
                                                            } catch (Exception e) {
                                                                Crashlytics.log(Log.ERROR, TAG, "Exception Expense " + e.getMessage());
                                                            } finally {
                                                                if (c_del_complete_sync_resp2 != null && !c_del_complete_sync_resp2.isClosed()) {
                                                                    c_del_complete_sync_resp2.close();
                                                                }
                                                                if (db_del_complete_resp2 != null && db_del_complete_resp2.isOpen()) {
                                                                    db_del_complete_resp2.close();
                                                                }
                                                            }
//    				       		    syncdelcnt=cursor.getCount();
                                                        }

                                                    } else {
                                                        //do nothing
                                                        Crashlytics.log(Log.ERROR, TAG, "session_sync_del_resp is null");
                                                    }
                                                }
                                                catch (Exception e) {
                                                    Crashlytics.log(Log.ERROR, TAG, "Exception session_sync_del_resp is null" + e.getMessage());
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Crashlytics.log(Log.ERROR, TAG, "SampleSchedulingService delivery session_DB_PATH is null");
                                            //onClickLogOut();
                                        }
                                    }
                                }).start();
                            }
                        });
                    }
                    catch(Exception e)
                    {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "SampleSchedulingService delivery Exception" + e.getMessage());
                    }
                    finally
                    {
                        if(cursor_del_complete != null && !(cursor_del_complete.isClosed()))
                        {
                            cursor_del_complete.close();
                        }
                        if(db_del_complete!=null && db_del_complete.isOpen())
                        {
                            db_del_complete.close();
                        }

                    }

                }
                else
                {
//            	 Toast.makeText(SampleSchedulingService.this,"Please Check Your Mobile Internet Connection", Toast.LENGTH_LONG).show();
                }
            }
            catch(Exception e)
            {
                e.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception AutoSyncSchedularService onCreate ConnectionDetector" + e.getMessage());
            }

        }
        catch(Exception e)
        {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception AutoSyncSchedularService onCreate " + e.getMessage());
        }
//        Toast.makeText(this, "MyAlarmService.onStart()", Toast.LENGTH_LONG).show();
    }

    public Bitmap reduceImageSize(String selectedImagePath){

        Bitmap m = null;
        try {
            File f = new File(selectedImagePath);

            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            //The new size we want to scale to
            final int REQUIRED_SIZE=150;

            //Find the correct scale value. It should be the power of 2.
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2 < REQUIRED_SIZE || height_tmp/2 < REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale*=2;
            }

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            m = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
//            Toast.makeText(getApplicationContext(), "Image File not found in your phone. Please select another image.", Toast.LENGTH_LONG).show();
            Crashlytics.log(android.util.Log.ERROR, TAG, "FileNotFoundException reduceImageSize " + e.getMessage());
        }
        catch(Exception e)
        {
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception reduceImageSize" + e.getMessage());
        }
        return  m;
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    *//* @Override
     public void onCreate()
     {*//*
//   @Override
    protected void onHandleIntent(Intent intent) {

        // Release the wake lock provided by the BroadcastReceiver.
        WakefulBroadcastReceiver.completeWakefulIntent(intent);
        // END_INCLUDE(service_onhandle)

    }*/

	 
	 /* Class My Location Listener */

  /*  public class MyLocationListener implements LocationListener

    {

        @Override

        public void onLocationChanged(Location loc)

        {

            try
            {
                final Calendar c = Calendar.getInstance();
                String month=c.get(Calendar.MONTH)+1+"";
                if(month.length()<2){
                    month="0"+month;
                }
                String date=c.get(Calendar.DAY_OF_MONTH)+"";
                if(date.length()<2)
                {
                    date="0"+date;
                }
                showdate =""+ c.get(Calendar.YEAR)+ "-" +month+ "-"+
                        date;
                showtime =" " +
                        c.get(Calendar.HOUR) + ":" +
                        c.get(Calendar.MINUTE) +  ":" +
                        c.get(Calendar.SECOND);

                currentTime = (c.get(Calendar.HOUR_OF_DAY)) + ":" +
                        (c.get(Calendar.MINUTE)) + ":" +
                        (c.get(Calendar.SECOND));
                Log.w("TIME:",String.valueOf(currentTime) + currentTime);

                DATE = showdate;
                tme_val =String.valueOf(currentTime);
                gps_dt_tme=showdate + "-" + String.valueOf(currentTime);
                TelephonyManager telemngr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                imei_num=telemngr.getDeviceId();
//		            deviceid =device_id;    
                loc.getLatitude();

                loc.getLongitude();

                String Text = "My current location is: " +"Latitud = " + loc.getLatitude() +"Longitud = " + loc.getLongitude();

//		    		Toast.makeText( getApplicationContext(),Text,Toast.LENGTH_SHORT).show();
                new_lat = String.valueOf(loc.getLatitude());
//				      String modifd_lat=new_lat.substring(0,8);
//				      Crashlytics.log(android.util.Log.ERROR,TAG,"modifd_lat " + modifd_lat);
                new_lon = String.valueOf(loc.getLongitude());
//				      String modifd_lon=new_lon.substring(0,8);
//				      Crashlytics.log(android.util.Log.ERROR,TAG,"modifd_lon " + modifd_lon);
                accurcy = loc.getAccuracy();
                spd = loc.getSpeed();
                boolean flag=true;
                if(flag==true)
                {

                    session_USER_NAME=TOMDel_pickup_screenActivity.session_USER_NAME;
                    session_CUST_ACC_CODE=TOMDel_pickup_screenActivity.session_CUST_ACC_CODE;
                    if(!(session_USER_NAME.isEmpty()) && session_USER_NAME !=null)
                    {

//		    		  Crashlytics.log(android.util.Log.ERROR,TAG,"session_USER_NAME navatech" + session_USER_NAME + session_CUST_ACC_CODE);
                        //Call to webservice gps insert            
                        entityString2 ="IMEI="+imei_num+"&Acq_latitude="+new_lat+"&Acq_longitude="+new_lon+"&speed="+spd+"&distance=0"+"&Accuracy="+accurcy+"&Date="+DATE+"&Time="+tme_val+"&Net_latitude=0"+"&Net_longitude=0"+"&UserID="+session_USER_NAME+"&CUST_ACC_NO="+session_CUST_ACC_CODE;
//	           Crashlytics.log(android.util.Log.ERROR,TAG,"Loction entityString2 " + entityString2);
                        Handler mHandler = new Handler(getMainLooper());
                        mHandler.post(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                new Thread(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {

                                        int reqId = 109;
                                        HttpHandler handler_os = new HttpHandler(URL_gps_log + "/" + METHOD_NAME_gps_log + "?" + entityString2,null, null, reqId);
                                        handler_os.addHttpLisner(AutoSyncSchedularService.this);
                                        handler_os.sendRequest();
                                    }
                                }).start();
                            }
                        });
                    }
                }
            }
            catch (Exception e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Crashlytics.log(android.util.Log.ERROR,TAG,"Exception onLocationChanged " + e.getMessage());
            }
            finally {

            }


        }*/


//    }
}

